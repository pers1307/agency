// Imit modules
var fs = require('fs');
var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cssmin = require('gulp-minify-css');
var gutil = require('gulp-util');

// Check version
var prevAssetVersion, currentAssetVersion;

//noinspection JSUnresolvedFunction
prevAssetVersion = +fs.readFileSync('./CORE/configs/assets.txt') || 1;
currentAssetVersion = prevAssetVersion + 1;
console.log('Assets version: v' + currentAssetVersion);
fs.writeFileSync('./CORE/configs/assets.txt', currentAssetVersion);

// Load aseets config
var assetsContent, stylesContent, scriptsContent;
assetsContent = JSON.parse(fs.readFileSync('./DESIGN/assets.json'));
stylesContent = assetsContent['css'] || [];
scriptsContent = assetsContent['js'] || [];

gulp.task('css', function () {
    var oldAssetPath, newAssetPath;

    for (var group in stylesContent) {
        oldAssetPath = "./STATIC/css/" + group + ".v" + prevAssetVersion + ".css";
        newAssetPath = "./STATIC/css/" + group + ".v" + currentAssetVersion + ".css";

        console.log(oldAssetPath);
        console.log(newAssetPath);

        // Remove old assets
        if (fs.existsSync(oldAssetPath)) {
            fs.unlinkSync(oldAssetPath);
        }

        // Css task
        gulp.src(stylesContent[group])
            .pipe(concat(newAssetPath))
            .pipe(cssmin({
                rebase: false,
                advanced: false
            }))
            .on('error', function (err) {
                gutil.log(gutil.colors.red('[Error]'), err.toString());
            })
            .pipe(gulp.dest(''));
    }
});

gulp.task('js', function () {
    var oldAssetPath, newAssetPath;

    for (var group in scriptsContent) {
        oldAssetPath = "./STATIC/js/" + group + ".v" + prevAssetVersion + ".js";
        newAssetPath = "./STATIC/js/" + group + ".v" + currentAssetVersion + ".js";

        // Remove old assets
        if (fs.existsSync(oldAssetPath)) {
            fs.unlinkSync(oldAssetPath);
        }

        // Js task
        gulp.src(scriptsContent[group])
            .pipe(concat(newAssetPath, {newLine: ';'}))
            .pipe(uglify())
            .on('error', function (err) {
                gutil.log(gutil.colors.red('[Error]'), err.toString());
            })
            .pipe(gulp.dest(''));
    }
});

gulp.task('default', ['css', 'js']);