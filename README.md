# Media Publisher 6.0

## Развертывание проекта

### Подготовка к работе

Обязательно к установке на машину разработчика
##### GIT
``` url
https://git-scm.com/download/win
```
##### NodeJs
``` url
https://nodejs.org/en/
```
### Bower
Установка bower
``` url
npm install -g bower
```

Все внешние пакеты для фронтенд части сайта
находятся в
``` url
www\DESIGN\SITE\vendor
```

Для установки внешних пакетов использовать команду
``` url
bower i
```

### Gulp и продакшн
Все файлы js и css можно объединить и минимизировать

Установка gulp в папке www
``` url
npm install
```

Запуск gulp
``` url
gulp
```

Сконфигурировать gulp можно в файле gulpfile.js
Настройка минифицированных модулей находится в конце файла 
config.php

## Внешние пакеты

Удобный комплект хэлперов для вывода информации

``` url
https://packagist.org/packages/pers1307/helpers
https://github.com/pers1307/helpers
```

Базовый класс корзины

``` url
https://packagist.org/packages/pers1307/cart
https://github.com/pers1307/cart
```

Конвертер картинок, примеры решений

``` url
https://packagist.org/packages/pers1307/convert
https://github.com/pers1307/convert
```

Готовый общий модуль для слайдера

``` url
https://packagist.org/packages/pers1307/modul-slider
https://github.com/pers1307/modul-slider
```

Готовый запил под форму

``` url
https://packagist.org/packages/pers1307/form
https://github.com/pers1307/form
```