<?php

$modelAgentSettings      = MSCore::settings('agents', MSCore::page()->path_id);
$modelPropertiesSettings = MSCore::settings('properties', MSCore::page()->path_id);

return array(
    'catalog' => array(
        'className' => 'ModelCatalog',
        'pathId' => MSCore::page()->path_id,
        //'path' => '', // если не нужен /catalog
        'vars' => MSCore::urls()->vars,

        'itemsOrder' => '`title`',

        'pagination' => array(
            'onPage' => 15
        )
    ),
    'agents' => array(
        'className' => 'ModelAgents',
        'pathId' => MSCore::page()->path_id,
        'path' => '/' . MSCore::page()->path,
        'order' => '`order` ASC',
        'pagination' => array(
            'onPage' => $modelAgentSettings->onPage
        )
    ),
    'property' => array(
        'className' => 'ModelProperty',
        'pathId' => 178,
        'path' => '/' . path(178),
        'order' => '`order` ASC',
        'pagination' => array(
            'onPage' => $modelPropertiesSettings->onPage
        )
    ),
    'about' => array(
        'className' => 'ModelAbout',
        'pathId'    => MSCore::page()->path_id,
        'path'      => '/' . MSCore::page()->path,
    ),
    'main' => array(
        'className' => 'ModelMain',
        'pathId'    => MSCore::page()->path_id,
        'path'      => '/' . MSCore::page()->path,
    ),
    'services' => array(
        'className' => 'ModelServices',
        'pathId' => MSCore::page()->path_id,
        'onlyHaveItems' => false,
        'path' => '', // если не нужен /catalog
        'vars' => MSCore::urls()->vars,
        'itemsOrder' => '`order`',
        /*'pagination' => array(
            'onPage' => 6
        )*/
    ),
);