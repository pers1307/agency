<!DOCTYPE html>
<html  lang="ru-RU">
<?= template('common/head') ?>

<body class="page-template page-template-template_agents_grid page-template-template_agents_grid-php page page-id-46 bg-display-cover elementor-default">
<? require_once BLOCKS_DIR . DS . 'common' . DS . 'header.php' ?>

<section class="module subheader bg-display-cover"
         style="background-image:url('/DESIGN/SITE/images/page-banner-default.jpg');">

    <div class="container">
        <h1 ><?= MSCore::page()->header ?></h1>
        <?= template('common/breadcrumbs') ?>
    </div>
</section>

<?= $_CONTENT_ ?>

<? require_once BLOCKS_DIR . DS . 'common' . DS . 'footer.php' ?>

<?= template('common/callback') ?>

<?= StaticFiles::getJS(MSCore::page()->js) ?>
<?= MSJs::getScripts(); ?>
</body>
</html>