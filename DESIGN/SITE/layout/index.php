<!DOCTYPE html>
<html  lang="en-US">

<?= template('common/head') ?>

<body class="home page-template page-template-template_home page-template-template_home-php page page-id-12 bg-display-cover elementor-default elementor-page elementor-page-12">

<? require_once BLOCKS_DIR . DS . 'common' . DS . 'header.php' ?>
<? require_once BLOCKS_DIR . '/slider/slider.php'; ?>

<section >
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-12 col-full">
                <div class="content">
                    <div class="elementor elementor-12">
                        <div class="elementor-inner">
                            <div class="elementor-section-wrap">
                                <? require_once BLOCKS_DIR . '/benefits/benefits.php'; ?>
                                <? require_once BLOCKS_DIR . '/testimonials/testimonials.php'; ?>
                                <? require_once BLOCKS_DIR . '/main/properties.php'; ?>
                                <? require_once BLOCKS_DIR . '/main/agents.php'; ?>

                                <section class="elementor-element elementor-element-88591c6 elementor-section-boxed elementor-section-height-default elementor-section-height-default module module-border elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
                                    <?= $_CONTENT_ ?>
                                </section>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- end row -->

    </div><!-- end container -->
</section>

<? require_once BLOCKS_DIR . '/main/sell.php'; ?>

<? require_once BLOCKS_DIR . DS . 'common' . DS . 'footer.php' ?>

<?= template('common/callback') ?>
<?= StaticFiles::getJS(MSCore::page()->js) ?>
<?= MSJs::getScripts(); ?>
</body>
</html>