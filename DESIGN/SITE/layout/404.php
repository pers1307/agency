<!DOCTYPE html>
<html  lang="ru-RU">
<?= template('common/head') ?>

<body class="page-template page-template-template_agents_grid page-template-template_agents_grid-php page page-id-46 bg-display-cover elementor-default">
<? require_once BLOCKS_DIR . DS . 'common' . DS . 'header.php' ?>

<section class="module subheader bg-display-cover"
         style="background-image:url('/DESIGN/SITE/images/page-banner-default.jpg');">

    <div class="container">
        <h1 ><?= MSCore::page()->title_h1 ?></h1>
        <?= template('common/breadcrumbs') ?>
    </div>
</section>

<section class="module page-not-found">
    <div class="container">

        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <h2>404</h2>
                <h3>Страница не найдена</h3>
                <p>Упс! Страница, которую вы искали не найдена</p>

                <a href="/" class="button button-icon">
                    <i class="fa fa-angle-left"></i> Вернуться на главную
                </a>
            </div>
        </div>

    </div><!-- end container -->
</section>

<? require_once BLOCKS_DIR . DS . 'common' . DS . 'footer.php' ?>

<?= StaticFiles::getJS(MSCore::page()->js) ?>
<?= MSJs::getScripts(); ?>
</body>
</html>