<?php

$action = getParam(0);
$params = explode('.', $action);

$class = (isset($params[0])) ? $params[0] : null;
$method = (isset($params[1])) ? $params[1] : null;

if(!is_null($class)) {
    $nameParts = explode('_', $class);
    $nameParts = array_map('ucfirst', $nameParts);
    $className = 'Api'.implode('', $nameParts);

    if(class_exists($className)) {
        $class = $className;
    } else {
        $class = 'MSBaseApi';
    }
} else {
    $class = 'MSBaseApi';
}

/** @var MSBaseApi $handler */
$handler = new $class;
$handler->setAction($method);
$handler->execute();
exit;