<div class="modal-window-close"></div>
<div class="modalTitle">
    Обратный звонок
</div>
<div class="modal">
    <div class="js-forms-wrap">
        <form id="callbackForm" action="/api/callback.send/" method="post">
            <label class="js-forms-block">
                <span>Имя <i>*</i></span>
                <input name="name" class="js-forms-input" maxlength="255" type="text">
                <span class="errorText js-forms-error"></span>
            </label>
            <label class="js-forms-block">
                <span>Телефон <i>*</i></span>
                <input name="phone" class="js-forms-input phone-mask" maxlength="255" type="text">
                <span class="errorText js-forms-error"></span>
            </label>
            <label class="js-forms-block">
                <span>Комментарий</span>
                <textarea name="text" class="js-forms-input"></textarea>
                <span class="errorText js-forms-error"></span>
            </label>
            <button type="submit" class="btn">Отправить</button>
        </form>
    </div>
</div>
