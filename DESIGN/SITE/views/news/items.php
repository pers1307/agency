<?php
    /**
     * User: Andruha
     * Date: 25.09.13
     * Time: 17:33
     *
     * @var ModelNews $model
     * @var array $items
     * @var MSBasePagination $pagination
     */
?>
    <div>
        <ul>
            <?php foreach ($items as $item): ?>
                <li>
                    <span><?php echo sql2date($item['date'], 4); ?></span><br>
                    <a href="<?php echo $item['itemLink'] ?>"><?php echo $item['title']; ?></a>
                    <span><?php echo $item['announce']; ?></span>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php
    echo $pagination;