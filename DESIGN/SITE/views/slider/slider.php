<? if(!empty($sliders)): ?>
    <section class="subheader subheader-slider">
        <div class="slider-wrap">

            <div class="slider-nav slider-nav-simple-slider">
                <span class="slider-prev"><i class="fa fa-angle-left"></i></span>
                <span class="slider-next"><i class="fa fa-angle-right"></i></span>
            </div>

            <div class="slider slider-simple ">
                <? foreach($sliders as $slider): ?>
                    <?
                    $imageUrl = MSFiles::getImageUrl($slider['image'], 'min');
                    $imageAlt = MSFiles::getImageDescription($slider['image'], 0);

                    if (empty($imageAlt)) {
                        $imageAlt = $slider['title'];
                    }

                    if (empty($imageUrl)) {
                        $imageUrl = '/DESIGN/SITE/images/no-image/no-image_1260_570.jpg';
                        $imageAlt = 'Нет изображения';
                    }
                    ?>

                    <div class="slide" style="background-image:url('<?= $imageUrl ?>');">
                        <div class="img-overlay black"></div>
                        <div class="container">
                            <? if(!empty($slider['title'])): ?>
                                <h1><?= $slider['title'] ?></h1>
                            <? endif; ?>

                            <? if(!empty($slider['text'])): ?>
                                <p class="property-address">
                                    <?= $slider['text'] ?>
                                </p>
                            <? endif; ?>

                            <? if(!empty($slider['link'])): ?>
                                <div class="slider-simple-buttons">
                                    <a href="<?= $slider['link'] ?>" class="button">
                                        Посмотреть
                                    </a>
                                </div>
                            <? endif; ?>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </section>
<? endif; ?>