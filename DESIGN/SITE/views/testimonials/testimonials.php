<? if(!empty($testimonials)): ?>
    <section data-id="3f0fc7e" class="elementor-element elementor-element-3f0fc7e elementor-section-boxed elementor-section-height-default elementor-section-height-default module light elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
        <div class="elementor-container elementor-column-gap-default">
            <div class="elementor-row">
                <div data-id="6dfaf4b7" class="elementor-element elementor-element-6dfaf4b7 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                    <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-widget-wrap">
                            <div data-id="2680ecbd" class="elementor-element elementor-element-2680ecbd elementor-widget elementor-widget-shortcode" data-element_type="shortcode.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-shortcode">
                                        <div class="module-header ">
                                            <? if(!empty($title)): ?>
                                                <h1><?= $title ?></h1>
                                                <table class="widget-divider"><tr><td><div class="bar"></div></td><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>
                                            <? endif; ?>

                                            <? if(!empty($subtitle)): ?>
                                                <p>
                                                    <?= $subtitle ?>
                                                </p>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div data-id="4f01d436" class="elementor-element elementor-element-4f01d436 elementor-widget elementor-widget-wp-widget-rypecore_testimonials_widget" data-element_type="wp-widget-rypecore_testimonials_widget.default">
                                <div class="elementor-widget-container">

                                    <div class="slider-wrap slider-wrap-testimonials">
                                        <div class="slider-nav slider-nav-testimonials">
                                            <span class="slider-prev slick-arrow"><i class="fa fa-angle-left"></i></span>
                                            <span class="slider-next slick-arrow"><i class="fa fa-angle-right"></i></span>
                                        </div>

                                        <div class="slider slider-testimonials">
                                            <? foreach($testimonials as $testimonial): ?>
                                                <div class="testimonial slide">
                                                    <div class="h3_title">
                                                        <?= $testimonial['text'] ?>
                                                    </div>
                                                    <div class="testimonial-details">
                                                        <? if(!empty($testimonial['title'])): ?>
                                                            <span class="testimonial-name">
                                                                <strong><?= $testimonial['title'] ?></strong>
                                                            </span>
                                                        <? endif; ?>

                                                        <? if(!empty($testimonial['author_position'])): ?>
                                                            <span class="testiomnial-title">
                                                                <em><?= $testimonial['author_position'] ?></em>
                                                            </span>
                                                        <? endif; ?>
                                                    </div>
                                                </div>
                                            <? endforeach; ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif; ?>