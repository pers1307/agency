<? if(!empty($networks)): ?>
    <div class="contact-item">
        <i class="fa fa-share-alt icon"></i>
        <h4>Свяжитесь с нами</h4>

        <ul class="social-icons">
            <noindex>
                <? foreach($networks as $network): ?>
                    <li>
                        <? if(!empty($network['url'])): ?>
                            <a rel="nofollow" href="<?= $network['url'] ?>" target="_blank">
                                <i class="fa <?= $network['icon_class'] ?>"></i>
                            </a>
                        <? else: ?>
                            <a target="_blank">
                                <i class="fa <?= $network['icon_class'] ?>"></i>
                            </a>
                        <? endif; ?>
                    </li>
                <? endforeach; ?>
            </noindex>
        </ul>
    </div>
<? endif; ?>