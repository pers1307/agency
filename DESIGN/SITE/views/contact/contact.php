<section class="module contact-details">
  <div class="container">
    <div class="center contact-list">
      <? if (!empty($email)): ?>
        <div class="contact-item">
          <i class="fa fa-envelope icon "></i>
          <h4>Email</h4>
          <p title="<?= $email ?>">
            <a href="mailto:<?= $email ?>"><?= $email ?></a>
          </p>
        </div>
      <? endif; ?>

      <? if (!empty($phone)): ?>
        <div class="contact-item">
          <i class="fa fa-phone icon "></i>
          <h4>Телефон</h4>
          <p>
            <a href="tel:<?= $phone ?>"><?= $phone ?></a>
          </p>
        </div>
      <? endif; ?>

      <? if (!empty($address)): ?>
        <div class="contact-item">
          <i class="fa fa-map-marker icon"></i>
          <h4>Наш офис</h4>
          <p><?= $address ?></p>
        </div>
      <? endif; ?>

      <?= template('contact/social-network', ['networks' => $networks]) ?>
    </div>
    <div class="center">
      <div id="map">
          [map=1]
      </div>
      <? if (!empty($text)): ?>
        <div class="contact-form-after">
          <?= $text ?>
        </div>
      <? endif; ?>
    </div>
  </div>
</section>

<section class="module">
  <div class="container">
    <div class="row">

      <div class="col-lg-8 col-md-8">
        <div class="module-header module-header-left">
          <h4>Форма обратной связи</h4>
          <table class="widget-divider">
            <tr>
              <td>
                <div class="rhex"></div>
              </td>
              <td>
                <div class="bar"></div>
              </td>
            </tr>
          </table>
        </div>

        <form
            method="post"
            class="contact-form js-form-contact"
            action="/api/callback.contact/"
            data-file="/api/order.file/"
            data-deleteFile="/api/order.deleteFile/"
        >
          <div class="contact-form-fields">
            <div class="form-block">
              <label>Имя*</label>
              <input type="text" name="name" autocomplete="off"/>
            </div>

            <div class="form-block">
              <label>Email*</label>
              <input type="email" name="email" autocomplete="off"/>
            </div>

            <div class="form-block">
              <label>Телефон*</label>
              <input type="text" name="phone" autocomplete="off"/>
            </div>

            <div class="form-block">
              <label>Комментарий</label>
              <textarea name="comment" style="resize: none"></textarea>
            </div>

            <div class="form-block">
              <input type="text" class="address" name="address">
              <input type="hidden" name="csrf" value="<?= MSCore::csrf()->getToken() ?>"/>
              <input type="submit" value="Отправить"/>
            </div>
          </div>
        </form>


      </div>

      <div class="col-lg-4 col-md-4 sidebar">
        <?php require_once BLOCKS_DIR . '/widget/properties.php'; ?>
      </div>
    </div><!-- end row -->
  </div><!-- end container -->
</section>

<script>
  "use strict";

  // Боже, прости мне грехи мои
  function initialize() {
    <? $coords = json_decode($addressMap, true) ?>

    <? if(!empty($coords['coords'])): ?>
    <? $coords['coords'] = explode(':', $coords['coords']); ?>

    var mapOptions = {
      zoom: 12,
      scrollwheel: false,
      center: new google.maps.LatLng(<?= $coords['coords'][1] ?>, <?= $coords['coords'][0] ?>)
    };

    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(<?= $coords['coords'][1] ?>, <?= $coords['coords'][0] ?>),
      icon: '/DESIGN/SITE/images/pin.png',
      map: map
    });
    <? endif; ?>

//        google.maps.event.addDomListener(window, 'load', initialize);
  }
</script>
<script>
  "use strict";

  // Боже, прости мне грехи мои
  function initialize() {
    <? $coords = json_decode($addressMap, true) ?>

    <? if(!empty($coords['coords'])): ?>
    <? $coords['coords'] = explode(':', $coords['coords']); ?>

    var mapOptions = {
      zoom: 12,
      scrollwheel: false,
      center: new google.maps.LatLng(<?= $coords['coords'][1] ?>, <?= $coords['coords'][0] ?>)
    };

    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(<?= $coords['coords'][1] ?>, <?= $coords['coords'][0] ?>),
      icon: '/DESIGN/SITE/images/pin.png',
      map: map
    });
    <? endif; ?>

//        google.maps.event.addDomListener(window, 'load', initialize);
  }
</script>