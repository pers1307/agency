<? if(!empty($networks)): ?>
    <ul class="social-icons circle clean-list">
        <noindex>
            <? foreach($networks as $network): ?>
                <li>
                    <? if(!empty($network['url'])): ?>
                        <a rel="nofollow" href="<?= $network['url'] ?>" target="_blank">
                            <i class="fa <?= $network['icon_class'] ?>"></i>
                        </a>
                    <? else: ?>
                        <a target="_blank">
                            <i class="fa <?= $network['icon_class'] ?>"></i>
                        </a>
                    <? endif; ?>
                </li>
            <? endforeach; ?>
        </noindex>
    </ul>
<? endif; ?>