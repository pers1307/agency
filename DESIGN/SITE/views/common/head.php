<head>
    <meta charset="<?= SITE_ENCODING ?>">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
		<?php setCanonicalUrl(); ?>
    <link rel="icon" href="/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="/favicon.png" type="image/x-icon">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext" rel="stylesheet">
    <title><?= MSCore::page()->title_page ?></title>

    <script type="text/javascript">
        window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/rypecreative.com\/homely-wp\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.8"}};
        !function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56760,9792,65039],[55358,56760,8203,9792,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
    </script>

    <?= StaticFiles::getCSS(MSCore::page()->css) ?>

    <script type='text/javascript' src='/DESIGN/SITE/js/vendor/jqueryb8ff.js'></script>
    <script type='text/javascript' src='/DESIGN/SITE/js/vendor/jquery-migrate.min330a.js'></script>
    <script type='text/javascript' src='/DESIGN/SITE/js/vendor/jquery.themepunch.tools.min23da.js'></script>
    <script type='text/javascript' src='/DESIGN/SITE/js/vendor/jquery.themepunch.revolution.min23da.js'></script>

    <script type='text/javascript'>
        /* <![CDATA[ */
        var simpleLikes = {"ajaxurl":"http:\/\/rypecreative.com\/homely-wp\/wp-admin\/admin-ajax.php","like":"Like","unlike":"Unlike"};
        /* ]]> */
    </script>

    <script type='text/javascript' src='/DESIGN/SITE/js/vendor/simple-likes-public6a79.js'></script>
    <script type='text/javascript' src='/DESIGN/SITE/js/vendor/html5shiv5010.js'></script>

    <? if (MSCore::page()->path_id != 137): ?>
        <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?key=AIzaSyAieN5h5Kk6EzbJMGCuI-vBsE4rGFPMsSw&amp;libraries=places&amp;ver=4.9.8'></script>
    <? endif; ?>

    <script type='text/javascript' src='/DESIGN/SITE/js/vendor/setREVStartSize.js'></script>
</head>