<? if(!empty($text)): ?>
    <section class="module">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="content">
                        <?= $text ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif; ?>