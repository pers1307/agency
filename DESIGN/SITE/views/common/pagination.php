<?php
/**
 * pagination.php
 * Предствление для постраничной навигации
 *
 * @author      Pereskokov Yurii
 * @version     1.2
 * @copyright   2015 Pereskokov Yurii
 * @license     Mediasite LLC
 * @link        http://www.mediasite.ru/
 *
 * @var array $pages массив страниц
 */

// Найдем все переключалки
$pageNext = null;
$pagePrev = null;

foreach ($pages as $page) {
    if ($page->value === '<span>вперед</span> &raquo;') {
        $pageNext = $page;
    }

    if ($page->value === '&laquo; <span>назад</span>') {
        $pagePrev = $page;
    }
}
?>

<div class="page-list">
    <? foreach($pages as $page): ?>

        <? if ($page->type === 'link'): ?>
            <?

            if ($page->value === '<span>вперед</span> &raquo;') {
                echo '<a class="next page-numbers" href="' . $page->url . '">' . $page->value . '</a>';
            } elseif ($page->value === '&laquo; <span>назад</span>') {
                echo '<a class="next page-numbers" href="' . $page->url . '">' . $page->value . '</a>';
            } else {
                echo '<a class=\'page-numbers\' href="' . $page->url . '">' . $page->value . '</a>';
            }

            ?>
        <? else: ?>

            <? if ($page->value === '...'): ?>
                <span aria-current='page' class='page-numbers current'><?= $page->value ?></span>
            <? else: ?>
                <span aria-current='page' class='page-numbers current'><?= $page->value ?></span>
            <? endif; ?>

        <? endif; ?>
    <? endforeach; ?>
</div>