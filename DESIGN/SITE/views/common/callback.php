<div class="js-noDisplay-callback">
    <div class="modal-window-close"></div>

    <div class="modalTitle">
        Перезвоните мне
    </div>

    <div class="modal">
        <div class="modalContent">
            <form
                class="js-form-callback"
                action="/api/callback.callback/"
                method="post"
                data-file="/api/order.file/"
                data-deleteFile="/api/order.deleteFile/"
            >
                <label class="labelBlock">
                    <span>Имя <i>*</i></span>
                    <input type="text" name="name" autocomplete="off">
                </label>

                <label class="labelBlock">
                    <span>Телефон <i>*</i></span>
                    <input type="text" name="phone" autocomplete="off">
                </label>

                <label class="labelBlock">
                    <span>Комментарий</span>
                    <textarea name="comment" style="resize: none"></textarea>
                </label>

                <input type="text" class="address" name="address">
                <input type="hidden" name="csrf" value="<?= MSCore::csrf()->getToken() ?>" />

                <button class="button">Заказать</button>
            </form>
        </div>
    </div>

</div>