<? if(!empty($menu)): ?>
    <div class="navbar-collapse collapse">
        <div class="main-menu-wrap">
            <div class="container-fixed">

                <div class="member-actions right">
                    <a class="button small alt button-icon js-callback">
                        <i class="fa fa-plus"></i>
                        Перезвоните мне
                    </a>
                </div>

                <ul id="menu-main-menu-1" class="nav navbar-nav right">
                    <? foreach($menu as $menuItem): ?>
                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-18">

                          <? if('uslugi' == trim($menuItem['path'], '/')): ?>

                                <span id="uslugi">
                                    <?= $menuItem['title'] ?>
                                  <i></i>
                                </span>
                                <ul id="sub-uslugi">
                                    <?php foreach($uslugi as $k=>$service) { ?>
                                        <li>
                                            <a href="<?= '/' . $service['code'].'/' ?>"><?= $service['name'] ?></a>
                                        </li>
                                    <?php	} ?>
                                </ul>

                                <? elseif ($menuItem['here'] === true): ?>

                                <span>
                                    <?= $menuItem['title'] ?>
                                </span>
                            <? elseif($menuItem['current'] === true): ?>
                                <a class="active" href="<?= $menuItem['path'] ?>">
                                    <?= $menuItem['title'] ?>
                                </a>
                            <? else: ?>
                                <a href="<?= $menuItem['path'] ?>">
                                    <?= $menuItem['title'] ?>
                                </a>
                            <? endif; ?>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
<? endif; ?>
