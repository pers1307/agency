<header class="header-default has-menu navbar-fixed">
    <div class="container">
        <div class="navbar-header">

            <div class="header-details">

                <? if(!empty($phoneViber)): ?>
                    <div class="header-item header-phone header-viber left">
                        <a href="viber://add?number=<?= $phoneViber ?>">
                            <div class="header-item-icon"><img src="/DESIGN/SITE/images/viber.png" alt=""><i class="icon-text">Viber</i><!--<i class="fa fa-viber fa-2x icon "></i>--></div>
                        </a>
                    </div>
                <? endif; ?>

                <? if(!empty($phoneWhatsUp)): ?>
                    <div class="header-item header-phone header-wh left">
                        <a href="whatsapp://send?phone=<?= $phoneWhatsUp ?>">
                            <div class="header-item-icon"><img src="/DESIGN/SITE/images/wh.png" alt=""><i class="icon-text">WhatsApp </i><!--<i class="fa fa-whatsapp icon "></i>--></div>
                        </a>
                    </div>
                <? endif; ?>

                <? if(!empty($phone)): ?>
                    <div class="header-item header-phone left">
                        <div class="header-item-icon"><i class="fa fa-phone icon "></i></div>
                        <div class="header-item-text">
                            Телефон:<br/>
                            <a href="tel:<?= $phone ?>"><span><?= $phone ?></span></a>
                        </div>
                    </div>
                <? endif; ?>

                <? if(!empty($email)): ?>
                    <div class="header-item header-email left">
                        <div class="header-item-icon"><i class="fa fa-envelope icon "></i></div>
                        <div class="header-item-text">
                            Email:<br/>
                            <a href="mailto:<?= $email ?>" title="<?= $email ?>">
                                <span><?= $email ?></span>
                            </a>
                        </div>
                    </div>
                <? endif; ?>

                <div class="clear"></div>
            </div>

          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>

            <? if(MSCore::page()->path === ''): ?>
                <a class="navbar-brand has-logo">
                    <img src="/DESIGN/SITE/images/logo/logo.png" alt="Логотип"/>
                </a>
            <? else: ?>
                <a class="navbar-brand has-logo" href="/">
                    <img src="/DESIGN/SITE/images/logo/logo.png" alt="Логотип"/>
                </a>
            <? endif; ?>


        </div>

        <?php require_once BLOCKS_DIR . '/common/menu.php'; ?>
    </div>
</header>