<footer id="footer" >
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 widget widget-footer social-links">
                <div class="socil-links-widget">
                    <div class="social-links-before-text">
                        <p style="margin-bottom:30px">
                            <img width="172px" src="/DESIGN/SITE/images/logo/logo-white.png" alt=""/>
                        </p>
                        <?= $footerText ?>
                    </div>

                    <div class="divider"></div>
                    <?php require_once BLOCKS_DIR . '/common/social-network.php'; ?>
                </div>
            </div>

            <? if(!empty($footerContacts)): ?>
                <div class="col-lg-3 col-md-3 col-sm-6 widget widget-footer widget_text">
                    <h4>
                        <span>Контакты</span>
                    </h4>
                    <table class="widget-divider"><tr><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>
                    <div class="textwidget">
                        <?= $footerContacts ?>
                    </div>
                </div>
            <? endif; ?>
        </div>
    </div>
</footer>

<div class="bottom-bar">
    <div class="container">
        <span><a href="http://pindato.ru/" target="_blank">Pindato</a> | © 2018</span>
    </div>
</div>