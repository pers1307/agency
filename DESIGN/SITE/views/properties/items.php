<?php
    /**
     * User: Andruha
     * Date: 25.09.13
     * Time: 17:33
     *
     * @var ModelNews $model
     * @var array $items
     * @var MSBasePagination $pagination
     */
?>

<section class="module no-padding property-listing-map ">

    <div class="property-listing-map-left">
        <?= template('properties/items-map', ['items' => $items]) ?>
    </div>

    <div class="property-listing-map-right">

        <? foreach($items as $row): ?>
            <div class="row listing">

                <? foreach($row as $item): ?>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="property shadow-hover post-23">
                            <div class="property-img">
                                <? if ($item['type_sell'] == 2): ?>
                                    <div class="property-tag button status">
                                        <a href="<?= $item['itemLink'] ?>">В аренду</a>
                                    </div>
                                <? endif; ?>

                                <? if ($item['type_sell'] == 1): ?>
                                    <a href="<?= $item['itemLink'] ?>" class="property-tag button alt featured">Продается</a>
                                <? endif; ?>

                                <div class="property-price">
                                    <? if ($item['type_sell'] == 1): ?>
                                        <?= $item['price'] ?>
                                    <? endif; ?>

                                    <? if ($item['type_sell'] == 2): ?>
                                        <?= $item['rent_price'] ?>
                                        <span class="price-postfix">В месяц</span>
                                    <? endif; ?>
                                </div>

                                <div class="property-color-bar"></div>
                                <div class="img-fade"></div>

                                <a class="property-img-link" href="<?= $item['itemLink'] ?>">
                                    <?
                                    $imageUrl = MSFiles::getImageUrl($item['image'], 'list');
                                    $imageAlt = MSFiles::getImageDescription($item['image'], 0);

                                    if (empty($imageAlt)) {
                                        $imageAlt = $item['title'];
                                    }

                                    if (empty($imageUrl)) {
                                        $imageUrl = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                                        $imageAlt = 'Нет изображения';
                                    }
                                    ?>

                                    <img
                                        width="800"
                                        height="600"
                                        src="<?= $imageUrl ?>"
                                        class="attachment-property-thumbnail size-property-thumbnail wp-post-image"
                                        alt="<?= $imageAlt ?>"
                                    />
                                </a>
                            </div>

                            <div class="property-content">

                                <div class="property-title">
                                    <h4>
                                        <a href="<?= $item['itemLink'] ?>"><?= $item['title'] ?></a>
                                    </h4>

                                    <? $coords = json_decode($item['geocoder'], true) ?>
                                    <? if(!empty($coords['text'])): ?>
                                        <p class="property-address">
                                            <i class="fa fa-map-marker icon"></i>
                                            <?= $coords['text'] ?>
                                        </p>
                                    <? endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        <? endforeach; ?>

        <?= $pagination ?>

        <br>

        <?= $text ?>
    </div>

</section>

<div class="clear"></div>
