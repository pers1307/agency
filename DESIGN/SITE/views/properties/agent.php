<? if(!empty($item)): ?>
    <div class="property-single-item widget property-agent_info">
        <div class="module-header module-header-left">
            <h4>Информация о агенте</h4>
            <table class="widget-divider"><tr><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>
            <div class="divider-fade"></div>
        </div>

        <div class="agent property-agent">
            <a href="<?= $item['itemLink'] ?>" class="agent-img">
                <? if(!empty($item['propertyCount'])): ?>
                    <div class="button alt agent-tag">Объекты : <?= $item['propertyCount'] ?></div>
                <? endif; ?>

                <div class="img-fade"></div>
                <img
                    width="1197"
                    height="1287"
                    src="<?= $item['images']['list'] ?>"
                    class="attachment-full size-full wp-post-image"
                    alt=""
                    srcset="<?= $item['images']['list'] ?> 1197w, <?= $item['images']['list2'] ?> 279w, <?= $item['images']['list3'] ?> 768w, <?= $item['images']['list4'] ?> 952w"
                    sizes="(max-width: 1197px) 100vw, 1197px"
                />
            </a>

            <div class="agent-content">

                <a href="<?= $item['itemLink'] ?>" class="button button-icon right">
                    <i class="fa fa-angle-right"></i>
                    Связаться с агентом
                </a>

                <div class="agent-details">
                    <h4>
                        <a href="<?= $item['itemLink'] ?>"><?= $item['title'] ?></a>
                    </h4>

                    <? if(!empty($item['position'])): ?>
                        <p>
                            <i class="fa fa-tag icon "></i>
                            <?= $item['position'] ?>
                        </p>
                    <? endif; ?>

                    <? if(!empty($item['email'])): ?>
                        <p>
                            <a href="mailto:<?= $item['email'] ?>">
                                <i class="fa fa-envelope icon "></i>
                                <?= $item['email'] ?>
                            </a>
                        </p>
                    <? endif; ?>

                    <? if(!empty($item['work_phone'])): ?>
                        <p>
                            <i class="fa fa-phone icon "></i>
                            <?= $item['work_phone'] ?>
                        </p>
                    <? else: ?>
                        <? if(!empty($item['mobile_phone'])): ?>
                            <p>
                                <i class="fa fa-phone icon "></i>
                                <?= $item['mobile_phone'] ?>
                            </p>
                        <? endif; ?>
                    <? endif; ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
<? endif; ?>
