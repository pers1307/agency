<? $coords = json_decode($coords, true); ?>

<? if(!empty($coords)): ?>
    <div class="property-single-item widget property-map">
        <div class="module-header module-header-left">
            <h4>Расположение на карте</h4>
            <table class="widget-divider"><tr><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>
            <div class="divider-fade"></div>
        </div>
        <div id="map-canvas-one-pin"></div>
        <? $coords = explode(':', $coords['coords']); ?>

        <script>
            // Время поджимает, оставим так. Если проект останется на поддержку, то зарефакторим
            "use strict";

            //intialize the map
            function initialize() {
                var propertyLatlng = new google.maps.LatLng(<?= $coords[1] ?>, <?= $coords[0] ?>);
                var mapOptions = {
                    zoom: 14,
                    center: propertyLatlng
                };

                var map = new google.maps.Map(document.getElementById('map-canvas-one-pin'), mapOptions);

                var marker = new google.maps.Marker({
                    position: propertyLatlng,
                    map: map,
                    icon: '/DESIGN/SITE/images/pin.png'
                });

                //show info box for marker1
                var contentString = '<div class="info-box">Relaxing Apartment</div>';

                var infowindow = new google.maps.InfoWindow({ content: contentString });

                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map,marker);
                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
    </div>
<? endif; ?>