<section class="subheader google-maps">
    <div id="map-canvas"></div>

    <script>
        "use strict";

        // Боже, прости мне грехи мои
        function initialize() {

            <? $mapInit = false; ?>
            <? $count   = 0; ?>

            <? foreach ($items as $keyRow => $row): ?>
                <? foreach ($row as $keyItem => $item): ?>

                    <? if($mapInit === false): ?>
                        <? $coords = json_decode($item['geocoder'], true) ?>

                        <? if(!empty($coords['coords'])): ?>
                            <? $coords['coords'] = explode(':', $coords['coords']); ?>

                            var mapOptions = {
                                zoom: 12,
                                scrollwheel: false,
                                center: new google.maps.LatLng(<?= $coords['coords'][1] ?>, <?= $coords['coords'][0] ?>)
                            };

                            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                            var marker<?= $count ?> = new google.maps.Marker({
                                position: new google.maps.LatLng(<?= $coords['coords'][1] ?>, <?= $coords['coords'][0] ?>),
                                icon: '/DESIGN/SITE/images/pin.png',
                                map: map
                            });

                            <?
                            $price = '';

                            if ($item['type_sell'] == 1) {
                                $price = '<div>' . $item['price'] . '</div>';
                            }

                            if ($item['type_sell'] == 2) {
                                $price = '<div>' . $item['rent_price'] . '<span class="price-postfix"> В месяц</span></div>';
                            }

                            $imageUrl = MSFiles::getImageUrl($item['image'], 'list');
                            $imageUrlMap1 = MSFiles::getImageUrl($item['image'], 'map1');
                            $imageUrlMap2 = MSFiles::getImageUrl($item['image'], 'map2');
                            $imageUrlMap3 = MSFiles::getImageUrl($item['image'], 'map3');
                            $imageUrlMap4 = MSFiles::getImageUrl($item['image'], 'map4');

                            if (empty($imageUrl)) {
                                $imageUrl = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                                $imageUrlMap1 = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                                $imageUrlMap2 = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                                $imageUrlMap3 = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                                $imageUrlMap4 = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                            }

                            $content = '<div class="info-box"><a href="' . $item['itemLink'] . '"><img width="150" height="98" src="' . $imageUrlMap1 . '" class="attachment-thumb size-thumb wp-post-image" alt="" srcset="' . $imageUrlMap1 . ' 1837w, ' . $imageUrlMap2 . ' 300w, ' . $imageUrlMap3 . ' 768w, ' . $imageUrlMap4 . ' 1024w" sizes="(max-width: 150px) 100vw, 150px" /><p><strong>' . $item['title'] . '</strong></p></a>' . $price . '</div>';

                            ?>

                            var infowindow<?= $count ?> = new google.maps.InfoWindow({ content: '<?= $content ?>' });
                            google.maps.event.addListener(marker<?= $count ?>, 'click', function() {
                                infowindow<?= $count ?>.open(map,marker<?= $count ?>);
                            });

                            <? $mapInit = true; ?>
                        <? else: ?>
                            <? continue; ?>
                        <? endif; ?>
                    <? else: ?>
                        <? $coords = json_decode($item['geocoder'], true) ?>

                        <? if(!empty($coords['coords'])): ?>
                            <? $coords['coords'] = explode(':', $coords['coords']); ?>

                            var marker<?= $count ?> = new google.maps.Marker({
                                position: new google.maps.LatLng(<?= $coords['coords'][1] ?>, <?= $coords['coords'][0] ?>),
                                icon: '/DESIGN/SITE/images/pin.png',
                                map: map
                            });

                            <?
                            $price = '';

                            if ($item['type_sell'] == 1) {
                                $price = '<div>' . $item['price'] . '</div>';
                            }

                            if ($item['type_sell'] == 2) {
                                $price = '<div>' . $item['rent_price'] . '<span class="price-postfix"> В месяц</span></div>';
                            }

                            $imageUrl = MSFiles::getImageUrl($item['image'], 'list');
                            $imageUrlMap1 = MSFiles::getImageUrl($item['image'], 'map1');
                            $imageUrlMap2 = MSFiles::getImageUrl($item['image'], 'map2');
                            $imageUrlMap3 = MSFiles::getImageUrl($item['image'], 'map3');
                            $imageUrlMap4 = MSFiles::getImageUrl($item['image'], 'map4');

                            if (empty($imageUrl)) {
                                $imageUrl = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                                $imageUrlMap1 = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                                $imageUrlMap2 = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                                $imageUrlMap3 = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                                $imageUrlMap4 = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                            }

                            $content = '<div class="info-box"><a href="' . $item['itemLink'] . '"><img width="150" height="98" src="' . $imageUrlMap1 . '" class="attachment-thumb size-thumb wp-post-image" alt="" srcset="' . $imageUrlMap1 . ' 1837w, ' . $imageUrlMap2 . ' 300w, ' . $imageUrlMap3 . ' 768w, ' . $imageUrlMap4 . ' 1024w" sizes="(max-width: 150px) 100vw, 150px" /><p><strong>' . $item['title'] . '</strong></p></a>' . $price . '</div>';

                            ?>

                            var infowindow<?= $count ?> = new google.maps.InfoWindow({ content: '<?= $content ?>' });
                            google.maps.event.addListener(marker<?= $count ?>, 'click', function() {
                                infowindow<?= $count ?>.open(map,marker<?= $count ?>);
                            });
                        <? endif; ?>
                    <? endif; ?>
                    <? ++$count; ?>
                <? endforeach; ?>
            <? endforeach; ?>
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</section>