<?php
    /**
     * User: Andruha
     * Date: 25.09.13
     * Time: 17:53
     *
     * @var ModelNews $model
     * @var array $item
     */
?>

<section class="module">
    <div class="container">

        <div class="row">
            <div class="col-lg-8 col-md-8">
                <div class="property-single">

                    <div class="property-single-item property-main">

                        <div class="property-header ">
                            <div class="property-title">
                                <h4><?= $item['title'] ?></h4>

                                <div class="property-price-single right">
                                    <? if ($item['type_sell'] == 1): ?>
                                        <?= $item['price'] ?> рублей
                                    <? endif; ?>

                                    <? if ($item['type_sell'] == 2): ?>
                                        <?= $item['rent_price'] ?> рублей
                                        <span class="price-postfix">В месяц</span>
                                    <? endif; ?>
                                </div>

                                <? $geocoder = json_decode($item['geocoder'], true); ?>
                                <? if(!empty($geocoder['text'])): ?>
                                    <p class="property-address">
                                        <i class="fa fa-map-marker icon "></i>
                                        <?= $geocoder['text'] ?>
                                    </p>
                                <? endif; ?>

                                <div class="clear"></div>
                            </div>

                            <div class="property-single-tags">
                                <? if ($item['type_sell'] == 1): ?>
                                    <a class="property-tag button alt featured">Продается</a>
                                <? endif; ?>

                                <? if ($item['type_sell'] == 2): ?>
                                    <div class="property-tag button status">
                                        <a><span>В аренду</span></a>
                                    </div>
                                <? endif; ?>
                            </div>

                            <div class="clear"></div>
                        </div>
                    </div>

                    <? if(!empty($item['gallery'])): ?>
                        <div class="property-single-item no-padding property-gallery">

                            <div class="slider-nav slider-nav-property-gallery">
                                <span class="slider-prev"><i class="fa fa-angle-left"></i></span>
                                <span class="slider-next"><i class="fa fa-angle-right"></i></span>
                            </div>

                            <div class="slide-counter "></div>

                            <div class="slider slider-property-gallery">
                                <? foreach($item['gallery'] as $image): ?>
                                    <div class="slide">
                                        <img
                                                src="<?= $image['path']['main'] ?>"
                                                alt=""
                                        />
                                    </div>
                                <? endforeach; ?>
                            </div>

                            <div class="slider property-gallery-pager">
                                <? foreach($item['gallery'] as $image): ?>
                                    <a class="property-gallery-thumb">
                                        <img src="<?= $image['path']['min1'] ?>" alt="" />
                                    </a>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? endif; ?>

                    <? if(!empty($item['description'])): ?>
                        <div class="property-single-item content widget property-description">
                            <div class="module-header module-header-left">
                                <h4>Описание</h4>
                                <table class="widget-divider"><tr><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>
                                <div class="divider-fade"></div>
                            </div>
                            <?= $item['description'] ?>
                        </div>
                    <? endif; ?>

                    <?= template('properties/map', ['coords' => $item['geocoder']]) ?>
                    <?= template('properties/agent', ['item' => $item['agent']]) ?>
                    <?= template('properties/related-properties', ['items' => $item['properties']]) ?>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 sidebar">
                <?php require_once BLOCKS_DIR . '/widget/properties.php'; ?>
            </div>
        </div>
    </div>

</section>