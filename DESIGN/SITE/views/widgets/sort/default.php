<? if($params['fields']): ?>

    <? foreach($params['fields'] as $headerItem): ?>
        <div>
            <? if($headerItem['current']): ?>
                <b><a href="<?= $headerItem['link'] ?>"><?= $headerItem['label'] ?></a></b>
                <? if($headerItem['arrow'] == 'top'): ?>
                    ^
                <? elseif($headerItem['arrow'] == 'bottom'): ?>
                    v
                <? endif; ?>
            <? else: ?>
                <a href="<?= $headerItem['link'] ?>"><?= $headerItem['label'] ?></a>
                <? if($headerItem['arrow'] == 'top'): ?>
                    ^
                <? elseif($headerItem['arrow'] == 'bottom'): ?>
                    v
                <? endif; ?>
            <? endif; ?>
        </div>
    <? endforeach; ?>
<? endif; ?>