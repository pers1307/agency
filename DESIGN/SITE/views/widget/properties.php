<? if(!empty($items)): ?>
    <div class="widget widget-sidebar widget-sidebar-properties list-properties-widget">
        <h4>Последние добавленные объекты</h4>
        <table class="widget-divider"><tr><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>

        <? foreach($items as $item): ?>
            <div class="list-property">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                        <div class="property-img">

                            <a href="<?= $item['itemLink'] ?>" class="property-img-link">
                                <?
                                $imageUrl = MSFiles::getImageUrl($item['image'], 'list');
                                $imageAlt = MSFiles::getImageDescription($item['image'], 0);

                                if (empty($imageAlt)) {
                                    $imageAlt = $item['title'];
                                }

                                if (empty($imageUrl)) {
                                    $imageUrl = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                                    $imageAlt = 'Нет изображения';
                                }
                                ?>

                                <img
                                    width="800"
                                    height="600"
                                    src="<?= $imageUrl ?>"
                                    class="attachment-property-thumbnail size-property-thumbnail wp-post-image"
                                    alt="<?= $imageAlt ?>"
                                />
                            </a>
                        </div>
                    </div>

                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                        <h5 title="<?= $item['title'] ?>">
                            <a href="<?= $item['itemLink'] ?>">
                                <?= $item['title'] ?>
                            </a>
                        </h5>

                        <? if ($item['type_sell'] == 2): ?>
                            <div class="property-tag button status">
                                <a href="<?= $item['itemLink'] ?>">В аренду</a>
                            </div>
                        <? endif; ?>

                        <? if ($item['type_sell'] == 1): ?>
                            <a href="<?= $item['itemLink'] ?>" class="property-tag button alt featured">Продается</a>
                        <? endif; ?>

                        <? if ($item['type_sell'] == 1): ?>
                            <p>
                                <strong><?= $item['price'] ?></strong>
                            </p>
                        <? endif; ?>

                        <? if ($item['type_sell'] == 2): ?>
                            <p>
                                <strong><?= $item['rent_price'] ?></strong> <span class="price-postfix">В месяц</span>
                            </p>
                        <? endif; ?>
                    </div>

                    <? $coords = json_decode($item['geocoder'], true) ?>
                    <? if(!empty($coords['text'])): ?>
                        <p class="property-address">
                            <i class="fa fa-map-marker icon"></i>
                            <?= $coords['text'] ?>
                        </p>
                    <? endif; ?>
                </div>
            </div>
        <? endforeach; ?>
    </div>
<? endif; ?>