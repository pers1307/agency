<? if(!empty($benefits)): ?>
    <section data-id="615d53a2" class="elementor-element elementor-element-615d53a2 elementor-section-boxed elementor-section-height-default elementor-section-height-default module services no-padding-top elementor-section elementor-top-section" data-element_type="section">
        <div class="elementor-container elementor-column-gap-default">
            <div class="elementor-row">
                <? foreach($benefits as $benefit): ?>
                    <div data-id="21cea603" class="elementor-element elementor-element-21cea603 elementor-column elementor-col-33 elementor-top-column" data-element_type="column">
                        <div class="elementor-column-wrap elementor-element-populated">
                            <div class="elementor-widget-wrap">
                                <div data-id="2789c0b5" class="elementor-element elementor-element-2789c0b5 elementor-widget elementor-widget-shortcode" data-element_type="shortcode.default">
                                    <div class="elementor-widget-container">
                                        <div class="elementor-shortcode">
                                            <div class="service-item shadow-hover">
                                                <? if(!empty($benefit['icon_class'])): ?>
                                                    <i class="fa <?= $benefit['icon_class'] ?> icon"></i>
                                                <? endif; ?>

                                                <? if(!empty($benefit['title'])): ?>
                                                    <h4><?= $benefit['title'] ?></h4>
                                                <? endif; ?>

                                                <? if(!empty($benefit['text'])): ?>
                                                    <p>
                                                        <?= $benefit['text'] ?>
                                                    </p>
                                                <? endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </section>
<? endif; ?>