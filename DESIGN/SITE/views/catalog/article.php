<?php
    /**
     * User: Andruha
     * Date: 22.10.13
     * Time: 9:55
     *
     * @var ModelCatalog $model
     * @var array $article текущий раздел
     * @var array $items позиции в разделе
     * @var MSBasePagination|null $pagination
     */
?>
    <h1><?php echo $article['name']; ?></h1>

<?php
    echo $article['text'];

    $arts = $model->getChildrenTree($article['id']);
    if (!empty($arts))
    {
        ?>
        <h2>Подразделы</h2>
        <ul>
            <?php foreach ($arts as $art): ?>
                <li><a href="<?php echo $model->getArticleLink($art['id'])?>"><?php echo $art['name']?></a> (<?php echo $art['itemsCount'] ?>)</li>
            <?php endforeach; ?>
        </ul>
    <?php
    }

?>
    <h2>Позиции</h2>
    <ul>
        <?php
            foreach ($items as $item)
            {
                ?>
                <li><a href="<?php echo $item['itemLink']; ?>"><?php echo $item['title']; ?></a></li>
            <?php
            }
        ?>
    </ul>
<?php

    echo $pagination;