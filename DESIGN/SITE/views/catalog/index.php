<?php
    /**
     * User: Andruha
     * Date: 22.10.13
     * Time: 9:55
     *
     * @var ModelCatalog $model
     * @var array $articles
     */
?>
<h1>Главная страница каталога</h1>
<ul>
    <?php foreach ($articles as $article): ?>
        <li><a href="<?php echo $model->getArticleLink($article['id'])?>"><?php echo $article['name']?></a> (<?php echo $article['itemsCount'] ?>)</li>
    <?php endforeach; ?>
</ul>