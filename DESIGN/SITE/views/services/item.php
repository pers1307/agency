<section class="module">
    <div class="container">

        <div class="row">

            <div class="col-lg-12 col-md-4 sidebar">

              <div class="widget-sidebar widget_categories">
                <?= template('services/card-menu', ['uri' => $uri, 'articles' => $mainArticles, 'model' => $model]) ?>
              </div>
            </div>

            <div class="col-lg-12 col-md-8">
                <article class="post-225 post type-post status-publish format-standard has-post-thumbnail hentry category-news">
                    <div class="blog-post shadow-hover">
                        <? if(!empty($item['image']['item'])): ?>
                            <a class="blog-post-img">
                                <img
                                    src="<?= $item['image']['item'] ?>"
                                    class="attachment-full size-full wp-post-image"
                                    alt=""
                                    sizes="(max-width: 1400px) 100vw, 1400px"
                                />
                            </a>
                        <? endif; ?>

                        <div class="content blog-post-content">
                            <?= $item['text'] ?>
                        </div>
                    </div>
                </article>
            </div>

        </div>
    </div><!-- end container -->
</section>