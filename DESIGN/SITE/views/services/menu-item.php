<? if(!empty($articles)): ?>
        <? if (!isset($title)): ?>
            <? $title = 'Услуги' ?>
        <? endif; ?>
        <ul>
	        <? foreach($articles as $article): ?>
		        <? if (isset($article['visible']) && $article['visible'] == '0'): ?>
			        <? continue; ?>
		        <? endif; ?>

							<li class="cat-item cat-item-26">
								<a href="<?= $model->getArticleLink($article['id']) ?>">
			        <?= $article['name'] ?>
								</a>
		          <?php
		          foreach($article['children'] as $child) {
			          echo '<a href="'.$model->getArticleLink($article['id']).$child['code'].'/">'.$child['name'].'</a>';
		          }
		          ?>
							</li>
	        <? endforeach; ?>
        </ul>
<? endif; ?>
