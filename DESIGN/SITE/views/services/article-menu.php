<? if(!empty($articles)): ?>
		<? if (!isset($title)): ?>
			<? $title = 'Услуги' ?>
		<? endif; ?>
		<ul>
			<? foreach($articles as $article): ?>
				<? if (isset($article['visible']) && $article['visible'] == '0'): ?>
					<? continue; ?>
				<? endif; ?>
				<?php if($article['code'] != getParam(0)) continue; ?>
				<li class="cat-item cat-item-26">
					<?php
					if($_SERVER['REQUEST_URI'] != $model->getArticleLink($article['id'])) { ?>
						<a href="<?= $model->getArticleLink($article['id']) ?>">
							<?= $article['name'] ?>
						</a> <?php
					} else { ?>
						<span><?= $article['name'] ?></span> <?php
					}
					?>
					<?php
					foreach($article['children'] as $child) { ?>
						&nbsp;&nbsp;
						<?php
						if($_SERVER['REQUEST_URI'] != $model->getArticleLink($article['id']).$child['code'].'/') { ?>
							<a href="<?=$model->getArticleLink($article['id']).$child['code'];?>/">
								<?=$child['name']?>
							</a> <?php
						} else { ?>
							<span><?=$child['name']?></span> <?php
						}?>
					<?php
					}
					?>
				</li>
			<? endforeach; ?>
		</ul>
<? endif; ?>