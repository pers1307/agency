<? if(!empty($articles)): ?>
    <div class="widget-sidebar widget_categories">
        <? if (!isset($title)): ?>
            <? $title = 'Услуги' ?>
        <? endif; ?>
        <ul>
            <? foreach($articles as $article): ?>
                <li class="cat-item cat-item-26">
                    <? if ($hereArticle['level'] == 1): ?>
                        <? if ($article['id'] == $hereArticle['parent']): ?>
                            <a href="<?= $model->getArticleLink($article['id']) ?>">
                                <?= $article['name'] ?>
                            </a>

                            <? $subArticles = $model->getChildrenTree($article['id']); ?>

                            <? if(!empty($subArticles)): ?>
                                <ul>
                                    <? foreach($subArticles as $subArticle): ?>
                                        <li>
                                            <? if ($subArticle['id'] == $hereArticle['id']): ?>
                                                <span>
                                                    <?= $subArticle['name'] ?>
                                                </span>
                                            <? else: ?>
                                                <a href="<?= $model->getArticleLink($subArticle['id'])?>">
                                                    <?= $subArticle['name'] ?>
                                                </a>
                                            <? endif; ?>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            <? endif; ?>
                        <? else: ?>
                            <a href="<?= $model->getArticleLink($article['id']) ?>">
                                <?= $article['name'] ?>
                            </a>
                        <? endif; ?>
                    <? else: ?>
                        <? if ($article['id'] == $hereArticle['id']): ?>
                            <span>
                                <?= $article['name'] ?>
                            </span>

                            <? $subArticles = $model->getChildrenTree($article['id']); ?>

                            <? if(!empty($subArticles)): ?>
                                <ul>
                                    <? foreach($subArticles as $subArticle): ?>
                                        <li>
                                            <a href="<?= $model->getArticleLink($subArticle['id'])?>">
                                                <?= $subArticle['name'] ?>
                                            </a>
                                        </li>
                                    <? endforeach; ?>
                                </ul>
                            <? endif; ?>
                        <? else: ?>
                            <a href="<?= $model->getArticleLink($article['id']) ?>">
                                <?= $article['name'] ?>
                            </a>
                        <? endif; ?>
                    <? endif; ?>
                </li>
            <? endforeach; ?>
        </ul>
    </div>
<? endif; ?>