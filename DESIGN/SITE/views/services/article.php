<?php
    /**
     * User: Andruha
     * Date: 22.10.13
     * Time: 9:55
     *
     * @var ModelCatalog $model
     * @var array $article текущий раздел
     * @var array $items позиции в разделе
     * @var MSBasePagination|null $pagination
     */
?>

<section class="module">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-4 sidebar">
              <div class="widget-sidebar widget_categories">
                <?= template('services/article-menu', [
                    'articles'    => $mainArticles,
                    'model'       => $model,
                    'hereArticle' => $article
                ]) ?>
              </div>

            </div>

            <div class="col-lg-12 col-md-8">
                <div class="row listing">
                    <? foreach($items as $item): ?>
                        <div class="col-lg-3 col-md-4 col-sm-6">
                            <div class="agent shadow-hover">

                                <div class="property-tag button status">
                                    <a href="<?= $item['itemLink'] ?>">
																			<?= $item['ankor'] ?>
																		</a>
                                </div>

                                <div class="agent-img">
                                    <a href="<?= $item['itemLink'] ?>" class="agent-img-link">
                                        <img
                                                class="attachment-full size-full wp-post-image"
                                                width="1197"
                                                height="1287"
                                                src="<?= $item['image']['list'] ?>"
                                                alt=""
                                                srcset="<?= $item['image']['list'] ?> 1197w, <?= $item['image']['list2'] ?> 279w, <?= $item['image']['list3'] ?> 768w, <?= $item['image']['list4'] ?> 952w"
                                                sizes="(max-width: 1197px) 100vw, 1197px"
                                        />
                                    </a>
                                </div>

                                <div class="agent-content agent-details--height">
                                    <div class="agent-details ">
                                        <h4>
                                            <a href="<?= $item['itemLink'] ?>"><?= $item['title'] ?></a>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>

                <?= $pagination ?>
                <br>

                <div class="content">
                    <?= $article['text'] ?>
                </div>
            </div>

        </div>
    </div>
</section>