<section data-id="4609d30e" class="elementor-element elementor-element-4609d30e elementor-section-boxed elementor-section-height-default elementor-section-height-default module elementor-section elementor-top-section" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div data-id="16c6252a" class="elementor-element elementor-element-16c6252a elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div data-id="652298e8" class="elementor-element elementor-element-652298e8 elementor-widget elementor-widget-shortcode" data-element_type="shortcode.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-shortcode">
                                    <div class="module-header ">
                                        <? if(!empty($titleStaff)): ?>
                                            <h2><?= $titleStaff ?></h2>
                                            <table class="widget-divider"><tr><td><div class="bar"></div></td><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>
                                        <? endif; ?>

                                        <?= $subtitleStaff ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <section data-id="77d5be71" class="team-member-list elementor-element elementor-element-77d5be71 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">
                                    <? foreach($staff as $staffItem): ?>
                                        <div data-id="7bf53735" class="elementor-element elementor-element-7bf53735 elementor-column elementor-col-33 elementor-inner-column" data-element_type="column">
                                            <a class="team-member-link" href="<?= $staffItem['itemLink'] ?>">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">
                                                        <div data-id="23f3f7f2" class="elementor-element elementor-element-23f3f7f2 elementor-widget elementor-widget-shortcode" data-element_type="shortcode.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-shortcode">
                                                                    <div class="team-member shadow-hover">
                                                                        <div class="team-member-img">
                                                                            <? if(!empty($staffItem['description'])): ?>
                                                                                <p><?= $staffItem['description'] ?></p>
                                                                            <? endif; ?>

                                                                            <div class="img-overlay"></div>

                                                                            <div class="img-fade"></div>

                                                                            <img src="<?= $staffItem['images']['about'] ?>" alt="<?= $staffItem['title'] ?>"/>
                                                                        </div>

                                                                        <div class="team-member-content">
                                                                            <img class="hex" src="/DESIGN/SITE/images/hexagon.png" alt="" />
                                                                            <? if(!empty($staffItem['title'])): ?>
                                                                                <h4><?= $staffItem['title'] ?></h4>
                                                                            <? endif; ?>

                                                                            <? if(!empty($staffItem['position'])): ?>
                                                                                <p><?= $staffItem['position'] ?></p>
                                                                            <? endif; ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>