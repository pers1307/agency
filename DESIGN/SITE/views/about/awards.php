<section data-id="5c49f5d" class="elementor-element elementor-element-5c49f5d elementor-section-boxed elementor-section-height-default elementor-section-height-default module light elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div data-id="654a0023" class="elementor-element elementor-element-654a0023 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div data-id="1f4b54a7" class="elementor-element elementor-element-1f4b54a7 elementor-widget elementor-widget-shortcode" data-element_type="shortcode.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-shortcode">
                                    <div class="module-header ">
                                        <? if(!empty($titleStats)): ?>
                                            <h2><?= $titleStats ?></h2>
                                            <table class="widget-divider"><tr><td><div class="bar"></div></td><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>
                                        <? endif; ?>

                                        <?= $subtitleStats ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <section data-id="70be933f" class="elementor-element elementor-element-70be933f elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-inner-section" data-element_type="section">
                            <div class="elementor-container elementor-column-gap-default">
                                <div class="elementor-row">

                                    <? foreach($params as $param): ?>
                                        <div data-id="33e30343" class="elementor-element elementor-element-33e30343 elementor-column elementor-col-25 elementor-inner-column" data-element_type="column">
                                            <div class="elementor-column-wrap elementor-element-populated">
                                                <div class="elementor-widget-wrap">
                                                    <div data-id="20c5e660" class="elementor-element elementor-element-20c5e660 elementor-widget elementor-widget-shortcode" data-element_type="shortcode.default">
                                                        <div class="elementor-widget-container">
                                                            <div class="elementor-shortcode">
                                                                <div class="cta-hex">
                                                                    <img src="/DESIGN/SITE/images/hexagon-dark.png" alt="" />
                                                                    <p>
                                                                        <span><?= $param['value'] ?></span>
                                                                        <?= $param['key'] ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>