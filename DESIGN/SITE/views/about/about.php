<section >
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-full">
                <div class="content">
                    <div class="elementor elementor-446">
                        <div class="elementor-inner">
                            <div class="elementor-section-wrap">

                                <section class="module">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12">
                                                <div class="content">

                                                    <div class="elementor-shortcode">
                                                        <? if(!empty($title)): ?>
                                                            <div class="module-header module-header-left">
                                                                <h2><?= $title ?></h2>
                                                                <table class="widget-divider"><tr><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>
                                                            </div>
                                                        <? endif; ?>

                                                    <?= $text ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <?= template('about/awards', [
                                    'params'           => $params,
                                    'titleStats'       => $titleStats,
                                    'subtitleStats'    => $subtitleStats,
                                ]) ?>

                                <?= template('about/agents', [
                                    'titleStaff'       => $titleStaff,
                                    'subtitleStaff'    => $subtitleStaff,
                                    'staff'            => $staff
                                ]) ?>

                                <section data-id="156e0628" class="elementor-element elementor-element-156e0628 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
                                    <div class="elementor-container elementor-column-gap-default">
                                        <div class="elementor-row">
                                            <div data-id="1973f2fa" class="elementor-element elementor-element-1973f2fa elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                                                <div class="elementor-column-wrap elementor-element-populated">
                                                    <div class="elementor-widget-wrap">


                                                        <? if(!empty($titleText)): ?>
                                                            <div data-id="41573bd1" class="elementor-element elementor-element-41573bd1 elementor-widget elementor-widget-heading" data-element_type="heading.default">
                                                                <div class="elementor-widget-container">
                                                                    <h2 class="elementor-heading-title elementor-size-default">
                                                                        <?= $titleText ?>
                                                                    </h2>
                                                                </div>
                                                            </div>
                                                        <? endif; ?>

                                                        <? if(!empty($subtitleText)): ?>
                                                            <div data-id="a37ff22" class="elementor-element elementor-element-a37ff22 elementor-widget elementor-widget-text-editor" data-element_type="text-editor.default">
                                                                <div class="elementor-widget-container">
                                                                    <div class="elementor-text-editor elementor-clearfix">
                                                                        <p style="text-align: center;">
                                                                            <?= $subtitleText ?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <? endif; ?>

                                                        <div data-id="15db6798" class="elementor-element elementor-element-15db6798 elementor-align-center elementor-widget elementor-widget-button" data-element_type="button.default">
                                                            <div class="elementor-widget-container">
                                                                <div class="elementor-button-wrapper">
                                                                    <a href="/<?= path(137) ?>" class="elementor-button-link elementor-button elementor-size-lg" role="button">
						                                                <span class="elementor-button-content-wrapper">
                                                                            <span class="elementor-button-icon elementor-align-icon-left">
                                                                                <i class="fa fa-user" aria-hidden="true"></i>
                                                                            </span>

                                                                            <span class="elementor-button-text">Поговорить с агентом</span>
                                                                        </span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- end row -->

    </div><!-- end container -->
</section>


<section class="module cta bg-display-cover">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <? if(!empty($titleProperty)): ?>
                    <h3><?= $titleProperty ?></h3>
                <? endif; ?>

                <?= $subtitleProperty ?>
            </div>

            <div class="col-lg-4 col-md-4">
                <a href="#" class="button right large js-property">Оставить заявку</a>
            </div>
        </div>
    </div>
</section>
<?= template('common/sell-property') ?>