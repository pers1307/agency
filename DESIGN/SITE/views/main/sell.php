<? if(!empty($titleProperty)): ?>
    <section class="module cta bg-display-cover">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <? if(!empty($titleProperty)): ?>
                        <h3><?= $titleProperty ?></h3>
                    <? endif; ?>

                    <?= $subtitleProperty ?>
                </div>

                <div class="col-lg-4 col-md-4">
                    <a href="#" class="button right large js-property">Оставить заявку</a>
                </div>
            </div>
        </div>
    </section>

    <?= template('common/sell-property') ?>
<? endif; ?>