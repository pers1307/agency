<section data-id="88591c6" class="elementor-element elementor-element-88591c6 elementor-section-boxed elementor-section-height-default elementor-section-height-default module module-border elementor-section elementor-top-section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}" data-element_type="section">
    <div class="elementor-container elementor-column-gap-default">
        <div class="elementor-row">
            <div data-id="5b0a2dc0" class="elementor-element elementor-element-5b0a2dc0 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                <div class="elementor-column-wrap elementor-element-populated">
                    <div class="elementor-widget-wrap">
                        <div data-id="721de754" class="elementor-element elementor-element-721de754 elementor-widget elementor-widget-shortcode" data-element_type="shortcode.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-shortcode">
                                    <div class="module-header ">
                                        <? if(!empty($titleStaff)): ?>
                                            <h2><?= $titleStaff ?></h2>
                                            <table class="widget-divider"><tr><td><div class="bar"></div></td><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>
                                        <? endif; ?>

                                        <?= $subtitleStaff ?>
                                    </div>
                            </div>
                        </div>
                        <div data-id="f33d2a6" class="elementor-element elementor-element-f33d2a6 elementor-widget elementor-widget-shortcode" data-element_type="shortcode.default">
                            <div class="elementor-widget-container">
                                <div class="elementor-shortcode">
                                    <div class="row listing">

                                        <? foreach($staff as $item): ?>
                                            <div class="col-lg-3 col-md-3 col-sm-6">
                                                <div class="agent shadow-hover">

                                                    <div class="agent-img">
                                                        <? if(!empty($item['propertyCount'])): ?>
                                                            <a href="<?= $item['itemLink'] ?>" class="button alt agent-tag">Объекты : <?= $item['propertyCount'] ?></a>
                                                        <? endif; ?>

                                                        <div class="img-fade"></div>
                                                        <a href="<?= $item['itemLink'] ?>" class="agent-img-link">
                                                            <img
                                                                    class="attachment-full size-full wp-post-image"
                                                                    width="1197"
                                                                    height="1287"
                                                                    src="<?= $item['images']['list'] ?>"
                                                                    alt=""
                                                                    srcset="<?= $item['images']['list'] ?> 1197w, <?= $item['images']['list2'] ?> 279w, <?= $item['images']['list3'] ?> 768w, <?= $item['images']['list4'] ?> 952w"
                                                                    sizes="(max-width: 1197px) 100vw, 1197px"
                                                            />
                                                        </a>
                                                    </div>

                                                    <div class="agent-content">

                                                        <div class="agent-details">
                                                            <h4>
                                                                <a href="<?= $item['itemLink'] ?>"><?= $item['title'] ?></a>
                                                            </h4>

                                                            <? if(!empty($item['position'])): ?>
                                                                <p title="<?= $item['position'] ?>">
                                                                    <i class="fa fa-tag icon "></i>
                                                                    <?= $item['position'] ?>
                                                                </p>
                                                            <? endif; ?>

                                                            <? if(!empty($item['email'])): ?>
                                                                <p title="<?= $item['email'] ?>">
                                                                    <a href="mailto:<?= $item['email'] ?>">
                                                                        <i class="fa fa-envelope icon "></i>
                                                                        <?= $item['email'] ?>
                                                                    </a>
                                                                </p>
                                                            <? endif; ?>

                                                            <? if(!empty($item['work_phone'])): ?>
                                                                <p title="<?= $item['work_phone'] ?>">
                                                                    <i class="fa fa-phone icon "></i>
                                                                    <?= $item['work_phone'] ?>
                                                                </p>
                                                            <? else: ?>
                                                                <? if(!empty($item['mobile_phone'])): ?>
                                                                    <p title="<?= $item['mobile_phone'] ?>">
                                                                        <i class="fa fa-phone icon "></i>
                                                                        <?= $item['mobile_phone'] ?>
                                                                    </p>
                                                                <? endif; ?>
                                                            <? endif; ?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        <? endforeach; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>