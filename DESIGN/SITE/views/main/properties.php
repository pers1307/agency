<? if(!empty($properties)): ?>
    <section class="elementor-element elementor-element-65640dfe elementor-section-boxed elementor-section-height-default elementor-section-height-default module elementor-section elementor-top-section" data-element_type="section">
        <div class="elementor-container elementor-column-gap-default">
            <div class="elementor-row">
                <div data-id="7adf63e4" class="elementor-element elementor-element-7adf63e4 elementor-column elementor-col-100 elementor-top-column" data-element_type="column">
                    <div class="elementor-column-wrap elementor-element-populated">
                        <div class="elementor-widget-wrap">

                            <div data-id="40b9f0ee" class="elementor-element elementor-element-40b9f0ee elementor-widget elementor-widget-shortcode" data-element_type="shortcode.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-shortcode">
                                        <div class="module-header">
                                            <? if(!empty($titleProperty)): ?>
                                                <h2><?= $titleProperty ?></h2>
                                                <table class="widget-divider"><tr><td><div class="bar"></div></td><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>
                                            <? endif; ?>

                                            <? if(!empty($subtitleProperty)): ?>
                                                <p>
                                                    <?= $subtitleProperty ?>
                                                </p>
                                            <? endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div data-id="10b90078" class="elementor-element elementor-element-10b90078 elementor-widget elementor-widget-shortcode" data-element_type="shortcode.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-shortcode">

                                        <div class="row listing">
                                            <? foreach($properties as $item): ?>
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="property shadow-hover post-23">

                                                        <div class="property-img">
                                                            <? if ($item['type_sell'] == 2): ?>
                                                                <div class="property-tag button status">
                                                                    <a href="<?= $item['itemLink'] ?>">В аренду</a>
                                                                </div>
                                                            <? endif; ?>

                                                            <? if ($item['type_sell'] == 1): ?>
                                                                <a href="<?= $item['itemLink'] ?>" class="property-tag button alt featured">Продается</a>
                                                            <? endif; ?>

                                                            <div class="property-price">
                                                                <? if ($item['type_sell'] == 1): ?>
                                                                    <?= $item['price'] ?>
                                                                <? endif; ?>

                                                                <? if ($item['type_sell'] == 2): ?>
                                                                    <?= $item['rent_price'] ?>
                                                                    <span class="price-postfix">В месяц</span>
                                                                <? endif; ?>
                                                            </div>

                                                            <div class="property-color-bar"></div>
                                                            <div class="img-fade"></div>

                                                            <a class="property-img-link" href="<?= $item['itemLink'] ?>">
                                                                <?
                                                                $imageUrl = MSFiles::getImageUrl($item['image'], 'list');
                                                                $imageAlt = MSFiles::getImageDescription($item['image'], 0);

                                                                if (empty($imageAlt)) {
                                                                    $imageAlt = $item['title'];
                                                                }

                                                                if (empty($imageUrl)) {
                                                                    $imageUrl = '/DESIGN/SITE/images/no-image/no-image_260_200.jpg';
                                                                    $imageAlt = 'Нет изображения';
                                                                }
                                                                ?>

                                                                <img
                                                                    width="800"
                                                                    height="600"
                                                                    src="<?= $imageUrl ?>"
                                                                    class="attachment-property-thumbnail size-property-thumbnail wp-post-image"
                                                                    alt="<?= $imageAlt ?>"
                                                                />
                                                            </a>
                                                        </div>

                                                        <div class="property-content">
                                                            <div class="property-title">
                                                                <h4>
                                                                    <a href="<?= $item['itemLink'] ?>"><?= $item['title'] ?></a>
                                                                </h4>

                                                                <? $coords = $item['geocoder'] ?>
                                                                <? if(!empty($coords['text'])): ?>
                                                                    <p class="property-address">
                                                                        <i class="fa fa-map-marker icon"></i>
                                                                        <?= $coords['text'] ?>
                                                                    </p>
                                                                <? endif; ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <? endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div data-id="7271ae42" class="elementor-element elementor-element-7271ae42 elementor-align-center elementor-widget elementor-widget-button" data-element_type="button.default">
                                <div class="elementor-widget-container">
                                    <div class="elementor-button-wrapper">
                                        <a href="/<?= path(178) ?>" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						                    <span class="elementor-button-content-wrapper">
                                                <span class="elementor-button-text">
                                                    Посмотреть больше объектов
                                                </span>
		                                    </span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<? endif; ?>