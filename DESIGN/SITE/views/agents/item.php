<?php
    /**
     * User: Andruha
     * Date: 25.09.13
     * Time: 17:53
     *
     * @var ModelNews $model
     * @var array $item
     */
?>

<section class="module">
    <div class="container">

        <div class="row">

            <div class="col-lg-8 col-md-8">
                <div class="agent agent-single">
                    <div class="agent-single-item agent-overview">

                        <a href="index.html" class="agent-img">
                            <img class="hex" src="/DESIGN/SITE/images/hexagon.png" alt=""/>

                            <? if(!empty($item['propertyCount'])): ?>
                                <div class="button alt button-icon agent-tag agent-assigned"><i class="fa fa-home icon "></i>Ведет объектов : <?= $item['propertyCount'] ?></div>
                            <? endif; ?>

                            <div class="img-fade"></div>
                            <img
                                width="1197"
                                height="1287"
                                src="<?= $item['images']['main'] ?>"
                                class="attachment-full size-full wp-post-image"
                                alt=""
                                srcset="<?= $item['images']['main'] ?> 1197w, <?= $item['images']['list2'] ?> 279w, <?= $item['images']['list3'] ?> 768w, <?= $item['images']['list4'] ?> 952w"
                                sizes="(max-width: 1197px) 100vw, 1197px"
                            />
                        </a>

                        <div class="agent-content">
                            <div class="agent-details">
                                <h4><?= $item['title'] ?></h4>

                                <? if(!empty($item['position'])): ?>
                                    <p><span><?= $item['position'] ?></span>
                                        <i class="fa fa-tag icon"></i>
                                        Должность:
                                    </p>
                                <? endif; ?>

                                <? if(!empty($item['email'])): ?>
                                    <p> <a href="mailto:<?= $item['email'] ?>"><?= $item['email'] ?></a>
                                        <i class="fa fa-envelope icon "></i>
                                        Email:
                                    </p>
                                <? endif; ?>

                                <? if(!empty($item['mobile_phone'])): ?>
                                    <p><a href="tel:<?= $item['mobile_phone'] ?>"><?= $item['mobile_phone'] ?></a>
                                        <i class="fa fa-phone icon"></i>
                                        Мобильный телефон:
                                    </p>
                                <? endif; ?>

                                <? if(!empty($item['work_phone'])): ?>
                                    <p><a href="tel:<?= $item['mobile_phone'] ?>"><?= $item['work_phone'] ?></a>
                                        <i class="fa fa-building icon"></i>
                                        Рабочий телефон:
                                    </p>
                                <? endif; ?>
                            </div>

                            <div class="button button-icon agent-message right">
                                <i class="fa fa-envelope icon"></i>Написать агенту
                            </div>
                        </div>

                        <div class="clear"></div>
                    </div>
                </div>

                <? if(!empty($item['description'])): ?>
                    <div class="agent-single-item property-single-item content widget agent-description">
                        <div class="module-header module-header-left">
                            <h4>Описание</h4>
                            <table class="widget-divider"><tr><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>
                            <div class="divider-fade"></div>
                        </div>
                        <?= $item['description'] ?>
                    </div>
                <? endif; ?>

                <?= template('agents/contact', ['agentId' => $item['id']]) ?>
                <?= template('agents/properties', ['items' => $item['properties']]) ?>
            </div>

            <div class="col-lg-4 col-md-4 sidebar">
                <?php require_once BLOCKS_DIR . '/widget/properties.php'; ?>
            </div>
        </div>
</section>