<a class="anchor" name="anchor-agent-contact"></a>
<div class="agent-single-item property-single-item widget agent-contact">
    <div class="module-header module-header-left">
        <h4>Написать агенту</h4>
        <table class="widget-divider"><tr><td><div class="rhex"></div></td><td><div class="bar"></div></td></tr></table>
        <div class="divider-fade"></div>
    </div>

    <form
            id="agent-contact-form"
            class="contact-form agent-contact-form js-form-agent"
            method="post"

            action="/api/callback.agent/"
            data-file="/api/order.file/"
            data-deleteFile="/api/order.deleteFile/"
    >
        <div class="contact-form-fields">
            <div>
                <input type="text" name="name" placeholder="Имя" class="border" autocomplete="off"/>
            </div>

            <div>
                <input type="email" name="email" placeholder="Email" class="border" autocomplete="off"/>
            </div>

            <div>
                <textarea name="comment" class="border" style="resize: none" placeholder="Комментарий"></textarea>
            </div>

            <div>
                <input type="text" class="address" name="agentId" value="<?= $agentId ?>">
                <input type="text" class="address" name="address">
                <input type="hidden" name="csrf" value="<?= MSCore::csrf()->getToken() ?>" />
                <input type="submit" name="submit" value="Отправить" />
            </div>
        </div>
    </form>

</div>