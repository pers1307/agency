<?php
    /**
     * User: Andruha
     * Date: 25.09.13
     * Time: 17:33
     *
     * @var ModelNews $model
     * @var array $items
     * @var MSBasePagination $pagination
     */
?>
<section class="module">
    <div class="container">

        <div class="row">
            <div class="col-lg-8 col-md-8 ">
              <div class="agency-filter">
                <? if(!empty($filter)): ?>
                  <ul>
                    <? foreach($filter as $url => $filterItem): ?>
                      <li>
                        <a href="<?= $url ?>"><?= $filterItem ?></a>
                      </li>
                    <? endforeach; ?>
                  </ul>
                <? endif; ?>
              </div>


                <div class="row listing row-flex">
                    <? foreach($items as $item): ?>
                        <? if (is_null($item['code'])): ?>
                            <? continue; ?>
                        <? endif; ?>

                        <div class="col-lg-4 col-md-4 col-sm-6">
                            <div class="agent shadow-hover">

                                <div class="agent-img">
                                    <? if(!empty($item['propertyCount'])): ?>
                                        <a href="<?= $item['itemLink'] ?>" class="button alt agent-tag">Объекты : <?= $item['propertyCount'] ?></a>
                                    <? endif; ?>

                                    <div class="img-fade"></div>
                                    <a href="<?= $item['itemLink'] ?>" class="agent-img-link">
                                        <img
                                            class="attachment-full size-full wp-post-image"
                                            width="1197"
                                            height="1287"
                                            src="<?= $item['images']['list'] ?>"
                                            alt=""
                                            srcset="<?= $item['images']['list'] ?> 1197w, <?= $item['images']['list2'] ?> 279w, <?= $item['images']['list3'] ?> 768w, <?= $item['images']['list4'] ?> 952w"
                                            sizes="(max-width: 1197px) 100vw, 1197px"
                                        />
                                    </a>
                                </div>

                                <div class="agent-content">

                                    <div class="agent-details">
                                        <h4>
                                            <a href="<?= $item['itemLink'] ?>"><?= $item['title'] ?></a>
                                        </h4>

                                        <? if(!empty($item['position'])): ?>
                                            <p title="<?= $item['position'] ?>">
                                                <i class="fa fa-tag icon "></i>
                                                <?= $item['position'] ?>
                                            </p>
                                        <? endif; ?>

                                        <? if(!empty($item['email'])): ?>
                                            <p title="<?= $item['email'] ?>">
                                                <a href="mailto:<?= $item['email'] ?>">
                                                    <i class="fa fa-envelope icon "></i>
                                                    <?= $item['email'] ?>
                                                </a>
                                            </p>
                                        <? endif; ?>

                                        <? if(!empty($item['work_phone'])): ?>
                                            <p title="<?= $item['work_phone'] ?>">
                                                <i class="fa fa-phone icon "></i>
                                                <?= $item['work_phone'] ?>
                                            </p>
                                        <? else: ?>
                                            <? if(!empty($item['mobile_phone'])): ?>
                                                <p title="<?= $item['mobile_phone'] ?>">
                                                    <i class="fa fa-phone icon "></i>
                                                    <?= $item['mobile_phone'] ?>
                                                </p>
                                            <? endif; ?>
                                        <? endif; ?>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <? endforeach; ?>
                </div>

                <?= $pagination ?>

                <br>

                <div class="content">
                    <?= $text ?>
                </div>
            </div>

            <div class="col-lg-4 col-md-4 sidebar">
                <?php require_once BLOCKS_DIR . '/widget/properties.php'; ?>
            </div>

        </div>
    </div>
</section>
