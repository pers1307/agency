<?php
    /**
     * User: Andruha
     * Date: 14.10.13
     * Time: 16:27
     *
     * @var ModelCatalog $Catalog
     */

    /** @var ModelServices $Catalog */
    $Catalog = MSCore::site()->services;

    $vars = MSCore::urls()->vars;
    $firstSegment = reset($vars);
    $lastSegment = end($vars);
    $currentUrl = preg_replace('/\/page\d+$/suiU','',MSCore::urls()->current);

    if($lastSegment && preg_match('/^page([0-9]+)$/', $lastSegment, $match)) {
        $Catalog->currentPage = $match[1];
        array_pop($vars);
    }

	$fullTree = $Catalog->fullTree;

    if ($Catalog->isCatalogIndexPage) {
        Page404();
    } else {
        $article = $Catalog->getArticle(array('*'), array(
            'fields' => array('*'),
            'recursive' => true
        ));

        if ($Catalog->isItem === false) {

            if ($article !== false) {
                $items = $Catalog->getItems();

                foreach ($article['items'] as &$item) {
                    $item['image'] = MSFiles::getFilePath($item['image']);
                }

                $article['mainArticles'] =
                    $Catalog->getArticlesByParent(0);

	            //Меняем порядок сортировки для сайд-меню в разделах
	            $columns = [];
	            foreach($article['mainArticles'] as $k=>$v) {
		            $columns = array_column($article['mainArticles'], 'order');
		            $result = array_multisort($columns, SORT_STRING, $article['mainArticles']);
	            }

	            //Создаем ярлыки для корточек услуг
	            foreach($article['items'] as $k=>&$item) {
		            foreach($fullTree as $k2=>$treeItem) {
			            if($item['parent'] == $treeItem['id']) {
				            $item['ankor'] = $treeItem['name'];
			            }
			            foreach($treeItem['children'] as $k3=>$child) {
				            if($item['parent'] == $child['id']) {
					            $item['ankor'] = $child['name'];
				            }
			            }
		            }
	            }

	            echo template('services/article', array(
		            'model'        => $Catalog,
		            'mainArticles' => $Catalog->fullTree,
		            'article'      => $article['article'],
		            'items'        => $article['items'],
		            'pagination'   => $article['pagination'],
		            'fullTree'     => $fullTree,
	            ));

                MSCore::page()->header = $article['article']['name'];

                if (!empty($article['article']['h1'])) {
                    MSCore::page()->header = $article['article']['h1'];
                }

                if (!empty($article['article']['title_page'])) {
                    MSCore::page()->title_page = $article['article']['title_page'];
                }

                if (!empty($article['article']['meta_description'])) {
                    MSCore::page()->meta_description = $article['article']['meta_description'];
                }

                if (!empty($article['article']['meta_keywords'])) {
                    MSCore::page()->meta_keywords = $article['article']['meta_keywords'];
                }
            } else {
                Page404();
            }
        } else {
            if (($item = $Catalog->getItem()) !== false) {
                $item['item']['image'] = MSFiles::getFilePath($item['item']['image']);

                $item['currentArticles'] =
                    $Catalog->getArticlesByParent($article['article']['parent']);

                if (isset($item['currentArticles'][0])) {
                    $item['parentCurrentArticles'] =
                        $Catalog->getArticleById($item['currentArticles'][0]['parent']);
                }

                $item['mainArticles'] =
                    $Catalog->getArticlesByParent(0);

	            //Узнаем ссылку перехода на карточке товара
                $uri = '';
	            if(getParam(1)) {
		            $uri = explode('/',trim($_SERVER['REQUEST_URI'],'/'));
		            unset($uri[2]);
		            $uri = '/'.implode('/', $uri).'/';
	            }

	            echo template('services/item', array(
		            'model'        => $Catalog,
		            'mainArticles' => $Catalog->fullTree,
		            'item'         => $item['item'],
		            'uri'          => $uri
	            ));

                MSCore::page()->header = $item['item']['title'];

                if (!empty($item['item']['h1'])) {
                    MSCore::page()->header = $item['item']['h1'];
                }

                if (!empty($item['item']['title_page'])) {
                    MSCore::page()->title_page = $item['item']['title_page'];
                }

                if (!empty($item['item']['meta_description'])) {
                    MSCore::page()->meta_description = $item['item']['meta_description'];
                }

                if (!empty($item['item']['meta_keywords'])) {
                    MSCore::page()->meta_keywords = $item['item']['meta_keywords'];
                }
            } else {
                Page404();
            }
        }
    }