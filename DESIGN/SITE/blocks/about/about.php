<?php

/** @var ModelAbout $modelAbout */
use pers1307\helpers\StringCutHelper;

$modelAbout = MSCore::site()->about;

$title            = $modelAbout->getValueByKey('title');
$text             = $modelAbout->getValueByKey('text');
$titleStats       = $modelAbout->getValueByKey('title_stats');
$subtitleStats    = $modelAbout->getValueByKey('subtitle_stats');
$params           = $modelAbout->getValueByKey('params');
$titleStaff       = $modelAbout->getValueByKey('title_staff');
$subtitleStaff    = $modelAbout->getValueByKey('subtitle_staff');
$staff            = $modelAbout->getValueByKey('staff');
$titleText        = $modelAbout->getValueByKey('title_text');
$subtitleText     = $modelAbout->getValueByKey('subtitle_text');
$titleProperty    = $modelAbout->getValueByKey('title_property');
$subtitleProperty = $modelAbout->getValueByKey('subtitle_property');

$params = unserialize($params);

/** @var ModelAgents $modelAgents */
$modelAgents = MSCore::site()->agents;
$staff = $modelAgents->getByIds($staff);

$stringCutTitle = new StringCutHelper();
$stringCutTitle->setMaxLenght(100);
$title = $stringCutTitle->cutString($title);

foreach ($staff as &$staffItem) {
    $staffItem['description'] = strip_tags($staffItem['description']);
    $staffItem['description'] = $stringCutTitle->cutString($staffItem['description']);
}

echo template('about/about', [
    'title'            => $title,
    'text'             => $text,
    'params'           => $params,
    'titleStats'       => $titleStats,
    'subtitleStats'    => $subtitleStats,
    'titleStaff'       => $titleStaff,
    'subtitleStaff'    => $subtitleStaff,
    'staff'            => $staff,
    'titleText'        => $titleText,
    'subtitleText'     => $subtitleText,
    'titleProperty'    => $titleProperty,
    'subtitleProperty' => $subtitleProperty,
]);