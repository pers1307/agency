<?php
/**
 * User: Andruha
 * Date: 18.03.13
 * Time: 18:11
 */

use pers1307\helpers\ColumnsHelper;

if (getParam(1))
{
    Page404();
}

$modelProperty = MSCore::site()->property;
$modelProperty->currentPage = getParam(0);
$columnsHelper = new ColumnsHelper();
$columnsHelper->setColumns(2);

if (false !== $item = $modelProperty->getItem(getParam(0)))
{
    if (empty($item))
    {
        Page404();
    }

    /** @var ModelAgents $modelAgents */
    $modelAgents = MSCore::site()->agents;
    $item['item']['agent'] = $modelAgents->getByPropertyId($item['item']['id']);

    $item['item']['properties'] = $modelProperty->getItemsByIds(
        $item['item']['properties']
    );
    $item['item']['properties'] = $columnsHelper->horizontalForTable(
        $item['item']['properties']
    );

    $item['item']['gallery'] = unserialize($item['item']['gallery']);

    echo template('properties/item', $item);

    MSCore::page()->header = $item['item']['title'];

    if (!empty($item['item']['h1'])) {
        MSCore::page()->header = $item['item']['h1'];
    }

    if (!empty($item['item']['title_page'])) {
        MSCore::page()->title_page = $item['item']['title_page'];
    }

    if (!empty($item['item']['meta_description'])) {
        MSCore::page()->meta_description = $item['item']['meta_description'];
    }

    if (!empty($item['item']['meta_keywords'])) {
        MSCore::page()->meta_keywords = $item['item']['meta_keywords'];
    }
}
elseif(!$modelProperty->getCurrentPage() && !empty(MSCore::urls()->vars[0])){
    Page404();
} else {
    $items = $modelProperty->getItems(
        ['id', 'code', 'title', 'type_sell', 'price', 'rent_price', 'image', 'geocoder']
    );

    $items['items'] = $columnsHelper->horizontalForTable($items['items']);

    $text = MSCore::page()->getZoneContent();
    $currentPage = $items['pagination']->getCurrentPage();

    if ($currentPage != 1) {
        $text = '';
    }

    echo template('properties/items', $items + [
            'text' => $text
        ]
    );
}