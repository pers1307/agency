<?php

/** @var ModelMain $modelMain */
$modelMain = MSCore::site()->main;

$titleProperty    = $modelMain->getValueByKey('title_property');
$subtitleProperty = $modelMain->getValueByKey('subtitle_property');

echo template('main/sell', [
    'titleProperty'    => $titleProperty,
    'subtitleProperty' => $subtitleProperty,
]);