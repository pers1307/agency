<?php

/** @var ModelMain $modelMain */
$modelMain = MSCore::site()->main;

$titleProperty    = $modelMain->getValueByKey('title_properties');
$subtitleProperty = $modelMain->getValueByKey('subtitle_properties');
$properties       = $modelMain->getValueByKey('properties');

/** @var ModelProperty $modelProperty */
$modelProperty = MSCore::site()->property;

if (!empty($properties)) {
    $properties = $modelProperty->getItemsByIds($properties);
} else {
    $properties = $modelProperty->getLastItems();
}


echo template('main/properties', [
    'titleProperty'    => $titleProperty,
    'subtitleProperty' => $subtitleProperty,
    'properties'       => $properties,
]);