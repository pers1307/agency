<?php

/** @var ModelMain $modelMain */
$modelMain = MSCore::site()->main;

$titleStaff       = $modelMain->getValueByKey('title_staff');
$subtitleStaff    = $modelMain->getValueByKey('subtitle_staff');
$staff            = $modelMain->getValueByKey('staff');

/** @var ModelAgents $modelAgents */
$modelAgents = MSCore::site()->agents;
$staff = $modelAgents->getByIds($staff);

echo template('main/agents', [
    'titleStaff'       => $titleStaff,
    'subtitleStaff'    => $subtitleStaff,
    'staff'            => $staff,
]);