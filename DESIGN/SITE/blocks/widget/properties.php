<?php

/** @var ModelProperty $modelProperty */
$modelProperty = MSCore::site()->property;
$items = $modelProperty->getLastItems();

echo template('widget/properties', ['items' => $items]);