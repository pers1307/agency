<?php
/**
 * @var MSPage $this
 */

if(isset($_SERVER['HTTP_USER_AGENT'])) {

    if (mb_strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6') || mb_strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 7')) {
        ob_start();
        ?>
        <style type="text/css">
            body, html {
                padding: 0;
                margin: 0;
            }

            #ie_overlay {
                position: absolute;
                width: 100%;
                /*
                height:100%;
                */
                height: 1500px;
                z-index: 100;
                left: 0;
                background: #e30613;
                filter: alpha(opacity=50);
                opacity: 0.5
            }

            #ie_window {
                background: url(/DESIGN/SITE/images/w_ie_bg.jpg);
                z-index: 10000000;
                color: #fff;
                font-family: 'Trebuchet MS', Arial, Verdana, serif;
                font-size: 12px;
                position: relative;
            }

            #ie_window .shadow {
                background: url(/DESIGN/SITE/images/w_ie_shadow.jpg) repeat-x;
                height: 10px;
                font-size: 1px;
            }

            #ie_window a, #ie_window a:visited {
                color: #0c7bc4;
                text-decoration: underline;
            }

            #ie_window a:hover {
                color: #fc2e38;
            }

            #ie_window .cover {
                margin: 0 auto;
                width: 960px;
                position: relative;
                padding-top: 30px;
            }

            #ie_window .logo {
                width: 200px;
                padding-top: 12px;
            }

            #ie_window .close {
                display: block;
                width: 30px;
                height: 30px;
                background: #29292a url(/DESIGN/SITE/images/w_ie_close.jpg) no-repeat 0 0;
                position: absolute;
                right: 0;
                top: 20px;
                cursor: pointer;
            }

            #ie_window .close:hover {
                background-position: 0 100%;
            }

            #ie_window .text1 {
                font-size: 30px;
            }

            #ie_window .text2 {
                font-size: 22px;
                padding: 1px 0 11px 0;
            }

            #ie_window .brws {
                clear: both;
            }

            #ie_window .brws a {
                color: #fff;
                font-size: 16px;
                text-decoration: none;
            }

            #ie_window .brws img {
                margin-bottom: -2px;
                border: none;
            }

            #ie_window .brws a:hover {
                text-decoration: underline;
                color: #fff;
            }

            #ie_window .brws .br {
                padding: 25px 0;
                width: 160px;
                text-align: center;
                margin-right: 40px;
            }

            #ie_window .brws .br-last {
                margin: 0;
            }

            #ie_window .f-left {
                float: left;
            }
        </style>

        <div id="ie_window">
            <div class="cover">
                <a href="#" class="close"></a>

                <div class="f-left logo"><img src="/DESIGN/SITE/images/w_ie_logo.jpg" alt=""/></div>

                <div class="f-left">
                    <div class="text1">Ваш браузер устарел</div>
                    <div class="text2">Предлагаем установить один из современных бесплатных браузеров</div>
                    <a href="http://ru.wikipedia.org/wiki/%D0%91%D1%80%D0%B0%D1%83%D0%B7%D0%B5%D1%80">Что такое
                        браузер?</a>
                </div>

                <div class="brws">
                    <div class="br f-left">
                        <a href="http://www.mozilla.com/ru/firefox/">
                            <img src="/DESIGN/SITE/images/browser2.jpg" alt=""><br>Mozilla Firefox
                        </a>
                    </div>

                    <div class="br f-left">
                        <a href="https://www.google.com/intl/ru/chrome/browser/">
                            <img src="/DESIGN/SITE/images/browser1.jpg" alt=""><br>Google Chrome
                        </a>
                    </div>

                    <div class="br f-left">
                        <a href="http://www.opera.com/browser/">
                            <img src="/DESIGN/SITE/images/browser3.jpg" alt=""><br>Opera
                        </a>
                    </div>

                    <div class="br f-left">
                        <a href="http://www.apple.com/ru/safari/">
                            <img src="/DESIGN/SITE/images/browser4.jpg" alt=""><br>Apple Safari
                        </a>
                    </div>

                    <div class="br f-left br-last">
                        <a href="http://windows.microsoft.com/ru-ru/internet-explorer/download-ie">
                            <img src="/DESIGN/SITE/images/browser5.jpg" alt=""><br>Internet Explorer
                        </a>
                    </div>
                </div>

                <div style="clear:both"></div>
            </div>

            <div class="shadow">&nbsp;</div>
        </div>

        <div id="ie_overlay"></div>

        <script type="text/javascript">
            window.jQuery || document.write('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"><\/script>');
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#ie_overlay, #ie_window .close').click(function () {
                    $('#ie_window').remove();
                    $('#ie_overlay').remove();
                });
            });

        </script>
        <?php

        $this->_html = preg_replace('/<body(.*?)>/', '<body$1>' . ob_get_clean(), $this->_html);
    }
}