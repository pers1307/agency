<?php
    /**
     * User: Andruha
     * Date: 18.03.13
     * Time: 18:11
     */

    if (getParam(1))
    {
        Page404();
    }

    $NewsSettings = MSCore::settings('news', MSCore::page()->path_id);

    $News = new ModelNews(array(
        'pathId' => MSCore::page()->path_id,
        'path' => '/' . MSCore::page()->path,
        'order' => '`date` DESC',
        'pagination' => array(
            'onPage' => $NewsSettings->onPage,
            'linkPref' => 'page'
        )
    ));

    $News->currentPage = getParam(0);

    if (false !== $item = $News->getItem(getParam(0)))
    {
        if (empty($item))
        {
            Page404();
        }

        echo template('news/item', $item);

        MSCore::page()->header = $item['item']['title'];

        if (!empty($item['item']['h1'])) {
            MSCore::page()->header = $item['item']['h1'];
        }

        if (!empty($item['item']['title_page'])) {
            MSCore::page()->title_page = $item['item']['title_page'];
        }

        if (!empty($item['item']['meta_description'])) {
            MSCore::page()->meta_description = $item['item']['meta_description'];
        }

        if (!empty($item['item']['meta_keywords'])) {
            MSCore::page()->meta_keywords = $item['item']['meta_keywords'];
        }
    }
    elseif(!$News->getCurrentPage() && !empty(MSCore::urls()->vars[0])){
        Page404();
    }
    else
    {
        $items = $News->getItems(
            array('id', 'code', 'title', 'image', 'announce', 'date')
        );

        echo template('news/items', $items);

    }