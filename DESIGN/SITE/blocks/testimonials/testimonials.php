<?php

$testimonialsSettings = MSCore::settings('testimonials', MSCore::page()->path_id);
$title = $testimonialsSettings->title;
$subtitle = $testimonialsSettings->subtitle;

echo template('testimonials/testimonials',
    [
        'testimonials' => (new ModelTestimonials())->getAll(),
        'title'        => $title,
        'subtitle'     => $subtitle
    ]
);