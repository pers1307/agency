<?php

    $files = array(
        'system/empty.php' => array('Пустой блок'),
        'common/text.php' => array('Текстовое содержание'),
        'news/news.php' => array('Новости', 'news'),
        'catalog/catalog.php' => array('Каталог', 'catalog'),
        'agents/agents.php' => array('Сотрудники', 'agents'),
        'contact/contact.php' => array('Контакты', 'contact'),
        'about/about.php' => array('О компании', 'about'),
        'services/services.php' => array('Услуги', 'services'),
    );