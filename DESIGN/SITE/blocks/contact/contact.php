<?php

$phone      = MSCore::settings()->getValue('phone');
$email      = MSCore::settings()->getValue('email');
$address    = MSCore::settings()->getValue('address');
$addressMap = MSCore::settings()->getValue('address_map');

$text = MSCore::page()->getZoneContent();

$query = new MSTable('{social_network}');
$query->setFields(['*']);
$query->setFilter('`active`=1');
$query->setOrder('`order` ASC');
$networks = $query->getItems();

MSCore::page()->js = 'site-contact-scripts';
addFooterScriptsObject('coodinates', json_decode($addressMap));

echo template('contact/contact', [
    'phone'       => $phone,
    'email'       => $email,
    'text'        => $text,
    'address'     => $address,
    'addressMap'  => $addressMap,
    'networks'    => $networks
]);