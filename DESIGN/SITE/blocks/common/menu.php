<?php
/**
 * menu.php
 * Контроллер для отображения меню
 *
 * @author      Pereskokov Yurii
 * @copyright   2015 Pereskokov Yurii
 * @license     Mediasite LLC
 * @link        http://www.mediasite.ru/
 */

/**
 * Функция возвращает содержимое меню
 *
 * @return array
 */
function getMenu()
{
    $menu = ModelMenu::getMainMenu(1, null, 2, 'order');

    return $menu;
}

$menu = getMenu();

$uslugi = MSCore::db()->getAll("
	SELECT `name`, `code` FROM `mp_services_articles` 
	WHERE `parent` = 0 
	ORDER BY `order`
");

echo template('common/menu',
    [
        'menu'   => $menu,
	    'uslugi' => $uslugi,
    ]
);