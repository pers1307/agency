<?php

$footerText     = MSCore::settings()->getValue('footer_text');
$footerContacts = MSCore::settings()->getValue('footer_contacts');

echo template('common/footer', [
    'footerText'     => $footerText,
    'footerContacts' => $footerContacts
]);