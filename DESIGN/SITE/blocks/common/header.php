<?php

$phone = MSCore::settings()->getValue('phone');
$email = MSCore::settings()->getValue('email');
$phoneViber   = MSCore::settings()->getValue('phone_viber');
$phoneWhatsUp = MSCore::settings()->getValue('phone_whatsup');

echo template('common/header', [
    'phone'        => $phone,
    'phoneViber'   => $phoneViber,
    'phoneWhatsUp' => $phoneWhatsUp,
    'email'        => $email,
]);