<?php
/**
 * slider.php
 * Контроллер для отображения Слайдера на главной
 *
 * @author      Pereskokov Yurii
 * @copyright   2015 Pereskokov Yurii
 * @license     Mediasite LLC
 * @link        http://www.mediasite.ru/
 */

$query = new MSTable('{social_network}');
$query->setFields(['*']);
$query->setFilter('`active`=1');
$query->setOrder('`order` ASC');

$networks = $query->getItems();

echo template('common/social-network', ['networks' => $networks]);