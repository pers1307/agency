<?php
/**
 * User: Andruha
 * Date: 18.03.13
 * Time: 18:11
 */

use pers1307\helpers\ColumnsHelper;

if (getParam(1)) {
    Page404();
}

/** @var ModelAgents $modelAgent */
$modelAgent = MSCore::site()->agents;
$modelAgent->currentPage = getParam(0);
/** @var ModelProperty $modelProperty */
$modelProperty = MSCore::site()->property;

$columnsHelper = new ColumnsHelper();
$columnsHelper->setColumns(2);

if (false !== $item = $modelAgent->getItem(getParam(0)))
{
    if (empty($item)) {
        Page404();
    }

    if (!empty($item['item']['properties']) && !is_null($item['item']['properties'])) {
        $item['item']['propertyCount'] = count(
            explode(',', $item['item']['properties'])
        );
    } else {
        $item['item']['propertyCount'] = 0;
    }

    $images = unserialize($item['item']['image']);

    $item['item']['images']['main']  = !empty($images[0]['path']['main'])  ? $images[0]['path']['list']  : '/DESIGN/SITE/images/no-image/no-image_260_280.jpg';
    $item['item']['images']['list2'] = !empty($images[0]['path']['list2']) ? $images[0]['path']['list2'] : '/DESIGN/SITE/images/no-image/no-image_280_300.jpg';
    $item['item']['images']['list3'] = !empty($images[0]['path']['list3']) ? $images[0]['path']['list3'] : '/DESIGN/SITE/images/no-image/no-image_770_830.jpg';
    $item['item']['images']['list4'] = !empty($images[0]['path']['list4']) ? $images[0]['path']['list4'] : '/DESIGN/SITE/images/no-image/no-image_950_1024.jpg';

    $item['item']['properties'] = $modelProperty->getItemsByIds(
        $item['item']['properties']
    );
    $item['item']['properties'] = $columnsHelper->horizontalForTable(
        $item['item']['properties']
    );

    echo template('agents/item', $item);

    MSCore::page()->header = $item['item']['title'];

    if (!empty($item['item']['h1'])) {
        MSCore::page()->header = $item['item']['h1'];
    }

    if (!empty($item['item']['title_page'])) {
        MSCore::page()->title_page = $item['item']['title_page'];
    }

    if (!empty($item['item']['meta_description'])) {
        MSCore::page()->meta_description = $item['item']['meta_description'];
    }

    if (!empty($item['item']['meta_keywords'])) {
        MSCore::page()->meta_keywords = $item['item']['meta_keywords'];
    }
}
elseif(!$modelAgent->getCurrentPage() && !empty(MSCore::urls()->vars[0])) {
    Page404();
} else {
    $filter = -1;

    if (isset($_GET['filter'])) {
        $filter = $_GET['filter'];
    }

    $items = $modelAgent->getItems(
        ['id', 'code', 'title', 'image', 'position', 'email', 'work_phone', 'mobile_phone', 'properties'],
        $filter
    );

    foreach ($items['items'] as &$item) {
        if (!empty($item['properties']) && !is_null($item['properties'])) {
            $item['propertyCount'] = count(
                explode(',', $item['properties'])
            );
        } else {
            $item['propertyCount'] = 0;
        }

        $images = unserialize($item['image']);

        $item['images']['list']  = !empty($images[0]['path']['list'])  ? $images[0]['path']['list']  : '/DESIGN/SITE/images/no-image/no-image_230_250.jpg';
        $item['images']['list2'] = !empty($images[0]['path']['list2']) ? $images[0]['path']['list2'] : '/DESIGN/SITE/images/no-image/no-image_280_300.jpg';
        $item['images']['list3'] = !empty($images[0]['path']['list3']) ? $images[0]['path']['list3'] : '/DESIGN/SITE/images/no-image/no-image_770_830.jpg';
        $item['images']['list4'] = !empty($images[0]['path']['list4']) ? $images[0]['path']['list4'] : '/DESIGN/SITE/images/no-image/no-image_950_1024.jpg';
    }

    $text = MSCore::page()->getZoneContent();
    $currentPage = $items['pagination']->getCurrentPage();

    if (isset($_GET['filter'])) {
        $filter = $_GET['filter'];
        $items['pagination']->linkMask .= '?filter=' . $filter;
    }

    if ($currentPage != 1) {
        $text = '';
    }
    $items['filter'] = $modelAgent->getFilter();

    echo template('agents/items', $items + [
            'text' => $text
        ]
    );
}