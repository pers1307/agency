<?php
    /**
     * User: Andruha
     * Date: 14.10.13
     * Time: 16:27
     *
     * @var ModelCatalog $Catalog
     */

    $Catalog = MSCore::site()->catalog;

    $vars = MSCore::urls()->vars;
    $firstSegment = reset($vars);
    $lastSegment = end($vars);
    $currentUrl = preg_replace('/\/page\d+$/suiU','',MSCore::urls()->current);

    if($lastSegment && preg_match('/^page([0-9]+)$/', $lastSegment, $match)) {
        $Catalog->currentPage = $match[1];
        array_pop($vars);
    }

    if ($Catalog->isCatalogIndexPage)
    {
        $items = $Catalog->getItems(['*'], false);

        echo template('catalog/index', array(
            'model'    => $Catalog,
            'articles' => $Catalog->fullTree,
            'items'    => $items,
        ));
    }
    else
    {
        $article = $Catalog->getArticle(array('*'), array(
            'fields' => array('*'),
            'recursive' => true
        ));

        if ($Catalog->isItem === false)
        {
            if ($article !== false)
            {
                $items = $Catalog->getItems();

                echo template('catalog/article', $article);

                MSCore::page()->header = $article['article']['name'];

                if (!empty($article['article']['h1'])) {
                    MSCore::page()->header = $article['article']['h1'];
                }

                if (!empty($article['article']['title_page'])) {
                    MSCore::page()->title_page = $article['article']['title_page'];
                }

                if (!empty($article['article']['meta_description'])) {
                    MSCore::page()->meta_description = $article['article']['meta_description'];
                }

                if (!empty($article['article']['meta_keywords'])) {
                    MSCore::page()->meta_keywords = $article['article']['meta_keywords'];
                }
            }
            else
            {
                Page404();
            }
        }
        else
        {
            if (($item = $Catalog->getItem()) !== false)
            {
                echo template('catalog/item', $item);

                MSCore::page()->header = $item['item']['title'];

                if (!empty($item['item']['h1'])) {
                    MSCore::page()->header = $item['item']['h1'];
                }

                if (!empty($item['item']['title_page'])) {
                    MSCore::page()->title_page = $item['item']['title_page'];
                }

                if (!empty($item['item']['meta_description'])) {
                    MSCore::page()->meta_description = $item['item']['meta_description'];
                }

                if (!empty($item['item']['meta_keywords'])) {
                    MSCore::page()->meta_keywords = $item['item']['meta_keywords'];
                }
            }
            else
            {
                Page404();
            }
        }
    }