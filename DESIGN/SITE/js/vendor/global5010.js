jQuery(document).ready(function($) {
  $('.main-menu-wrap span').on('click', function  () {
      if($(this).next('ul').length){
        $(this).parent().toggleClass('open-drop')
      }
  })

"use strict";

  $('p').each(function  () {
    if($(this).text() === "[#READMORE#]"){
      $(this).hide();
      $(this).nextAll().wrapAll('<div class="drop-text"></div>')
      $('.drop-text').after('<a class="button small alt button-icon js-toggle-text">Читать далее</a>')
    }
  })

  $('.js-toggle-text').on('click', function  () {
      $(this).toggleClass('open').prev().slideToggle()
    $(this).text($(this).hasClass('open') ? "Скрыть": "Читать далее")
  })


	/***************************************************************************/
	//MAIN MENU SUB MENU TOGGLE
	/***************************************************************************/
	$('.nav.navbar-nav > li.menu-item-has-children > a').on('click', function(event){
		event.preventDefault();
		$(this).parent().find('.sub-menu').toggle();
		$(this).parent().find('.sub-menu li .sub-menu').hide();
	});

	$('.nav.navbar-nav li .sub-menu li.menu-item-has-children > a ').on('click', function(event){
		event.preventDefault();
		$(this).parent().find('.sub-menu').toggle();
	});

	/***************************************************************************/
	//FIXED HEADER
	/***************************************************************************/
	var navToggle = $('.header-default.navbar-fixed .navbar-toggle');
	var mainMenuWrap = $('.header-default.navbar-fixed .main-menu-wrap');
    var stickPoint = $('.header-default.navbar-fixed').outerHeight();
	
	if ($(window).scrollTop() > stickPoint) { 
		navToggle.addClass('fixed'); 
		mainMenuWrap.addClass('fixed');
	}

	$(window).bind('scroll', function () {
		if ($(window).scrollTop() > stickPoint) {
		    navToggle.addClass('fixed');
		    mainMenuWrap.addClass('fixed');
		} else {
		    navToggle.removeClass('fixed');
		    mainMenuWrap.removeClass('fixed');
		}
	});

    /***************************************************************************/
    //HEADER ICONS TOGGLE
    /***************************************************************************/
    if(window.outerWidth < 589) {
        $('.header-default .header-item .header-item-icon').click(function(e) {
            var headerText = $(this).parent().find('.header-item-text, .header-search-form');
            if(headerText.is(":visible")) {
                $('.header-default .header-item .header-item-text, .header-default .header-item .header-search-form').hide();
                headerText.slideUp('fast');
            } else {
                $('.header-default .header-item .header-item-text, .header-default .header-item .header-search-form').hide();
                headerText.slideDown('fast');
            }
        }); 
    }

    /***************************************************************************/
    //ADJUST LINE ICON SIZE
    /***************************************************************************/
    $('.icon-line').each(function() {
        var iconLineSize = $(this).css('font-size');
        iconLineSize = parseInt(iconLineSize, 10);
        iconLineSize = iconLineSize + 2;
        $(this).css('font-size', iconLineSize);
    });

    /***************************************************************************/
    //TABS
    /***************************************************************************/
    $( function() {
        $( ".tabs" ).tabs({
            create: function(event, ui) { 
                $(this).fadeIn(); 
            }
        });
    });

    /***************************************************************************/
    //ACCORDIONS
    /***************************************************************************/
    $(function() {
        $( "#accordion" ).accordion({
            heightStyle: "content",
            closedSign: '<i class="fa fa-minus"></i>',
            openedSign: '<i class="fa fa-plus"></i>'
        });
    });

    /***************************************************************************/
    //ACTIVATE CHOSEN 
    /***************************************************************************/
    $("select").chosen({disable_search_threshold: 11});

	/******************************************************************************/
	/** MULTI PAGE FORM  **/
	/******************************************************************************/
	$('.multi-page-form .multi-page-form-progress-item').click(function(e) {
        e.preventDefault();
		var currentID = $(this).attr('href');
        currentID = currentID.substring(1, currentID.length);

        $('.multi-page-form .multi-page-form-progress-item').removeClass('active');
        $(this).addClass('active');

        $( '.multi-page-form-content' ).each(function( index ) {
            if($(this).attr('id') == currentID) {
                $( '.multi-page-form-content' ).removeClass('active');
                $(this).addClass('active');
            }
        });

        var active = $(this).parent().parent().find('.multi-page-form-content.active');
        if(active.next('.multi-page-form-content').length <= 0) {
            $(this).parent().parent().find('.form-next').hide();
            $(this).parent().parent().find('input[type="submit"]').show();
        } else {
            $(this).parent().parent().find('.form-next').show();
            $(this).parent().parent().find('input[type="submit"]').hide();
        }

        if(active.prev('.multi-page-form-content').length > 0) {
            $(this).parent().parent().find('.disabled').hide();
            $(this).parent().parent().find('.form-prev').css('display', 'inline-block');
        } else {
            $(this).parent().parent().find('.disabled').show();
            $(this).parent().parent().find('.form-prev').hide();
        }
	});

    $('.multi-page-form .form-next').click(function() {
    	
    	//validate required fields
		var errors = [];
		$('.multi-page-form').find('.error').remove();
		$(".multi-page-form .multi-page-form-content.active input.required" ).each(function( index ) {
			if(!$(this).val()) {
				$(this).parent().prepend('<span class="error"> '+rypecore_local_script.required_field+'</span>');
				errors.push(index);
			}
		});

		//if no errors, go to next section
		if (errors.length === 0) {
			var active = $(this).parent().parent().find('.multi-page-form-content.active');
			var activeStep = $(this).parent().parent().find('.multi-page-form-progress-item.active');

			$(this).parent().find('.disabled').hide();
			$(this).parent().find('.form-prev').css('display', 'inline-block');

			if(active.next('.multi-page-form-content').next('.multi-page-form-content').length > 0) {
				active.removeClass('active');
				active.next().addClass('active');
				activeStep.removeClass('active');
				activeStep.next().addClass('active');
			} else {
				active.removeClass('active');
				active.next().addClass('active');
				activeStep.removeClass('active');
				activeStep.next().addClass('active');
				$(this).hide();
			 	$(this).parent().find('input[type="submit"]').show();
			}
		}

    });

    $('.multi-page-form .form-prev').click(function() {
    	var active = $(this).parent().parent().find('.multi-page-form-content.active');
    	var activeStep = $(this).parent().parent().find('.multi-page-form-progress-item.active');

    	$(this).parent().find('input[type="submit"]').hide();
		$(this).parent().find('.form-next').show();

    	if(active.prev('.multi-page-form-content').prev('.multi-page-form-content').length > 0) {
		    active.removeClass('active');
			active.prev().addClass('active');
			activeStep.removeClass('active');
			activeStep.prev().addClass('active');
		}
		else {
			active.removeClass('active');
			active.prev().addClass('active');
			activeStep.removeClass('active');
			activeStep.prev().addClass('active');
		 	$(this).hide();
		 	$(this).parent().find('.disabled').show();
		}
    });

    /******************************************************************************/
    /** SUBMIT PROPERTY - ADD LOCATION **/
    /******************************************************************************/
    $('.property-location-new-toggle').click(function(e) {
        e.preventDefault();
        $(this).parent().find('.property-location-new-content').slideToggle('fast');
    });

    $('.property-location-new-content .button').click(function(e) {
        e.preventDefault();
        var newLocation = $(this).parent().find('input').val();
        $('.user-submit-property-form #property-location').append($("<option selected></option>").attr('value', newLocation).text(newLocation)); 
        $('.user-submit-property-form #property-location').trigger("chosen:updated");
        $(this).parent().find('input').val('');
        $(this).parent().slideUp('fast');
    });

    /******************************************************************************/
	/** SUBMIT PROPERTY - ADDITIONAL IMAGES  **/
	/******************************************************************************/
	var files_count = $('.additional-img-container .additional-image').length + 1;
    $('.add-additional-img').click(function() {
        files_count++;
        $('.additional-img-container').append('<table><tr><td><div class="media-uploader-additional-img"><input type="file" class="additional_img" name="additional_img'+ files_count +'" value="" /><span class="delete-additional-img appended right"><i class="fa fa-trash"></i> '+ rypecore_local_script.delete_text +'</span></div></td></tr></table>');
    });

    $('.additional-img-container').on("click", ".delete-additional-img", function() {
        $(this).parent().parent().parent().parent().parent().remove();
    });

    /********************************************/
    /* SUBMIT PROPERTY - FLOOR PLANS */
    /********************************************/
    $('.property-floor-plans').on('click', '.add-floor-plan', function() {
    
        var count = $('.property-floor-plans #accordion .floor-plan-item').length;

        var floorPlanItem = '\
            <h3 class="accordion-tab"><span class="floor-plan-title-mirror">'+ rypecore_local_script.new_floor_plan +'</span> <span class="delete-floor-plan right"><i class="fa fa-trash"></i> '+ rypecore_local_script.delete_text +'</span></h3> \
            <div class="floor-plan-item"> \
                <div class="floor-plan-left"> \
                    <label>'+ rypecore_local_script.floor_plan_title +' </label> <input class="floor-plan-title border" type="text" name="rypecore_floor_plans['+count+'][title]" placeholder="'+ rypecore_local_script.new_floor_plan +'" /><br/> \
                    <label>'+ rypecore_local_script.floor_plan_size +' </label> <input class="border" type="text" name="rypecore_floor_plans['+count+'][size]" /><br/> \
                    <label>'+ rypecore_local_script.floor_plan_rooms +' </label> <input class="border" type="number" name="rypecore_floor_plans['+count+'][rooms]" /><br/> \
                    <label>'+ rypecore_local_script.floor_plan_bathrooms +' </label> <input class="border" type="number" name="rypecore_floor_plans['+count+'][baths]" /><br/> \
                </div> \
                <div class="floor-plan-right"> \
                    <label>'+ rypecore_local_script.floor_plan_description +' </label> \
                    <textarea class="border" name="rypecore_floor_plans['+count+'][description]"></textarea> \
                    <div> \
                        <label>'+ rypecore_local_script.floor_plan_img +' </label> \
                        <input class="border" type="text" name="rypecore_floor_plans['+count+'][img]" /> \
                        <span><em>'+rypecore_local_script.floor_plan_note+'</em></span> \
                    </div> \
                </div> \
                <div class="clear"></div> \
            </div> \
        ';
    
        $(this).parent().find('#accordion').append(floorPlanItem);
        $( "#accordion" ).accordion( "refresh" );
    });
    
    $('.property-floor-plans').on('keypress keyup blur', '.floor-plan-title', function() {
        var mirrorTitle = $(this).parent().parent().prev().find('.floor-plan-title-mirror');
        if(mirrorTitle.html() == '') {
            mirrorTitle.html('New Floor Plan');
        } else {
            mirrorTitle.html($(this).val());
        }
    });
    
    $('.property-floor-plans').on("click", ".delete-floor-plan", function() {
        $(this).parent().next().remove();
        $(this).parent().remove();
    });

    /********************************************/
	/* SUBMIT PROPERTY - AMENITIES */
	/********************************************/
	$('.add-amentity').click(function() {
        $('.amentities-list-container').append('<div class="amentity-item amentity-item-new"><span class="amentity-label">'+ rypecore_local_script.amenity_name +' </span><input type="text" class="border" name="amentities[]" id="amentity1" value="" /><span class="delete-amentity right"><i class="fa fa-trash"></i> '+ rypecore_local_script.delete_text +'</span></div>');
    });

    $('.amentities-list-container').on("click", ".delete-amentity", function() {
        $(this).parent().remove();
    });

    $('.amentities-list-container').on("click", ".edit-amentity", function() {
        var input = $(this).parent().find('.amentity-hidden-input');
        var label = $(this).parent().find('label');
        var doneEditing = $(this).parent().find('.done-editing-amentity');
        input.fadeIn();
        label.hide();
        doneEditing.fadeIn();
    });

    $('.amentities-list-container').on("click", ".done-editing-amentity", function() {
        var input = $(this).parent().find('.amentity-hidden-input');
        var label = $(this).parent().find('label');
        var doneEditing = $(this).parent().find('.done-editing-amentity');
        input.hide();
        label.text(input.val());
        label.fadeIn();
        doneEditing.hide();
    });

    /******************************************************************************/
	/** SUBMIT PROPERTY - OWNER INFO **/
	/******************************************************************************/
	$('#owner-info input[type="radio"]').click(function() {
		var input = $(this).val();
		$('#owner-info .form-block-agent-options').hide();
		if(input == 'agent') {
			$('#owner-info .form-block-select-agent').slideDown('fast');
		}
		if(input == 'custom') {
			$('#owner-info .form-block-custom-agent').slideDown('fast');
		}
	});

	/***************************************************************************/
	//SLICK SLIDER - SIMPLE SLIDER
	/***************************************************************************/
	if(rtl == 'true') { rtl =  true; } else { rtl =  false; }
    if(home_slider_transition == 'horizontal') { home_slider_transition =  false; } else { home_slider_transition =  true; }
	if(home_slider_duration == '') { home_slider_duration = 5000 }
	if(home_slider_auto_start == 'true') { home_slider_auto_start =  true; } else { home_slider_auto_start =  false; }

	$('.slider.slider-simple').slick({
		prevArrow: $('.slider-nav-simple-slider .slider-prev'),
		nextArrow: $('.slider-nav-simple-slider .slider-next'),
		adaptiveHeight: true,
		fade: home_slider_transition,
		autoplay: home_slider_auto_start,
		autoplaySpeed: home_slider_duration,
        rtl: rtl
	});

	/***************************************************************************/
	//SLICK SLIDER - PROPERTY GALLERY 
	/***************************************************************************/
    $('.slider.slider-property-gallery').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		adaptiveHeight: true,
		arrows: false,
		fade: true,
		infinite:false,
		asNavFor: '.property-gallery-pager',
        rtl: rtl
	});

    $('.property-gallery-pager').on('init', function(event, slick){
        if(slick.slideCount > 5) { 
            $('.slide-counter').text('1 / ' + slick.slideCount);
        } else {
            $('.slide-counter').hide();
        }
    });

	$('.property-gallery-pager').slick({
		prevArrow: $('.slider-nav-property-gallery .slider-prev'),
		nextArrow: $('.slider-nav-property-gallery .slider-next'),
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '.slider.slider-property-gallery',
		dots: false,
		focusOnSelect: true,
		infinite:false,
        rtl: rtl
	});

    $('.property-gallery-pager').on('afterChange', function(event, slick, currentSlide, nextSlide){
        currentSlide = currentSlide + 1;
        var counter = currentSlide + ' / ' + slick.slideCount;
        $('.slide-counter').text(counter);
    });

    /***************************************************************************/
    //SLICK SLIDER - TESTIMONIALS 
    /***************************************************************************/
    $('.slider-wrap-testimonials').each(function (idx, item) {
        var carouselId = "slider-wrap-testimonials-" + idx;
        this.id = carouselId;
        $(this).find('.slider.slider-testimonials').slick({
            prevArrow: $('#' + carouselId + ' .slider-nav-testimonials .slider-prev'),
            nextArrow: $('#' + carouselId + ' .slider-nav-testimonials .slider-next'),
            adaptiveHeight: true,
            rtl: rtl
        });
    });

    /***************************************************************************/
    //SLICK SLIDER - FEATURED PROPERTIES
    /***************************************************************************/
    $('.slider.slider-featured').slick({
        prevArrow: $('.slider-nav-properties-featured .slider-prev'),
        nextArrow: $('.slider-nav-properties-featured .slider-next'),
        slidesToShow: 4,
        slidesToScroll: 1,
        rtl: rtl,
        responsive: [
            {
              breakpoint: 990,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 589,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
        ]
    });

	//INITIATE SLIDES
	$('.slide').addClass('initialized');


	/******************************************************************************/
	/** INITIALIZE PRETTY PHOTO  **/
	/******************************************************************************/
	$("a[rel='prettyPhoto']").prettyPhoto({
		default_width: 920,
		default_height: 520,
		social_tools: ''
	});

	/******************************************************************************/
	/** VALIDATE LOGIN FORM  **/
	/******************************************************************************/
	$(".login-form #wp-submit").click(function() {
        var user = $("input#user_login").val();
        var pass = $("input#user_pass").val();
        if (user == "" || pass == "") {
        	$('.login-form .alert-box').remove();
            $('.login-form').prepend('<div class="alert-box error"><i class="fa fa-warning"></i> '+ rypecore_local_script.login_error +'</div>');
            return false;
        }
    });

    /******************************************************************************/
	/** REMOVE AVATAR  **/
	/******************************************************************************/
    $('.avatar-remove').click(function() {
    	$(this).parent().parent().find('.avatar-img img').attr('src', home_url + '/images/avatar-default.png');
    	$(this).parent().parent().find('.avatar-img img').attr('width', '96');
    	$(this).parent().parent().find('.avatar-img img').attr('height', '96');
    	$('.user-profile-form .form-submit').append('<input name="avatar_remove" type="hidden" value="true" />');
    });

    /******************************************************************************/
	/** ADD FAVORITE  **/
	/******************************************************************************/
    $('.property-favorite').click(function(event) {
    	event.preventDefault();
    	$(this).addClass('favorited');

    	var post_id = $(this).parent().parent().attr('class');
    	post_id = post_id.replace('property post-','');
    	
    });

	/******************************************************************************/
	/** CONTACT FORM  **/
	/******************************************************************************/
	// $('form.contact-form').submit(function() {
    //
     //    var contactForm = $(this);
    //
	// 	  contactForm.find('.form-loader').show();
	//       contactForm.find('.error').remove();
    //
     //      var successMessage = '';
     //      if(contactForm.hasClass('agent-contact-form')) {
     //        successMessage = agent_form_success;
     //      } else {
     //        successMessage = contact_form_success;
     //      }
    //
	//       var hasError = false;
    //
	//       contactForm.find('.requiredField').each(function() {
	//         if($.trim($(this).val()) == '') {
	//           var labelText = $(this).attr('placeholder');
	//           contactForm.find('.contact-form-fields').prepend('<div class="alert-box error"><span>'+rypecore_local_script.contact_form_error+'</span> '+labelText+'.</div>');
	//           $(this).addClass('inputError');
     //          $('.form-loader').hide();
	//           hasError = true;
	//         } else if($(this).hasClass('email')) {
	//           var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	//           if(!emailReg.test($.trim($(this).val()))) {
	//             var labelText = $(this).prev('label').text();
	//             contactForm.find('.contact-form-fields').prepend('<div class="alert-box error"><span>'+rypecore_local_script.contact_form_error_email+'</span> email.</div>');
	//             $(this).addClass('inputError');
	//             hasError = true;
	//           }
	//         }
	//       });
	//       if(!hasError) {
	//         var formInput = $(this).serialize();
	//         $.post($(this).attr('action'),formInput, function(data){
	//            contactForm.find('.form-loader').hide();
	//            contactForm.find('.contact-form-fields').slideUp("fast", function() {
	//                $(this).before('<div class="alert-box success">'+successMessage+'</div>');
	//            });
	//         });
	//       }
	//
	//     return false;
	// });

	/******************************************************************************/
	/** CREATE PRICE RANGE SLIDER  **/
	/******************************************************************************/
    if(filter_price_min == '') { filter_price_min = 0; } else { filter_price_min = parseInt(filter_price_min); }
    if(filter_price_max == '') { filter_price_max = 1000000; } else { filter_price_max = parseInt(filter_price_max); }
    if(filter_price_min_start == '') { filter_price_min_start = 200000; } else { filter_price_min_start = parseInt(filter_price_min_start); }
    if(filter_price_max_start == '') { filter_price_max_start = 600000; } else { filter_price_max_start = parseInt(filter_price_max_start); }

    function data ( element, key ) {
        return element.getAttribute('data-' + key);   
    }

    function createSlider ( slider ) {

        var filter_price_min_new = filter_price_min;
        var termPriceMin = $(slider).closest('.filter-item').find('.term-price-min').val();
        if(termPriceMin != '' && termPriceMin != null) { 
            termPriceMin = parseInt(termPriceMin);
            filter_price_min_new = termPriceMin; 
        }

        var filter_price_max_new = filter_price_max;
        var termPriceMax = $(slider).closest('.filter-item').find('.term-price-max').val();
        if(termPriceMax != '' && termPriceMax != null) { 
            termPriceMax = parseInt(termPriceMax);
            filter_price_max_new = termPriceMax; 
        }

        var filter_price_min_start_new = filter_price_min_start;
        var termPriceMinStart = $(slider).closest('.filter-item').find('.term-price-min-start').val();
        if(termPriceMinStart != '' && termPriceMinStart != null) { 
            termPriceMinStart = parseInt(termPriceMinStart);
            filter_price_min_start_new = termPriceMinStart; 
        }

        var filter_price_max_start_new = filter_price_max_start;
        var termPriceMaxStart = $(slider).closest('.filter-item').find('.term-price-max-start').val();
        if(termPriceMaxStart != '' && termPriceMaxStart != null) { 
            termPriceMaxStart = parseInt(termPriceMaxStart);
            filter_price_max_start_new = termPriceMaxStart; 
        }

        if(currency_symbol_position == 'before') {
          var pricePrefix = currency_symbol;
          var pricePostfix = '';
        } else {
          var pricePrefix = '';
          var pricePostfix = currency_symbol;
        }

        var priceSliderDirection = 'ltr';
        if(rtl == true) { priceSliderDirection = 'rtl'; } else { priceSliderDirection = 'ltr'; }
        
        noUiSlider.create(slider, {
                start: [filter_price_min_start_new, filter_price_max_start_new],
                connect: true,
                step: 1,
                margin:1,
                direction: priceSliderDirection,
                range: {
                    'min': filter_price_min_new,
                    'max': filter_price_max_new
                },
                //tooltips: [ wNumb({ decimals: currency_decimal_num, mark: currency_decimal, thousand: currency_thousand, prefix: pricePrefix, postfix: pricePostfix }) , wNumb({ decimals: currency_decimal_num, mark: currency_decimal, thousand: currency_thousand, prefix: pricePrefix, postfix: pricePostfix }) ],
                format: wNumb({
                    // Set formatting
                    decimals: currency_decimal_num,
                    mark: currency_decimal,
                    thousand: currency_thousand,
                    prefix: pricePrefix,
                    postfix: pricePostfix
                 })
        });

        var count = Number(data(slider, 'count'));

        var nodes = [
            document.getElementById('priceMin'+count),
            document.getElementById('priceMax'+count) 
        ];

        slider.noUiSlider.on('update', function ( values, handle ) {
            nodes[handle].value = values[handle];
            $('#priceMinLabel'+count).html(values[0]);
            $('#priceMaxLabel'+count).html(values[1]);
        });

    }

    Array.prototype.forEach.call(document.querySelectorAll('.price-slider'), createSlider);

    /******************************************************************************/
    /** PROPERTY FILTER WIDGET  **/
    /******************************************************************************/
    $('.filter').on('change', '.property-status-dropdown', function() {
        var index = $(this).prop('selectedIndex');
        var tabIndex = index+1;
        var val = $(this).val();
        $(this).closest('.tabs').tabs( "option", "active", index );
        $(this).closest('.tabs').find('#tabs-'+tabIndex+' .property-status-dropdown').val(val);
        $(this).closest('.tabs').find('#tabs-'+tabIndex+' .property-status-dropdown').trigger("chosen:updated");
    });


    /******************************************************************************/
    /** PROPERTY SHARE  **/
    /******************************************************************************/
    $('.property-share-toggle').click(function() {
        $(this).parent().find('.property-share-content').slideDown();
    });

    $('.property-share-close').click(function(event) {
        event.preventDefault();
        $(this).parent().parent().slideUp();
    });

    $('.property-share-email a').click(function(event) {
        event.preventDefault();
        $(this).parent().find('.property-share-email-form').stop(true, true).slideToggle('fast');
    });

    $('.property-share').on('click', '.property-share-email-send', function() {
        var recipient = $(this).parent().find('.property-share-email-input').val();
        var propertyLink = $(this).parent().find('.property-share-email-link').val();

        if(recipient != '') {
            $(this).attr('href', 'mailto:'+recipient+'?&subject=Check out this property&body='+propertyLink);
            var href = $(this).attr('href');
            window.location = href;
        } else {
            alert(rypecore_local_script.share_email_error);
        }
    });

    /******************************************************************************/
    /** AGENT SCROLL TO PROPERTIES  **/
    /******************************************************************************/
    function scrollToAnchor(aid){
        var aTag = $("a[name='"+ aid +"']");
        $('html,body').animate({scrollTop: aTag.offset().top},'slow');
    }

    $('.button.agent-assigned').click(function(event) {
        event.preventDefault();
        scrollToAnchor('anchor-agent-properties');
    });

    $('.button.agent-message').click(function(event) {
        event.preventDefault();
        scrollToAnchor('anchor-agent-contact');
    });

    /********************************************/
    /* MORTGAGE CALCULATOR WIDGET */
    /********************************************/
    ! function(e) {
        e.fn.homenote = function(t) {
            function r() {
                var t = e("#purchasePrice").val().replace(/[^0-9\.]/g, ""),
                    r = e("#dpamount").val().replace(/[^0-9\.]/g, "");
                if ("percentage" === p.dptype) return r / t * 100 + "%";
                var a = r / 100;
                return t * a
            }

            function a() {
                var t = e("#term").val();
                return "months" === e('input:radio[name="termtype"]:checked').val() ? 12 * t : t / 12
            }

            function n() {
                var t = e('input:radio[name="dptype"]:checked').val(),
                    r = e("#purchasePrice").val().replace(/[^0-9\.]/g, ""),
                    a = e("#dpamount").val().replace(/[^0-9\.]/g, "");
                if ("percentage" === t) {
                    var n = a / 100;
                    return r - r * n
                }
                return r - a
            }

            function l() {
                var t = e("#term").val();
                return "months" === e('input:radio[name="termtype"]:checked').val() ? t : 12 * t
            }

            function c() {
                var t = n(),
                    r = e("#rate").val().replace(/[^0-9\.]/g, "") / 100 / 12,
                    a = l(),
                    c = Math.pow(1 + r, a),
                    o = (t * (r * c / (c - 1))).toFixed(2);
                return o
            }

            function o() {
                var e = "<form id='homenote' role='form' class='well'>";
                return e += "<div class='form-group'><label for='purchasePrice'>"+ rypecore_local_script.purchase_price +" (" + p.currencysym + ")</label>", e += "<input type='text' class='border' id='purchasePrice' value='" + p.principal + "'></div></hr>", e += "<div class='form-group'><label for='downPayment'>"+rypecore_local_script.down_payment+"</label><input type='text' class='border' id='dpamount' value='" + p.dpamount + "'></div>", e += "<label class='label-radio'><input type='radio' name='dptype' id='downpercentage' value='percentage'", "percentage" === p.dptype && (e += " checked"), e += ">"+rypecore_local_script.percent+" (%)</label>", e += "<label class='label-radio'><input type='radio' name='dptype' id='downlump' value='downlump'", "downlump" === p.dptype && (e += " checked"), e += ">" + p.currency + " (" + p.currencysym + ")</label><hr>", e += "<div class='form-group'><label for='rate'>"+rypecore_local_script.rate+" (%)</label><input type='text' class='border' id='rate' value='" + p.rate + "'></div><hr>", e += "<div class='form-group'><label for='rate'>"+rypecore_local_script.term+"</label><input type='text' class='border' id='term' value='" + p.term + "'></div>", e += "<label class='label-radio'><input type='radio' name='termtype' id='years' value='years' ", "years" === p.termtype && (e += "checked"), e += ">"+rypecore_local_script.years+"</label>", e += "<label class='label-radio'><input type='radio' name='termtype' id='months' value='months'", "months" === p.termtype && (e += "checked"), e += ">"+rypecore_local_script.months+"</label><hr>", e += "<div class='alert alert-success' style='display:none;' id='results'></div>", e += "<button type='submit' class='button' id='calchomenote'>"+rypecore_local_script.calculate+"</button></form>"
            }
            var p = e.extend({
                currencysym: currency_symbol,
                currency: rypecore_local_script.fixed,
                termtype: "years",
                term: "30",
                principal: "250,000",
                dptype: "percentage",
                dpamount: "20%",
                rate: "6.5",
                resulttext: rypecore_local_script.monthly_payment
            }, t);
            t = e.extend(p, t), e(document).on("change", 'input[name="termtype"]', function() {
                p.termtype = e(this).val(), e("#term").val(a())
            }), e(document).on("change", 'input[name="dptype"]', function() {
                p.dptype = e(this).val(), e("#dpamount").val(r())
            }), e(document).on("click", "#calchomenote", function(t) {

                var moneyFormat = wNumb({
                    decimals: currency_decimal_num,
                    mark: currency_decimal,
                    thousand: currency_thousand,
                });

                var totalNum = parseFloat(c());

                if(currency_symbol_position == 'before') {
                    var total = p.currencysym + moneyFormat.to(totalNum);
                } else {
                    var total = moneyFormat.to(totalNum) + p.currencysym;
                }

                t.preventDefault(), e("#results").hide().html(p.resulttext + " <strong>" + total + "</strong>").show()
            }), e(this).html(o())
        }
    }(jQuery);

    var moneyFormat = wNumb({
        decimals: currency_decimal_num,
        mark: currency_decimal,
        thousand: currency_thousand,
    });

    var principal = moneyFormat.to(200000);

    $('.mortgage-calculator-container').homenote({
        principal: principal,
    });

    /********************************************/
    /* FILTER SHORT */
    /********************************************/
    $('.filter-short .filter-short-advanced .price-hidden-input').prop('disabled', true);

    $('.filter-short .advanced-options-toggle').click(function() {
        var filterShortAdvanced = $(this).parent().parent().find('.filter-short-advanced');
        
        if(filterShortAdvanced.is(':hidden')) {
            $(this).parent().parent().find('.filter-short-advanced .price-hidden-input').prop('disabled', false);
        } else {
            $(this).parent().parent().find('.filter-short-advanced .price-hidden-input').prop('disabled', true);
        }

        filterShortAdvanced.slideToggle('fast');
    });

    $('.filter-short .filter-short-mobile-toggle').click(function() {
        $(this).parent().find('.filter-item').show();
        $(this).hide();
    });





});