; var Site = Site || {};

(function (global, window, document, undefined) {
    'use strict';

    var InputValidate = function($inputsCollection, inputsSettings, commonCallback, resetFunction) {

        if(!$inputsCollection || !$inputsCollection.length) {
            throw new Error('You have to set the collection of inputs');
        }

        if(!inputsSettings) {
            throw new Error('You have to set the rules of inputs collection');
        }

        this.defaultInputSettings = {
            name: null,
            rule: null,
            required: true,
            errorMessage: null,
            requiredMessage: null,
            callback: commonCallback || function() {}
        };

        this.rules = {
            free: {
                errorMessage: 'Поле заполнено неверно',
                requiredMessage: 'Поле не заполнено',
                action: function(value, settings) {
                    // Проверка на обязательность заполнения
                    if(settings.required && !value) {
                        return {
                            valid: false,
                            errorExplain: settings.requiredMessage
                        };
                    }
                    return {
                        valid: true,
                        errorExplain: null
                    };
                }
            },
            phone: {
                errorMessage: 'Телефо указан неверно',
                requiredMessage: 'Телефон не указан',
                action: function(value, settings) {
                    if(settings.required && !value) {
                        return {
                            valid: false,
                            errorExplain: settings.requiredMessage
                        };
                    }
                    return {
                        valid: value.match(/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/),
                        errorExplain: settings.errorMessage
                    };
                }
            },
            /* TODO: Сделать проверку такой же как у Validator php */
            email: {
                errorMessage: 'Эл. почта указана неверно',
                requiredMessage: 'Эл. почта не указана',
                action: function(value, settings) {
                    if(settings.required && !value) {
                        return {
                            valid: false,
                            errorExplain: settings.requiredMessage
                        };
                    }
                    return {
                        valid: value.match(/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i),
                        errorExplain: settings.errorMessage
                    };
                }
            }
        };

        this.$inputsCollection = $inputsCollection;
        this.inputsSettings = inputsSettings;

        this.resetFunction = resetFunction;

        this.init.call(this);

    };

    InputValidate.prototype.init = function() {

        var that;
        that = this;

        $.each(this.inputsSettings, function(key, settings) {
            var $input, timerId;
            settings = $.extend({}, that.defaultInputSettings, settings);

            if(!settings.name) {
                throw new Error('Name is empty in inputsSettings (key: '+ key +')');
            }
            if(!settings.rule) {
                throw new Error('Rule is empty in inputsSettings (key: '+ key +')');
            }
            $input = that.$inputsCollection.filter('input[name="'+ settings.name +'"]');
            if(!$input.length) {
                throw new Error('There is no input with name "' + settings.name +'" in inputsCollection');
            }
            if(!that.rules[settings.rule]) {
                throw new Error('There is no rule "' + settings.rule +'"');
            }

            settings.errorMessage = settings.errorMessage || that.rules[settings.rule]['errorMessage'];
            settings.requiredMessage = settings.requiredMessage || that.rules[settings.rule]['requiredMessage'];

            $input.on({
                keyup: function(event) {
                    if(event.keyCode == 9) return false;
                    clearTimeout(timerId);
                    timerId = setTimeout(function() {
                        that.checkInputOnType.call(that, $input, settings);
                    }, 250);
                },
                keydown: function() {
                    clearTimeout(timerId);
                }
            });

            $input.on('blur', function() {
                that.checkInputOnBlur.call(that, $input, settings);
            });

            that.inputsSettings[key] = settings;
        });
    };

    InputValidate.prototype.checkInputOnType = function($input, settings) {
        var that, value, ruleExecution, result;

        that = this;
        value = $input.val();
        ruleExecution = that.rules[settings.rule];
        result = ruleExecution.action(value, settings);

        settings.callback($input, result.valid, result.errorExplain, settings);
    };

    InputValidate.prototype.checkInputOnBlur = (function() {

        var recall = false;

        return function($input, settings) {
            var that, value, ruleExecution, result;

            that = this;
            value = $input.val();
            ruleExecution = that.rules[settings.rule];

            if(!value && !settings.required) {
                that.resetFunction($input);
            } else {
                result = ruleExecution.action(value, settings);
                settings.callback($input, result.valid, result.errorExplain, settings);
            }

            if(!recall) {
                recall = true;
                setTimeout(function() {
                    that.checkInputOnBlur.call(that, $input, settings);
                    recall = false;
                }, 300);
            }

        }
    }());

    window.InputValidate = InputValidate;

})(Site, window, document);