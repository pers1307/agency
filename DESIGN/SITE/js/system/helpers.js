
    ; var Site = Site || {};

    (function (global, window, document, undefined) {
        'use strict';

        if (!Function.prototype.bind) {
            Function.prototype.bind = function (oThis) {
                if (typeof this !== "function") {
                    // closest thing possible to the ECMAScript 5 internal IsCallable function
                    throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
                }

                var aArgs = Array.prototype.slice.call(arguments, 1),
                    fToBind = this,
                    fNOP = function () {
                    },
                    fBound = function () {
                        return fToBind.apply(this instanceof fNOP
                                ? this
                                : oThis,
                            aArgs.concat(Array.prototype.slice.call(arguments)));
                    };

                fNOP.prototype = this.prototype;
                fBound.prototype = new fNOP();

                return fBound;
            };
        }

        String.prototype.hashCode = function() {
            var hash = 0, i, chr, len;
            if (this.length == 0) return hash;
            for (i = 0, len = this.length; i < len; i++) {
                chr   = this.charCodeAt(i);
                hash  = ((hash << 5) - hash) + chr;
                hash |= 0; // Convert to 32bit integer
            }
            return hash;
        };

        window.getElem = (function() {
            var elems = global.registry.get('cachedElems') || {};
            return function (selector) {
                if(selector) {
                    var hashCode = selector.hashCode();
                    if(elems[hashCode]) {
                        return elems[hashCode] || null;
                    } else {
                        elems[hashCode] = $(selector);
                        global.registry.set('cachedElems', elems);
                        return elems[hashCode];
                    }
                }
                return false;
            }
        }());

        window.callbackByOffset = (function() {
            var defaultConfig, calculateOffset;
            defaultConfig = {
                id: 'unique-id',
                $elem: null,
                offset: 10,
                callbackBefore: function() {},
                callbackAfter: function() {},
                repeat: false
            };
            calculateOffset = function(config) {
                var beforeAchieved, afterAchieved, handler;
                beforeAchieved = false;
                afterAchieved = false;
                if(config.repeat) {
                    handler = function() {
                        var currentOffset;
                        currentOffset = config.$elem.offset().top;
                        if(currentOffset >= config.offset) {
                            config.callbackAfter();
                        } else {
                            config.callbackBefore();
                        }
                    };
                } else {
                    handler = function() {
                        var currentOffset;
                        currentOffset = config.$elem.offset().top;
                        if(currentOffset >= config.offset && !afterAchieved) {
                            config.callbackAfter();
                            afterAchieved = true;
                            beforeAchieved = false;
                        }
                        if(currentOffset < config.offset && !beforeAchieved) {
                            config.callbackBefore();
                            beforeAchieved = true;
                            afterAchieved = false;
                        }
                    };
                }
                $(window).on('scroll.'+config.id, handler);
                return true;
            };

            return function(config) {
                config = $.extend({}, defaultConfig, config);
                config.$elem = config.$elem || getElem('.header');
                if(!config.id) {
                    throw new Error('You have to set an unique id');
                }
                return calculateOffset(config);

            }
        }());

        window.resizeCallback = (function() {
            var timers = {};
            return function (callback, ms, uniqueId) {
                if (!uniqueId) {
                    uniqueId = "Don't call this twice without a uniqueId";
                }
                if (timers[uniqueId]) {
                    clearTimeout (timers[uniqueId]);
                }
                timers[uniqueId] = setTimeout(callback, ms);
            };
        }());

        window.scrollToElem = (function() {
            var $htmlBody = $('html,body');
            return function($elem, speed, offset, callback) {
                offset = offset || 0;
                callback = callback ||  function() {};
                if($elem.length) {
                    var destination = $elem.offset().top - offset;
                    $htmlBody.animate({scrollTop: destination}, speed).promise().then(function() {
                        callback();
                    });
                }
            }
        }());

        window.objectType = function(item) {
            return Object.prototype.toString.call(item).slice(8, -1).toLowerCase();
        };

        window.pluralForm = function(n, forms) {
            return forms[n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2];
        };

        window.strReplace = function(search, replace, subject) {
            return subject.replace(new RegExp (search, 'g'), replace);
        };

        window.ajaxError = (function() {
            var errorStyle = 'position:fixed;top:0;padding:10px;background:red;opacity:0.4;font-size:12px;color:#fff',
                $body = getElem('body');
            return function( data ){
                var content = '<div style="' + errorStyle + '">' +
                    '<a href="#" onclick="$(this).parent().remove();return false" style="float:right;margin:0 0 0 10px">[X]</a>' +
                    'AJAX [' + data.url + ']: ' + data.error +
                    '</div>';
                $body.after(content);
            }
        }());

        window.trim = function(str, charlist) {
            var whitespace, l = 0,
                i = 0;
            str += '';
            if (!charlist) {
                // default list
                whitespace =
                    ' \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000';
            } else {
                // preg_quote custom list
                charlist += '';
                whitespace = charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
            }
            l = str.length;
            for (i = 0; i < l; i++) {
                if (whitespace.indexOf(str.charAt(i)) === -1) {
                    str = str.substring(i);
                    break;
                }
            }
            l = str.length;
            for (i = l - 1; i >= 0; i--) {
                if (whitespace.indexOf(str.charAt(i)) === -1) {
                    str = str.substring(0, i + 1);
                    break;
                }
            }
            return whitespace.indexOf(str.charAt(0)) === -1 ? str : '';
        };

        window.number_format = function( number, decimals, dec_point, thousands_sep ) {
            var i, j, kw, kd, km;
            if( isNaN(decimals = Math.abs(decimals)) ){
                decimals = 2;
            }
            if( dec_point == undefined ){
                dec_point = ",";
            }
            if( thousands_sep == undefined ){
                thousands_sep = ".";
            }
            i = parseInt(number = (+number || 0).toFixed(decimals)) + "";
            if( (j = i.length) > 3 ){
                j = j % 3;
            } else{
                j = 0;
            }
            km = (j ? i.substr(0, j) + thousands_sep : "");
            kw = i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands_sep);
            //kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).slice(2) : "");
            kd = (decimals ? dec_point + Math.abs(number - i).toFixed(decimals).replace(/-/, 0).slice(2) : "");
            return km + kw + kd;
        };

        window.formatPhone = function(phone) {
            var regExp = /^((\+?\d{1}\s)?\(?\d{3,4}\))?/;
            return phone.replace(regExp, '<b>$1</b>');
        };

        window.formatCost = function(cost) {
            return number_format(parseFloat(cost), 0, '', ' ');
        };


        window.loadScript = (function() {
            var loadedScripts = {};
            return function(src, callback) {
                var hashCode, script;
                hashCode = src.hashCode();
                if(!(hashCode in loadedScripts)) {
                    script = document.createElement("script");
                    script.type = "text/javascript";
                    script.src = src;
                    document.body.appendChild(script);
                    script.onload = script.onerror = function() {
                        if (!this.executed) { // выполнится только один раз
                            this.executed = true;
                            callback();
                        }
                    };
                    script.onreadystatechange = function() {
                        var self = this;
                        if (this.readyState == "complete" || this.readyState == "loaded") {
                            setTimeout(function() {
                                self.onload()
                            }, 0);
                        }
                    };
                    loadedScripts[hashCode] = true;
                } else {
                    callback();
                }
            };
        })();

        window.getScrollWidth = (function() {
            var scrollWidth;
            return function() {
                if(!scrollWidth) {
                    var div = document.createElement('div');
                    div.style.overflowY = 'scroll';
                    div.style.width = '50px';
                    div.style.height = '50px';
                    div.style.visibility = 'hidden';
                    document.body.appendChild(div);
                    scrollWidth = div.offsetWidth - div.clientWidth;
                    document.body.removeChild(div);
                }
                return scrollWidth;
            }
        })();

        window.is = function(type, obj) {
            var clas = Object.prototype.toString.call(obj).slice(8, -1);
            return obj !== undefined && obj !== null && clas === type;
        };

        $(function() {

            /**
             * Устанавливает максимальную высоту
             * найденную среди элементов набора
             * всем элементам если ret = undefined или false
             * и возвращает максимальное значение как число,
             * если ret = true
             *
             * @param ret
             * @returns {*}
             */
            $.fn.equalizeHeights = function(ret) {
                ret = ret || false;
                var maxHeight = this.map(function(i,e) {
                    return $(e).height();
                }).get();
                return ret
                    ? Math.max.apply(this, maxHeight)
                    : this.height( Math.max.apply(this, maxHeight) );
            };

            /**
             * Выравнивает высоту изображения по переданному параметру
             * @param checkHeight
             */
            $.fn.equalizeImages = function(checkHeight) {
                this.each(function() {
                    var $image, height, diff;
                    $image = $(this);
                    height = $image.height();
                    diff = checkHeight - height;
                    if(diff > 0) {
                        diff = diff / 2;
                        $image.css({
                            borderTop: diff+'px solid transparent',
                            borderBottom: diff+'px solid transparent'
                        });
                    }
                });
            };

            $.fn.serializeObject = function() {
                var o = {};
                var a = this.serializeArray();
                $.each(a, function() {
                    if (o[this.name] !== undefined) {
                        if (!o[this.name].push) {
                            o[this.name] = [o[this.name]];
                        }
                        o[this.name].push(this.value || '');
                    } else {
                        o[this.name] = this.value || '';
                    }
                });
                return o;
            };
        });

    })(Site, window, document);