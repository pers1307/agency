

    // Пример обработки второстепенных целей
    $(document).on('eventMetrics', function(event, params) {

        // Здесь выбирается какое из событий отрабатывает
        switch(params.type) {

            // Попытка отправки формы
            case 'formSubmit':

                // Здесь определятся с какой формой идет работа
                switch(params.formId) {

                    // callOrder в данном случае уникальный идентификтаор формы «Заказать звонок»
                    case 'callOrder':

                        // Достигнута цель: Попытка отправки формы «Заказать звонок»
                        // yaCounter22038058.reachGoal('callOrder_submit');
                        console.log('Достигнута цель: Попытка отправки формы «Заказать звонок»');
                        break;

                    // feedback в данном случае уникальный идентификтаор формы «Обратная связь»
                    case 'feedback':

                        // Достигнута цель: Попытка отправки формы «Обратная связь»
                        // yaCounter22038058.reachGoal('feedback_submit');
                        console.log('Достигнута цель: Попытка отправки формы «Обратная связь»');
                        break;
                }
                break;

            // Успешная отправка формы
            case 'formSuccess':

                // Здесь определятся с какой формой идет работа
                switch(params.formId) {

                    // callOrder в данном случае уникальный идентификтаор формы «Заказать звонок»
                    case 'callOrder':

                        // Достигнута цель: Успешная отправка формы «Заказать звонок»
                        // yaCounter22038058.reachGoal('callOrder_success');
                        console.log('Достигнута цель: Успешная отправка формы «Заказать звонок»');
                        break;

                    // feedback в данном случае уникальный идентификтаор формы «Обратная связь»
                    case 'feedback':

                        // Достигнута цель: Успешная отправка формы «Обратная связь»
                        // yaCounter22038058.reachGoal('feedback_success');
                        console.log('Достигнута цель: Успешная отправка формы «Обратная связь»');
                        break;
                }
                break;

            // Переход к форме, если к примеру форма вызывается во всплывающем окне
            case 'toForm':

                // Здесь определятся с какой формой идет работа
                switch(params.formId) {

                    // callOrder в данном случае уникальный идентификтаор формы «Заказать звонок»
                    case 'callOrder':

                        // Достигнута цель: Переход к форме «Заказать звонок»
                        // yaCounter22038058.reachGoal('callOrder_toForm');
                        console.log('Достигнута цель: Переход к форме «Заказать звонок»');
                        break;

                }
                break;

            // Переход в корзину, если это интернет-магазин
            case 'toCart':

                // Здесь определятся откуда произошел переход
                switch(params.transition) {

                    // clickButton — уникальный идентификтаор элемента, кликнув на который произошел переход
                    case 'clickButton':

                        // Достигнута цель: Переход в корзину по клику на кнопку
                        // yaCounter22038058.reachGoal('toCart_from_ClickButton');
                        console.log('Достигнута цель: Переход в корзину по клику на кнопку');
                        break;

                }
                break;
        }
    });


    // Пример обработки главной цели — заказ с сайта, если это интернет-магазин
    $(document).on('orderSuccess', function(event, params) {

        // В качестве params сюда передается объект для eCommerce
        /*try { var yaCounter22038058 = new Ya.Metrika({id: 22038058,params:window.params||{ }}); }
        catch(e) { }*/
        console.log('Отправка данных о заказе в eCommerce');
    });


    /**
     * Вызывает событие eventMetrics, передавая параметры.
     *
     * @param {string} type       Тип события (formSubmit, formSuccess, toForm, toCart).
     * @param {string} formId     Уникальный идентификатор формы.
     * @param {string} transition Элемент или действие, которое привело пользователя в корзину.
     */
    function eventMetrics(type, formId, transition) {

        transition = typeof(transition) !== 'undefined' ? transition : null;

        var params = {

            type:       type,
            formId:     formId,
            transition: transition
        };

        if(typeof(params.type)       !== 'undefined' &&
           typeof(params.formId)     !== 'undefined' ||
           typeof(params.type)       !== 'undefined' &&
           typeof(params.transition) !== 'undefined') {

            $(document).trigger('eventMetrics', [params]);
        }

        return false;
    }

    /**
     * Вызывает событие orderSuccess, передавая параметры.
     *
     * @param {string} params Объект параметров для eCommerce в формате json.
     */
    function orderSuccess(params) {

        if(typeof(params.order_id) !== 'undefined' &&
           typeof(params.order_price !== 'undefined' &&
           typeof(params.goods !== 'undefined'))) {

            $(document).trigger('orderSuccess', [params]);
        }

        return false;
    }