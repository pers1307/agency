
; var Site = Site || {};

(function (global, window, document, undefined) {
    'use strict';
    var Registry = (function () {
        var instance, scope = {};
        return function ConstructRegistry() {
            if (instance) {
                return instance;
            }
            ConstructRegistry.prototype.set = function(key, value) {
                return scope[key] = value;
            };
            ConstructRegistry.prototype.get = function(key, decode) {
                var result;
                decode = decode || false;
                result = scope[key] || undefined;
                if(result && decode) {
                    result = JSON && JSON.parse(result) || $.parseJSON(result);
                }
                return result;
            };
            ConstructRegistry.prototype.del = function(key) {
                if(scope[key]) {
                    delete scope[key];
                    return true;
                }
                return false;
            };
            if (this && this.constructor === ConstructRegistry) {
                instance = this;
            } else {
                return new ConstructRegistry();
            }
        }
    }());

    global.registry = new Registry();

})(Site, window, document);