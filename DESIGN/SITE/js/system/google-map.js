
    ; var Site = Site || {};

    (function (global, window, document, $, undefined) {
        'use strict';

        var GoogleMap = function() {
            this.apikey = 'AIzaSyBF5ggz0-1ujj8oYeIRT3DvxuFSWrxNdio';
            this.elem = null;
            this.map = null;
            this.mapTypes = [];
            this.settings = {};
            this.apiLoaded = false;
            this.callback = function() {};
        };

        GoogleMap.prototype.fillMapTypes = function() {

            this.mapTypes['roadmap'] = google.maps.MapTypeId.ROADMAP;
            this.mapTypes['satellite'] = google.maps.MapTypeId.SATELLITE;
            this.mapTypes['hybrid'] = google.maps.MapTypeId.HYBRID;
            this.mapTypes['terrain'] = google.maps.MapTypeId.TERRAIN;
        };

        GoogleMap.prototype.buildConfig = function() {

            var config = {},
                center = this.settings.center = typeof this.settings.center === 'string'
                    ? this.settings.center.split(':')
                    : this.settings.center;

            config.center = new google.maps.LatLng(+center[1], +center[0]);
            config.zoom = +this.settings.zoom;
            config.mapTypeId = this.mapTypes[(this.settings.mapTypeId || 'roadmap')];

            return config;
        };

        GoogleMap.prototype.apiLoad = function() {

            if(document.getElementById('google-map-api')) {
                return false;
            }

            var script = document.createElement("script");
            script.type = "text/javascript";
            script.src = "https://maps.googleapis.com/maps/api/js?v=3.exp&key="+ this.apikey +"&callback=Site.googleMap.init";
            script.id = 'google-map-api';
            document.body.appendChild(script);
        };

        GoogleMap.prototype.init = function() {

            this.apiLoaded = true;
            this.fillMapTypes.call(this);
            this.insert.call(this, undefined, undefined, undefined, true);
        };

        GoogleMap.prototype.insertIcons = function() {

            var that = this, icons = [];

            if(!this.settings.icons) return undefined;

            if(objectType(this.settings.icons) !== 'array') {
                this.settings.icons = [this.settings.icons];
            }

            $.each(this.settings.icons, function(key, iconItem) {

                var icon, coords, offset;

                if(iconItem.coords) {
                    coords = typeof iconItem.coords === 'string'
                        ? iconItem.coords.split(':')
                        : iconItem.coords;
                }

                if(iconItem.offset) {
                    offset = typeof iconItem.offset === 'string'
                        ? iconItem.offset.split(':')
                        : iconItem.offset;
                }

                if(iconItem.url) {
                    icon = {};
                    coords = coords || that.settings.center;
                    icon.position = new google.maps.LatLng(coords[1], coords[0]);

                    if(offset) {
                        icon.icon = {
                            url: iconItem.url
                            //anchor: [offset[0], offset[1]]  TODO: Доделать смещение иконки
                        };
                    } else {
                        icon.icon = iconItem.url;
                    }

                    icon.map = that.map;

                } else if(coords) {
                    icon = {};
                    icon.position = new google.maps.LatLng(coords[1], coords[0]);
                    icon.map = that.map;
                }

                if(icon) {
                    var marker = new google.maps.Marker(icon);
                    icons.push(marker);

                    if("infowindow" in iconItem) {
                        that.infowindows = that.infowindows || [];
                        that.infowindows[key] = new google.maps.InfoWindow({
                            content: iconItem.infowindow
                        });

                        marker.addListener('click', function() {
                            that.infowindows[key].open(that.map, marker);
                        });
                    }
                }
            });

            return icons.length ? (icons.length === 1 ? icons[0] : icons) : undefined;
        };

        GoogleMap.prototype.setCenter = function(map, coords) {
            var center;
            map = map || this.map;
            coords = coords || this.settings.center;
            coords = typeof coords === 'string' ? coords.split(':') : coords;
            center = new google.maps.LatLng(coords[1], coords[0]);
            map.setCenter(center);
        }

        GoogleMap.prototype.setZoom = function(map, zoom) {
            map = map || this.map;
            zoom = zoom || this.settings.zoom;
            map.setZoom(+zoom);
        }

        GoogleMap.prototype.insert = function(settings, elemId, callback, recall) {
            var settingsDefault, mapIcons, elem;

            if(!recall) {
                callback = callback || function() {};

                if(!(elem = document.getElementById(elemId))) {
                    throw new Error('You have to set the element id which will be used for map inserting');
                }

                if(!settings) {
                    throw new Error('You have to set the settings for your map');
                }
            }

            this.elem = elem || this.elem;
            this.callback = callback || this.callback;
            this.settings = settings || this.settings;

            if(!this.apiLoaded) {
                this.apiLoad.call(this);
                return false;
            }

            settingsDefault = {
                center: [-34.397, 150.644],
                zoom: 8
            };

            this.settings = $.extend({}, settingsDefault, this.settings);
            this.map = new google.maps.Map(this.elem, this.buildConfig.call(this));
            mapIcons = this.insertIcons.call(this);
            this.callback.call(this, this.map, mapIcons);
        };

        global.googleMap = new GoogleMap();

    })(Site, window, document, jQuery);