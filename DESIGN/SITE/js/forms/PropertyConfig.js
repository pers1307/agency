var modalWindowProperty = new ModalWindow();

modalWindowProperty.clickClass = '.js-property';
modalWindowProperty.withModalPostFix = '-property';
modalWindowProperty.formPostFix = '-property';
modalWindowProperty.inputsName = ['name', 'phone'];
modalWindowProperty.errorLvl2 = [
    "<span class='errorText'>Имя должно содержать только буквы</span>",
    "<span class='errorText'>Поле должно содержать только цифры</span>"
];

modalWindowProperty.theEnd = "<span class='succes'>Ваша заявка успешно отправлена! Мы свяжимся с вами как можно скорее.</span>";

modalWindowProperty.withFile = false;
modalWindowProperty.init();
