var contactForm = new Form();

contactForm.formPostFix = '-contact';
contactForm.inputsName = ['name', 'email', 'phone'];
contactForm.errorLvl2 = [
    "<span class='errorText'>Имя должно содержать только буквы</span>",
    "<span class='errorText'>Электронный адрес введен не верно</span>",
    "<span class='errorText'>Поле должно содержать только цифры</span>"
];

contactForm.theEnd = "<div class='alert-box success'>Ваша заявка успешно отправлена!</div>";

contactForm.withFile = false;
contactForm.init();
