var contactForm = new Form();

contactForm.formPostFix = '-agent';
contactForm.inputsName = ['name', 'email'];
contactForm.errorLvl2 = [
    "<span class='errorText'>Имя должно содержать только буквы</span>",
    "<span class='errorText'>Электронный адрес введен не верно</span>"
];

contactForm.theEnd = "<div class='alert-box success'>Ваша заявка успешно отправлена!</div>";

contactForm.withFile = false;
contactForm.init();
