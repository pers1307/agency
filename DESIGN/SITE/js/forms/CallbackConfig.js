var modalWindowCallback= new ModalWindow();

modalWindowCallback.clickClass = '.js-callback';
modalWindowCallback.withModalPostFix = '-callback';
modalWindowCallback.formPostFix = '-callback';
modalWindowCallback.inputsName = ['name', 'phone'];
modalWindowCallback.errorLvl2 = [
    "<span class='errorText'>Имя должно содержать только буквы</span>",
    "<span class='errorText'>Поле должно содержать только цифры</span>"
];

modalWindowCallback.theEnd = "<span class='succes'>Ваша заявка успешно отправлена! Мы свяжимся с вами как можно скорее.</span>";

modalWindowCallback.withFile = false;
modalWindowCallback.init();
