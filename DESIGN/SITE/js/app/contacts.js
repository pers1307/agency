
; var Site = Site || {};

(function(global, window, document, $, undefined) {
    "use strict";

    function Contacts() {
        this.$mapWrapper = $('#map');
        this.mapSettings = this.getSettings();
    }

    Contacts.prototype.getSettings = function() {
        return global.registry.get('coodinates', true);
    };

    Contacts.prototype.initMap = function() {
        var that, mapSettings, icons, icon, currentCoords;

        that = this;
        icons = [];
        icon = {};

        icon.coords = that.mapSettings['coords'];
        icons.push(icon);

        mapSettings = {
            center: that.mapSettings['coords'],
            zoom: 14,
            icons: icons
        };

        global.googleMap.insert(mapSettings, 'map', function(map) {
            that.googleMapObject = map;
        });
    };
    
    $(function() {
        global.contacts = new Contacts();
        global.contacts.initMap();
    });

})(Site, window, document, jQuery);