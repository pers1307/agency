;$(function () {
    App.define('modules.common');

    var Common = function () {

        this.$document = $(document);

        /**
         * Example
         */
        // this.sliderClass          = '.main-slider';
        // this.sliderClass2         = '.brands';
        // this.itemTitleClass       = '.acc-item-title';
        // this.reviewsFancyboxClass = '.js-fancybox';

        this.bindEvents();
    };

    //-----------------------------------------------------
    // Event handlers
    //-----------------------------------------------------

    Common.prototype.bindEvents = function () {
        var instance = this;

        /**
         * Example
         */
        // $(instance.sliderClass).bxSlider();
        //
        // $(instance.sliderClass2).bxSlider({
        //     pager: false,
        //     minSlides: 4,
        //     maxSlides: 4,
        //     moveSlides: 1,
        //     slideWidth: 240,
        //     auto: true,
        //     pause: 6000
        // });
        //
        // $(instance.itemTitleClass).on('click', function () {
        //
        //     $(this).parent().toggleClass('open');
        // });
        //
        // $(instance.reviewsFancyboxClass).fancybox();
    };

    App.modules.common = new Common();
});