<?php
    if (isset($_REQUEST['process']))
    {

        $login = request('login', '');
        $pass = request('pass', '');

        $success = (auth_LoginUser($login, $pass, 0) === true);

        if (isset($_SERVER['HTTP_X_REQUESTED_WITH']))
        {
            header('Content-Type:application/json; charset=' . SITE_ENCODING);

            echo json_encode(
                array(
                    'success' => $success
                )
            );

            die();
        }
        else
        {
            if ($success)
            {
                Redirects::go('/' . ROOT_PLACE . '/');
            }
        }
    }
?>

<div class="login" id="controlAuthFrom">
    <form action="/<?= ROOT_PLACE ?>/auth/" method="POST">
        <div>
            <label><input type="text" name="login" placeholder="Логин"></label>
        </div>

        <div>
            <label><input type="password" name="pass" placeholder="Пароль"></label>
        </div>

        <input type="hidden" name="process" value="1">

        <button class="button">Войти</button>
    </form>
</div>