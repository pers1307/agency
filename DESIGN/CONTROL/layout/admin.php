<?php
    /**
     * User: Andruha
     * Date: 26.03.13
     * Time: 14:35
     *
     * @var string $_TREE_ контент зоны вывода «_TREE_»
     * @var string $_CONTENT_ контент зоны вывода «_CONTENT_»
     */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php echo SITE_ENCODING ?>">
    <title><?php echo MSCore::page()->title_page ?></title>

    <?php echo StaticFiles:: getCSS('admin-general-styles') ?>
    <!--[if lt IE 9]>
    <link rel="stylesheet" href="/DESIGN/CONTROL/css/ie.css" type="text/css">
    <![endif]-->
    <link rel="icon" href="/DESIGN/CONTROL/images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/DESIGN/CONTROL/images/favicon.ico" type="image/x-icon">

    <script>
        var Site = Site || {},
            ROOT_PLACE = Site.ROOT_PLACE = '<?php echo ROOT_PLACE ?>';
    </script>

</head>
<body>
<div class="left-column<?php echo !AUTH_IS_ROOT ? ' not-root' : '' ?>">
    <div class="siteName">
        <a href="/" target="_blank"><?php echo DOMAIN ?></a>
    </div>
    <?php echo $_TREE_ ?>
</div>

<div class="right-column">
    <div class="razdelName" id="title_page_cont"></div>

    <div class="main" id="center">
        <?php echo $_CONTENT_ ?>
    </div>
</div>

<?php echo StaticFiles::getJS('admin-general-scripts') ?>
</body>
</html>