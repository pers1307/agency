/**
 * User: Andruha
 * Date: 15.04.13
 * Time: 17:11
 */

var Site = Site || {};

(function (global, window, $) {
    'use strict';

    function TreeMenu(container, cookieName){

        $.cookie.json = true;

        this.cookieName = cookieName;
        this.treeOpen = $.cookie(this.cookieName) || [];
        this.container = typeof container == 'object' ? container : $(container);

        this.container.on('click', '.icon-sort-up, .icon-sort-down', this.toggleShowChild.bind(this));
        this.container.on('changeClass', '.icon-sort-up, .icon-sort-down', this.upDownIconsChangeClass.bind(this));

    }

    TreeMenu.prototype.toggleShowChild = function (event) {

        var currentTarget = $(event.currentTarget),
            parent = currentTarget.parents('li').eq(0),
            open = parent.find('>ul').hasClass('visible'),
            child = parent.find(open ? 'ul:visible' : '>ul');

        if (open) {
            currentTarget = parent.find('.icon-sort-up');
        }

        if (child.is(':animated')) return;

        child.slideToggle('fast', function () {

            $(this).toggleClass('visible');
            $(window).trigger('updateBaronScrollers');

        });

        currentTarget.toggleClass('icon-sort-up icon-sort-down').trigger('changeClass');

    };

    TreeMenu.prototype.upDownIconsChangeClass = function (event) {

        var item = $(event.currentTarget),
            itemId = item.data('item-id'),
            open = item.hasClass('icon-sort-up');

        if (open) {
            this.treeOpen.push(itemId);
        } else {
            var index = this.treeOpen.indexOf(itemId);
            this.treeOpen.splice(index, 1);
        }

        $.cookie(this.cookieName, this.treeOpen);

    };

    $(function(){
        global.treeMenu = TreeMenu.bind(this);
    });

})(Site, window, jQuery);