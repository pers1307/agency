/**
 * User: Andruha
 * Date: 12.04.13
 * Time: 17:45
 */

var Site = Site || {};

(function(global, window, document, $)
{
    'use strict';

    function Site()
    {
        this.window = {};

        $(window).on('resize load', this.getWindowSize.bind(this));
    }

    Site.prototype.getWindowSize = function(event)
    {
        var currentTarget = $(event.currentTarget);

        this.window.width = currentTarget.width();
        this.window.height = currentTarget.height();
    };

    function SiteMenuSelector(target, selector)
    {
        this.activeTarget = null;

        $(target).on('click setActive', selector, this.setActiveMenu.bind(this));
    }

    SiteMenuSelector.prototype.setActiveMenu = function(event)
    {
        var currentTarget = $(event.currentTarget),
            parent = currentTarget.parent('');

        if (currentTarget.hasClass('no-active')) return;

        event.preventDefault();

        if (this.activeTarget != null) this.activeTarget.removeClass('active');
        this.activeTarget = parent.removeClass('wasActive').addClass('active');
    };

    $(function()
    {
        global.site = new Site();

        new SiteMenuSelector('#pagesTree, #generalModules, #rootControl', 'a');
        new SiteMenuSelector('.right-column', '.catalogModuleWrap-articles a');
    });

})(Site, window, document, jQuery);