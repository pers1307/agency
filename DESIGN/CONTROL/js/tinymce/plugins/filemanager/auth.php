<?php
    /**
     * User: Andruha
     * Date: 18.07.13
     * Time: 15:44
     */

    require_once dirname(__FILE__) . '/../../../../../../config.php';
    require_once CORE_DIR . '/includes/authorizate_functions.php';
    require_once CORE_DIR . '/includes/global_functions.php';
    require_once CORE_DIR . DS . 'classes' . DS . 'MSCore.php';
    require_once CORE_DIR . '/classes/system/UrlsAndPaths.php';

    $Urls = new UrlsAndPaths();
    unset($_CONFIG);
    $_USER = auth_Authorization();
    $groups = explode(",", $_USER['groups']);

    if (!in_array(1, $groups) && !in_array(2, $groups))
    {
        Page404();
    }