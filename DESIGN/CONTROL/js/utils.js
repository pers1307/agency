/**
 * User: Andruha
 * Date: 18.03.13
 * Time: 14:45
 */

var Site = Site || {};

(function (global, window, document, $, undefined) {
    'use strict';

    /**
     * Эмуляция Function.prototype.bind из Mozilla Developer Network
     */
    if (!Function.prototype.bind) {
        Function.prototype.bind = function (oThis) {
            if (typeof this !== "function") {
                // closest thing possible to the ECMAScript 5 internal IsCallable function
                throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
            }

            var aArgs = Array.prototype.slice.call(arguments, 1),
                fToBind = this,
                fNOP = function () {
                },
                fBound = function () {
                    return fToBind.apply(this instanceof fNOP
                        ? this
                        : oThis,
                        aArgs.concat(Array.prototype.slice.call(arguments)));
                };

            fNOP.prototype = this.prototype;
            fBound.prototype = new fNOP();

            return fBound;
        };
    }

    /**
     * Эмуляция Array.prototype.indexOf для IE8
     */
    if(!Array.prototype.indexOf) {
        Array.prototype.indexOf = function(what, i) {
            i = i || 0;
            var L = this.length;
            while (i < L) {
                if(this[i] === what) return i;
                ++i;
            }
            return -1;
        };
    }

    /**
     * Функция транслитерации url
     *
     * @param string
     * @returns {string}
     */
    global.translitUrl = function (string) {

        var lettersConformity = {
            'а': 'a', 'б': 'b', 'в': 'v', 'г': 'g', 'д': 'd', 'е': 'e', 'ё': 'e', 'ж': 'zh', 'з': 'z',
            'и': 'i', 'й': 'y', 'к': 'k', 'л': 'l', 'м': 'm', 'н': 'n', 'о': 'o', 'п': 'p', 'р': 'r',
            'с': 's', 'т': 't', 'у': 'u', 'ф': 'f', 'х': 'h', 'ц': 'ts', 'ч': 'ch', 'ш': 'sh', 'щ': 'sh',
            'ъ': '', 'ы': 'y', 'ь': '', 'э': 'e', 'ю': 'yu', 'я': 'ya', '№': '#', ' ': '-'
        };

        return string.toLowerCase()

            .replace(/([\u0410-\u0451\u2116 ])/g, function (str) {

                if (lettersConformity[str] != undefined) {
                    return lettersConformity[str];
                } else {
                    return '';
                }

            }) // меняем русские буквы на соответствующие английские
            .replace(/[^a-z0-9-_]+/g, '')	// удаляем все символы, кроме букв, цифр и тире
            .replace(/\s|[-_]+/g, '-')	// заменяем пробелы на тире + удаляем лишние пробелы и нижнее подчёркивание
            .replace(/(^-|-$)+/g, '');	// удаляем тире с начала и конца строки

    };

})(Site, window, document, jQuery);