/**
 * User: Andruha
 * Date: 29.03.13
 * Time: 15:23
 */
/** @namespace Site.ROOT_PLACE */

var Site = Site || {};

(function(global, window, $)
{
    'use strict';

    function PagesTree(container)
    {
        this.container = $(container);

        this.treeController = new global.treeMenu(container, 'pagesTreeOpen');

        this.container.on('click', '.module-controls-menu span', this.controlsOnClick.bind(this));
        this.container.on('click', 'li > span> a', this.pagesOnClick.bind(this));
        this.container.on('contextmenu', 'li > span> a', this.copyLink.bind(this));
        this.container.on('sortupdate', 'ul', this.onSortUpdate.bind(this));

        this.initSortable.call(this);

    }

    PagesTree.prototype.onSortUpdate = function(event, item){
        event.stopPropagation();

        var items = item.parent().find('>li'),
            itemsIdList = [],
            id;

        items.each(function(key, value){
            id = $(value).find('>span a').data('id');
            itemsIdList.push(id);
        });

        $.post('/' + Site.ROOT_PLACE + '/ajax/swap_pages/', {items: itemsIdList});

    };

    PagesTree.prototype.initSortable = function()
    {
        var items = this.container.find('ul:first ul');

        items.each(function(key, item){
            $(item).sortable('destroy');
            $(item).sortable({
                forcePlaceholderSize: true,
                handle: '.move-handler'
            });
        });
    };

    PagesTree.prototype.controlsOnClick = function(event)
    {
        var currentTarget = $(event.currentTarget),
            dataObject = currentTarget.parent(''),
            action = currentTarget.data('action'),

            pageId = dataObject.data('page-id'),
            parentId = dataObject.data('parent-id'),
            typeId = dataObject.data('type-id');

        switch (action)
        {
            case 'edit':

                displayMessage('/' + global.ROOT_PLACE + '/admin/page_options/' + pageId + '/', 800, 600);

                break;

            case 'add':

                displayMessage('/' + global.ROOT_PLACE + '/admin/structure/add_path/node' + pageId + '_' + typeId + '/', 400, 320);

                break;

            case 'vis-show':
            case 'vis-hide':

                $.getJSON('/' + global.ROOT_PLACE + '/admin/toggle_vis/' + pageId + '/' + $('#page' + pageId + ' .move-handler').size() + '/', function(data)
                {
                    this.updatePage(pageId, data.content);
                }.bind(this));

                break;

            case 'move-up':

                $.post('/' + global.ROOT_PLACE + '/ajax/swap_page/up/' + typeId + '/', {data: pageId}, function()
                {

                    this.moveUp(pageId);

                }.bind(this));

                break;

            case 'move-down':

                $.post('/' + global.ROOT_PLACE + '/ajax/swap_page/down/' + typeId + '/', {data: pageId}, function()
                {

                    this.moveDown(pageId);

                }.bind(this));

                break;

            case 'del':

                if (confirm('Вы действительно хотите удалить раздел?'))
                {
                    $.getJSON('/admin/ajax/delete/', {
                        'page-id': pageId,
                        'parent-id': parentId,
                        'type-id': typeId
                    }, function(data)
                    {
                        if (parentId == 1)
                        {
                            this.updateType(typeId, data.content);
                        }
                        else
                        {
                            this.updatePage(parentId, data.content);
                        }
                    }.bind(this));
                }

                break;
        }
    };

    PagesTree.prototype.rearrangedItems = function(firstItem, secondItem)
    {
        var firstItemContent = firstItem.html(),
            firstItemId = firstItem.attr('id');

        firstItem.html(secondItem.html()).attr('id', secondItem.attr('id'));
        secondItem.html(firstItemContent).attr('id', firstItemId);
    };

    PagesTree.prototype.moveUp = function(id)
    {
        var page = $('#page' + id),
            prev = page.prev();

        this.rearrangedItems(page, prev);
    };

    PagesTree.prototype.moveDown = function(id)
    {
        var page = $('#page' + id),
            next = page.next();

        this.rearrangedItems(page, next);
    };

    PagesTree.prototype.pagesOnClick = function(event)
    {
        event.preventDefault();

        var currentTarget = $(event.currentTarget),
            id = currentTarget.data('id');

        openPage('center', 'edit_path' + id, '/' + global.ROOT_PLACE + '/admin/structure/edit_path/' + id + '/', currentTarget.text(), currentTarget.text());
    };

    PagesTree.prototype.updatePage = function(id, content)
    {
        $('#page' + id).replaceWith(content);
        this.container.find('.wasActive a').trigger('setActive');
        $(window).trigger('updateBaronScrollers');
        this.initSortable();
    };

    PagesTree.prototype.updateType = function(id, content)
    {
        $('#type' + id).replaceWith(content);
        this.container.find('.wasActive a').trigger('setActive');
        $(window).trigger('updateBaronScrollers');
        this.initSortable();
    };

    //noinspection JSUnusedGlobalSymbols
    PagesTree.prototype.refreshTree = function(parentId, typeId)
    {
        $.getJSON('/admin/ajax/refresh_tree/', {
            'parent-id': parentId,
            'type-id': typeId
        }, function(data)
        {
            if (parentId == 1)
            {
                this.updateType(typeId, data.content);
            }
            else
            {
                //TODO: сделать возможным добавлять страницы без типа (пока только главная обновляется в дереве)
                parentId = parentId || 1;

                this.updatePage(parentId, data.content);
            }
        }.bind(this));
    };

    PagesTree.prototype.copyLink = function(event){
        event.preventDefault();
        var target = $(event.currentTarget),
            text = target.attr('title').split("\r\n")[0];

        window.prompt ("Чтобы скопировать текст в буфер обмена, нажмите Ctrl+C и Enter", text);
    };

    $(function()
    {
        global.pagesTree = new PagesTree('#pagesTree');
    });

})(Site, window, jQuery);