/**
 * User: Andruha
 * Date: 25.03.13
 * Time: 11:15
 */

var Site = Site || {};

(function(global, window, document, $, undefined){
    'use strict';

    $(function(){

        /** Placeholder для IE */
        $('input, textarea').placeholder();

        /** Загружаем главную страницу, при первой загрузке */
        $('#page1').find('a:first').trigger('click');

        /**
         * После загрузки всех изображений, вызывается событие allImagesLoaded
         */
        $(document).on('ajaxComplete', function(event, xhr, ajaxOptions){

            xhr.done(function(){

                var images = [],
                    counter = 0,
                    htmlPattern = /<.*?>/g;

                if (ajaxOptions.dataType == 'json')
                {

                    $.each($.parseJSON(xhr.responseText), function(key, value){

                        if (typeof value != 'string') value = value.join('');
                        value = value.replace(/\r?\n|\r/g, '');

                        if (htmlPattern.test(value))
                        {
                            $.merge(images, $(value).find('img'));
                        }

                    });

                }
                else if (ajaxOptions.dataType == 'html')
                {
                    images = $(xhr.responseText).find('img');
                }

                if (images.length)
                {
                    $(images).one('load', function(){

                        if (++counter == images.length)
                        {
                            $(window).trigger('allImagesLoaded');
                        }

                    });
                }

            });

        });

    });

})(Site, window, document, jQuery);