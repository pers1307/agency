/**
 * User: Andruha
 * Date: 12.04.13
 * Time: 15:06
 */
var Site = Site || {};

(function(global, window, $, undefined)
{
    'use strict';

    function ModalController()
    {
        this.modals = [];
        this.container = $('body');
    }

    ModalController.prototype.open = function(title, url, settings)
    {
        settings = settings || {};

        if (typeof url === 'object')
        {
            settings = url || {};
        }
        else
        {
            settings.url = url || null;
        }

        settings.title = title;
        settings.container = this.container;

        this.container.addClass('locked').css('overflow', 'hidden');
        this.modals.push(new ModalWindow(settings));
    };

    ModalController.prototype.close = function()
    {
        this.modals.pop().close(!this.modals.length);
        if (!this.modals.length)
        {
            this.container.removeClass('locked').css('overflow', '');
        }
    };

    ModalController.prototype.closeAll = function()
    {

    };

    ModalController.prototype.positionAll = function()
    {
        $.each(this.modals, function(i, value)
        {
            value.position();
        });
    };

    function ModalWindow(settings)
    {
        var defaultSettings = {
            url: null,
            title: 'Модальное окно',
            content: '',
            autoPosition: false,
            closeOnBgClick: true,

            container: 'body'
        };

        this.controller = global.modal;
        this.settings = $.extend(defaultSettings, settings);
        this.container = typeof this.settings.container === 'object' ? this.settings.container : $(this.settings.container);

        this.init.call(this);
    }

    ModalWindow.prototype.init = function()
    {
        this.createWindow.call(this);

        !this.settings.autoPosition || $(window).on('resize', this.position.bind(this));

        this.modalWindowCloseButton.on('click', this.controller.close.bind(this.controller));

        if (this.settings.closeOnBgClick)
        {
            this.modalWindowWrap.on('click', function(event)
            {

                if (event.target == this.modalWindowWrap[0])
                {
                    this.controller.close.call(this.controller)
                }

            }.bind(this));
        }

        if (this.settings.url)
        {
            this.modalWindowBody.addClass('loading');

            $.get(this.settings.url, function(data)
            {

                this.modalWindowBody.removeClass('loading');
                this.modalWindowBody.html(data);
                !this.settings.autoPosition || this.position.call(this);

            }.bind(this), 'html');
        }
        else
        {
            this.modalWindowBody.html(this.settings.content);
            !this.settings.autoPosition || this.position.call(this);
        }
    };

    ModalWindow.prototype.createWindow = function()
    {

        this.modalWindowWrap = $('<div class="modal-window-wrap"></div>');
        this.modalWindow = $('<div class="modal-window"></div>');
        this.modalWindowHeader = $('<div class="modal-window-header">' + this.settings.title + '</div>');
        this.modalWindowCloseButton = $('<span class="modal-window-close" title="Закрыть окно">&times;</span>');
        this.modalWindowBody = $('<div class="modal-window-body"></div>');

        this.modalWindowWrap.append(this.modalWindow);
        this.modalWindowHeader.append(this.modalWindowCloseButton);
        this.modalWindow.append(this.modalWindowHeader, this.modalWindowBody);

        this.container.append(this.modalWindowWrap);

        this.modalWindowWrap.data('modal', this);
    };

    ModalWindow.prototype.position = function()
    {
        var height = this.modalWindow.height(),
            marginTop = this.modalWindow.css('marginTop'),
            marginBottom = this.modalWindow.css('marginBottom'),
            top;

        top = (global.site.window.height - height - parseInt(marginTop) - parseInt(marginBottom)) / 2;

        if (top > 0)
        {
            this.modalWindow.css('top', top);
        }
        else
        {
            this.modalWindow.css('top', 'auto');
        }
    };

    ModalWindow.prototype.close = function(last)
    {
        if (typeof tinymce != 'undefined' && last){
            tinymce.remove('.wysiwyg');
        }

        this.modalWindowWrap.remove();
    };

    $(function()
    {
        global.modal = new ModalController();
    });

})(Site, window, jQuery);