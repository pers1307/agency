/**
 * User: Andruha
 * Date: 10.04.13
 * Time: 16:19
 */
var Site = Site || {};

(function (global, window, document, $) {
    'use strict';

    function ModuleControls() {

        $(document).on({
                'mouseenter': this.onMouseEnter.bind(this),
                'mouseleave': this.onMouseLeave.bind(this)
            },
            '.module-controls-wrap'
        );

    }

    ModuleControls.prototype.onMouseEnter = function (event) {

        var currentTarget = $(event.currentTarget),
            child = currentTarget.find('>span'),
            childWidth = child.width(),
            childHeight = child.height(),
            childOffset = child.offset(),
            childOffsetTop = childOffset.top,
            childOffsetLeft = childOffset.left,
            baronWrapper = currentTarget.closest('div.baron-wrapper'),
            closestParent = baronWrapper.length ? baronWrapper : $(window),
            closestParentOffset = closestParent.offset() || {top: 0, left: 0},
            closestParentTop = closestParent.height() + closestParentOffset.top,
            closestParentLeft = closestParent.width() + closestParentOffset.left;

        if (childOffsetLeft + childWidth > closestParentLeft){
            child.addClass('offset-left');
        }

        if (childOffsetTop + childHeight + 10 > closestParentTop){
            child.addClass('offset-top');
        }

    };

    ModuleControls.prototype.onMouseLeave = function (event) {

        var currentTarget = $(event.currentTarget),
            child = currentTarget.find('>span');

        child.removeClass('offset-left offset-top');

    };

    global.modulesControls = new ModuleControls();

})(Site, window, document, jQuery);