/**
 * User: Andruha
 * Date: 26.07.13
 * Time: 16:38
 */

var Site = Site || {};

(function (global, window, $) {
    'use strict';

    function LoginForm() {

        this.wrap = $('#controlAuthFrom');
        this.form = $('form', this.wrap);
        this.inputs = $(':text, :password', this.form).jrumble({
            x: 10,
            y: 0,
            rotation: 0,
            speed: 50,
            opacity: false,
            opacityMin: 5
        });

        this.form.on('submit', this.submitForm.bind(this));

        this.inputs.on('removeError focus keyup', function(){
            $(this).removeClass('error');
        });
    }

    LoginForm.prototype.submitForm = function (event) {
        event.preventDefault();

        this.inputs.trigger('removeError');

        $.post(this.form.attr('action'), this.form.serialize(), function (data) {
            if (data.success) {
                window.location.href = '/' + global.ROOT_PLACE + '/'
            } else {

                this.inputs.addClass('error');

                this.inputs.trigger('startRumble');
                setTimeout(function(){
                    this.inputs.trigger('stopRumble');
                }.bind(this), 300);
            }
        }.bind(this));

    };

    $(function () {
        global.LoginForm = new LoginForm();
    });

})(Site, window, jQuery);