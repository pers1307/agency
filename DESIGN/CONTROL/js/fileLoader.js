/**
 * User: Andruha
 * Date: 11.06.13
 * Time: 16:58
 */
var Site = Site || {};

(function(global, window, $, undefined)
{
    'use strict';

    function FileLoader(object, settings)
    {
        var defaults = {
            sizeLimit: 0,
            minSizeLimit: 0,
            limit: 1,
            moduleName: null,
            countUploaded: 0
        };

        this.object = object;
        this.data = this.object.data();
        this.settings = $.extend(defaults, settings, this.data);
        this.listElement = $('#fileLoader-list' + this.settings.configName);
        this.scroller = this.listElement.closest('.baron-scroller');
        this.errorBox = this.object.next();

        this.init();
    }

    FileLoader.prototype.init = function()
    {
        var settings = {
            debug: false,
            multiple: this.settings.limit != 1,
            listElement: this.listElement[0],
            maxConnections: 1,
            disableCancelForFormUploads: true,

            request: {
                endpoint: '/' + global.ROOT_PLACE + '/admin/fileLoader/',
                params: {
                    moduleName: this.settings.moduleName,
                    configName: this.settings.configName
                }
            },

            validation: {
                allowedExtensions: this.settings.allowedExtensions ? this.settings.allowedExtensions.replace(/[\s]+/g, '').split(',') : [],
                acceptFiles: null,
                sizeLimit: this.settings.sizeLimit,
                minSizeLimit: 0,
                itemLimit: this.settings.limit
            },

            messages: {
                typeError: '{file} имеет неверное расширение. Допустимые расширение(я): {extensions}.',
                sizeError: '{file} слишком большой, максимальный размер — {sizeLimit}.',
                minSizeError: '{file} слишком маленький, минимальный размер файла — {minSizeLimit}.',
                emptyError: '{file} is empty, please select files again without it.',
                noFilesError: 'Нет файлов для загрузки.',
                tooManyItemsError: "Превышен лимит на загрузку файлов — {itemLimit}.",
                retryFailTooManyItems: "Retry failed - you have reached your file limit.",
                onLeave: 'The files are being uploaded, if you leave now the upload will be cancelled.'
            },

            display: {
                fileSizeOnSubmit: false,
                prependFiles: false
            },

            text: {
                cancelButton: 'Отменить',
                retryButton: 'Повторить',
                deleteButton: 'Удалить',
                failUpload: 'Ошибка загрузки',
                dragZone: 'Переместите сюда файлы',
                dropProcessing: 'Обработка перенесенных файлов...',
                formatProgress: "{percent}% из {total_size}",
                waitingForResponse: "Обработка...",
                defaultResponseError: 'Upload failure reason unknown',
                uploadButton: this.settings.limit == 1 ? 'Выберите файл' : 'Выберите файлы',
                sizeSymbols: [' Кб', ' Мб', ' Гб', ' Тб', ' Пб', ' Еб']
            },

            template: '<div class="qq-uploader">' +
                '<div class="qq-upload-drop-area"><span>{dragZoneText}</span></div>' +
                '<div class="qq-upload-button"><button class="grey">{uploadButtonText}</button></div>' +
                '<span class="qq-drop-processing"><span>{dropProcessingText}</span><span class="qq-drop-processing-spinner"></span></span>' +
                '</div>',

            fileTemplate: '<div class="fileLoader-item">' +
                '<div class="qq-progress-bar"></div>' +
                '<span class="qq-upload-spinner"></span>' +
                '<span class="qq-upload-file"></span>' +
                '<span class="qq-upload-size"></span>' +
                '<a class="qq-upload-cancel" href="#">{cancelButtonText}</a>' +
                '<span class="qq-upload-status-text">{statusText}</span>' +
                '</div>',

            showMessage: function(message)
            {
                this.errorBox.hide().html(message).fadeTo('fast', 1);
            }.bind(this),

            camera: {
                ios: true
            }
        };

        this.object.fineUploader(settings).addClass('init');
        this.api = this.object.data('fineuploader').uploader;

        this.object
            .on('validateBatch', this.onValidateBatch.bind(this))
            .on('submitted', this.onSubmitted.bind(this))
            .on('progress', this.onProgress.bind(this))
            .on('complete', this.onComplete.bind(this))
        ;

        this.initSortable.call(this);

        this.listElement.on('click', '.fileLoader-deleteItem', this.onDelete.bind(this));

        this.listElement.on('dragProcess.h5s', '.fileLoader-item', function(event, e)
        {
            var scrollParent = $(e.currentTarget.offsetParent),
                topParent = scrollParent.offset().top,
                bottomParent = topParent + scrollParent.outerHeight(),
                mouseTop = e.originalEvent.pageY,
                scrollZoneHeight = 50;

            if(mouseTop < topParent + scrollZoneHeight){
                this.scroller.data('api').scrollMove(-7);
            }
            if(mouseTop > bottomParent - scrollZoneHeight){
                this.scroller.data('api').scrollMove(7);
            }
        }.bind(this));

        this.api._netUploaded = this.settings.countUploaded;
        this.api._netUploadedOrQueued = this.settings.countUploaded;
    };

    FileLoader.prototype.onValidateBatch = function()
    {
        if (this.settings.limit == 1)
        {
            this.api._options.validation.itemLimit = 2;
        }
    };

    FileLoader.prototype.onSubmitted = function(event, id, name)
    {
        if (this.settings.limit == 1)
        {
            this.api._options.validation.itemLimit = 1;
            this.api._netUploaded = 1;
            this.api._netUploadedOrQueued = 1;
            this.listElement.find('.fileLoader-deleteItem').trigger('click');
        }

        this.errorBox.fadeTo('fast', 0);
        $(window).trigger('updateBaronScrollers');
        this.scroller.trigger('scrollBottom');
    };

    FileLoader.prototype.onProgress = function(event, id, name, loaded, total)
    {
        //console.log('Callback: onProgress');
    };

    FileLoader.prototype.onComplete = function(event, id, name, responseJSON, xhr)
    {
        var item = $(this.api.getItemByFileId(id));

        if(typeof responseJSON.error != "undefined"){
            item.find('.qq-upload-status-text').html(responseJSON.error);
            return false;
        }

        var newItem = $(responseJSON.itemContent);

        item.replaceWith(newItem);
        newItem.fadeTo(0, 0.1).fadeTo(1000, 1);

        newItem.find('img').one('load', function()
        {
            $(window).trigger('allImagesLoaded');
        });

        $(window).trigger('allImagesLoaded');
        this.scroller.trigger('scrollBottom');
        this.initSortable.call(this);
    };

    FileLoader.prototype.initSortable = function()
    {
        if (this.settings.limit != 1)
        {
            this.listElement.sortable('destroy');
            this.listElement.sortable({
                forcePlaceholderSize: true
            });
        }
    };

    FileLoader.prototype.onDelete = function(event)
    {
        this.api._netUploaded--;
        this.api._netUploadedOrQueued--;

        var target = $(event.currentTarget).closest('.fileLoader-item');

        target.fadeTo('fast', 0, function()
        {
            target.remove();

            $(window).trigger('updateBaronScrollers');
        }.bind(this));
    };

    $(document).on('ready.fileLoader ajaxSuccess.fileLoader', function(event, xhr, settings)
    {
        var $fileUploader = $('.fileLoader').not('.init'),
            $fileQueue = $('.fileQueue').not('.init');

        $fileUploader.initFileLoader();
        $fileQueue.initFileQueue();
    });

    $(function()
    {
        $.fn.initFileLoader = function(settings)
        {
            $.each(this, function()
            {
                new FileLoader($(this), settings);
            });
        };

        $.fn.initFileQueue = function()
        {
            $.each(this, function()
            {
                var self = $(this);
                var settings = {
                    request: {
                        endpoint: '/' + global.ROOT_PLACE + '/admin/fileQueue/',
                        params: {
                            moduleName: self.data('module-name') || '',
                            configName: self.data('config-name') || '',
                            tableName: self.data('table-name') || self.data('module-name') || '',
                            pathId: self.data('path-id') || 0,
                            parent: self.data('parent') || 0
                        }
                    },
                    messages: {
                        typeError: '{file} имеет неверное расширение. Допустимые расширение(я): {extensions}.',
                        sizeError: '{file} слишком большой, максимальный размер — {sizeLimit}.',
                        minSizeError: '{file} слишком маленький, минимальный размер файла — {minSizeLimit}.',
                        emptyError: '{file} is empty, please select files again without it.',
                        noFilesError: 'Нет файлов для загрузки.',
                        tooManyItemsError: "Превышен лимит на загрузку файлов — {itemLimit}.",
                        retryFailTooManyItems: "Retry failed - you have reached your file limit.",
                        onLeave: 'The files are being uploaded, if you leave now the upload will be cancelled.'
                    },

                    text: {
                        cancelButton: 'Отменить',
                        retryButton: 'Повторить',
                        deleteButton: 'Удалить',
                        failUpload: 'Ошибка загрузки',
                        dragZone: 'Переместите сюда файлы',
                        dropProcessing: 'Обработка перенесенных файлов...',
                        formatProgress: "{percent}% из {total_size}",
                        waitingForResponse: "Обработка...",
                        uploadButton: 'Загрузить файлы',
                        sizeSymbols: [' Кб', ' Мб', ' Гб', ' Тб', ' Пб', ' Еб']
                    },
                    display: {
                        fileSizeOnSubmit: false,
                        prependFiles: false
                    },
                    validation: {
                        allowedExtensions: self.data('allowed-extensions') ? self.data('allowed-extensions').replace(/[\s]+/g, '').split(',') : [],
                        acceptFiles: null,
                        sizeLimit: self.data('size-limit'),
                        minSizeLimit: 0,
                        itemLimit: 0
                    },
                    template: '<div class="qq-uploader">' +
                        '<div class="qq-upload-drop-area" style="display: none;"><span>{dragZoneText}</span></div>' +
                        '<div class="qq-upload-button"><button class="grey uploadButton" style="margin-left: 5px">{uploadButtonText}</button></div>' +
                        '<span class="qq-drop-processing"><span>{dropProcessingText}</span><span class="qq-drop-processing-spinner"></span></span>' +
                        '<ul class="qq-upload-list" style="display: none;"></ul></div>'
                }

                var uploader = $(this).fineUploader(settings);
                uploader.on('submitted', function (event, id, name) {
                    var button = self.find('.uploadButton:first');
                    button.text('Загрузка...').fadeTo(250, 0.4);
                }).on('complete', function (event) {
                    if (uploader.fineUploader('getInProgress') === 0) {
                        var button = self.find('.uploadButton:first');
                        button.text('Загрузить файлы').fadeTo(250, 1);
                        if(self.data('success')) {
                            var parent = self.closest(self.data('wrapper')).attr('id');
                            if(parent) {
                                doLoad('', self.data('success'), parent);
                            }
                        }
                    }
                });

                $(this).addClass('init');
            });
        };
    });

})(Site, window, jQuery);