/**
 * User: Andruha
 * Date: 20.08.13
 * Time: 14:20
 */

var Site = Site || {};

(function (global, window, document, $) {
    'use strict';

    function GeneralModules()
    {
        this.container = $('#generalModules');
        this.init.call(this);
    }

    GeneralModules.prototype.init = function()
    {
        this.initSortable.call(this);

        this.container.on('sortupdate', '.generalModules-list', this.onSortUpdate.bind(this));
        this.container.on('update', this.initSortable.bind(this));
    };

    GeneralModules.prototype.initSortable = function()
    {
        this.groups = $('.generalModules-list', this.container);

        this.groups.sortable('destroy');
        this.groups.sortable({
            forcePlaceholderSize: true,
            handle: '.move-handler'
        });
    };

    GeneralModules.prototype.onSortUpdate = function(event, item)
    {
        event.stopPropagation();

        var items = item.parent().find('>li'),
            itemsIdList = [],
            id;

        items.each(function(key, value){
            id = $(value).data('id');
            itemsIdList.push(id);
        });

        $.post('/' + Site.ROOT_PLACE + '/ajax/swap_modules/', {items: itemsIdList});
    };

    global.generalModules = new GeneralModules();

})(Site, window, document, jQuery);