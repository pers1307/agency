<?php
/**
 * User: Andruha
 * Date: 20.06.13
 * Time: 13:48
 */

    $itemData = array_merge(array(

    ), $itemData);
?>
<div class="fileLoader-item fileLoader-itemFile clearfix">
    <span class="icon-paper-clip"></span>
    <span class="fileLoader-item-fileSize"><?php echo $itemData['fileSize'] ?></span>
    <span class="fileLoader-item-fileName"><?php echo $itemData['fileName'] ?></span>
    <span class="fileLoader-deleteItem">
        <i class="icon-trash" title="Удалить"></i>
    </span>
    <input type="hidden" name="<?php echo $sysName ?>[id]" value="<?php echo $itemData['id'] ?>">
</div>