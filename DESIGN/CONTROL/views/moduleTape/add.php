<?php

$key_field = $CONFIG['tables']['items']['key_field'];
?>

<form id="add" enctype="multipart/form-data">
    <div class="form-wrap"><?=$_FORM_?></div>
    <div id="div_inserted_id">
        <input id="inserted_id" type="hidden" name="<?= $key_field; ?>" value="<?= ${$key_field} ?>">
    </div>
</form>


<script>
    $('input.numberInt').numberMask();
    $('input.numberFloat').numberMask({decimalMark:['.',','],type:'float'});
    function postForm() {
        doLoad(getObj('add'), '/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/add/<?=(int)$path_id;?>/<?=(int)$page;?>/', <?=MSModuleController::prepareOutput($output_id);?>+';div_inserted_id');
    }

    //вызывается при сохранении и применении с проверкой заполненности
    function addFunc(close){

        var inpErr = $('input.required');
        inpErr.removeClass('error');
        $('.errorTextInp').remove();
        if(typeof inpErr.val() != "undefined"){
            var num = 1;
            inpErr.each(function(){
                if($(this).val()==''){
                    if(num==1)
                        $(this).focus();
                    $(this).addClass('error').parent().append('<span class="errorTextInp">Поле обязательно для заполнения</span>');

                    num++;
                }
            });
            if(num>1)
                return false;
        }

        doLoad(getObj('add'),'/<?= ROOT_PLACE; ?>/<?= $CONFIG['module_name']; ?>/add/<?= (int)$path_id; ?>/<?= (int)$page; ?>/0/'+(close?0:1)+'/', <?= MSModuleController::prepareOutput($output_id); ?>+';div_inserted_id', null, 'rewrite');
        if(close)
            Site.modal.close();
    }
</script>
<div class="form-bottom-controls">
    <ul>
        <li>
            <button onclick="addFunc(true)">Сохранить</button>
        </li>
        <li>
            <button onclick="addFunc()" class="grey">Применить</button>
        </li>
        <li>
            <button class="grey" onclick="Site.modal.close()">Отменить</button>
        </li>
    </ul>
</div>
