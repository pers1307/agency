<?php

$mod_name = $CONFIG['module_name'];
$d_width = (int)$CONFIG['tables']['items']['dialog']['width'];
$d_height = (int)$CONFIG['tables']['items']['dialog']['height'];
$table_config = $CONFIG['tables']['items'];

$key_field = $CONFIG['tables']['items']['key_field'];

$md5 = md5($mod_name);
?>

<div class="tapeModuleWrap">
    <div class="tapeModuleTableWrap">
        <div class="scrollableTable baron-scroll">
            <div class="form-bottom-controls" style="background-color: inherit">
                <ul>
                    <li>
                        <button onclick="addFuncItem(true)">Сохранить</button>
                    </li>
                </ul>
            </div>

            <form id="<?= $md5 ?>" enctype="multipart/form-data">
                <div class="form-wrap"><?=$_FORM_?></div>
                <div id="div_inserted_id">
                    <input id="inserted_id" type="hidden" name="<?= $key_field; ?>" value="<?= ${$key_field} ?>">
                </div>
            </form>

            <script>
                if (typeof tinymce != 'undefined') {
                    tinymce.remove('.wysiwyg');
                }

                $('input.numberInt').numberMask();
                $('input.numberFloat').numberMask({decimalMark:['.',','],type:'float'});

                //вызывается при сохранении и применении с проверкой заполненности
                function addFuncItem(close) {

                    var inpErr = $('input.required');
                    inpErr.removeClass('error');
                    $('.errorTextInp').remove();
                    if(typeof inpErr.val() != "undefined"){
                        var num = 1;
                        inpErr.each(function(){
                            if($(this).val()==''){
                                if(num==1)
                                    $(this).focus();
                                $(this).addClass('error').parent().append('<span class="errorTextInp">Поле обязательно для заполнения</span>');

                                num++;
                            }
                        });
                        if(num>1)
                            return false;
                    }

                    if (typeof tinymce != 'undefined') {
                        tinymce.remove('.wysiwyg');
                    }

                    doLoad(getObj('<?= $md5 ?>'),'/<?= ROOT_PLACE; ?>/<?= $CONFIG['module_name']; ?>/fastview/<?= (int)$path_id; ?>/<?= (int)$page; ?>/0/'+(close?0:1)+'/', <?= MSModuleController::prepareOutput($output_id); ?>, null);
                }
            </script>

            <div class="form-bottom-controls" style="background-color: inherit">
                <ul>
                    <li>
                        <button onclick="addFuncItem(true)">Сохранить</button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
