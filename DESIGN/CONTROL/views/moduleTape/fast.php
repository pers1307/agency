<?php

$mod_name = $CONFIG['module_name'];
$d_width = (int)$CONFIG['tables']['items']['dialog']['width'];
$d_height = (int)$CONFIG['tables']['items']['dialog']['height'];
$table_config = $CONFIG['tables']['items'];

$tapeModuleTopControls = array();

$linkPathButton = MSCore::forms()->writeModule_LinksPath();
empty($linkPathButton) or $tapeModuleTopControls[] = $linkPathButton;

if($actionAdd)
{
    $tapeModuleTopControls[] = '<button onclick="displayMessage(\'/'. ROOT_PLACE .'/'. $mod_name .'/add/'. (int)$path_id .'/'. (int)$page .'\')">Добавить</button>';
}

if(isset($CONFIG['upload']) && isset($CONFIG['upload']['settings']))
{
    $data = '';
    foreach ($CONFIG['upload']['settings'] as $settingsKey => $settingsValue) {
        $data .= ' data-' . $settingsKey . '="' . $settingsValue . '"';
    }
    $tapeModuleTopControls[] = '<div class="fileQueue" data-path-id = "'.(int) $path_id.'" data-success="/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/fastview/'. (int)$path_id.'/'. (int)$page.'" data-wrapper="'.(empty($path_id) ? '.main': '.moduleWrap').'"  '.$data.'></div>';
}

if(isset($CONFIG['settings']))
{
    $tapeModuleTopControls[] = '<button class="grey rightButton" onclick="displayMessage(\'/'. ROOT_PLACE .'/settings/add/'. (int)$path_id .'/'.$mod_name.'/\')">Настройки</button>';
}
?>
<div class="tapeModuleWrap">
    <?php
    echo template('moduleTape/tapeModuleTopControls', array(
        'items' => $tapeModuleTopControls
    ));
    ?>
    <div class="tapeModuleTableWrap<?php echo $pager ? ' pager' : '' ?>">
        <?php

        echo
        MSCore::forms()->writeModuleTableStart(),
        MSCore::forms()->writeModuleTableHeader($CONFIG['tables']['items'], true);

        foreach ($items as $item)
        {

            echo MSCore::forms()->moduleTableCell($table_config, $item, '', $item['editAction']);
        }

        echo
        MSCore::forms()->writeModuleTableEnd(''),
        $pager;
        ?>
    </div>
</div>
