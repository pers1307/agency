<form id="add" enctype="multipart/form-data"<? /* onkeypress="if(event.keyCode==13){doLoad(getObj('add'), '/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/add_item/<?=(int)$path_id;?>/<?=(int)$parent;?>/<?=(int)$page;?>/', 'items'); closeDialog();}"*/ ?>>
<div id="div_inserted_id">
<input id="inserted_id" type="hidden" name="id" value="<?=$id?>">
</div>
    <div class="form-wrap"><?=$_FORM_?></div>
</form>

<SCRIPT type="text/javascript">
    $('input.numberInt').numberMask();
    $('input.numberFloat').numberMask({decimalMark:['.',','],type:'float'});


    //вызывается при сохранении и применении с проверкой заполненности
    function addFunc(close){

        var inpErr = $('input.required');
        inpErr.removeClass('error');
        $('.errorTextInp').remove();
        if(typeof inpErr.val() != "undefined"){
            var num = 1;
            inpErr.each(function(){
                if($(this).val()==''){
                    if(num==1)
                        $(this).focus();
                    $(this).addClass('error').parent().append('<span class="errorTextInp">Поле обязательно для заполнения</span>');

                    num++;
                }
            });
            if(num>1)
                return false;
        }

        doLoad(getObj('add'), '/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/add_item/<?=(int)$path_id;?>/<?=(int)$parent;?>/<?=(int)$page;?>/0/0/', 'items;div_inserted_id');
        if(close)
            Site.modal.close();
    }

function postForm()	{
  doLoad(getObj('add'), '/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/add_item/<?=(int)$path_id;?>/<?=(int)$parent;?>/<?=(int)$page;?>/', 'items;div_inserted_id');
  
}
</SCRIPT>
<div class="form-bottom-controls">
    <ul>
        <li>
            <button onclick="addFunc(true)">Сохранить</button>
        </li>
        <li>
            <button onclick="addFunc()" class="grey">Применить</button>
        </li>
        <li>
            <button class="grey" onclick="Site.modal.close()">Отменить</button>
        </li>
    </ul>
</div>