<div class="treeMenu catalogModuleWrap-articles-label">
    <span>Разделы <?= $controls ?></span>
</div>
<div class="catalogModuleWrap-articles-wrap baron-scroll treeMenu">
    <ul>
        <li>
            <?php echo empty($articles_html) ? '<div>Нет разделов</div>' : $articles_html; ?>
        </li>
    </ul>
</div>
<script>$('.catalogModuleWrap-articles-wrap .wasActive a').trigger('setActive');</script>