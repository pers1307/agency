<?php

$mod_name = $CONFIG['module_name'];
$d_width = (int)$CONFIG['tables']['items']['dialog']['width'];
$d_height = (int)$CONFIG['tables']['items']['dialog']['height'];
$table_config = $CONFIG['tables']['items'];
$order = isset($CONFIG['tables']['items']['order_field']) ? $CONFIG['tables']['items']['order_field'] : '`order`';

$tapeModuleTopControls = array();

$linkPathButton = MSCore::forms()->writeModule_LinksPath();
empty($linkPathButton) or $tapeModuleTopControls[] = $linkPathButton;

if($actionAdd)
{
    $tapeModuleTopControls[] = '<button onclick="displayMessage(\'/'. ROOT_PLACE .'/'. $mod_name .'/add_item/'. (int)$path_id .'/'.(int)$parent.'/'. (int)$page .'\')">Добавить</button>';
}

if(isset($CONFIG['upload']) && isset($CONFIG['upload']['settings']))
{
    $data = '';
    foreach ($CONFIG['upload']['settings'] as $settingsKey => $settingsValue) {
        $data .= ' data-' . $settingsKey . '="' . $settingsValue . '"';
    }
    $tapeModuleTopControls[] = '<div class="fileQueue" data-path-id = "'.(int) $path_id.'" data-parent="'.(int)$parent.'" data-success="/'.ROOT_PLACE.'/'.$CONFIG['module_name'].'/show_items/'. (int)$path_id.'/'. (int)$parent.'/'. (int)$page.'" data-wrapper="#items"  '.$data.'></div>';
}

if(isset($CONFIG['settings']))
{
    $tapeModuleTopControls[] = '<button class="grey rightButton" onclick="displayMessage(\'/'. ROOT_PLACE .'/settings/add/'. (int)$path_id .'/'.$mod_name.'/\')">Настройки</button>';
}

?>
<div class="tapeModuleWrap">
    <?php
    echo template('moduleTape/tapeModuleTopControls', array(
        'items' => $tapeModuleTopControls
    ));
    ?>
    <div class="tapeModuleTableWrap<?= $pager ? ' pager' : '' ?>">
        <?php

        echo MSCore::forms()->writeModuleTableStart();
        echo MSCore::forms()->writeModuleTableHeader($CONFIG['tables']['items'], true);

        foreach ($items as $item) {

            echo MSCore::forms()->moduleTableCell($table_config, $item, '', $item['editAction']);
        }

        echo MSCore::forms()->writeModuleTableEnd('');
        ?>
        <?= $pager ?>
    </div>
</div>
