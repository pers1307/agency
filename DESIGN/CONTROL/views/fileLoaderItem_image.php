<?php
    /**
     * User: Andruha
     * Date: 20.06.13
     * Time: 13:48
     */

    $itemData = array_merge(array(
        'text' => ''
    ), $itemData);

    isset($showLabel) or $showLabel = true;
?>
<div class="fileLoader-item fileLoader-itemImage clearfix">
    <div class="fileLoader-itemImage-wrap">
        <img src="<?php echo $itemData['path']['sys_thumb'] ?>" alt="">
    </div>
    <?php
        if ($showLabel)
        {
            ?>
            <label class="fileLoader-item-text">
                <input type="text" name="<?php echo $sysName ?>[text]" value="<?php echo safe($itemData['text']) ?>">
            </label>
        <?php
        }
    ?>
    <span class="fileLoader-deleteItem">
        <i class="icon-trash" title="Удалить"></i>
    </span>
    <input type="hidden" name="<?php echo $sysName ?>[id]" value="<?php echo $itemData['id'] ?>">
</div>