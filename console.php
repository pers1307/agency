<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 17.07.14
 * Time: 15:11
 */

require_once 'config.php';

/** Базовый класс */
require_once CORE_DIR . DS . 'classes' . DS . 'MSCore.php';

/** Модуль дебага */
require_once CORE_DIR . '/includes/error_handling.php';

/** Функции авторизации */
require_once CORE_DIR . '/includes/authorizate_functions.php';

/** Глобальные функции */
require_once CORE_DIR . '/includes/global_functions_console.php';

/** Функции работы с изображениями */
require_once CORE_DIR . '/includes/images_functions.php';

/** Пользовательские функции */
require_once CORE_DIR . '/includes/user_functions.php';

define('AUTH_IS_ROOT', false);