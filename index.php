<?php
    define('START_GEN', microtime(true));

    /** Кодировка сайта */
    define('SITE_ENCODING', 'UTF-8');

    /** Установка временной зоны */
    date_default_timezone_set('Asia/Yekaterinburg');

    /** Установка локали */
    setlocale(LC_ALL, array('ru_RU.' . SITE_ENCODING, 'rus_RUS.' . SITE_ENCODING, 'russian', 'ru_RU', 'ru'));

    if (PHP_VERSION < 5.6)
    {
        header('HTTP/1.1 503 Service Temporarily Unavailable');
        header('Status: 503 Service Temporarily Unavailable');
        header('Retry-After: 3600');
        die("Требуется версия PHP 5.6+");
    }

    $last_modified = gmdate('D, d M Y H:i:s', time()) . ' GMT';

    if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
    {

        $if_modified_since = preg_replace('/;.*$/', '', $_SERVER['HTTP_IF_MODIFIED_SINCE']);

        if ($if_modified_since == $last_modified)
        {

            header('HTTP/1.0 304 Not Modified');
            header('Cache-Control: max-age=86400, must-revalidate');
            exit;

        }

    }

    header('Cache-Control: max-age=86400, must-revalidate');
    header('Last-Modified: ' . $last_modified);

    /** Подключение основного конфига */
    require_once(dirname(__FILE__) . DIRECTORY_SEPARATOR . 'config.php');

	if (HTTPS_REDIRECT) {
		if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
			$redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $redirect);
			exit();
		}
	}

    /** @noinspection PhpIncludeInspection */
    /** Подключение ядра */
    require_once(CORE_DIR . DS . 'core.php');