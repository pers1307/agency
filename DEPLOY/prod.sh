#!/bin/sh

git pull origin master
php composer.phar self-update
php composer.phar update
# usage migration https://github.com/ruckus/ruckusing-migrations/wiki
bash migrate.sh db:migrate
bash phpunit.sh