<?php


    require_once dirname(__FILE__).'/../console.php';

    // Прмер конфига, конфиг можно не передавать, он составится сам из существующих модулей
    $tables = array(
        'mp_catalog_sale_items' => array(
            'primary_key' => 'id',
            'files_fields' => array(
                'gallery'
            )
        ),
        'mp_catalog_rent_items' => array(
            'primary_key' => 'id',
            'files_fields' => array(
                'gallery',
                'img'
            )
        ),
        'mp_rent_orders_files' => array(
            'primary_key' => 'id',
            'files_fields' => array('file')
        )
    );

    // create an exemplar of files garbage collector
    $gcf = new MSGCF();

    // synchronize the files and database entries
    $gcf->sync();