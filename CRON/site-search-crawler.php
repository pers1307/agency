<?php

/**
 * В поле текст будет заполнять только содержимое тэга p
 */

require_once dirname(__DIR__) . '/console.php';
function safe_str($str){
    return htmlspecialchars(html_entity_decode($str, ENT_QUOTES), ENT_QUOTES);
}

function normalize($str){
    return html_entity_decode($str, ENT_QUOTES);
}

function url($url){
    if(!trim($url)){
        $url = '/';
    }

    $url =
        preg_replace('/#(.*)?/sim', '',
            preg_replace('#/+#', '/',
                preg_replace('#\\\+#', '/', //WARNING возможно не стабильно =)
                    preg_replace('#^([a-z0-9]+://)?(www\.)?'.HTTP_HOST.'#i', '', $url))));

    if(strpos($url, '?') === false){
        $url = preg_replace('#/+$#i', '', $url);
    }

    if($url == ''){
        $url = '/';
    }

    return $url;
}

function _mctime() {
    list ($sec, $msec)= explode(' ', microtime());
    return $sec + $msec;
}


$TIME = array();

$host = isset($_REQUEST['host']) ? $_REQUEST['host'] : CONFIG_DBHOST;
$user = isset($_REQUEST['user']) ? $_REQUEST['user'] : CONFIG_DBUSER;
$pass = isset($_REQUEST['pass']) ? $_REQUEST['pass'] : CONFIG_DBPASS;
$dbname = isset($_REQUEST['dbname']) ? $_REQUEST['dbname'] : CONFIG_DBNAME;
$sitehost = isset($argv[1]) ? $argv[1] : DOMAIN;

if ($host=='' || $user=='')
{
    echo 'Не указан доступ к БД MySQL';
    exit;
}

$link = mysqli_connect($host, $user, $pass)
or die("Could not connect to DB");
mysqli_select_db($link, $dbname) or die("Could not select DB");

define('DB_TABLE_SEARCH', 'mp_search_index');
//define('HTTP_HOST', $sitehost);
defined('HTTP_HOST') or define('HTTP_HOST', $sitehost);

//Время, до которого будет работать скрипт.
//На всякий случай только 80% разрешенного времени
$TIME['stop'] = _mctime() + ( ( ($met = ini_get('max_execution_time') )? $met : 40)*0.8);


/*======================================================================*/

$main_SQL = 'SET NAMES `utf8`;';
$main = mysqli_query($link, $main_SQL);

$main_SQL = 'SELECT `uri` FROM `'.DB_TABLE_SEARCH.'` ORDER BY `time` LIMIT 300';
$main = mysqli_query($link, $main_SQL);

if(mysqli_num_rows($main)){
    $r = array('uri' => '/');
}else{
    $r = mysqli_fetch_assoc($main);
}

$i = 1;
$new_links = 0;
$size = 0;

do{
    $url = 'http://'.HTTP_HOST.normalize(preg_match('/\/\?|#/', $r['uri']) || $r['uri'] == '/' ? $r['uri'] : $r['uri'] . '/');
    $sql_uri = safe_str(url($url));
    $lang = preg_match('/^\/en\/(.*)/i', $r['uri']) ? 'en' : '';

    //Открываем
    $c = @file_get_contents($url);
    $responseStatus = isset($http_response_header[0]) ? $http_response_header[0] : null;

    //Читаем титл
    if(!preg_match('#\<!--\[TITLE\]--\>(.*?)\<!--\[/TITLE\]--\>#si', $c, $p)) {
        preg_match('#\<title>(.*?)<\/title\>#si', $c, $p);
    }

    $title = @$p[1];
    $title = strip_tags($title);
    $linksOnly = preg_match('#\<!--\[index_links\]--\>#si', $c);

    if(!$c || preg_match('#\<!--\[noindex_page\]--\>#si', $c)){
        //mysql_query('DELETE FROM `'.DB_TABLE_SEARCH.'` WHERE `uri` = "'.$sql_uri.'"');
        //continue;
    }

    //Берем индекс-зоны
    preg_match_all('#\<!--\[INDEX(\*)?\]--\>(.*)\<!--\[/INDEX\]--\>#siU', $c, $p);
    $c = implode(' ', $p[2]);
    $c = preg_replace('#\<!--\[noindex\*\]--\>(.*?)\<!--\[/noindex\*\]--\>#si', '', $c);

    //Берем список ссылок и приводим их к стандарту
    preg_match_all('#\<a[^\>\-]+href="([^http,^https,^javascript,^mailto,^ftp,^\#].*?)"#sim', $c, $p2);
    $links = array_unique(array_map('url', $p2[1]));
    $linksCount = count($links);

    //Избавляемся от ссылок на левые ресурсы (картинки, архивы и пр..)
    foreach ($links as $_k => $_l){
        if(preg_match('#\.([a-z0-9]+)$#i', $_l, $p3)){
            if(!in_array(strtolower($p3[1]), array('htm', 'html', 'php', 'phtml', 'php3', 'php4', 'txt')))
                unset($links[$_k]);
        }
    }

    unset($p2);
    unset($c);


    $content = '';

    foreach ($p[1] as $_k => $_v){

        $result = '';

        /**
         * Дергаем из content только содержимое тэгов p
         */
        if (preg_match_all('%(<p[^>]*>.*?</p>)%i', $p[2][$_k], $regs)) {

            foreach ($regs as $reg) {

                foreach ($reg as $item)

                $result .= $item;
            }
        }

        $content .= $result;
    }
    unset($p);

    //Убираем тэги и всякую фигню из индекса
    $content = preg_replace('#\<!--\[?noindex\]?--\>(.*?)\<!--\[?/noindex\]?--\>#si', '', $content);
    $content = preg_replace('#\<select(.*?)<\/select\>#si','',$content);
    $content = preg_replace('#\<table(.*?)<\/table\>#si','',$content);
    $content = preg_replace('#\<script(.*?)<\/script\>#si','',$content); // режем скрипты (c) Andruha
    $content = preg_replace('#\<style(.*?)<\/style\>#si','',$content); // режем стили (c) Andruha
    $content = preg_replace('#\<br\>|\<br\/\>#si',' ',$content);
    $content = preg_replace('#\>\n?\<#si','> <', $content);
    $content = preg_replace('/(\s+)|(&[a-zA-Z0-9#]+;)/', ' ', strip_tags($content));

    //Сохраняем НОВЫЕ ссылочки в базу
    if(count($links) > 0){
        //Берем из ссылочек только новые и суем их в БД
        $sql = 'SELECT `uri` FROM `'.DB_TABLE_SEARCH.'` WHERE `uri` IN ("'.implode('","', array_map('safe_str', $links)).'")';
        $exists = array();
        $res = mysqli_query($link, $sql);
        while( $r = mysqli_fetch_array($res) ){
            $exists[] = $r[0];
        }

        if(count($exists)){
            $links = array_diff($links, array_map('normalize', $exists));
            unset($exists);
        }

        if($links){
            $sql = 'INSERT INTO `'.DB_TABLE_SEARCH.'` (`uri`) VALUES ("'.implode('"),("', array_map('safe_str', $links)).'")';
            if(mysqli_query($link, $sql)){
                $new_links += count($links);
            }
        }
    }

//    print_r(array(
//        'uri' => $sql_uri,
//        'title' => $title,
//        'content' => $content
//    ));

    if(!empty($content) || !empty($title)) {
        //Прибиваем ненужное (старое), короче - обновляем.
        mysqli_query($link, 'DELETE FROM `'.DB_TABLE_SEARCH.'` WHERE `uri` = "'.$sql_uri.'"');
        if(mysqli_query($link, '
	    	INSERT INTO `'.DB_TABLE_SEARCH.'`
	    		SET
	    			`uri` = "'.$sql_uri.'",
	    			`title` = "'.(mysqli_real_escape_string($link, $title)).'",
	    			`text` = "'.(mysqli_real_escape_string($link, $content)).'",
	    			`time` = UNIX_TIMESTAMP(),
	    			`lang` = "'.$lang.'",
	    			`status` = "'.$responseStatus.'",
	    			`links_count` = ' . $linksCount . '
	    	'))
        {
            $size = $size + (strlen($title) + strlen($content));
        }
    }

    if($i++ < 5 && is_resource($main) && (mysqli_num_rows($main) < 30)){
        $main = mysqli_query($link, $main_SQL);
    }

}while( (_mctime() < $TIME['stop']) && ($r = mysqli_fetch_array($main)) );
mysqli_query($link, 'OPTIMIZE TABLE `'.DB_TABLE_SEARCH.'`');