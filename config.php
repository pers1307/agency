<?php
    define('NOW_TIME', time());

    /** Установка внутренней кодировки */
    defined('SITE_ENCODING') or define('SITE_ENCODING', 'UTF-8');
    mb_internal_encoding(SITE_ENCODING);

    /** Определяем системные директории:
     * DS — альяс для константы DIRECTORY_SEPARATOR
     * DOC_ROOT — корень сайта
     * CORE_DIR — папка с ядром системы
     * MODULES_DIR — папка с модулями
     * DES_DIR — папка с дизайном
     * BLOCKS_DIR — папка с блоками вывода
     * FILES_DIR — папка с загружаемыми файлами
     * TMP_DIR — временная папка
     */

    define('DS',  DIRECTORY_SEPARATOR);
    define('DOC_ROOT', dirname(__FILE__));
    define('CORE_DIR', DOC_ROOT . DS . 'CORE');
    define('MODULES_DIR', DOC_ROOT . DS . 'MODULES');
    define('DES_DIR', DOC_ROOT . DS . 'DESIGN');
    define('BLOCKS_DIR', DES_DIR . DS . 'SITE' . DS . 'blocks');
    define('FILES_DIR', DOC_ROOT . DS . 'UPLOAD');
    define('TMP_DIR', DOC_ROOT . DS . 'TMP');
    define('VENDOR_DIR', DOC_ROOT . DS . 'VENDOR');
    define('CONFIG_DIR', CORE_DIR . DS . 'configs');

    /** Проверяем, есть ли файл с локальными настройками. Если есть, то подключаем его */
    if (is_file($localConfig = DOC_ROOT . DS . 'config.local.php')) {

        require_once $localConfig;
    }

    /** Определяем откуда пользователь (из МС или нет) */
    defined('MS') or define('MS', (isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], array('82.193.139.18'))));

    /** Имя сайта (домен) */
    // TODO: Вписать реальны домены
    $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'real-site-domain.ru';
    defined('DOMAIN') or define('DOMAIN', str_replace('www.', '', $domain));

    defined('DOMAIN_MOBILE') or define('DOMAIN_MOBILE', str_replace('www.', '', 'm.real-site-domain.ru'));
    // defined('DOMAIN_DESKTOP') or define('DOMAIN_DESKTOP', str_replace('www.', '', 'real-site-domain.ru'));

    define('HTTP_HOST', $domain);

    /** Имя сессии которое будет установлено в куку */
    define('SESS_NAME', 'mp6' . str_replace('.', '', DOMAIN));

    define('REMOTE_ADDR', isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'unknown');

    /** Реквизиты для базы данных (продакшн) */
    defined('CONFIG_DBHOST') or define('CONFIG_DBHOST', 'localhost');
    defined('CONFIG_DBUSER') or define('CONFIG_DBUSER', 'root');
    defined('CONFIG_DBPASS') or define('CONFIG_DBPASS', 'root');
    defined('CONFIG_DBNAME') or define('CONFIG_DBNAME', 'publisher');

    /** Уровень ошибок */
    defined('ERROR_REPORTING') or define('ERROR_REPORTING', 0);
    error_reporting(defined(ERROR_REPORTING) ? constant(ERROR_REPORTING) : ERROR_REPORTING);

    /** Префикс для таблиц БД */
    define('PRFX', 'mp_', true);

    /** Сжимать JS */
    defined('COMPRESS_JS') or define('COMPRESS_JS', true);

    /** Сжимать CSS */
    defined('COMPRESS_CSS') or define('COMPRESS_CSS', true);

    /** Делать редирект c domain.ru на www.domain.ru */
    defined('WWW_REDIRECT') or define('WWW_REDIRECT', true);

    //Директива для HTTPS
    defined('HTTPS_REDIRECT') or define('HTTPS_REDIRECT', false);

    /** БЕЗОПАСНОСТЬ */
    /** Авторизация для уникального айпи */
    define('CHECK_UNIQ_IP', 0);
    define('ROOT_PLACE', 'admin');
    /** Дефолтный доступ 0 - запрещено все, 1 - разрешено все */
    define('DEFAULT_ACCESS', 0);
    /** Отображать системные страницы для ROOT */
    define('SHOW_SYSTEM_PAGES', false);

    /** Включить дебаг функцию */
    defined('MS_DEBUG') or define('MS_DEBUG', false);

    /** Настройки cookie*/
    defined('COOKIE_SALT') or define('COOKIE_SALT', '3292a11b524a41aac4993659fc1a98cd');
    defined('COOKIE_LIFETIME') or define('COOKIE_LIFETIME', '31556926');

    define('IS_WIN', strtoupper(substr(PHP_OS, 0, 3)) === 'WIN');

    /** Пути до подключаемых файлов стилей и скриптов */
    $assetsVersion = (int) @file_get_contents(CONFIG_DIR . '/assets.txt');
    defined('ASSETS_VERSION') or define('ASSETS_VERSION', $assetsVersion);
    $assetsContent = json_decode(file_get_contents(DES_DIR . '/assets.json'), true);
    $GLOBALS['staticFiles'] = array_merge(array(), $assetsContent['css'], $assetsContent['js']);