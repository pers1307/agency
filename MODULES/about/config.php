<?php
/**
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
 */

$module_name = 'about';
$module_caption = 'О компании';

$CONFIG = array(
    'module_name' => $module_name,
    'module_caption' => $module_caption,
    'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
    'version' => '2',
    'tables' => array(
        'items' => array(
            'db_name' => $module_name,
            'dialog' => array('width' => 660, 'height' => 410),
            'key_field' => 'id',
            'config' => array(

                'title' => array(
                    'caption' => 'Название',
                    'value' => '',
                    'type' => 'string',
                ),
                'text' => array(
                    'caption' => 'Текст',
                    'value' => '',
                    'type' => 'wysiwyg'
                ),

                array(
                    'type' => 'divider',
                ),
                array(
                    'type' => 'section',
                    'caption' => 'Блок статистики',
                    'output' => 'open',
                ),
                'title_stats' => array(
                    'caption' => 'Заголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                'subtitle_stats' => array(
                    'caption' => 'Подзаголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                'params' => array(
                    'caption' => 'Показатели',
                    'value' => '',
                    'type' => 'options_without_thead',
                    'labels' => [
                        'key'   => 'Параметр',
                        'value' => 'Числовое значение'
                    ]
                ),
                array(
                    'type' => 'section',
                    'output' => 'close',
                ),
                array(
                    'type' => 'divider',
                ),

                array(
                    'type' => 'divider',
                ),
                array(
                    'type' => 'section',
                    'caption' => 'Блок сотрудников',
                    'output' => 'open',
                ),
                'title_staff' => array(
                    'caption' => 'Заголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                'subtitle_staff' => array(
                    'caption' => 'Подзаголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                'staff' => array(
                    'caption' => 'Агенты',
                    'value' => '',
                    'values' => [],
                    'from' => array(
                        'table_name' => PRFX . 'agents',
                        'key_field' => 'id',
                        'name_field' => 'title',
                    ),
                    'type' => 'select',
                    'multi' => true
                ),
                array(
                    'type' => 'section',
                    'output' => 'close',
                ),
                array(
                    'type' => 'divider',
                ),

                array(
                    'type' => 'divider',
                ),
                array(
                    'type' => 'section',
                    'caption' => 'Блок c текстом',
                    'output' => 'open',
                ),
                'title_text' => array(
                    'caption' => 'Заголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                'subtitle_text' => array(
                    'caption' => 'Подзаголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                array(
                    'type' => 'section',
                    'output' => 'close',
                ),
                array(
                    'type' => 'divider',
                ),

                array(
                    'type' => 'divider',
                ),
                array(
                    'type' => 'section',
                    'caption' => 'Блок c продажей объекта',
                    'output' => 'open',
                ),
                'title_property' => array(
                    'caption' => 'Заголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                'subtitle_property' => array(
                    'caption' => 'Подзаголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                array(
                    'type' => 'section',
                    'output' => 'close',
                ),
                array(
                    'type' => 'divider',
                ),
            ),
        ),
    ),
);

return $CONFIG;