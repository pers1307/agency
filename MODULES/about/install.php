<?php

$tableName = PRFX . $module['module_name'];

MSCore::db()->execute("
        CREATE TABLE IF NOT EXISTS `{$tableName}` (
          `path_id` INT(11) NOT NULL,
          `key` VARCHAR(255) NOT NULL,
          `value` TEXT,
          PRIMARY KEY (`path_id`,`key`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
        ");