<?php

    /**
     * Рекурсивное изменение `path`
     */
    function changeUrl($parent, $oldUrl, $newUrl)
    {
        $items = MSCore::db()->getAll("SELECT `path`, `path_id` FROM `".PRFX."www` WHERE `parent` = {$parent}");

        foreach ($items as $item)
        {
            $regExp = '/^' . preg_quote($oldUrl, '/') . '/';
            $newPath = MSCore::db()->pre(preg_replace($regExp, $newUrl, $item['path'], 1));

            $sql = "UPDATE `".PRFX."www` SET `path` = '{$newPath}' WHERE `path_id` = {$item['path_id']}";

            changeUrl($item['path_id'], $item['path'], $newPath);

            MSCore::db()->execute($sql);
        }

    }

    /**
     * Рекурсивное удаление детей страницы
     *
     * @param integer $parent
     */
    function delete_childs($parent = 0)
    {
        $cur_del = MSCore::db()->getAll("SELECT `path_id` FROM `" . PRFX . "www` WHERE `parent`='" . MSCore::db()->pre($parent) . "'");
        if (sizeof($cur_del))
        {
            foreach ($cur_del as $cd)
            {
                MSCore::db()->execute("DELETE FROM `" . PRFX . "www` WHERE `path_id`='" . MSCore::db()->pre($cd['path_id']) . "'");
                delete_childs($cd['path_id']);
            }
        }
        else
        {
            return;
        }
    }

    /*
    ERROR_CODES:
    0 - успешно сохранено
    1 - задано пустое системное имя
    2 - указано существующее системное имя
    3 - системное имя содержит недопустимые символы

    */
    function add_new_path($parent_id = 0, $sys_name = '', $header = 'нет заголовка', $title = '', $title_menu = '', $design = 0, $www_type)
    {
        global $nodel, $noadd, $noview, $notfound, $getVars;

        if ($sys_name == '')
        {
            return 1;
        }


        $allowed_chars = ' qwertyuiopasdfghjklzxcvbnm01234567890_-.,';
        for ($p = 0; $p < mb_strlen($sys_name); $p++)
        {
            if (mb_strpos($allowed_chars, $sys_name[$p]) == false)
            {
                return 3;
            }
        }


        $new_path = EndSlash(MSCore::urls()->tree[MSCore::urls()->ids[$parent_id]]['path']) . mb_strtolower($sys_name);


        $title_menu = $title_menu ? $title_menu : $header;
        $title = $title ? $title : $header;

        if (isset(MSCore::urls()->tree[trim($new_path, '/')]))
        {
            return 2;
        }

        if ($new_path[0] == '/')
        {
            $new_path[0] = '';
        }


        //return $new_path;

        $sql = "SELECT `structure` FROM `" . PRFX . "tpl` WHERE `id` = " . (int)$design;
        $temp = unserialize(MSCore::db()->getOne($sql));

        // Зададим ордер
        $sql = "SELECT `order` FROM `" . PRFX . "www` WHERE `parent` = '" . (int)$parent_id . "' AND `path_id` NOT IN (2,3,4,5) ORDER BY `order` DESC LIMIT 1";
        $order = MSCore::db()->getOne($sql);

        $sql = array();
        $sql[] = "`parent` = '" . MSCore::db()->pre($parent_id) . "'";
        $sql[] = "`path` = '" . MSCore::db()->pre($new_path) . "'";
        $sql[] = "`config` = '" . MSCore::db()->pre(serialize(isset($temp['config']) ? $temp['config'] : array())) . "'";
        $sql[] = "`www_type` = '" . MSCore::db()->pre($www_type) . "'";
        $sql[] = "`order` = '" . ((int)$order + 1) . "'";
        $sql[] = "`visible` = 0";
        $sql[] = "`header` = '" . MSCore::db()->pre($header) . "'";
        $sql[] = "`title_page` = '" . MSCore::db()->pre($title) . "'";
        $sql[] = "`title_menu` = '" . MSCore::db()->pre($title_menu) . "'";
        $sql[] = "`meta_description` = ''";
        $sql[] = "`meta_keywords` = ''";
        $sql[] = "`main_template` = '" . MSCore::db()->pre($temp['main_template'] != "" ? $temp['main_template'] : 'inner.php') . "'";

        $sql[] = "`noadd` = " . $noadd;
        $sql[] = "`noview` = " . $noview;
        $sql[] = "`nodel` = " . $nodel;
        $sql[] = "`notfound` = " . $notfound;
        $sql[] = "`getvars` = '" . $getVars . "'";

        $sql = "INSERT INTO `" . PRFX . "www` SET " . implode(',', $sql);
        MSCore::db()->execute($sql);

        return 0;
    }

    function unic_path($cur_path)
    {
        $cur_path .= mt_rand(0, 9999);
        if (isset(MSCore::urls()->tree[$cur_path]))
        {
            unic_path($cur_path);
        }

        return $cur_path;
    }


    /**
     * Возвращает ID страницы с которой необходимо поменять местами
     *
     * @param integer $order
     * @param int $parent
     * @param boolean $up
     *
     * @return integer
     */
    function get_page_to_swap($order = 0, $parent = 0, $up = true)
    {
        if ($order == 0 || $parent == 0)
        {
            return false;
        }
        $order = ($up) ? $order - 1 : $order + 1;

        $sql = "SELECT `path_id` FROM `" . PRFX . "www` WHERE `order` = '" . (int)$order . "' AND `parent` = '" . (int)$parent . "';";

        return MSCore::db()->getOne($sql);
    }

    function reset_orders($parent = 1)
    {
        global $already;

        $sql = "SELECT `path_id` FROM `" . PRFX . "www` WHERE `parent` = '" . (int)$parent . "' AND `path_id` NOT IN (2,3,4,5) ORDER BY `order`;";

        $paths = MSCore::db()->getCol($sql);
        if (sizeof($paths))
        {
            $start = 0;
            foreach ($paths as $path_id)
            {
                if (isset($already[$path_id]) && $already[$path_id])
                {
                    continue;
                }

                $already[$path_id] = 1;

                $start++;

                $sql = "
				UPDATE `" . PRFX . "www` SET `order` = '" . $start . "' WHERE `path_id` = '" . (int)$path_id . "';
			";

                MSCore::db()->execute($sql, true, false);
                $tree = MSCore::urls()->tree;
                $tree[MSCore::urls()->ids[$path_id]]['order'] = $start;
                MSCore::urls()->tree = $tree;
                reset_orders($path_id);
            }
        }
    }

    function write_parent_Form($data_content)
    {
        $content = "
			<form method=\"post\" id=\"parent_form\" enctype=\"multipart/form-data\">
				" . $data_content . "
				<table cellspacing=\"0\" cellpadding=\"0\">
					<tr>
						<td><a href=\"#\" onclick=\"doLoad(document.getElementById('parent_form'),'/" . ROOT_PLACE . "/ajax/update_parent/','parent_form_div;parent_report', 'post', 'rewrite', callback_updateTree); closeAjaxWorking();\" class=\"save\">Сохранить</a></td>
						<td width=\"80\" align=\"right\"><div id=\"parent_report\" style=\"font-weight: bold\"></div></td>
					</tr>
				</table>
			</form>
		";

        return $content;
    }

    function writeBlockListInContent($info_path, $zone, $path_id)
    {
        $content = "<Table>";
        foreach ($info_path[$zone] as $key => $data)
        {
            $content .= "<tr><td width=\"5\">
			<a href=\"#\" title=\"Удалить блок\" onclick=\"doLoad('" . $key . "','/" . ROOT_PLACE . "/ajax/delete_script/" . $path_id . "/" . $key . "/" . $zone . "/','" . $path_id . $zone . "');\" class=\"delete\"><i class=\"icon-trash\"></i></a></td>
			<td width=\"5\"><a href=\"#\" title=\"Переместить вверх\" onclick=\"doLoad('" . $key . "','/" . ROOT_PLACE . "/ajax/swap_scripts/" . $path_id . "/" . $key . "/up/" . $zone . "/','" . $path_id . $zone . "');\" class=\"up\"><i class=\"icon-chevron-up\"></i></a></td>
			<td width=\"5\"><a href=\"#\" title=\"Переместить вниз\" onclick=\"doLoad('" . $key . "','/" . ROOT_PLACE . "/ajax/swap_scripts/" . $path_id . "/" . $key . "/down/" . $zone . "/','" . $path_id . $zone . "');\" class=\"down\"><i class=\"icon-chevron-down\"></i></a></td>
			<td>" . getScriptName($data['script']) . "</td></tr>";
        }
        $content .= "</table>";

        return $content;
    }