<?php
    ob_clean();
    
// Развернутость дерева
    $GLOBALS['menu_tree_opened'] = array();
    if (isset($_SESSION['menu_tree_opened']))
    {

        $GLOBALS['menu_tree_opened'] = $_SESSION['menu_tree_opened'];

    };


    switch (MSCore::urls()->vars[1])
    {
        case 'save_field_data':
        {
            debug(MSCore::urls()->vars);
            $_RESULT = array('content' => 'saved');
            die(json_encode($_RESULT));
        }
            break;

        case 'www_pages_copy':
        {
            $from_id = isset($_REQUEST['from_vetka']) ? $_REQUEST['from_vetka'] : 0;;
            $to_id = isset($_REQUEST['to_vetka']) ? $_REQUEST['to_vetka'] : 0;

            if (isset($_REQUEST['www_pages_action']) && $from_id > 0 && $to_id > 0)
            {
                switch ($_REQUEST['www_pages_action'])
                {
                    case 'quick':
                    {
                        $res = copyPages($from_id, $to_id, 'quick');
                    }
                        break;

                    case 'config':
                    {
                        $res = copyPages($from_id, $to_id, 'config');
                    }
                        break;

                    case 'all':
                    {
                        $res = copyPages($from_id, $to_id, 'all');
                    }
                        break;

                    case 'childs_quick':
                    {
                        $res = copyPages($from_id, $to_id, 'quick', true);
                    }
                        break;

                    case 'childs_config':
                    {
                        $res = copyPages($from_id, $to_id, 'config', true);
                    }
                        break;

                    case 'childs_all':
                    {
                        $res = copyPages($from_id, $to_id, 'all', true);
                    }
                        break;


                    default:
                        $res1 = 'Ошибка копирования';
                        break;
                }
            }

            if ($res === false)
            {
                $res = $res1;
            }
            else
            {
                $res = 'Копирование завершено';
            }

            $vars = array();
            $from = getTypesNode_templates(true, 'from');
            $to = getTypesNode_templates(true, 'to');

            $_RESULT = array('content' => array($res, $from, $to));
            die(json_encode($_RESULT));
        }
            break;


        case 'apply_template':
        {
            $stpl = '';
            $mtpl = 'inner.php';
            if ((int)$_REQUEST["tpl_id"] > 0)
            {
                $tpl = MSCore::db()->getRow('SELECT structure FROM `' . PRFX . 'tpl` WHERE `id`="' . (int)$_REQUEST["tpl_id"] . '"');
                $tpl = unserialize($tpl['structure']);
                $stpl = $tpl['config'];
                $mtpl = $tpl['main_template'];
                $conf_tpl = serialize($stpl);
            }
            else
            {
                $conf_tpl = serialize(array());
            }


            $pages_ids = array();
            foreach ($_REQUEST as $key => $value)
            {
                if (preg_match("#^node(\d+)$#", $key, $m))
                {
                    $id = (int)$m[1];

                    if ($id > 0)
                    {
                        $pages_ids[] = $id;
                    }
                }
            }

            if (is_array($pages_ids) && sizeof($pages_ids) > 0)
            {
                $pages_ids = implode(',', $pages_ids);
                MSCore::db()->Execute('UPDATE `' . PRFX . 'www` SET `config`="' . MSCore::db()->pre($conf_tpl) . '", `main_template`="' . MSCore::db()->pre($mtpl) . '" WHERE `path_id` IN (' . $pages_ids . ')');

                $_RESULT = array("content" => array('Шаблон применен'));
            }
            else
            {
                $_RESULT = array("content" => array('Нет выбранных страниц'));
            }

            die(json_encode($_RESULT));
        }
            break;


        case 'copy_block':
        {
            $ids = array();

            $content = '';

            //debug ($_REQUEST);

            foreach ($_REQUEST as $key => $value)
            {
                if (preg_match("#^node(\d+)$#", $key, $m))
                {
                    $page_id = (int)$m[1];

                    if ($page_id > 0)
                    {
                        $ids[] = $page_id;

                        $config_page = MSCore::db()->getOne('SELECT config FROM `' . PRFX . 'www` WHERE `path_id`="' . MSCore::db()->pre($page_id) . '"');
                        $config_page = unserialize($config_page);

                        $modify = false;

                        if (sizeof($_REQUEST['zones']))
                        {
                            foreach ($_REQUEST['zones'] as $idx => $zone)
                            {
                                if (isset($config_page[$zone]))
                                {
                                    $exists = false;

                                    if (isset($_REQUEST['is_clear_zone']) && $_REQUEST['is_clear_zone'] == 1)
                                    {
                                        $config_page[$zone] = array();
                                    }
                                    else
                                    {
                                        foreach ($config_page[$zone] as $idx2 => $block)
                                        {
                                            if ($block['script'] == $_REQUEST['block_file'])
                                            {
                                                $exists = true;
                                                break;
                                            }
                                        }
                                    }

                                    if (!$exists)
                                    {

                                        $modify = true;
                                        $config_page[$zone][] = array(
                                            'caption' => '',
                                            'script' => (isset($_REQUEST['block_file']) && $_REQUEST['block_file'] != "" ? $_REQUEST['block_file'] : ''),
                                            'module' => (isset($_REQUEST['module']) && $_REQUEST['module'] != "" ? $_REQUEST['module'] : ''),
                                        );
                                    }
                                }
                                else
                                {
                                    $modify = true;
                                    $config_page[$zone][] = array(
                                        'caption' => '',
                                        'script' => $_REQUEST['block_file'],
                                        'module' => $_REQUEST['module'],
                                    );
                                }
                            }
                        }
                        else
                        {
                            $modify = true;
                        }

                        if ($modify)
                        {
                            $config_page = serialize($config_page);

                            $sql = 'UPDATE `' . PRFX . 'www` SET config="' . MSCore::db()->pre($config_page) . '" ' . ($_REQUEST['verstka'] != "" ? ', `main_template`="' . $_REQUEST['verstka'] . '"' : '') . ' WHERE `path_id`="' . MSCore::db()->pre($page_id) . '"';

                            MSCore::db()->execute($sql);
                        }
                    }
                }
            }

            if (is_array($ids) && sizeof($ids) > 0)
            {
                $_RESULT = array("content" => array('Шаблон применен'));
            }
            else
            {
                $_RESULT = array("content" => array('Нет выбранных страниц'));
            }

            die(json_encode($_RESULT));
        }
            break;


        case 'clean_zones':
        {
            $ids = array();
            foreach ($_REQUEST as $key => $value)
            {
                if (preg_match("#^node(\d+)$#", $key, $m))
                {
                    $id = (int)$m[1];

                    if ($id > 0)
                    {
                        $ids[] = $id;

                        $config_page = MSCore::db()->getOne('SELECT config FROM `' . PRFX . 'www` WHERE `path_id`="' . MSCore::db()->pre($id) . '"');
                        $config_page = unserialize($config_page);

                        foreach ($_REQUEST['zones'] as $idx => $zone)
                        {
                            $config_page[$zone] = array();
                        }

                        $config_page = serialize($config_page);
                        $sql = 'UPDATE `' . PRFX . 'www` SET config="' . MSCore::db()->pre($config_page) . '" WHERE `path_id`="' . MSCore::db()->pre($id) . '"';
                        MSCore::db()->execute($sql);
                    }
                }
            }

            if (is_array($ids) && sizeof($ids) > 0)
            {
                if (sizeof($_REQUEST['zones']) > 1)
                {
                    $_RESULT = array("content" => array($content, 'Зоны очищены'));
                }
                else
                {
                    $_RESULT = array("content" => array($content, 'Зона очищена'));
                }
            }
            else
            {
                $_RESULT = array("content" => array($content, 'Нет выбранных страниц'));
            }

            die(json_encode($_RESULT));
        }
            break;


        /*	test	*/
        case 'nodel':
        {
            MSCore::db()->execute('UPDATE `' . PRFX . 'www` SET `nodel`="0"');
            foreach ($_REQUEST as $key => $value)
            {
                if (preg_match("#^node(\d+)$#", $key, $m))
                {
                    $id = (int)$m[1];

                    if ($id > 0)
                    {
                        MSCore::db()->execute('UPDATE `' . PRFX . 'www` SET `nodel`="1" WHERE `path_id`="' . MSCore::db()->pre($id) . '"');
                    }
                }
            }
        }
            break;
        /*	end	*/

        case 'swap_modules':

            $items = isset($_POST['items']) ? $_POST['items'] : null;
            if (empty($items))
                Page404();

            $i = 0;

            foreach ($items as $itemId)
            {
                MSCore::db()->execute("UPDATE `".PRFX."modules` SET `order` = " . ++$i . " WHERE `module_id` = " . getInt($itemId));
            }
            die();
            break;

        case 'swap_pages':

            $items = isset($_POST['items']) ? $_POST['items'] : null;
            if (empty($items))
                Page404();

            $i = 0;

            foreach ($items as $itemId)
            {
                MSCore::db()->execute("UPDATE `".PRFX."www` SET `order` = " . ++$i . " WHERE `path_id` = " . getInt($itemId));
            }
            die();
            break;

        case 'swap_page':
        {
            $action = (isset(MSCore::urls()->vars[2]) && MSCore::urls()->vars[2] == 'up') ? 1 : 0;
            $type_id = isset(MSCore::urls()->vars[3]) ? MSCore::urls()->vars[3] : -1;
            $info_page = (int)str_replace('node', '', request('data', ''));
            $info_page = MSCore::urls()->tree[MSCore::urls()->ids[$info_page]];

            if ($type_id > -1)
            {
                switch ($action)
                {
                    // Двигаем вверх
                    case 1:
                        $to_swap = get_page_to_swap($info_page['order'], $info_page['parent'], true);
                        if (is_numeric($to_swap))
                        {
                            $sql = "
							UPDATE `" . PRFX . "www` SET `order` = '" . ((int)$info_page['order'] - 1) . "' WHERE `path_id` = '" . $info_page['path_id'] . "';
						";
                            $log_action = 'Страница "' . getTP($info_page['path_id']) . '" передвинута вверх';
                            MSCore::db()->execute($sql);

                            $sql = "
							UPDATE `" . PRFX . "www` SET `order` = '" . ((int)$info_page['order']) . "' WHERE `path_id` = '" . (int)$to_swap . "';
						";
                            MSCore::db()->execute($sql, true, false);
                        }
                        break;

                    // Двигаем вниз
                    case 0:
                        $to_swap = get_page_to_swap($info_page['order'], $info_page['parent'], false);
                        if (is_numeric($to_swap))
                        {
                            $sql = "
							UPDATE `" . PRFX . "www` SET `order` = '" . ((int)$info_page['order'] + 1) . "' WHERE `path_id` = '" . $info_page['path_id'] . "';
						";
                            $log_action = 'Страница "' . getTP($info_page['path_id']) . '" передвинута вниз';
                            MSCore::db()->execute($sql);

                            $sql = "
							UPDATE `" . PRFX . "www` SET `order` = '" . ((int)$info_page['order']) . "' WHERE `path_id` = '" . (int)$to_swap . "';
						";
                            MSCore::db()->execute($sql, true, false);
                        }
                        break;
                }

                /*MSCore::urls()->getTree();

                $type = MSCore::db()->getRow('SELECT * FROM `' . PRFX . 'www_types` WHERE id="' . (int)$type_id . '" ORDER BY `sortir` ASC');
                $content = writeTreeMenu($type);*/
                $content = '';

                $_RESULT = array("content" => array($content));
                die(json_encode($_RESULT));


            };
        }
            break;


        case 'refresh_tree':

            $parentId = getInt(request('parent-id', ''), true);
            $typeId = getInt(request('type-id', ''), true);

            $PagesTree = new PagesTree();

            if ($parentId == 1)
            {
                $_RESULT['content'] = $PagesTree->makeTree(null, $typeId)->render(true);
            }
            else
            {

                //TODO: сделать возможным добавлять страницы без типа (пока только главная обновляется в дереве)
                !empty($parentId) or $parentId = 1;

                $_RESULT['content'] = $PagesTree->makeTree($parentId)->render(true);
            }

            die(json_encode($_RESULT));

            break;


        case 'load_subtree':
            $type_id = isset(MSCore::urls()->vars[2]) ? MSCore::urls()->vars[2] : -1;
            $info_page = (int)str_replace('node', '', request('data', ''));
            $info_page = MSCore::urls()->tree[MSCore::urls()->ids[$info_page]];

            debug($info_page['path_id'], false, true);

            $_SESSION['menu_tree_opened'][$info_page['path_id']] = 1;
            $GLOBALS['menu_tree_opened'][$info_page['path_id']] = 1;


            MSCore::urls()->getTree();
            $content = generateMainPage() . getTypesNode();
            $_RESULT = array("content" => array($content));
            die(json_encode($_RESULT));

            break;
        case 'unload_subtree':
            $type_id = isset(MSCore::urls()->vars[2]) ? MSCore::urls()->vars[2] : -1;
            if ($type_id > -1)
            {
                unset($_SESSION['menu_tree_opened'][$type_id]);
                unset($GLOBALS['menu_tree_opened'][$type_id]);

            }
            break;


        /**
         * Запросили добавить модуль в зону
         */
        case 'add_module':
            $data = $_REQUEST['conf']['1'];
            $path_id = (int)$data['path_id'];
            $info_path = MSCore::urls()->tree[MSCore::urls()->ids[$path_id]]['config'];
            $info_module = MSCore::modules()->info[$data['module']];
            $zone = $data['zone'];


            if (isset($info_module['output'][$zone]) && $info_module['output'][$zone])
            {
                $info_path[$zone][] = array(
                    'caption' => '',
                    'script' => $info_module['output'][$zone],
                    'module' => $data['module'],
                );

                $sql = "UPDATE `" . PRFX . "www` SET `config` = '" . MSCore::db()->pre(serialize($info_path)) . "' WHERE `path_id` = '" . MSCore::db()->pre($path_id) . "'";
                MSCore::db()->execute($sql);
            }
            else
            {
                debug('Привязка модуля к странице не возможна. Сначала укажите блок вывода в разделе "Модули" в настройках', 1, 0, '', false);
            }

            $content = writeBlockListInContent($info_path, $zone, $path_id);

            $_RESULT = array("content" => $content);
            die(json_encode($_RESULT));
            break;


        /**
         * Меняем очередь вывода скриптов
         */
        case 'swap_scripts':
            $path_id = MSCore::urls()->vars[2]; // id пути
            $sid = MSCore::urls()->vars[3]; // id скрипта в массиве
            $action = MSCore::urls()->vars[4]; // в какую сторону двигаем
            $zone = MSCore::urls()->vars[5]; // зона в которой двигаем

            $info_path = MSCore::urls()->tree[MSCore::urls()->ids[$path_id]]['config'];
            $info_path[$zone] = array_values($info_path[$zone]);


            /**
             * Если двигают вверх и есть с чем менять
             */
            if ($action == 'up' && isset($info_path[$zone][$sid - 1]))
            {
                $temp = $info_path[$zone][$sid - 1]; //запомнили элемент с которым мы меняем
                $info_path[$zone][$sid - 1] = $info_path[$zone][$sid]; //сдвинули
                $info_path[$zone][$sid] = $temp; //передвинули старый элемент
                $sid = $sid - 1;
            }

            if ($action == 'down' && isset($info_path[$zone][$sid + 1]))
            {
                $temp = $info_path[$zone][$sid + 1]; //запомнили элемент с которым мы меняем
                $info_path[$zone][$sid + 1] = $info_path[$zone][$sid]; //сдвинули
                $info_path[$zone][$sid] = $temp; //передвинули старый элемент
                $sid = $sid + 1;
            }

            $sql = "UPDATE `" . PRFX . "www` SET `config` = '" . MSCore::db()->pre(serialize($info_path)) . "' WHERE `path_id` = '" . MSCore::db()->pre($path_id) . "'";
            MSCore::db()->execute($sql);

            $content = writeBlockListInContent($info_path, $zone, $path_id);

            $_RESULT = array("content" => $content);
            die(json_encode($_RESULT));
            break;


        case 'delete_script':
            $path_id = MSCore::urls()->vars[2]; // id пути
            $sid = MSCore::urls()->vars[3]; // id скрипта в массиве
            $zone = MSCore::urls()->vars[4]; // зона в которой двигаем

            $info_path = MSCore::urls()->tree[MSCore::urls()->ids[$path_id]]['config'];
            $info_path[$zone] = array_values($info_path[$zone]);

            if (isset($info_path[$zone][$sid]))
            {
                unset($info_path[$zone][$sid]);
            }

            $info_path[$zone] = array_values($info_path[$zone]);

            $sql = "UPDATE `" . PRFX . "www` SET `config` = '" . MSCore::db()->pre(serialize($info_path)) . "' WHERE `path_id` = '" . MSCore::db()->pre($path_id) . "'";
            MSCore::db()->execute($sql);

            $content = writeBlockListInContent($info_path, $zone, $path_id);

            $_RESULT = array("content" => $content);
            die(json_encode($_RESULT));
            break;


        /**
         * Добавление блока на страницу
         */
        case 'add_block':

            $script = request('data', '');

            $path_id = MSCore::urls()->vars[2];
            $target = MSCore::urls()->vars[3];

            $info_path = MSCore::urls()->tree[MSCore::urls()->ids[$path_id]]['config'];

            $zone = $target;

            $info_path[$zone][] = array(
                'caption' => '',
                'script' => $script,
                'module' => '',
            );

            $sql = "UPDATE `" . PRFX . "www` SET `config` = '" . MSCore::db()->pre(serialize($info_path)) . "' WHERE `path_id` = '" . MSCore::db()->pre($path_id) . "'";
            MSCore::db()->execute($sql);

            $content = writeBlockListInContent($info_path, $zone, $path_id);

            $_RESULT = array("content" => $content);
            die(json_encode($_RESULT));
            break;


        case 'update_tdk':

            $data = isset($_REQUEST['conf'][FormConfigs::FORM_ID_TDK_FORM]) ? $_REQUEST['conf'][FormConfigs::FORM_ID_TDK_FORM] : '';
            $config_save = array();

            $config = array(
                'path_id' => array(
                    'value' => '',
                    'type' => 'hidden',
                ),

                'title_menu' => array(
                    'caption' => 'Заголовок в меню',
                    'value' => '',
                    'type' => 'string',
                ),

                'header' => array(
                    'caption' => 'Заголовок страницы',
                    'value' => '',
                    'type' => 'string',
                ),

                'title_page' => array(
                    'caption' => 'Тег TITLE',
                    'value' => '',
                    'type' => 'string',
                ),

                'description' => array(
                    'caption' => 'Описание страницы (DESCRIPTION)',
                    'value' => '',
                    'type' => 'string',
                ),

                'keywords' => array(
                    'caption' => 'Ключевые слова страницы (KEYWORDS)',
                    'value' => '',
                    'type' => 'string',
                ),

            );

            foreach ($GLOBALS['PAGE_CONFIG']['pages']['config'] as $field => $data_conf)
            {
                $config[$field] = $data_conf;
            }


            if (!is_array($data) || !sizeof($data) || !isset(MSCore::urls()->ids[(int)$data['path_id']]))
            {
                $content = write_template_Form(MSCore::forms()->make($config, '', FormConfigs::FORM_ID_TDK_FORM));
                $_RESULT = array("content" => array('Ошибка изменения параметров страницы'));
                die(json_encode($_RESULT));
            }
            else
            {
                $path_id = (int)$data['path_id'];
                $title_menu = htmlspecialchars(stripslashes($data['title_menu']), ENT_QUOTES);
                $header = htmlspecialchars(stripslashes($data['header']), ENT_QUOTES);
                $title_page = htmlspecialchars(stripslashes($data['title_page']), ENT_QUOTES);
                $description = $data['description'];
                $keywords = $data['keywords'];

                $header = $header ? $header : $title_menu;
                $title_page = $title_page ? $title_page : $title_menu;

                if (is_array($config))
                {
                    $config_save = MSCore::forms()->save($config);
                }

                $pages_fileds = '';
                foreach ($GLOBALS['PAGE_CONFIG']['pages']['config'] as $field => $field_data)
                {
                    is_numeric($config_save[$field]['value']) ? $pages_fileds .= ", `" . $field . "` = " . (int)$config_save[$field]['value'] : $pages_fileds .= ", `" . $field . "` = '" . $config_save[$field]['value'] . "'";
                }

                $sql = "UPDATE `" . PRFX . "www` SET `header` = '" . MSCore::db()->pre($header) . "', `title_menu` = '" . MSCore::db()->pre($title_menu) . "', `title_page` = '" . MSCore::db()->pre($title_page) . "', `meta_description` = '" . MSCore::db()->pre($description) . "', `meta_keywords` = '" . MSCore::db()->pre($keywords) . "' " . ($pages_fileds != "" ? $pages_fileds : '') . " WHERE `path_id`='" . MSCore::db()->pre($path_id) . "'";
                MSCore::db()->execute($sql);

                $_RESULT = array("content" => array('<span class="msg-complete"><span class="icon-ok"></span> сохранено</span>'));
                die(json_encode($_RESULT));
            }
            break;


        /**
         * Сохраняем системное имя страницы
         */
        case 'save_pagepath_page':

            $data = $_REQUEST['conf'][FormConfigs::FORM_ID_PAGEPATH_FORM];

            if (!isset($data['page_path']) || !isset($data['path_id']))
            {
                $_RESULT = array('content' => 'Ошибка при сохранении');
                die(json_encode($_RESULT));
            }

            $data['page_path'] = translitUrl($data['page_path']);

            $path_cur_page = $oldPath = MSCore::db()->getOne('SELECT path FROM '.PRFX.'www WHERE path_id="' . (int)$data['path_id'] . '"');
            if ($path_cur_page != "")
            {
                $path_cur_page = trim($path_cur_page, '/');
                $path_cur_page = str_Replace("//", "/", $path_cur_page);
                $path_cur_page = explode('/', $path_cur_page);
                $path_cur_page[sizeof($path_cur_page) - 1] = $data['page_path'];
                $path_cur_page = implode('/', $path_cur_page);

                MSCore::db()->execute('UPDATE '.PRFX.'www SET path = "' . $path_cur_page . '" WHERE path_id ="' . (int)$data['path_id'] . '"');

                changeUrl($data['path_id'], $oldPath, $path_cur_page);

                $_RESULT = array('content' => '<span class="msg-complete"><span class="icon-ok"></span> сохранено</span>');
            }
            else
            {
                $_RESULT = array('content' => '<span class="msg-error"><span class="icon-remove"></span> Ошибка изменения системного имени</span>');
            }

            die(json_encode($_RESULT));

            break;


        /**
         * Сохраняем страницу как шаблон
         */
        case 'save_tpl_page':
        {
            $data = $_REQUEST['conf'][FormConfigs::FORM_ID_SAVETPL];

            if (trim($data['caption']) == "")
            {
                $_RESULT = array('content' => '<span class="msg-error"><span class="icon-remove"></span> Не указано название шаблона</span>');
            }
            elseif (!isset($data['caption']) || !isset($data['path_id']))
            {
                $_RESULT = array('content' => '<span class="msg-error"><span class="icon-remove"></span> Ошибка при сохранении</span>');
            }
            else
            {
                $info_path = MSCore::urls()->tree[MSCore::urls()->ids[$data['path_id']]];

                $info['config'] = $info_path['config'];
                $info['main_template'] = $info_path['main_template'];

                $sql[] = "`caption` = '{$data['caption']}'";
                $sql[] = "`structure` = '" . serialize($info) . "'";

                $sql = "INSERT INTO `" . PRFX . "tpl` SET " . implode(', ', $sql);
                MSCore::db()->execute($sql);


                $_RESULT = array('content' => '<span class="msg-complete"><span class="icon-ok"></span> сохранено</span>');
            }
            die(json_encode($_RESULT));
        }
            break;


        // Смена верстки
        case 'update_template':

            $data = $_REQUEST['conf'][FormConfigs::FORM_ID_TEMPLATE];

            if (!is_array($data) || !sizeof($data) || !isset(MSCore::urls()->ids[(int)$data['path_id']]))
            {
                return 'Неверно переданы параметры';
            }

            $path_id = (int)$data['path_id'];
            $design = $data['html'];
            $parent = isset($data['parent']) ? (int)$data['parent'] : null;

            $sql = "UPDATE `" . PRFX . "www` SET `main_template` = '" . MSCore::db()->pre($design) . "' WHERE `path_id` = '" . MSCore::db()->pre($path_id) . "' LIMIT 1";
            MSCore::db()->execute($sql);

            $_RESULT = array("content" => array('<span class="msg-complete"><span class="icon-ok"></span> сохранено</span>'));
            die(json_encode($_RESULT));

            break;


        // Смена родителя
        case 'update_parent':

            $data = $_REQUEST['conf'][FormConfigs::FORM_ID_PARENT];

            if (!is_array($data) || !sizeof($data) || !isset(MSCore::urls()->ids[(int)$data['path_id']]))
            {
                return 'Неверно переданы параметры';
            }

            $path_id = (int)$data['path_id'];
            $parent = (int)$data['parent'];

            $log_action = 'Изменен родитель страницы "' . getTP($path_id) . '"';

            update_paths($path_id, $parent);


            $cur_path = trim(trim(path($path_id), '/'));

            $pages = generateTreeSelectArray((int)MSCore::urls()->tree[$cur_path]['www_type']);

            $config_parent['parent'] = array(
                'caption' => 'Родитель страницы:',
                'value' => MSCore::urls()->tree[MSCore::urls()->ids[$path_id]]['parent'],
                'values' => $pages,
                'type' => 'select',
            );

            $config_parent['path_id'] = array(
                'value' => $path_id,
                'type' => 'hidden',
            );

            $content = write_parent_Form(MSCore::forms()->make($config_parent, '', FormConfigs::FORM_ID_PARENT));


            $_RESULT = array("content" => array($content, '<span class="msg-complete"><span class="icon-ok"></span> сохранено</span>'));
            die(json_encode($_RESULT));
            break;


        case 'update_rdr':

            $data = $_REQUEST['conf'][FormConfigs::FORM_ID_REDIRECT];

            if (!is_array($data) || !sizeof($data) || !isset(MSCore::urls()->ids[(int)$data['path_id']]))
            {
                return 'Неверно переданы параметры';
            }

            $path_id = (int)$data['path_id'];
            $rdr_path_id = (int)$data['rdr_path_id'];
            $rdr_url = $data['rdr_url'];

            $sql = "UPDATE `" . PRFX . "www` SET `rdr_path_id` = '" . MSCore::db()->pre($rdr_path_id) . "', `rdr_url` = '" . MSCore::db()->pre($rdr_url) . "' WHERE `path_id` = '" . MSCore::db()->pre($path_id) . "'";

            $log_action = 'Изменена инфомация о перенаправлениях страницы "' . getTP($path_id) . '"';

            MSCore::db()->execute($sql);

            $_RESULT = array("content" => array('<span class="msg-complete"><span class="icon-ok"></span> сохранено</span>'));
            die(json_encode($_RESULT));

            break;

        case 'explorer':

            $path = str_replace('//', '/', EndSlash(request('path', '')));
            $basepath = str_replace('//', '/', EndSlash(request('basepath', '')));

            //if (mb_strpos($path,$basepath) === false) die('Попытка взлома');

            /** Создадим ссыку для кнопки назад*/
            $backlink = explode('/', trim($path, '/'));
            if (sizeof($backlink) > 1)
            {
                $backlink = array_reverse($backlink);
                unset($backlink[key($backlink)]);
                $backlink = array_reverse($backlink);
            }
            $backlink = '/' . ROOT_PLACE . '/ajax/explorer/?path=' . EndSlash('/' . implode('/', $backlink)) . '&basepath=' . $basepath . '&path_id=' . request('path_id', '') . '&zone=' . request('zone', '') . '&conf=' . request('conf', '');

            $vars['path'] = $path;
            $vars['folders'] = cmsGetFoldersAndFiles(DOC_ROOT . DS . $vars['path']);
            $vars['backlink'] = $backlink;

            $content = template('explorer', $vars, 'admin');

            $_RESULT = array(
                "content" => $content,
            );
            die(json_encode($_RESULT));
            break;

        case 'update_tree':

            $content = generateMainPage() . getTypesNode();

            $_RESULT = array("content" => $content,);
            die(json_encode($_RESULT));

            break;

        case 'delete':
            $path_id = getInt(request('page-id', ''));

            if ($path_id === 0)
            {
                die();
            }

            $parentId = getInt(request('parent-id', ''), true);
            $typeId = getInt(request('type-id', ''), true);

            delete_childs($path_id);

            $sql = "DELETE FROM `" . PRFX . "www` WHERE `path_id` = '" . MSCore::db()->pre($path_id) . "' LIMIT 1";

            $log_action = 'Удалена страница "' . getTP($path_id) . '"';

            MSCore::db()->execute($sql);

            reset_orders(1);

            $PagesTree = new PagesTree();

            if ($parentId == 1)
            {
                $_RESULT['content'] = $PagesTree->makeTree(null, $typeId)->render(true);
            }
            else
            {
                $_RESULT['content'] = $PagesTree->makeTree($parentId)->render(true);
            }

            die(json_encode($_RESULT));
            break;

        case 'change_config':

            $module = MSCore::modules()->by_dir(MSCore::urls()->vars[2]);

            $output = $module['output'];
            $output[MSCore::urls()->vars[3]] = str_replace('DESIGN/', '', $_REQUEST['path']);

            $sql = "UPDATE `" . PRFX . "modules` SET `output` = '" . MSCore::db()->pre(serialize($output)) . "' WHERE `module_id` = '" . MSCore::db()->pre($module['module_id']) . "'";
            MSCore::db()->execute($sql);

            MSCore::modules()->getModulesInfo();
            $config = MSCore::modules()->by_dir(MSCore::urls()->vars[2]);

            $config['config'] = array();

            foreach (MSCore::page()->allZones as $_zone)
            {
                $config['config']['mod_' . $_zone['value']] = array(
                    'caption' => $_zone['value'],
                    'value' => (isset($config['output'][$_zone['value']]) ? $config['output'][$_zone['value']] : ''),
                    'module' => MSCore::urls()->vars[2],
                    'zone' => $_zone['value'],
                    'type' => 'explorer',
                );
            }


            $vars['_FORM_'] = MSCore::forms()->make($config['config']);
            $vars['mod'] = MSCore::urls()->vars[2];

            $content = template('module_config', $vars);

            $_RESULT = array("content" => $content);
            die(json_encode($_RESULT));
            break;

        case 'passwd_change':
            $r_img = 'icon-remove';
            $r_class = 'msg-error';

            $oldpass = $_REQUEST['oldpass'];
            $newpass = $_REQUEST['newpass'];
            $newpass2 = $_REQUEST['newpass2'];

            if (md5($oldpass) != $_USER['pass'])
                $r_txt = 'Не тот текущий пароль';
            else if ($newpass == '')
                $r_txt = 'Не указан новый пароль';
            else if ($newpass != $newpass2)
                $r_txt = 'Подтверждение пароля не совпадает';
            else if (!is_valid_pass($newpass))
                $r_txt = 'Новый пароль слишком простой или короткий';
            else
            {
                MSCore::db()->execute("UPDATE `" . PRFX . "users` SET `pass`='" . md5($newpass) . "' WHERE `user_id`=" . $_USER['user_id']);
                $r_txt = 'Пароль изменен';
                $r_img = 'icon-ok';
                $r_class = 'msg-complete';
            }

            $content = array('<span class="' . $r_class . '"><span class="' . $r_img . '"></span> ' . $r_txt . '</span>');

            $_RESULT = array("content" => $content);
            die(json_encode($_RESULT));

            break;

        case 'robotstxt_save':
            $r_img = 'not-complete.jpg';
            $r_txt = 'Ошибка при сохранении';

            if (isset($_REQUEST['robots_content']))
            {
                saveData(DOC_ROOT . DS . 'robots.txt', $_REQUEST['robots_content']);
                $saved_content = file_exists(DOC_ROOT . DS . 'robots.txt') ? file_get_contents(DOC_ROOT . DS . 'robots.txt') : '';
                if ($saved_content == $_REQUEST['robots_content'])
                {
                    $r_txt = 'Файл сохранен';
                    $r_img = 'complete.jpg';
                }
            }

            $content = array('<table><tr><td><img src="/DESIGN/CONTROL/images/' . $r_img . '">&nbsp;</td><td>' . $r_txt . '</td></tr></table>');
            $_RESULT = array("content" => $content);
            die(json_encode($_RESULT));
            break;

        case 'add_path_save':
            $info = $_REQUEST['conf']['1'];

            $parent_id = $info['root'];
            $www_type = $info['www_type'];
            $sys_name = MSCore::urls()->modify_to_valid(translitUrl($info['code']));
            $title = $info['title'];
            $header = $info['header'];
            $title_menu = $info['title_menu'];

            $noadd = isset($info['noadd']) ? 1 : 0;
            $nodel = isset($info['nodel']) ? 1 : 0;
            $noview = isset($info['noview']) ? 1 : 0;
            $notfound = isset($info['notfound']) ? 1 : 0;
            $getVars = isset($info['getvars']) ? $info['getvars'] : '';

            $r_img = 'not-complete.jpg';
            $r_txt = 'Ошибка при сохранении';

            $error_code = add_new_path($parent_id, $sys_name, $header, $title, $title_menu, (isset($info['html']) ? $info['html'] : 0), $www_type);

            $error_code_txt = array(
                0 => 'Страница добавлена',
                1 => 'Не указано системное имя',
                2 => 'Указано существующее системное имя',
                3 => 'Системное имя содержит недопустимые символы<br>разрешены a-z 0-9 _ - .,',
            );

            $r_txt = $error_code_txt[$error_code];

            if ($error_code == 0)
            {
                $content = array('
   				<script>
   					Site.pagesTree.refreshTree(' . $parent_id . ', ' . $www_type . ');
   					Site.modal.close();
   				</script>
   			'
                );
            }
            else
            {
                $content = array('<table><tr><td><img src="/DESIGN/CONTROL/images/' . $r_img . '">&nbsp;</td><td>' . $r_txt . '</td></tr></table>');
            }
            $_RESULT = array(
                'content' => $content,
                'error' => $error_code
            );
            die(json_encode($_RESULT));

            break;
    }
    die();