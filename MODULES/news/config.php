<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'news';
    $module_caption = 'Новости';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(

            'items' => array(

                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`date` DESC',
                'onpage' => 20,
                'config' => array(

                    'title' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 1,
                        'to_code' => true,
                    ),
                    'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 0,
                        'value' => '',
                    ),
                    'date' => array(
                        'type' => 'calendar',
                        'show_time' => false,
                        'caption' => 'Дата',
                        'in_list' => 1,
                        'value' => '',
                    ),
                    'image' => array(
                        'caption' => 'Картинка',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'min' => array(100, 100),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 1,
                            'module-name' => $module_name
                        ),
                        'in_list' => 1,
                        'filter' => 0,
                    ),

                    'announce' => array(
                        'caption' => 'Анонс',
                        'value' => '',
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),

                    'images' => array(
                        'caption' => 'Картинки',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'min' => array(100, 100),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 10,
                            'module-name' => $module_name
                        ),
                        'in_list' => 1,
                        'filter' => 0,
                    ),

                    'text' => array(
                        'caption' => 'Текст',
                        'value' => '',
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),

                    'files' => array(
                        'caption' => 'Файлы',
                        'value' => '',
                        'type' => 'loader',
                        'settings' => array(
                            'allowed-extensions' => null,
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 10,
                            'module-name' => $module_name
                        ),
                        'in_list' => 1,
                        'filter' => 0,
                    ),

                    'file' => array(
                        'caption' => 'Файл',
                        'value' => '',
                        'type' => 'loader',
                        'settings' => array(
                            'allowed-extensions' => null,
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 1,
                            'module-name' => $module_name
                        ),
                        'in_list' => 1,
                        'filter' => 0,
                    ),

                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    ),

                    array(
                        'type' => 'section',
                        'caption' => 'SEO',
                        'output' => 'open',
                    ),

                    'h1' => array(
                        'caption' => 'H1',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'title_page' => array(
                        'caption' => 'Page title',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_description' => array(
                        'caption' => 'Meta description',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_keywords' => array(
                        'caption' => 'Meta keywords',
                        'value' => '',
                        'type' => 'string',
                    ),

                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),
                ),
            ),
        ),

        'settings' => array(
            'onPage' => array(
                'caption' => 'Количество новостей на странице',
                'value' => 10,
                'type' => 'string'
            )
        ),


       /** Upload queue example config
        'upload' => array(
            'thumbs' => array(
                'min' => array(80, 80),
            ),
            'settings' => array(
                'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
                'size-limit' => 5 * 1024 * 1024,
                'module-name' => $module_name,
                'config-name' => 'image',
                'table-name' => $module_name,
            ),
        )*/
    );

    return $CONFIG;