<form id="zone_text_form<?= $zone ?>" style="margin:0px; padding:0px;height: 98%;">
    <input type="hidden" name="zone" value="<?= $zone; ?>">
    <input type="hidden" name="path_id" value="<?= $path_id; ?>">

    <textarea id="content<?= $zone; ?>" name="content<?= $zone; ?>" class="wysiwyg" rows="15" cols="80" style="width: 100%;height:500px;">
        <?= $content; ?>
    </textarea>

</form>

<!-- TinyMCE -->
<? require_once(CORE_DIR . '/forms/wysiwyg/config.php') ?>

<script type="text/javascript">

    function postForm() {
        doLoad(getObj('zone_text_form<?=$zone?>'), '/<?=ROOT_PLACE?>/admin/zone_wysiwyg_save/<?=$zone?>/<?=(int)$path_id?>/', 'content_div_<?=$zone?>_2');
    }
</script>
<!-- /TinyMCE -->
<div class="form-bottom-controls">
    <ul>
        <li>
            <button onclick="doLoad(getObj('zone_text_form<?= $zone ?>'),'/<?= ROOT_PLACE ?>/admin/zone_wysiwyg_save/<?= $zone ?>/<?= (int)$path_id ?>/','content_div_<?= $zone ?>_2'); Site.modal.close()">Сохранить</button>
        </li>
        <li>
            <button onclick="doLoad(getObj('zone_text_form<?= $zone ?>'),'/<?= ROOT_PLACE ?>/admin/zone_wysiwyg_save/<?= $zone ?>/<?= (int)$path_id ?>/','content_div_<?= $zone ?>_2');" class="grey">Применить</button>
        </li>
        <li>
            <button class="grey" onclick="Site.modal.close()">Отменить</button>
        </li>
    </ul>
</div>