<div class="generalModulesWrap">
    <div class="tapeModuleWrap baron-scroll">
        <div style="display: inline-block; width: 300px">
            <?php
                ob_Start();
            ?>
            <div style="width: 300px; vertical-align: top">
                <table class="table">
                    <tr>
                        <th colspan="2">Основные настройки сервера</th>
                    </tr>
                    <tr>
                        <td><b>allow_url_fopen</b></td>
                        <td><?= ini_get('allow_url_fopen'); ?></td>
                    </tr>
                    <tr>
                        <td><b>allow_url_include</b></td>
                        <td><?= ini_get('allow_url_include'); ?></td>
                    </tr>
                    <tr>
                        <td><b>disable_classes</b></td>
                        <td><?= ini_get('disable_classes'); ?></td>
                    </tr>
                    <tr>
                        <td><b>disable_functions</b></td>
                        <td><?= ini_get('disable_functions'); ?></td>
                    </tr>
                    <tr>
                        <td><b>display_errors</b></td>
                        <td><?= ini_get('display_errors'); ?></td>
                    </tr>
                    <tr>
                        <td><b>error_reporting</b></td>
                        <td><?= ini_get('error_reporting'); ?></td>
                    </tr>
                    <tr>
                        <td><b>file_uploads</b></td>
                        <td><?= ini_get('file_uploads'); ?></td>
                    </tr>
                    <tr>
                        <td><b>html_errors</b></td>
                        <td><?= ini_get('html_errors'); ?></td>
                    </tr>
                    <tr>
                        <td><b>log_errors</b></td>
                        <td><?= ini_get('log_errors'); ?></td>
                    </tr>
                    <tr>
                        <td><b>magic_quotes_gpc</b></td>
                        <td><?= ini_get('magic_quotes_gpc'); ?></td>
                    </tr>
                    <tr>
                        <td><b>magic_quotes_runtime</b></td>
                        <td><?= ini_get('magic_quotes_runtime'); ?></td>
                    </tr>
                    <tr>
                        <td><b>max_execution_time</b></td>
                        <td><?= ini_get('max_execution_time'); ?></td>
                    </tr>
                    <tr>
                        <td><b>memory_limit</b></td>
                        <td><?= ini_get('memory_limit'); ?></td>
                    </tr>
                    <tr>
                        <td><b>post_max_size</b></td>
                        <td><?= ini_get('post_max_size'); ?></td>
                    </tr>
                    <tr>
                        <td><b>register_argc_argv</b></td>
                        <td><?= ini_get('register_argc_argv'); ?></td>
                    </tr>
                    <tr>
                        <td><b>register_globals</b></td>
                        <td><?= ini_get('register_globals'); ?></td>
                    </tr>
                    <tr>
                        <td><b>register_long_arrays</b></td>
                        <td><?= ini_get('register_long_arrays'); ?></td>
                    </tr>
                    <tr>
                        <td><b>safe_mode</b></td>
                        <td><?= ini_get('safe_mode'); ?></td>
                    </tr>
                    <tr>
                        <td><b>upload_max_filesize</b></td>
                        <td><?= ini_get('upload_max_filesize'); ?></td>
                    </tr>
                    <tr>
                        <td><b>upload_tmp_dir</b></td>
                        <td><?= ini_get('upload_tmp_dir'); ?></td>
                    </tr>
                    <tr>
                        <td><b>zend.ze1_compatibility_mode</b></td>
                        <td><?= ini_get('zend.ze1_compatibility_mode'); ?></td>
                    </tr>
                </table>
            </div>

            <?php
                $out = ob_get_clean();

                $out = str_Replace('<td>1</td>', '<tD style="font-weight: bold; color: green;">Да</td>', $out);
                $out = str_Replace('<td></td>', '<tD style="font-weight: bold; color: red;">Нет</td>', $out);

                echo $out;
            ?>
        </div>
        <div style="display: inline-block; width: 300px; vertical-align: top">
            <Table class="table">
                <tr>
                    <th colspan="2">Права на основные директории</th>
                </tr>
                <Tr>
                    <td>TMP</td>
                    <td>
                        <span style="color: <?= is_writable(TMP_DIR) ? 'green' : 'red'; ?>"><?= getCHMOD(TMP_DIR); ?></span>
                    </td>
                </tr>
                <tr>
                    <td>UPLOAD</td>
                    <Td>
                        <span style="color: <?= is_writable(FILES_DIR) ? 'green' : 'red'; ?>"><?= getCHMOD(FILES_DIR); ?></span>
                    </td>
                </tr>
            </Table>
        </div>

        <?php

            function getCHMOD($file)
            {
                $perms = fileperms($file);

                if (($perms & 0xC000) == 0xC000)
                {
                    // Сокет
                    $info = 's';
                }
                elseif (($perms & 0xA000) == 0xA000)
                {
                    // Символическая ссылка
                    $info = 'l';
                }
                elseif (($perms & 0x8000) == 0x8000)
                {
                    // Обычный
                    $info = '-';
                }
                elseif (($perms & 0x6000) == 0x6000)
                {
                    // Специальный блок
                    $info = 'b';
                }
                elseif (($perms & 0x4000) == 0x4000)
                {
                    // Директория
                    $info = 'd';
                }
                elseif (($perms & 0x2000) == 0x2000)
                {
                    // Специальный символ
                    $info = 'c';
                }
                elseif (($perms & 0x1000) == 0x1000)
                {
                    // Поток FIFO
                    $info = 'p';
                }
                else
                {
                    // Неизвестный
                    $info = 'u';
                }

// Владелец
                $info .= (($perms & 0x0100) ? 'r' : '-');
                $info .= (($perms & 0x0080) ? 'w' : '-');
                $info .= (($perms & 0x0040) ?
                    (($perms & 0x0800) ? 's' : 'x') :
                    (($perms & 0x0800) ? 'S' : '-'));

// Группа
                $info .= (($perms & 0x0020) ? 'r' : '-');
                $info .= (($perms & 0x0010) ? 'w' : '-');
                $info .= (($perms & 0x0008) ?
                    (($perms & 0x0400) ? 's' : 'x') :
                    (($perms & 0x0400) ? 'S' : '-'));

// Мир
                $info .= (($perms & 0x0004) ? 'r' : '-');
                $info .= (($perms & 0x0002) ? 'w' : '-');
                $info .= (($perms & 0x0001) ?
                    (($perms & 0x0200) ? 't' : 'x') :
                    (($perms & 0x0200) ? 'T' : '-'));

                return $info;
            }

        ?>
    </div>
</div>