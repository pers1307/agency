<div class="form-wrap">
    <div>
        <form method="post" id="robotstxt" enctype="multipart/form-data">
            <textarea name="robots_content" style="height: 200px"><?= file_exists(DOC_ROOT . DS . 'robots.txt') ? file_get_contents(DOC_ROOT . DS . 'robots.txt') : '' ?></textarea>
        </form>
    </div>
</div>
<div class="form-bottom-controls">
    <ul>
        <li>
            <button onclick="doLoad(document.getElementById('robotstxt'),'/<?=ROOT_PLACE;?>/ajax/robotstxt_save/', 'robots_report','post','rewrite'); Site.modal.close()">Сохранить</button>
        </li>
        <li>
            <button class="grey" onclick="doLoad(document.getElementById('robotstxt'),'/<?=ROOT_PLACE;?>/ajax/robotstxt_save/', 'robots_report','post','rewrite');">Применить</button>
        </li>
        <li>
            <button onclick="Site.modal.close()" class="grey">Отменить</button>
        </li>
    </ul>
</div>