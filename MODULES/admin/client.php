<?php
    function getPages($type_id = 0, $show_hidden = false)
    {
        $wheres = array();
        if ($type_id > 0)
        {
            $wheres[] = '`www_type` = "' . (int)$type_id . '"';
        }
        if (!$show_hidden)
        {
            $wheres[] = '`visible` = 1';
        }

        $wheres[] = '`path_id`>10';

        $all = MSCore::db()->getAll(
            'SELECT * FROM `' . PRFX . 'www` ' . (sizeof($wheres) ? 'WHERE ' . implode(' AND ', $wheres) . '' : '') . ''
        );

        return $all;
    }

    function generateMenuDiv()
    {
        $adminMenu = array(
            0 => array(
                'caption' => 'Модули',
                'onClick' => "clearLinkPaths_and_loadQuickModule('modules', 'center', 'Управление модулями'); return false;"
            ),
            1 => array(
                'caption' => 'Зоны вывода',
                'onClick' => "clearLinkPaths_and_loadQuickModule('zones', 'center', 'Зоны вывода'); return false;"
            ),
            2 => array(
                'caption' => 'Типы модулей',
                'onClick' => "clearLinkPaths_and_loadQuickModule('modules_types', 'center', 'Типы модулей'); return false;"
            ),
            3 => array(
                'caption' => 'Типы страниц',
                'onClick' => "clearLinkPaths_and_loadQuickModule('www_types', 'center', 'Типы страниц'); return false;"
            ),
            4 => array(
                'caption' => 'Шаблоны страниц',
                'onClick' => "clearLinkPaths_and_loadQuickModule('tpl', 'center', 'Шаблоны страниц'); return false;"
            ),
            5 => array(
                'caption' => 'Информация о сервере',
                'onClick' => "openPage('center','controladminserv_info','/control/admin/serv_info/','Информация о сервере','Информация о сервере'); return false;"
            ),
            6 => array(
                'caption' => 'Информация о PHP',
                'onClick' => "openPage('center','controladminphpinfo','/control/admin/phpinfo/','Информация о PHP','Информация о PHP'); return false;"
            ),

        );

        ob_start();

        ?>
        <div class="leftMenu treeMenu baron-scroll">
            <ul>
                <li><span><i class="icon-folder-close"></i>Основные настройки</span>
                    <ul>
                        <?php

                            foreach ($adminMenu as $path => $info)
                            {
                                $onClick = empty($info['onClick']) ? "openPage('center','" . str_replace('/', '', $path) . "','/" . $path . "','" . $info['caption'] . "','" . $info['caption'] . "'); return false;" : $info['onClick'];
                                ?>
                                <li>
                                    <span><a href="#" onclick="<?php echo $onClick ?>"><?= $info['caption'] ?></a></span>
                                </li>
                            <?php
                            }
                        ?>
                    </ul>
                </li>
                <li id="modulesSettingsMenu">
                    <?php echo generateModulesSettingsMenu(); ?>
                </li>
            </ul>
        </div>
        <?php

        return ob_get_clean();
    }

    function generateModulesSettingsMenu()
    {
        ob_start();

        foreach (MSCore::modules()->info as $path => $info)
        {
            if ($path == 'admin' || $info['fastcall'] == '' || in_array(
                    $path,
                    MSCore::modules()->sys_modules
                ) || $info['installed'] == 0
            )
            {
                continue;
            }
            $path_to_config = '/' . ROOT_PLACE . '/' . $path . '/config/';
            ?>
            <li>
                <span><a href="#" onclick="openPage('center', '<?= str_replace("/", "", $path_to_config) ?>', '<?= $path_to_config ?>', '<?= $info['module_caption'] ?>', '<?= $info['module_caption'] ?>'); return false"><b><?= $info['module_caption'] ?></b></a></span>
            </li>
        <?php
        }

        $menu = ob_get_clean();

        ob_start();

        if (!empty($menu))
        {
            ?>
            <span><i class="icon-folder-close"></i>Настройки вывода модулей</span>
            <ul>
                <?php
                    echo $menu;
                ?>
            </ul>
        <?php
        }

        return ob_get_clean();
    }

    function getModulesList()
    {
        ob_start();
        ?>
        <div class="leftMenu treeMenu baron-scroll">
            <ul>
                <?php
                    $modules = getModulesListType();
                    if ($modules)
                    {
                        ?>
                        <li><span><i class="icon-folder-close"></i>Без группы</span>
                            <?= $modules ?>
                        </li>
                    <?
                    }

                    $modules_types = MSCore::db()->GetAll("SELECT * FROM `" . PRFX . "modules_types` ORDER by `order`");
                    foreach ($modules_types as $mt)
                    {

                        $modules = getModulesListType($mt['id']);

                        if ($modules)
                        {
                            ?>
                            <li><span><i class="icon-folder-close"></i><?= $mt['title'] ?></span>
                                <?= $modules ?>
                            </li>
                        <?php
                        }
                    }

                    //----------действия------------------------------------
                ?>
                <li><span><i class="icon-folder-close"></i>Другие действия</span>
                    <ul>
                        <li>
                            <span>
                                <a href="#" onclick="Site.modal.open('Общие настройки','/<?= ROOT_PLACE ?>/settings/add/'); return false;" class="no-active">Общие&nbsp;настройки</a>
                            </span>
                        </li>

                        <li>
                            <span>
                                <a href="#" onclick="Site.modal.open('Редактирование robots.txt','/<?= ROOT_PLACE ?>/admin/robotstxt/'); return false;" class="no-active">Редактировать&nbsp;robots.txt</a>
                            </span>
                        </li>

                        <li>
                            <span>
                                <a href="#" onclick="displayMessage('/<?= ROOT_PLACE ?>/admin/passwd/','400','300'); return false;" class="no-active">Сменить&nbsp;пароль</a>
                            </span>
                        </li>

                        <li><span><a href="/<?= ROOT_PLACE; ?>/exit/" class="no-active">Выход</a></span></li>
                    </ul>
                </li>
                <?
                    //----------действия------------------------------------
                ?>
            </ul>
        </div>
        <?php
        return ob_get_clean();
    }


    function getModulesListType($type = 0)
    {
        $mods = array();

        foreach (MSCore::modules()->info as $mod_name => $mod_info)
        {
            if (!$mod_info['installed'] || (!AUTH_IS_ROOT && !auth_access2Module($mod_info['module_id'])))
            {
                continue;
            }

            $cancel = false;
            if (isset($mod_info['output']))
            {
                foreach ($mod_info['output'] as $out_script)
                {
                    if (mb_strlen($out_script) > 0)
                    {
                        $cancel = true;
                        break;
                    }
                }
            }

            if ($cancel === false && $mod_info['fastcall']
                && (array_search($mod_name, MSCore::modules()->sys_modules) === false || array_search(
                        $mod_name,
                        MSCore::modules()->sys_modules_inlist
                    ) !== false)
            )
            {

                if (isset($mod_info['type']) && $mod_info['type'] != $type)
                {
                    continue;
                }

                if (!AUTH_IS_ROOT && !auth_access2Module($mod_info['module_id']))
                {
                    continue;
                }

                /** @noinspection PhpIncludeInspection */
                require(MODULES_DIR . DS . $mod_info['module_name'] . DS . 'config.php');
                if (isset($CONFIG['show']) && $CONFIG['show'] == false)
                {
                    continue;
                }
                unset($CONFIG);

                $mods[$mod_name] = $mod_info;
            }
        }

        if (empty($mods))
        {
            return '';
        }

        ob_start();

        ?>
        <ul class="generalModules-list">
            <?
                $modsCount = count($mods);

                foreach ($mods as $mod_info)
                {
                    ?>

                    <li data-id="<?php echo $mod_info['module_id'] ?>">
                        <span>
                            <?php
                                if ($modsCount > 1)
                                {
                                    ?><span class="icon-move move-handler" title="Переместить"></span><?php
                                }
                            ?>
                            <a class="top" href="#" onclick="clearLinkPaths_and_loadQuickModule('<?= $mod_info['module_name'] ?>', 'center','<?= $mod_info['module_caption'] ?>');return false;"><?= $mod_info['module_caption'] ?></a>
                        </span>
                    </li>

                <? } ?>
        </ul>
        <?

        return ob_get_clean();
    }


    function generateTreeSelectArray($type = 0)
    {
        global $vac_out;
        $temp = array();
        $temp[] = array('id' => 1,
            'parent' => 0,
            'name' => ' ');

        $nodes = MSCore::db()->getAll('SELECT * FROM `' . PRFX . 'www` WHERE `www_type`>1 ORDER BY `order`');

        if (sizeof($nodes))
        {
            foreach ($nodes as $key => $value)
            {
                $cur =& $temp[];
                $cur['id'] = (int)$value['path_id'];
                $cur['parent'] = (int)$value['parent'];
                $cur['name'] = (string)$value['title_menu'];
            }
        }

        $temp = array2tree($temp, 0);
        $vac_out = array();
        prepareLevel($temp);

        return $vac_out;
    }

    /**
     * Генерирует дерево для селектов
     */
    function prepareLevel($mass = array(), $level = 0)
    {
        global $vac_out;
        foreach ($mass as $key => $info)
        {
            $vac_out[$info['id']] = str_repeat('&nbsp;&nbsp;&nbsp;&nbsp;', $level) . ' ' . $info['name'];
            if (isset($info['child_nodes']) && sizeof($info['child_nodes']))
            {
                $level++;
                prepareLevel($info['child_nodes'], $level);
                $level--;
            }
            else
            {
                unset($mass[$key]);
            }

            if (!sizeof($mass))
            {
                return;
            }
        }
    }

    function checkCodeUniq($param)
    {

        /**
         * Чекалка уникальности символьного урла
         */
        $opts = $sqlWheres = array();

        if (isset($param['table']) && preg_match('/^[a-z_]+$/', $param['table']))
        {

            $opts['table'] = $param['table'];

        }

        if (isset($param['path_id']) && preg_match('/^[0-9]+$/', $param['path_id']))
        {

            $opts['path_id'] = $param['path_id'];

        }

        if (isset($param['parent']) && preg_match('/^[0-9]+$/', $param['parent']))
        {

            $opts['parent'] = $param['parent'];

        }

        if (isset($param['code']) && preg_match('/^[a-zA-Z_]+$/', $param['code']))
        {

            $opts['code'] = $param['code'];

        }

        if (isset($opts['table']) && isset($opts['code']))
        {

            if (isset($opts['path_id']))
            {
                $sqlWheres[] = ' `path_id` = ' . $opts['path_id'];
            }

            if (isset($opts['parent']))
            {
                $sqlWheres[] = ' `parent` = ' . $opts['parent'];
            }

            $row = MSCore::db()->getRow(
                'SELECT * FROM `' . $opts['table'] . '` WHERE `code` = "' . $opts['code'] . '"' . (count(
                    $sqlWheres
                ) ? ' AND ' : '') . implode(' AND ', $sqlWheres)
            );

            return count($row) ? 1 : 0;

        }

    }

    function write_tdk_Form($data_content, $data = array())
    {
        ob_start();

        ?>
        <form method="post" id="tdk_form" enctype="multipart/form-data">
            <?= $data_content ?>
            <div class="table">
                <div>
                    <button type="button" onclick="doLoad(getObj('tdk_form'),'/<?= ROOT_PLACE ?>/ajax/update_tdk/','tdk_report', 'post', 'rewrite', function(){
                        Site.pagesTree.refreshTree(<?= $data['parent'] ?>, <?= $data['www_type'] ?>);
                        });">Сохранить
                    </button>
                </div>
                <div id="tdk_report" class="msg-wrap"></div>
            </div>
        </form>
        <?

        return ob_get_clean();
    }

    function array2tree($source_arr, $parent_id, $key_children = 'child_nodes', $key_id = 'id', $key_parent_id = 'parent')
    {

        $tree = array();
        if (empty($source_arr))
        {
            return $tree;
        }

        _array2treer($source_arr, $tree, $parent_id, $parent_id, $key_children, $key_id, $key_parent_id);

        return $tree;
    }

    function _array2treer($source_arr, &$_this, $parent_id, $_this_id, $key_children, $key_id, $key_parent_id)
    {
        // populate current children
        foreach ($source_arr as $value)
        {
            if ($value[$key_parent_id] === $_this_id)
            {
                $_this[$key_children][$value[$key_id]] = $value;
            }
        }

        if (isset($_this[$key_children]))
        {
            // populate children of the current children
            foreach ($_this[$key_children] as $value)
            {
                _array2treer($source_arr, $_this[$key_children][$value[$key_id]], $parent_id, $value[$key_id], $key_children, $key_id, $key_parent_id);
            }

            // make the tree root look pretty (more convenient to use such tree)
            if ($_this_id === $parent_id)
            {
                $_this = $_this[$key_children];
            }
        }
    }
