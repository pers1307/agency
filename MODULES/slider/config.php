<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'slider';
    $module_caption = 'Слайдер';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(

            'items' => array(

                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(

                    'title' => array(
                        'caption' => 'Заголовок',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 1,
                        'to_code' => true,
                    ),
//                    'code' => array(
//                        'type' => 'code',
//                        'caption' => 'Символьный код',
//                        'in_list' => 0,
//                        'value' => '',
//                    ),
                    'image' => array(
                        'caption' => 'Картинка',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'min' => array(1260, 570, true),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
                            'size-limit' => 100 * 1024 * 1024,
                            'limit' => 1,
                            'module-name' => $module_name
                        ),
                        'in_list' => 1,
                        'filter' => 0,
                    ),
                    'text' => array(
                        'caption' => 'Текст',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
                    'link' => array(
                        'caption' => 'Ссылка',
                        'help' => 'Ссылка для конпки "Посмотреть"',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    ),
                ),
            ),
        ),
    );

    return $CONFIG;