<?php

    $module_name = 'settings';
    $module_caption = 'Настройки';

    return $CONFIG = array(
        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'version' => '6.0',

        'settings' => array(
            array(
                'type' => 'divider',
            ),
            array(
                'type' => 'section',
                'caption' => 'Метрика',
                'output' => 'open',
            ),
            'metrika' => array(
                'caption' => 'Код',
                'value' => '',
                'type' => 'metrika'
            ),
            array(
                'type' => 'section',
                'output' => 'close',
            ),
            array(
                'type' => 'divider',
            ),

            array(
                'type' => 'divider',
            ),
            array(
                'type' => 'section',
                'caption' => 'Общие настройки сайта',
                'output' => 'open',
            ),
            'phone' => array(
                'caption' => 'Телефон',
                'value' => '',
                'type' => 'string'
            ),
            'phone_viber' => array(
                'caption' => 'Телефон для контакта через Viber',
                'value' => '',
                'type' => 'string'
            ),
            'phone_whatsup' => array(
                'caption' => 'Телефон для контакта через Whats up',
                'value' => '',
                'type' => 'string'
            ),
            'email' => array(
                'caption' => 'Email',
                'value' => '',
                'type' => 'string'
            ),
            'address' => array(
                'caption' => 'Адрес организации',
                'value' => '',
                'type' => 'string'
            ),
            'address_map' => array(
                'caption' => 'Адрес организации (используется для карты)',
                'value' => '',
                'type' => 'geocoder',
                'in_list' => 0,
                'filter' => 0,
            ),
            'footer_text' => array(
                'caption' => 'Текст в подвале сайта',
                'value' => '',
                'type' => 'memo'
            ),
            'footer_contacts' => array(
                'caption' => 'Контакты в подвале сайта',
                'value' => '',
                'type' => 'wysiwyg'
            ),
            array(
                'type' => 'section',
                'output' => 'close',
            ),
            array(
                'type' => 'divider',
            ),
        )
    );