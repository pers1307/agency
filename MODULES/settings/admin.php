<?php
    /*
    СПИСОКОВЫЙ МОДУЛЬ, используется для создания на его основе модулей новости, статьи, faq и т.д.
     */
    header('Content-type: text/html; charset=utf-8');

    $CONFIG = MSCore::modules()->getModuleConfig(MSCore::urls()->vars[0]);

    /* Начало работы модуля - действия, реакции */
    switch (MSCore::urls()->vars[1])
    {
        case 'add':
        {
            $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
            $moduleName = (isset(MSCore::urls()->vars[3])) ? MSCore::urls()->vars[3] : MSCore::urls()->vars[0];

            $Settings = MSCore::settings($moduleName, $path_id);

            if (isset($_REQUEST['conf']))
            {
                $Settings->save();
            }
            else
            {
                $vars['CONFIG'] = $CONFIG;
                $vars['path_id'] = (int)$path_id;
                $vars['moduleName'] = $moduleName;
                $formConfig = $Settings->formConfig;
                $formConfig['code_ms'] = array(
                    'caption' => 'hidden_code',
                    'value' => '',
                    'type' => 'hidden',
                );
                $vars['_FORM_'] = MSCore::forms()->make($formConfig);

                echo template('moduleSettings/add', $vars);
            }
        }
            break;
    }

    die();