<?php
    $tableName = PRFX . $module['module_name'];

    MSCore::db()->execute("
        CREATE TABLE IF NOT EXISTS `{$tableName}` (
          `path_id` INT(11) NOT NULL,
          `module_id` INT(11) NOT NULL,
          `key` VARCHAR(255) NOT NULL,
          `value` TEXT,
          PRIMARY KEY (`path_id`,`module_id`,`key`),
          KEY `module_id` (`module_id`,`path_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;
        ");