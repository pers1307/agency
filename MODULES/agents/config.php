<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'agents';
    $module_caption = 'Сотрудники';

    $CONFIG = array(
        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(

            'items' => array(

                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(

                    'title' => array(
                        'caption' => 'Фамилия Имя',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 1,
                        'to_code' => true,
                    ),
                    'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 0,
                        'value' => '',
                    ),
                    'image' => array(
                        'caption' => 'Фото',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'list'  => array(230, 250, true),
                            'list2' => array(280, 300, true),
                            'list3' => array(770, 830, true),
                            'list4' => array(950, 1024, true),
                            'main'  => array(260, 280, true),
                            'about' => array(350, 380, true),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 1,
                            'module-name' => $module_name
                        ),
                        'in_list' => 1,
                        'filter' => 0,
                    ),
                    'position' => array(
                        'caption' => 'Должность',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'type' => array(
                        'caption' => 'Профиль',
                        'value' => '0',
                        'values' => ModelAgents::$type,
                        'type' => 'select'
                    ),
                    'email' => array(
                        'caption' => 'Email',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'work_phone' => array(
                        'caption' => 'Рабочий телефон',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'mobile_phone' => array(
                        'caption' => 'Мобильный телефон',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'description' => array(
                        'caption' => 'Описание',
                        'value' => '',
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),
                    'properties' => array(
                        'caption' => 'Объекты',
                        'value' => '',
                        'values' => [],
                        'from' => array(
                            'table_name' => PRFX . 'properties',
                            'key_field' => 'id',
                            'name_field' => 'title',
                        ),
                        'type' => 'select',
                        'multi' => true
                    ),
                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'SEO',
                        'output' => 'open',
                    ),
                    'h1' => array(
                        'caption' => 'H1',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'title_page' => array(
                        'caption' => 'Page title',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_description' => array(
                        'caption' => 'Meta description',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_keywords' => array(
                        'caption' => 'Meta keywords',
                        'value' => '',
                        'type' => 'string',
                    ),
                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),
                ),
            ),
        ),

        'settings' => array(
            'onPage' => array(
                'caption' => 'Количество сотрудников на странице',
                'value' => 10,
                'type' => 'string'
            )
        ),
    );

    return $CONFIG;