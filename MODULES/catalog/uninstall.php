<?php

$CONFIG = MSCore::modules()->getModuleConfig($module['module_name']);
$path_module = $_SERVER['DOCUMENT_ROOT'] . '/MODULES/' . $module['module_name'] . '/';

if (isset($CONFIG['tables']['articles'])) {
    MSCore::db()->execute('DROP TABLE `' . PRFX . $CONFIG['tables']['articles']['db_name'] . '`', false);
}

if (isset($CONFIG['tables']['items'])) {
    MSCore::db()->execute('DROP TABLE `' . PRFX . $CONFIG['tables']['items']['db_name'] . '`', false);
}

if (isset($CONFIG['tables']['brands'])) {
    MSCore::db()->execute('DROP TABLE `' . PRFX . $CONFIG['tables']['brands']['db_name'] . '`', false);
}

if (isset($CONFIG['tables']['brand2item'])) {
    MSCore::db()->execute('DROP TABLE `' . PRFX . $CONFIG['tables']['brand2item']['db_name'] . '`', false);
}
