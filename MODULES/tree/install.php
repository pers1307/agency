<?php

    $CONFIG = MSCore::modules()->getModuleConfig($module['module_name']);
	
	MSCore::db()->moduleType = 'catalog';

	if (isset($CONFIG['tables']['tree'])){
		
		$CONFIG['tables']['tree']['config']['visible'] = array( 'caption' => 'visible', 'value' => 0, 'type' => 'boolean', );
		
		MSCore::db()->createModuleTable($CONFIG['tables']['tree']);
		
	}

	unset($CONFIG);
	unset($module_name);