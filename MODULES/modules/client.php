<?php
    /**
     *  Управления модулями
     */
    function manage_modules()
    {
        /** Собираем инфу о не инсталлированных модулях */
        MSCore::modules()->getNotInstalledModules();

        $Controls = new ModuleControls();

        $vars_installed = array();
        $vars_notinstalled = array();
        $vars_system = array();

        /** Пробегаемся по всем модулям и отстраиваем содержимое страницы */
        foreach (MSCore::modules()->info as $module_name => $values)
        {

            $system = MSCore::modules()->isSystem($module_name);
            if ($values['installed'] && !$system)
            {
                $cur = & $vars_installed['elements'][];

                $editAction = 'displayMessage(\'/' . ROOT_PLACE . '/modules/add/0/1/' . (int)$values['module_id'] . '/\');';
                $deleteAction = "doLoad('','/" . ROOT_PLACE . "/modules/uninstall/" . $values['module_dir'] . "/','center;modulesSettingsMenu;generalModules','post');";

                $Controls->addControl(
                    'edit',
                    array(
                        'events' => array(
                            'onClick="' . $editAction . '"'
                        )
                    )
                );

                $Controls->addControl(
                    'del',
                    array(
                        'events' => array(
                            'onClick="' . $deleteAction . '"'
                        )
                    )
                );

            }
            elseif ($values['installed'] && $system)
            {
                $cur = & $vars_system['elements'][];
            }
            else
            {
                $cur = & $vars_notinstalled['elements'][];

                $addAction = "doLoad('','/" . ROOT_PLACE . "/modules/install/" . $values['module_dir'] . "/','center;modulesSettingsMenu;generalModules','post');";

                $Controls->addControl(
                    'add',
                    array(
                        'title' => 'Установить',
                        'events' => array(
                            'onClick="' . $addAction . '"'
                        )
                    )
                );

            }

            $cur['module_caption'] = $values['module_caption'];
            $cur['module_name'] = $values['module_name'];
            $cur['module_dir'] = $values['module_dir'];
            $cur['version'] = $values['version'];
            $cur['installed'] = $values['installed'];
            $cur['system'] = MSCore::modules()->isSystem($module_name);
            $cur['controls'] = $Controls->render();

        };
        $vars['inst'] = $vars_installed;
        $vars['syst'] = $vars_system;
        $vars['notinst'] = $vars_notinstalled;


        unset ($module_name, $values, $cur, $vars_installed, $vars_notinstalled, $vars_system);

        return $vars;
    }