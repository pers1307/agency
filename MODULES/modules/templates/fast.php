<div class="generalModulesWrap">
    <div id="modules" class="tapeModuleWrap baron-scroll">

        <? if (isset($inst['elements']) && sizeof($inst['elements'])): ?>
            <h3>Установленные модули</h3>

            <table>
                <tr>
                    <th width="32">№</th>
                    <th>Модуль</th>
                    <th width="80">Версия</th>
                    <th>&nbsp;</th>
                </tr>
                <? $i = 0;
                    foreach ($inst['elements'] as $module):$i++; ?>
                        <tr>
                            <td width="24"><?= $i; ?>.</td>
                            <td>
                                <?= $module['module_caption'] ?>
                            </td>
                            <td><?= $module['version'] ?></td>
                            <td class="module-controls">
                                <span><?php echo $module['controls'] ?></span>
                            </td>
                        </tr>
                    <? endforeach; ?>
            </table>

        <? endif; ?>



        <? if (isset($syst['elements']) && sizeof($syst['elements'])): ?>
            <h3>Системные модули</h3>
            <table>
                <tr>
                    <th width="32">№</th>
                    <th>Модуль</th>
                    <th width="80">Версия</th>
                    <th></th>
                </tr>
                <?
                    $i = 0;
                    foreach ($syst['elements'] as $module)
                    {
                        $i++;
                        ?>
                        <tr>
                            <td width="24"><?= $i; ?>.</td>
                            <td>
                                <?= $module['module_caption'] ?>
                            </td>
                            <td><?= $module['version'] ?></td>
                            <td class="module-controls">
                                <span><?php echo $module['controls'] ?></span>
                            </td>
                        </tr>

                    <? } ?>
            </table>
        <? endif; ?>



        <? if (isset($notinst['elements']) && sizeof($notinst['elements'])): ?>
            <h3>Неустановленные модули</h3>
            <table>
                <tr>
                    <th width="32">№</th>
                    <th>Модуль</th>
                    <th width="80">Версия</th>
                    <th>&nbsp;</th>
                </tr>
                <? $i = 0;
                    foreach ($notinst['elements'] as $module):$i++; ?>

                        <tr>
                            <td width="24"><?= $i; ?>.</td>
                            <td>
                                <?= $module['module_caption'] ?>
                            </td>
                            <td><?= $module['version'] ?></td>
                            <td class="module-controls">
                                <span><?php echo $module['controls'] ?></span>
                            </td>
                        </tr>

                    <? endforeach; ?>
            </table>
        <? endif; ?>


    </div>
</div>