<?php
    /*
    СПИСОКОВЫЙ МОДУЛЬ, используется для создания на его основе модулей новости, статьи, faq и т.д.
     */
    $Tape = new MSTapeControl;

    header('Content-type: text/html; charset=utf-8');
    global $CONFIG;

    $Tape->loadConfig();

    /* Подкотовительные работы для модуля */
    $table_name = $CONFIG['tables']['items']['db_name'];
    $key_field = $CONFIG['tables']['items']['key_field'];

    list($output_id) = $Tape->prepareLinkPath($CONFIG);

    $Tape->checkModuleIntegrity();

    /* Начало работы модуля - действия, реакции */
    switch (MSCore::urls()->vars[1])
    {

        case 'fastview':
        {
            $vars = manage_modules();
            $_RESULT = array('content' => template('fast', $vars));
            die(json_encode($_RESULT));
        }
            break;

        case 'add':
        {
            $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
            $page = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : 0;
            $new_item_id = (isset(MSCore::urls()->vars[4])) ? (int)MSCore::urls()->vars[4] : 0;

            /* ACTION */
            $OUT_CONFIG = $CONFIG;
            $CONFIG = $Tape->generateConfigValues($new_item_id);

            if (isset($_REQUEST['conf']))
            {

                if ($inserted_id = $Tape->saveItem(false))
                {

                    MSCore::modules()->getModulesInfo();
                    $vars = manage_modules();
                    $vars['apply'] = isset(MSCore::urls()->vars[5]) && MSCore::urls()->vars[5] > 0 ? 1 : 0;

                    $inserted_id = '<input id="inserted_id" type="hidden" value="' . $inserted_id . '" name="' . $key_field . '">';

                    $_RESULT = array('content' => array(template('fast', $vars), $inserted_id));
                    die(json_encode($_RESULT));

                }
                else
                {

                    echo '<i style="display:none">Fatal error: </i>Введенный "Символьный код" уже занят';

                    /**
                     * TODO: Сейчас на все ошибки одна причина, исправить :)
                     */

                }

            }
            else
            {
                $vars['CONFIG'] = $CONFIG;
                $vars[$key_field] = (int)$new_item_id;
                $vars['path_id'] = (int)$path_id;
                $vars['page'] = $page;
                $vars['output_id'] = $output_id;

                $vars['_FORM_'] = MSCore::forms()->make($CONFIG['tables']['items']['config']);

                echo template('moduleTape/add', $vars);
            }
            /* ACTION */

            die();
        }
            break;

        case 'install':

            $install = MSCore::urls()->vars[2];
            MSCore::modules()->InstallModule($install);

            $vars = manage_modules();
            $content = template('fast', $vars);

            $_RESULT = array("content" => array($content, generateModulesSettingsMenu(), getModulesList()));
            die(json_encode($_RESULT));

            break;

        case 'uninstall':

            $module = MSCore::modules()->by_dir(MSCore::urls()->vars[2]);

            if (($module == false || MSCore::modules()->isSystem($module['module_name'])) && !AUTH_IS_ROOT)
            {
                die('Модуль не удалось деисталлировать. Проверьте, имеете ли Вы на это право.');
            }

            MSCore::modules()->UnInstallModule(MSCore::urls()->vars[2]);
            $vars = manage_modules();
            $content = template('fast', $vars);

            $_RESULT = array("content" => array($content, generateModulesSettingsMenu(), getModulesList()));
            die(json_encode($_RESULT));

            break;
    }

    die();