<?php
/**
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
 */

$module_name = 'benefits';
$module_caption = 'Преимущества';

$icons = [
    'fa-home'      => 'Домик',
    'fa-users'     => 'Группа',
    'fa-file-text' => 'Текстовый файл',
];

$CONFIG = array(

    'module_name' => $module_name,
    'module_caption' => $module_caption,
    'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
    'version' => '1.1.0.0',
    'tables' => array(

        'items' => array(

            'db_name' => $module_name,
            'dialog' => array('width' => 660, 'height' => 410),
            'key_field' => 'id',
            'order_field' => '`order` ASC',
            'onpage' => 20,
            'config' => array(
                'icon_class' => array(
                    'caption' => 'Иконка',
                    'values' => $icons,
                    'type' => 'select',
                    'in_list' => 1,
                    'filter' => 1
                ),
                'title' => array(
                    'caption' => 'Заголовок',
                    'value' => '',
                    'type' => 'string',
                    'in_list' => 1,
                    'filter' => 1,
                    'to_code' => true,
                ),
//                'code' => array(
//                    'type' => 'code',
//                    'caption' => 'Символьный код',
//                    'in_list' => 0,
//                    'value' => '',
//                ),
                'text' => array(
                    'caption' => 'Текст',
                    'value' => '',
	                'type' => 'wysiwyg'
                ),
                'active' => array(
                    'caption' => 'Активность',
                    'value' => '1',
                    'type' => 'checkbox',
                    'in_list' => 1,
                ),
            ),
        ),
    ),
);

return $CONFIG;