<?php
/**
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
 */

$module_name = 'main';
$module_caption = 'Настройки';

$CONFIG = array(
    'module_name' => $module_name,
    'module_caption' => $module_caption,
    'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
    'version' => '2',
    'tables' => array(
        'items' => array(
            'db_name' => $module_name,
            'dialog' => array('width' => 660, 'height' => 410),
            'key_field' => 'id',
            'config' => array(
                array(
                    'type' => 'divider',
                ),
                array(
                    'type' => 'section',
                    'caption' => 'Блок с объектами',
                    'output' => 'open',
                ),
                'title_properties' => array(
                    'caption' => 'Заголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                'subtitle_properties' => array(
                    'caption' => 'Подзаголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                'properties' => array(
                    'caption' => 'Объекты',
                    'value' => '',
                    'values' => [],
                    'from' => array(
                        'table_name' => PRFX . 'properties',
                        'key_field' => 'id',
                        'name_field' => 'title',
                    ),
                    'type' => 'select',
                    'multi' => true
                ),
                array(
                    'type' => 'section',
                    'output' => 'close',
                ),
                array(
                    'type' => 'divider',
                ),

                array(
                    'type' => 'divider',
                ),
                array(
                    'type' => 'section',
                    'caption' => 'Блок сотрудников',
                    'output' => 'open',
                ),
                'title_staff' => array(
                    'caption' => 'Заголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                'subtitle_staff' => array(
                    'caption' => 'Подзаголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                'staff' => array(
                    'caption' => 'Агенты',
                    'value' => '',
                    'values' => [],
                    'from' => array(
                        'table_name' => PRFX . 'agents',
                        'key_field' => 'id',
                        'name_field' => 'title',
                    ),
                    'type' => 'select',
                    'multi' => true
                ),
                array(
                    'type' => 'section',
                    'output' => 'close',
                ),
                array(
                    'type' => 'divider',
                ),

                array(
                    'type' => 'divider',
                ),
                array(
                    'type' => 'section',
                    'caption' => 'Блок c продажей объекта',
                    'output' => 'open',
                ),
                'title_property' => array(
                    'caption' => 'Заголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                'subtitle_property' => array(
                    'caption' => 'Подзаголовок блока',
                    'value' => '',
                    'type' => 'string',
                ),
                array(
                    'type' => 'section',
                    'output' => 'close',
                ),
                array(
                    'type' => 'divider',
                ),
            ),
        ),
    ),
);

return $CONFIG;