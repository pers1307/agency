<?php
$Tape = new MSTapeControl;

header('Content-type: text/html; charset=utf-8');
global $CONFIG;

$Tape->loadConfig();

/* Подготовительные работы для модуля */
$table_name = $CONFIG['tables']['items']['db_name'];
$key_field = $CONFIG['tables']['items']['key_field'];

list($output_id) = $Tape->prepareLinkPath($CONFIG);

/* Начало работы модуля - действия, реакции */
switch (MSCore::urls()->vars[1]) {
    case 'config': {
        $config = MSCore::modules()->by_dir(MSCore::urls()->vars[0]);

        $config['config'] = array();

        foreach (MSCore::page()->allZones as $_zone)
        {
            $config['config']['mod_' . $_zone['value']] = array(
                'caption' => $_zone['value'],
                'value' => (isset($config['output'][$_zone['value']]) ? $config['output'][$_zone['value']] : ''),
                'module' => MSCore::urls()->vars[0],
                'zone' => $_zone['value'],
                'type' => 'explorer',
            );
        }

        $vars['_FORM_'] = MSCore::forms()->make($config['config']);
        $vars['mod'] = MSCore::urls()->vars[0];

        die (template('module_config', $vars));
    }
        break;

    case 'fastview': {
        /**
         * Тут по идее нужно все отобразить
         */
        $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
        $page = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : 0;
        $new_item_id = (isset(MSCore::urls()->vars[4])) ? (int)MSCore::urls()->vars[4] : 0;

        $CONFIG = $Tape->generateConfigValues($new_item_id);
        $tableName = PRFX . $CONFIG['tables']['items']['db_name'];

        // Тут по идее тоже надо подшаманить с отображением
        $result = MSCore::db()->getAll("SELECT * FROM `{$tableName}` WHERE `path_id` = {$path_id}");

        foreach ($CONFIG['tables']['items']['config'] as $keyInConfig => &$itemInConfig) {
            foreach ($result as $item) {
                if ($item['key'] == $keyInConfig) {
                    $itemInConfig['value'] = $item['value'];
                }
            }
        }

        /**
         * todo: Очень грубая работа, но в следующих версиях модуля доработаем.
         */
        if (isset($_REQUEST['conf'])) {
            // Все сохраняем в табличку и отдаем отображение
            foreach ($_REQUEST['conf'][1] as $key => &$value) {
                if ($key == 'staff') {
                    $value = implode(',', $value);
                }

                if ($key == 'properties') {
                    $value = implode(',', $value);
                }

                MSCore::db()->execute(
                    "INSERT INTO `{$tableName}` (`path_id`, `key`, `value`)
                        VALUES({$path_id}, '{$key}', '{$value}')
                        ON DUPLICATE KEY UPDATE `value` = '{$value}';"
                );

                $CONFIG['tables']['items']['config'][$key]['value'] = $value;
            }
        }

        $vars['CONFIG'] = $CONFIG;
        $vars[$key_field] = (int)$new_item_id;
        $vars['path_id'] = (int)$path_id;
        $vars['page'] = $page;
        $vars['output_id'] = $output_id;
        $vars['_FORM_'] = MSCore::forms()->make($CONFIG['tables']['items']['config']);

        $_RESULT = array('content' => template('moduleTape/item', $vars));
        die(json_encode($_RESULT));
    }
        break;
}

die();