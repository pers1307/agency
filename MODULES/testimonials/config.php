<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'testimonials';
    $module_caption = 'Рекомендации';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(

            'items' => array(

                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(

                    'title' => array(
                        'caption' => 'Автор',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 1,
                        'to_code' => true,
                    ),
                    'author_position' => array(
                        'caption' => 'Позиция автора',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
//                    'code' => array(
//                        'type' => 'code',
//                        'caption' => 'Символьный код',
//                        'in_list' => 0,
//                        'value' => '',
//                    ),
                    'text' => array(
                        'caption' => 'Текст высказывания',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    ),
                ),
            ),
        ),
        'settings' => array(
            'title' => array(
                'caption' => 'Заголовок блока',
                'value' => 'Наши рекомендации',
                'type' => 'string'
            ),
            'subtitle' => array(
                'caption' => 'Подзаголовок блока',
                'value' => 'Подзаголовок',
                'type' => 'string'
            )
        ),
    );

    return $CONFIG;