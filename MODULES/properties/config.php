<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'properties';
    $module_caption = 'Объекты';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(

            'items' => array(
                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(
                    'title' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 1,
                        'to_code' => true,
                    ),
                    'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 0,
                        'value' => '',
                    ),
                    'image' => array(
                        'caption' => 'Картинка в списке',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'list' => array(260, 200, true),
                            'map1' => array(800, 600, true),
                            'map2' => array(300, 200, true),
                            'map3' => array(750, 500, true),
                            'map4' => array(1024, 650, true),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 1,
                            'module-name' => $module_name
                        ),
                        'in_list' => 1,
                        'filter' => 0,
                    ),
                    'gallery' => array(
                        'caption' => 'Галлерея',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'main' => array(800, 600, true),
                            'min1' => array(300, 200, true),
                            'min2' => array(760, 500, true),
                            'min3' => array(1024, 670, true),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 10,
                            'module-name' => $module_name
                        ),
                        'filter' => 0,
                    ),
                    'price' => array(
                        'caption' => 'Цена',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'rent_price' => array(
                        'caption' => 'Цена в аренду',
                        'help'    => 'Цена за месяц',
                        'value'   => '',
                        'type'    => 'string',
                    ),
                    'description' => array(
                        'caption' => 'Описание',
                        'value' => '',
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),
                    'type_sell' => array(
                        'caption' => 'Тип продажи',
                        'value' => '',
                        'values' => [
                            1 => 'Продается',
                            2 => 'В аренду',
                        ],
                        'type' => 'select'
                    ),
                    'geocoder' => array(
                        'caption' => 'Адрес',
                        'value' => '',
                        'type' => 'geocoder',
                        'in_list' => 0,
                        'filter' => 0,
                    ),
                    'properties' => array(
                        'caption' => 'С этими проектом часто смотрят',
                        'value' => '',
                        'values' => [],
                        'from' => array(
                            'table_name' => PRFX . 'properties',
                            'key_field' => 'id',
                            'name_field' => 'title',
                        ),
                        'type' => 'select',
                        'multi' => true
                    ),
                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'SEO',
                        'output' => 'open',
                    ),
                    'h1' => array(
                        'caption' => 'H1',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'title_page' => array(
                        'caption' => 'Page title',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_description' => array(
                        'caption' => 'Meta description',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_keywords' => array(
                        'caption' => 'Meta keywords',
                        'value' => '',
                        'type' => 'string',
                    ),
                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),
                ),
            ),
        ),

        'settings' => array(
            'onPage' => array(
                'caption' => 'Количество объектов на странице',
                'value' => 10,
                'type' => 'string'
            )
        ),
    );

    return $CONFIG;