<?php
/**
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
*/

$module_name = 'social_network';
$module_caption = 'Ссылки на социальные сети';

$icons = [
    'fa-facebook'    => 'Facebook',
    'fa-twitter'     => 'Twitter',
    'fa-google-plus' => 'Google +',
    'fa-linkedin'    => 'LinkedIn',
    'fa-youtube'     => 'Youtube',
    'fa-vk'          => 'Вконтакте',
    'fa-instagram'   => 'Instagram',
];

$CONFIG = array(
  'module_name' => $module_name,
  'module_caption' => $module_caption,
  'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
  'version' => '1.1.0.0',
  'tables' => array(

    'items' => array(

      'db_name' => $module_name,
      'dialog' => array('width' => 660, 'height' => 410),
      'key_field' => 'id',
      'order_field' => '`order` ASC',
      'onpage' => 20,
      'config' => array(
        'icon_class' => array(
          'caption' => 'Социальная сеть',
          'values' => $icons,
          'type' => 'select',
          'in_list' => 1,
          'filter' => 1
        ),
        'url' => array(
          'caption' => 'Ссылка на группу в соцсети',
          'value' => '',
          'type' => 'string',
        ),
        'active' => array(
          'caption' => 'Активность',
          'value' => '1',
          'type' => 'checkbox',
          'in_list' => 1,
        )
      ),
    ),
  ),
);

return $CONFIG;
