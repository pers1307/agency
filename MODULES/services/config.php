<?php
    /*
    родительский элемент и у разделов и у элементов называется parent
    управление видимостью - поле visible (только у разделов)
    для элементов управление видимостью можно сделать отдельным полем
    */

    $module_name = 'services';
    $module_caption = 'Услуги';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '2.0.0.0',

        'tables' => array(
            /*
                Названия таблиц "articles" и "items" обязательны, эти имена используются в различных функциях
            */

            //------------------------------Разделы-------------------------------------------------------
            'articles' => array(

                'db_name' => $module_name . '_articles',
                'dialog' => array('width' => 650, 'height' => 400),
                'onpage' => 20,

                //-------если поставить true то нельзя будет добавлять разделы первого уровня
                'noAddArticles' => false,

                //------если ордер не указан то сортировка будет вручную
//			'order_field' => '`name`',

                //---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
                'add_new_on_top' => false,

                //------масимальное количество уровней вложенности----------
                'max_levels' => 4,

                //------поле, которое будет выводиться в списке
                'title_field' => 'name',
                'is_order' => true, // Ручная сортировка

                'config' => array(

                    'name' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'to_code' => true,
                    ),
                    'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 1,
                        'value' => '',
                    ),
                    'text' => array(
                        'caption' => 'Текст',
                        'value' => '',
                        'type' => 'wysiwyg',
                    ),
                    'active' => array(
                        'caption' => 'Опубликовано',
                        'value' => 1,
                        'type' => 'boolean',
                        'in_list' => 1,
                        'filter' => 1,
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'SEO',
                        'output' => 'open',
                    ),
                    'h1' => array(
                        'caption' => 'H1',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'title_page' => array(
                        'caption' => 'Page title',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_description' => array(
                        'caption' => 'Meta description',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_keywords' => array(
                        'caption' => 'Meta keywords',
                        'value' => '',
                        'type' => 'string',
                    ),
                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),

                ),
            ),
            //------------------------------Разделы-------------------------------------------------------

            //------------------------------Товары-------------------------------------------------------
            'items' => array(

                'db_name' => $module_name . '_items',
                'dialog' => array('width' => 650, 'height' => 500),
                'onpage' => 20,
                //------если ордер не указан то сортировка будет вручную
//			'order_field' => '`title`',

                //---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
                'add_new_on_top' => true,

                'config' => array(

                    'title' => array(
                        'caption' => 'Заголовок',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 1,
                        'to_code' => true,
                    ),
                    'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 1,
                        'value' => '',
                        'filter' => 1,
                    ),
                    'image' => array(
                        'caption' => 'Фото',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'list'  => array(230, 250, true),
                            'list2' => array(280, 300, true),
                            'list3' => array(770, 830, true),
                            'list4' => array(950, 1024, true),
                            'main'  => array(260, 280, true),
                            'about' => array(350, 380, true),
                            'item'  => array(750, 320, true),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 1,
                            'module-name' => $module_name
                        ),
                        'in_list' => 1,
                        'filter' => 0,
                    ),
                    'text' => array(
                        'caption' => 'Текст',
                        'value' => '',
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),
                    'active' => array(
                        'caption' => 'Опубликовано',
                        'value' => 1,
                        'type' => 'boolean',
                        'in_list' => 1,
                        'filter' => 1,
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'SEO',
                        'output' => 'open',
                    ),
                    'h1' => array(
                        'caption' => 'H1',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'title_page' => array(
                        'caption' => 'Page title',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_description' => array(
                        'caption' => 'Meta description',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_keywords' => array(
                        'caption' => 'Meta keywords',
                        'value' => '',
                        'type' => 'string',
                    ),
                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),

                ),
            ),
            //------------------------------Товары-------------------------------------------------------
        )
    );

    return $CONFIG;