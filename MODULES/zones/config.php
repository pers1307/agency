<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'zones';
    $module_caption = 'Зоны';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '6.0',
        'tables' => array(

            'items' => array(

                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`id`',
                'onpage' => 20,
                'config' => array(

                    'caption' => array(
                        'caption' => 'Название зоны',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 0,
                    ),
                    'value' => array(
                        'caption' => 'Системное имя (<b>_ZONENAME_</b>)',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 0,
                    ),
                    'for_search' => array(
                        'caption' => 'Для поиска',
                        'value' => 0,
                        'type' => 'boolean',
                        'in_list' => 1,
                        'filter' => 0,
                    ),
                    'search_links' => array(
                        'caption' => 'Блок ссылок (для поиска)',
                        'value' => 0,
                        'type' => 'boolean',
                        'in_list' => 1,
                        'filter' => 0,
                    )
                ),
            ),
        ),
    );

    return $CONFIG;