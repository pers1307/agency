<?php

class AddAgentType extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->execute("ALTER TABLE mp_agents ADD type TINYINT DEFAULT 0 NULL;");
    }//up()

    public function down()
    {
    }//down()
}
