<?php

class FixPriceColumn extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->execute("ALTER TABLE mp_properties MODIFY rent_price VARCHAR(255) DEFAULT '';");
        $this->execute("ALTER TABLE mp_properties MODIFY price VARCHAR(255) DEFAULT '';");
    }//up()

    public function down()
    {
    }//down()
}
