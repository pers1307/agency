<?php
function is_valid_email($email) {

    if (mb_eregi("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$]", $email)) {
        return false;
    }

    if (mb_strpos($email, '@')) {
        list ($Username, $Domain) = preg_split("@", $email);

        if (getmxrr($Domain, $MXHost)) {
            return true;
        } else {
            if (@fsockopen($Domain, 25, $errno, $errstr, 30)) {
                return true;
            } else {
                return false;
            }
        }
    } else {
        return false;
    }
//	return preg_match( '/[.+a-zA-Z0-9_-]+@[a-zA-Z0-9-]+.[a-zA-Z]+/', $email );
}


function is_valid_phone($phone) {
    return preg_match('/^[\s0-9-()+]+$/', $phone);
}

function is_valid_pass($pass) {
    $out = true;

    if (mb_strlen($pass) <= 4) {
        $out = false;
    }
    if (in_array($pass, array('123456', '111111', 'qwerty', '123123', '654321'))) {
        $out = false;
    }

    return $out;
}

function saveData($filename, $data, $to = '') {
    $fp = fopen($to . $filename, 'w+');
    fwrite($fp, $data);
    fclose($fp);
}

/**
 * Преобразование номера месяца к строковому типу
 *
 * @param integer $monthnum
 * @param integer $mode
 *
 * @return string
 */
function Month2String($monthnum, $mode = 0) {
    $monthnum = (int)$monthnum;

    if ($mode == 0) {
        return FormConfigs::$months1[$monthnum];
    }
    if ($mode == 1) {
        return FormConfigs::$months2[$monthnum];
    }
}

/**
 * Преобразвание Mysql даты в человеческий тип
 *
 * @param mysqldate $datetime
 * @param integer $mode
 *
 * @return string
 */
function Sql2Date($datetime = "0000-00-00 00:00:00", $mode = 0, $typeDateTime = 2) {
    $regs = array();
    $ret = array();
    if (preg_match('/((19|20)[0-9]{2})[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01]) ([012][0-9]):([0-5][0-9]):([0-5][0-9])/', $datetime, $regs)) {
    } elseif (preg_match('/((19|20)[0-9]{2})[- \/.](0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])/', $datetime, $regs)) {
    } else {
        return false;
    }

    if (isset($regs[1]) && isset($regs[3]) && isset($regs[4])) {
        $ret['year'] = $regs[1];
        $ret['month'] = $regs[3];
        $ret['day'] = $regs[4];
    }
    if (isset($regs[5]) && isset($regs[6]) && isset($regs[7])) {
        $ret['hour'] = $regs[5];
        $ret['min'] = $regs[6];
        $ret['sec'] = $regs[7];
    }

    switch ($mode) {
        case 1:
            //29 июля 2007
            return $ret['day'] . ' ' . Month2String($ret['month'], 1) . ' ' . $ret['year'];
        case 2:
            //13:40
            return $ret['hour'] . ':' . $ret['min'];
        case 3:
            //2007-Июль-29 17:40
            return $ret['year'] . ' ' . Month2String($ret['month']) . ' ' . $ret['day'] . ' ' . $ret['hour'] . ':' . $ret['min'];
        case 4:
            //29.07.07
            return $ret['day'] . '.' . $ret['month'] . '.' . mb_substr($ret['year'], 2, 2);

        case 5:
            //29.07.2007
            return $ret['day'] . '.' . $ret['month'] . '.' . $ret['year'];

        case 6:
            //29.07.2007 13:40
            return $ret['day'] . '.' . $ret['month'] . '.' . $ret['year'] . ' ' . (isset($ret['hour']) && $ret['min'] ? '' . $ret['hour'] . ':' . $ret['min'] . '' : '');

        case 7:
            //29 июля 2007 13:55
            return $ret['day'] . ' ' . Month2String($ret['month'], 1) . ' ' . $ret['year'] . ' ' . $ret['hour'] . ':' . $ret['min'];

        case 8:
            //29.07.2007 13:40
            return $ret['day'] . '.' . $ret['month'] . '.' . $ret['year'] . ' / ' . $ret['hour'] . ':' . $ret['min'];

        case 9:
            //29 июля
            return $ret['day'] . ' ' . Month2String($ret['month'], 1);

        case 10:
            // если год текущий, то 29 июля, если нет то 29 июля 2010
            return $ret['day'] . ' ' . Month2String($ret['month'], 1) . (date('Y') != $ret['year'] ? ' ' . $ret['year'] : '');

        case 11:
            //Для атрибута datetime тега time
            switch ($typeDateTime) {
                case 0:
                    //2012
                    return $ret['year'];
                case 1:
                    //2012-12
                    return $ret['year'] . '-' . $ret['month'];
                case 2:
                    //2012-12-23
                    return $ret['year'] . '-' . $ret['month'] . '-' . $ret['day'];
                case 3:
                    //2004-07-24T18:18
                    return $ret['year'] . '-' . $ret['month'] . '-' . $ret['day'] . 'T' . $ret['hour'] . ':' . $ret['min'];
                case 4:
                    //2004-07-24T18:18:18
                    return $ret['year'] . '-' . $ret['month'] . '-' . $ret['day'] . 'T' . $ret['hour'] . ':' . $ret['min'] . ':' . $ret['sec'];
            }
            break;

        case 0:
        default:
            return $ret;
    }
}

/**
 * Возващает название скрипта если есть
 *
 * @param string $path
 */
function getScriptName($path) {
    if (is_file(EndSlash(DES_DIR . DS . dirname($path), DS) . 'config.php')) {
        include EndSlash(DES_DIR . DS . dirname($path), DS) . 'config.php';

        return isset($files[basename($path)][0]) ? $files[basename($path)][0] : $path;
    } else {
        return '<span class="warning_script" title="Файл не найден">' . $path . '</span>';
    }
}

function getSelectValuesFromTable($table, $key_value, $key_name, $where = '', $order = '') {
    $out = array();

    $all = MSCore::db()->getAll('SELECT ' . $key_value . ' as val, ' . $key_name . ' as name FROM ' . $table . ' ' . ($where != "" ? 'WHERE ' . $where : '') . ' ' . ($order != "" ? 'ORDER BY ' . $order . '' : '') . '');
    foreach ($all as $num => $item) {
        $out[$item['val']] = $item['name'];
    }

    return $out;
}

/**
 * Функция транслитерации url
 *
 * @param $string
 *
 * @param string $spaceSymbol на что заменять пробел
 *
 * @return mixed
 */
function translitUrl($string, $spaceSymbol = '-') {

    $lettersConformity = array(
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd', 'е' => 'e', 'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
        'и' => 'i', 'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n', 'о' => 'o', 'п' => 'p', 'р' => 'r',
        'с' => 's', 'т' => 't', 'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'ts', 'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sh',
        'ъ' => '', 'ы' => 'y', 'ь' => '', 'э' => 'e', 'ю' => 'yu', 'я' => 'ya', '№' => '#', ' ' => $spaceSymbol
    );

    $string = mb_strtolower($string);

    $string = preg_replace_callback('/[а-яё ]/u', function ($str) use ($lettersConformity) {

        return !empty($str) && isset($lettersConformity[$str[0]]) ? $lettersConformity[$str[0]] : '';

    }, $string);

    $string = preg_replace('/[^a-z0-9-_]/', '', $string);

    $string = preg_replace('/' . $spaceSymbol . '{2,}/', $spaceSymbol, $string);

    return $string;

}

/**
 *  Функция возвращает файл и строку вызова (трассировка назад по вызовам функций).
 *  $step == 0 - файл и строка вызова этой функции
 * @access public
 * @return array Структура: array( '/home/site/filename.inc', 222 )
 *
 * @param integer $step Шаг назад
 */
function LastFileLine($step = 0) {
    $export = array('undefined', 0);
    if (function_exists('debug_backtrace')) {
        $bt = debug_backtrace();
        if (isset ($bt[$step]['file']) && $bt[$step]['line']) {
            $export = array($bt[$step]['file'], $bt[$step]['line']);
        }
    } else {
        die('Версия PHP4 должна быть равна 4.3.1 или выше');
    }

    unset ($bt, $step);

    return $export;
}

/**
 *  Экранирует переменные для выражения RegExp.
 *  Пример: ( '/'._pregQuote($ass).'.+/' )
 *
 * @access public
 *
 * @param string $regexp Переменная
 *
 * @return string
 */
function _pregQuote($regexp) {

    $regexp = (string)$regexp;
    $regexp = preg_quote($regexp);
    $regexp = str_replace('/', '\/', $regexp);
    $regexp = str_replace('#', '\#', $regexp);

    return $regexp;

}

/**
 * Поставить конечный слеш к пути. Замена обратного слеша на прямой.
 * @param  string $folder Путь
 *
 * @param string $slash
 *
 * @return string Directory
 */
function EndSlash($folder, $slash = '/') {
    $folder = rtrim($folder, '/\\, ') . $slash;
    $folder = trim($folder);
    $folder = str_replace('\\', $slash, $folder);

    return $folder;
}

/**
 *  Временная функция для тестирования скриптов.
 *  Вывод текста.
 * @access public
 *
 * @param mixed $text Текст для записи
 * @param boolean $die Остановить скрипты
 * @param boolean $tofile Вывести в файл debug
 * @param string $mode
 * @param bool $debug_hint
 *
 * @return void
 */
function debug($text, $die = false, $tofile = false, $mode = 'w', $debug_hint = true) {

    if ($text == "" || !MS || !MS_DEBUG) {
        return;
    }

    $text = print_r($text, true);

    list ($file, $line) = LastFileLine(1);

    if ($tofile) {
        $text = $text . "\n" . $file . ': ' . $line . "\n\n";
        $fp = fopen(TMP_DIR . DS . 'debug.txt', $mode);
        fwrite($fp, $text);
        fclose($fp);

        if ($die) {
            die();
        }
    } else {
        if ($debug_hint) {
            $text = '<pre style="border: 1px dashed gray; background: #FFFFE1; padding:5px;"><b>DEBUG:</b><br>' . htmlspecialchars($text) . '</pre><b>' . $file . ': ' . $line . '</b>';
        }

        if ($die) {
            die($text);
        } else {
            echo $text;
        }

    }

}

/**
 * Проверка на существование директории.
 * @param string $folder Полный путь к директории
 * @param bool|string $CreateIfNot Создать директорию, если не существует
 * @param integer $mod Права создаваемой директории
 *
 * @return boolean
 */
function DirExists($folder, $CreateIfNot = true, $mod = 755) {

    $folder = EndSlash($folder, DS);

    if (!is_dir($folder)) {

        if (!$CreateIfNot) {
            return false;
        }

        $dirs = explode(DS, $folder);

        if (sizeof($dirs) < 2) {
            return false;
        }

        $path = $dirs[0] . DS;
        unset ($dirs[0]);

        foreach ($dirs as $dir) {
            if (!trim($dir)) {
                continue;
            }

            $path .= $dir . DS;

            if (!is_dir($path)) {
                $result = mkdir($path);
                try {
                    chmod($path, 0755);
                } catch (Exception $e) {}
            }

        }


        unset ($path, $dirs, $dir);

        return is_dir($folder);
    }

    return true;
}


/**
 *  Возвращает расширение файла, по пути к файлу
 * @access public
 *
 * @param string $filepath Путь к файлу
 *
 * @return string Расшрение
 */
function ExtractExt($filepath) {

    $filepath = (string)$filepath;

    return preg_match('#\.([^\./\\\]+)$#', $filepath, $filepath) ? $filepath[1] : '';
}

/**
 *  Ассоциирует значения массива по содержимому указаного поля.
 *  Порядок элементов сохраняется.
 * @access public
 *
 * @param string $field Название существующего в массиве поля
 * @param array $array Неассоциированный массив
 *
 * @return array Ассоциированный массив
 */
function assoc($field, $array) {

    $array = (array)$array;

    if (!sizeof($array) || key($array) === false) {
        return $array;
    }

    $result = array();
    foreach ($array as $row) {
        $result[$row[$field]] = $row;
    }

    unset ($array, $row);

    return $result;
}

function mapInsert($matches) {
    $coordComplete = false;
    $map = MSCore::db()->getRow('SELECT * FROM `' . PRFX . 'maps` WHERE `id` = ' . $matches[1]);

    if (count($map)) {
        $map['icon'] = unserialize($map['icon']);

        /** подключаем js-api я.карты **/
        if (!defined('YA_MAP_API') && !preg_match('%<script(.*)src="//api-maps.yandex.ru(.*).></script>%i', MSCore::page()->html)) {
            MSCore::urls()->AddBeforeTag('<script src="//api-maps.yandex.ru/2.1?apikey=74d61e2c-7b5f-44de-af3c-ed289a080d30&load=package.standard&lang=ru-RU" type="text/javascript"></script>' . "\r\n", '</head>');
            define('YA_MAP_API', true);
        }

        /** если готовые координаты **/
        if (preg_match('/^-?[\d]{1,3}.[\d]{0,6}([,;])-?[\d]{1,3}.[\d]{0,6}$/', $map['coord'])) {
            $coordComplete = true;
            $map['coord'] = preg_replace('/;/', ',', $map['coord']);
        }

        ob_start();
        $map['icon']['original'] = MSFiles::getImageUrl($map['icon'], 'original');
        ?>
        <div class="contactsBlock_map">
            <div class="yamap"
                <?= $coordComplete ? 'data-coords="' . $map['coord'] . '"' : 'data-address="' . $map['coord'] . '"' ?>
                 data-geocoded="<?= $coordComplete ?>"
                 data-zoom="<?= round($map['scale'] / 5.8) ?>"
                 data-title="<?= safe($map['title']) ?>"
                 id="YaMap<?= $map['id'] ?>"
                <?php
                if (!empty($map['icon']['original'])) {
                    list($imageWidth, $imageHeight) = getimagesize(DOC_ROOT . DS . $map['icon']['original']);
                    ?>
                    data-image-width="<?= $imageWidth ?>"
                    data-image-height="<?= $imageHeight ?>"
                    data-image="<?= $map['icon']['original'] ?>"
                <?
                } else {
                    ?>
                    data-preset="twirl#blueStretchyIcon"
                <?
                }
                ?>
                 style="width:<?= empty($map['map_width']) ? '400px' : (is_numeric($map['map_width']) ? $map['map_width'] . 'px' : $map['map_width']) ?>;height:<?= empty($map['map_height']) ? '200px' : (is_numeric($map['map_height']) ? $map['map_height'] . 'px' : $map['map_height']) ?>;">
            </div>
            <div style="display: none"
                 class="baloon-content"><?= preg_replace('/[\r\n\']/', '', $map['description']) ?></div>
        </div>
        <?
        return ob_get_clean();

    }

    return '<p><strong>Карта с ID ' . $matches[1] . ', не найдена</strong></p>';

}

function GetBlocks($folder, $designName) {
    $files = array();
    $folders = GetDirsFromDir($folder . DS . $designName, true, false);

    foreach ($folders as $key => $path) {
        if ((mb_strpos($path, 'BLOCKS') !== false || mb_strpos($path, 'blocks') !== false)) {

            foreach (cmsGetFoldersAndFiles(EndSlash($path, DS), true) as $info) {
                $files[] = $info;
            }
        }
    }

    return $files;
}

/**
 * Взять папки с вложениями
 *
 *
 */
function GetDirsFromDir($folder, $recurse = false, $fullpath = false) {

    $folder = EndSlash($folder, DS);

    if (($dr = @ opendir($folder)) === false) {
        return array();
    }

    $dirs = array();
    while (($filename = readdir($dr)) !== false) {
        if (is_dir($folder . $filename) && $filename[0] != '.') {
            $dirs[] = $recurse || $fullpath ? $folder . $filename : $filename;

            if ($recurse) {
                $dirs = array_merge($dirs, GetDirsFromDir($folder . $filename, true));
            }
        }
    }

    sort($dirs);

    return $dirs;

}


/**
 * Взять файлы с вложениями
 *
 *
 */
function GetFilesFromDir($folder, $fullPath = false, $cutPath = false) {

    $folder = EndSlash($folder, DS);

    if (($dr = @ opendir($folder)) === false) {
        return array();
    }

    $files = array();
    while (($filename = readdir($dr)) !== false) {
        if (is_file($folder . $filename) && $filename[0] != '.' && ExtractExt($filename) == 'php') {
            $files[$filename] = $fullPath ? $folder . $filename : $filename;
            if ($cutPath !== false) {
                $files[$filename] = str_replace($cutPath, '', $files[$filename]);
            }
        }
    }

    ksort($files);

    return $files;
}

/**
 * Проверить модуль на существование
 * @param module_var $module_name имя модуля
 * @param string $ver_from version
 * @param string $ver_to version
 *
 * @return boolean
 */
function isModule($module_name, $ver_from = null, $ver_to = null) {
    $module_name = getModuleName($module_name);

    if (!isset (MSCore::modules()->info[$module_name])) {
        return false;
    }

    return (boolean)(MSCore::modules()->info[$module_name]['installed']);

}

function cmsGetFoldersAndFiles($currentFolder, $_files = false)
{
    // Map the virtual path to the local server path.
    $sServerDir = $currentFolder;

    // Arrays that will hold the folders and files names.
    $aFolders = array();
    $aFiles = array();

    $oCurrentFolder = opendir($sServerDir);

    while ($sFile = readdir($oCurrentFolder))
    {
        if ($sFile == '.' || $sFile == '..')
        {
            continue;
        }

        $prefix = explode('blocks', $sServerDir);
        $prefix = @$prefix[1] == "\\" ? null : @$prefix[1];
        $prefix = str_replace('\\', '/', $prefix);
        $prefix = ltrim($prefix, '/');

        if (is_dir($sServerDir . $sFile))
        {
            $aFolders[] = array('file' => $sFile, 'filesize' => '', 'chmod' => mb_substr(decoct(fileperms($sServerDir . $sFile)), -3), 'type' => 'folder');
        }
        else
        {
            $names = array();
            if (is_file(BLOCKS_DIR . DS . 'config.php'))
            {
                @include BLOCKS_DIR . DS . 'config.php';
            }
            if (ExtractExt($sFile) == 'php' && $sFile != 'config.php')
            {
                if ($_files)
                {
                    $aFiles[] = array('file' => $sServerDir . $sFile, 'filesize' => filesize($sServerDir . $sFile), 'chmod' => mb_substr(decoct(fileperms($sServerDir . $sFile)), -3), 'type' => ExtractExt($sFile), 'caption' => (isset($files[$prefix.$sFile][0]) ? $files[$prefix.$sFile][0] : ''), 'module' => (isset($files[$prefix.$sFile][1]) ? $files[$prefix.$sFile][1] : ''));
                }
                else
                {
                    $aFiles[] = array('file' => $sFile, 'filesize' => filesize($sServerDir . $sFile), 'chmod' => mb_substr(decoct(fileperms($sServerDir . $sFile)), -3), 'type' => ExtractExt($sFile), 'caption' => (isset($files[$prefix.$sFile][0]) ? $files[$prefix.$sFile][0] : ''), 'module' => (isset($files[$prefix.$sFile][1]) ? $files[$prefix.$sFile][1] : ''));
                }

            }

        }
    }
    $aFolders = ArraySortByField($aFolders, 'file');
    $aFiles = ArraySortByField($aFiles, 'file');
    if ($_files === true)
    {
        return $aFiles;
    }
    else
    {
        return array('folders' => $aFolders, 'files' => $aFiles);
    }
}

/**
 * Получить имя модуля по его пути, имени или null для текущего модуля
 *
 * @access private
 *
 * @param  module_var $module_var Имя модуля ($file, $module_name, $module_dir или null для текущего)
 *
 * @return  false|string Directory
 */
function getModuleName($module_var = null) {
    if (empty ($module_var)) {
        list ($module_var,) = LastFileLine(1);
    }

    if (is_file($module_var)) {

        $module_var = str_replace('\\', '/', $module_var);

        if (mb_strpos($module_var, MODULES_DIR) !== 0) {
            return false;
        }

        $module_var = str_replace(MODULES_DIR, '', $module_var);

        if (!preg_match('#([^/]+)/#', $module_var, $module_var)) {
            return false;
        }


        $module_var = isset ($module_var[1]) ? $module_var[1] : '';
    }

    if (isset (MSCore::modules()->dirs[$module_var])) {
        return MSCore::modules()->dirs[$module_var];
    }

    return (isset (MSCore::modules()->info[$module_var])) ? $module_var : false;

}

/**
 *  Добавляет указаный HTML-текст в перед указаным тагом
 *  текущей страницы после генерациию
 *
 * @access public
 *
 * @param string $text Добавляемый текст
 * @param string $tag Перед каким тагом добавить ( &lt;body&gt;, &lt;head&gt; и т.д. )
 * @param const $file Системный параметр
 *
 * @return void
 */
function AddBeforeTag($text, $tag) {
    if (trim($text) == '') {
        return;
    }

    $tag = mb_strtolower($tag);

    if (empty ($var)) {
        $var = '';
    }

    MSCore::urls()->AddBeforeTag($text, $tag);
}


/**
 * Транслитим имя файла
 */
function ValidFileName($name) {

    $pathInfo = pathinfo($name);

    return translitUrl($pathInfo['filename']) . '.' . $pathInfo['extension'];
}

function ValidFolderName($name) {
    $tmp = translitUrl($name, '_');

    return $tmp;
}

/**
 *  Возвращает значение переменной из $_REQUEST
 *  и назначает этому значению тип дефолтового значения.
 *  Если тип string, то на получаемое значение применяется функция trim
 *
 * @access public
 *
 * @param string $varname Имя переменной
 * @param mixed $default Значение по умолчанию
 *
 * @return mixed
 */
function request($varname, $default) {

    $type = gettype($default);
    $export = isset ($_REQUEST[$varname]) ? $_REQUEST[$varname] : $default;
    settype($export, $type);

    if ($type == 'string') {
        $export = str_replace('\r\n', "\r\n", trim($export));
    }

    return $export;
}


function current_path($add = null) {
    return ($add === null) ? EndSlash(MSCore::urls()->path) : EndSlash(MSCore::urls()->path . $add);
}

function current_url($add = null) {
    return ($add === null) ? MSCore::urls()->current : EndSlash(MSCore::urls()->current) . $add;
}


function path($path_id = null, $add = '') {
    return (isset(MSCore::urls()->ids[$path_id])) ? EndSlash(MSCore::urls()->ids[$path_id]) . $add : '404';
}

function getPathId($path = false) {
    if (!$path) {
        return MSCore::page()->path_id;
    } else {
        return MSCore::db()->getOne("SELECT * FROM `" . PRFX . "www` WHERE `path` LIKE '" . MSCore::db()->pre(trim($path, "/")) . "'");
    }
}


function getParam($num = 0) {
    return isset(MSCore::urls()->vars[$num]) ? MSCore::urls()->vars[$num] : false;
}

function getSegment($num = 0) {
    return isset(MSCore::urls()->segments[$num]) ? MSCore::urls()->segments[$num] : false;
}

function getTP($id) {
    $title = MSCore::db()->getRow("select title_page,title_menu from " . PRFX . "www where `path_id` = '" . MSCore::db()->pre($id) . "'");

    return $title = ($title['title_page'] == '') ? $title['title_menu'] : $title['title_page'];
}

/** Вывод шаблона. Файл ищется относительно места вызова*/
function template($tmp_name = null, $vars = array(), $module = '') {
    $Page = MSCore::page();

    if (!is_array($vars)) {
        return 'Переменная параметров должна быть массивом!';
    }

    $tmp_name = str_replace('/', DS, $tmp_name);
    $layoutPaths = array();

    if (!empty($Page->main_template)) {
        $layoutPath = explode('|', $Page->main_template);
        $layoutPath = $layoutPath[1] . DS . 'views';
    }

    list($file,) = LastFileLine(1);

    if (!empty($layoutPath)) {
        $pathInfo = pathinfo($file);
        $dir = explode(DS, $pathInfo['dirname']);
        $parentDir = end($dir);

        $layoutPaths[] = DES_DIR . DS . $layoutPath . DS . $tmp_name . '.php';
        $layoutPaths[] = DES_DIR . DS . $layoutPath . DS . $parentDir . DS . $tmp_name . '.php';

    }

    if ($module == '') {
        $layoutPaths[] = dirname($file) . DS . 'templates' . DS . $tmp_name . '.php';
    } else {
        $layoutPaths[] = MODULES_DIR . DS . $module . DS . 'templates' . DS . $tmp_name . '.php';
    }

    $layoutPaths[] = DES_DIR . DS . 'SITE' . DS . 'layout' . DS . $tmp_name . '.php';
    $layoutPaths[] = CORE_DIR . DS . 'templates' . DS . $tmp_name . '.php';

    foreach ($layoutPaths as $file) {

        if (file_exists($file)) {

            ob_start();
            extract($vars);
            require($file);

            return ob_get_clean();

        }

    }

    ob_start();
    echo '<b>Шаблон ' . $tmp_name . ' не найден!</b>';
    debug($layoutPaths);
    $pathSearch = ob_get_clean();

    return $pathSearch;
}

/**
 *  Сортирует неассоциированный массив по указаному полю в прямом или обратном порядке.
 *  Ключи массива переопределяются.
 * @access public
 *
 * @param array $array Входящий массив
 * @param string $field pdescr
 * @param asc|desc $type Порядок сортировки
 *
 * @return array
 */
function ArraySortByField($array, $field, $type = 'asc') {
    global $array_sorting;

    $array = (array)$array;
    $field = (string)$field;
    $type = $type == 'asc' ? 'asc' : 'desc';

    if (!sizeof($array)) {
        return array();
    }

    $test = current($array);
    if (!isset ($test[$field])) {
        return array();
    }

    $array_sorting = array($field, $type);

    usort($array, 'ArraySortByField_usort');

    unset ($test);

    return $array;
}

function ArraySortByField_usort($first, $second) {
    global $array_sorting;

    if (!is_array($array_sorting) || sizeof($array_sorting) != 2) {
        die();
    }

    list ($field, $type) = $array_sorting;

    $first = $first[$field];
    $second = $second[$field];

    if ($first == $second) {
        return 0;
    }

    if (is_numeric($first)) {
        return ($type == 'asc') ? ($first < $second ? -1 : 1) : ($first > $second ? -1 : 1);
    } else {

        return ($type == 'asc') ? strcmp($first, $second) : strcmp($second, $first);
    }

}

/**
 * Проверям уникальность данного имени файла
 *
 * @param string $path
 *
 * @return string
 */
function uniqFile($path = "") {
    //echo $path;exit;

    if (is_file($path)) {
        $file_e = '.' . ExtractExt($path);
        $new = str_replace($file_e, '', $path);
        $new = $new . '_' . mt_rand(0, 99999) . $file_e;

        return uniqFile($new);
    } else {
        return $path;
    }
}

/**
 * Правильная закачка файла
 */
function UploadFiles() {
    static $files = null;

    if ($files === null) {

        /** Если переменные передачи файлов в форме были массивом */
        if (sizeof($_FILES) && is_array($_FILES[key($_FILES)]['name'])) {

            $f = array();
            foreach ($_FILES as $param) {
                foreach ($param as $field => $data) {
                    foreach ($data as $value) {
                        foreach ($value as $file => $data) {
                            $f[$file][$field] = $data;
                        }
                    }
                }
            }
            $_FILES = $f;
            unset ($f, $field, $data);

        }

        /**
         * Построим путь папок куда надо залить файл(ы)
         */
        $folder = explode(':', date('Y:m:d', NOW_TIME));
        $folder = FILES_DIR . DS . EndSlash(implode(DS, $folder), DS);
        foreach ($_FILES as $var => $file) {
            $files[$var] = UploadFile($file, $var, $folder);
        }

    }

    return $files;
}

/**
 * Загрузка едничиного файла
 */
function UploadFile($tmpfile, $post_field, $folder) {
    global $_DIRECTIVES;

    if (!$tmpfile['size'] && !$tmpfile['error']) {
        $tmpfile['error'] = 3;
    }

    $tmp_on = ValidFileName(mb_strtolower($tmpfile['name']));

    $file['var'] = $post_field;
    $file['original_name'] = ($tmp_on != "." || mb_strlen($tmp_on) > 3) ? $tmp_on : '';
    $file['error'] = $tmpfile['error'];

    switch ($file['error']) {
        case 0:
            $file['result_text'] = 'Файл успешно загружен';
            break;
        case 1:
            $file['result_text'] = 'Установки PHP не позволяют загружать столь большой файл';
            break;
        case 2:
            $file['result_text'] = 'Размер файла выше разрешенного';
            break;
        case 3:
            $file['result_text'] = 'Файл был закачен только частично';
            break;
        case 4:
            $file['result_text'] = 'Файл не был загружен, возможно не был указан';
            break;
        default:
            $file['result_text'] = 'Ошибка запроса. Вероятно файл не был загружен.';
            break;
    }

    if ($file['error'] != 0 || !is_uploaded_file($tmpfile['tmp_name'])) {
        return $file;
    }


    $end_file = uniqFile($folder . $file['original_name']);


    if (!DirExists($folder)) {
        debug('Не могу создать директорию ' . $folder, true, true);
    }
    if (move_uploaded_file($tmpfile['tmp_name'], $end_file) === false) {
        chmod($folder, 0755);
        if (!move_uploaded_file($tmpfile['tmp_name'], $end_file)) {
            debug('Невозможно перенести файл из временной директории. Проверьте права конечной директории хранения файлов', true, true);
        }
    }
    chmod($end_file, 0755);
    $file['path'] = str_replace(DOC_ROOT, '', $end_file);
    unset ($file['var'], $file['error'], $file['result_text']);

    return $file;
}

/**
 * Отдать страницу с 404 ошибкой + хедер
 */
function page404() {
    ob_clean();
    $page = MSCore::page('404');
    $page->lastFileLine = lastFileLine(1);
    echo $page;
    die();
}

if (!function_exists('mime_content_type')) {
    function mime_content_type($file, $method = 0) {
        if ($method == 0) {
            ob_start();
            system('/usr/bin/file -i -b ' . realpath($file));
            $type = ob_get_clean();

            $parts = explode(';', $type);

            return trim($parts[0]);
        } else {
            if ($method == 1) {
                // another method here
            }
        }
    }
}

function getTextMod($id, $strip_tags = false) {
    $text = MSCore::db()->GetOne("SELECT `text` FROM `" . PRFX . "texts` WHERE `id`=" . $id);

    return ($strip_tags) ? strip_tags($text) : $text;
}


function safe($str) {
    return htmlentities($str, ENT_QUOTES, SITE_ENCODING);
}

//------------------------------------------------------------------------------------------------------------------------------

// получаем числовую переменную из адресной строки, при этом проверяем ее и в случае неверного ввода выдаем 404 (с) Andruha
function getIntVal($num) {
    return mb_strlen(getParam($num)) && (!preg_match('/^[0-9]+$/i', getParam($num)) || getParam($num) == 0 || mb_strlen(getParam($num)) > mb_strlen((int)getParam($num))) ? page404() : (int)getParam($num);
}

function getInt($val, $empty = false) {
    return mb_strlen($val) && (!preg_match('/^[0-9]+$/i', $val) || ($val == 0 && !$empty) || mb_strlen($val) > mb_strlen((int)$val)) ? page404() : (int)$val;
}

function validExt($data, $get_ext = false) {
    if ($get_ext) {
        $info = pathinfo($data);
        $data = $info['extension'];
    }

    return !(in_array(mb_strtolower($data), array('php', 'php3', 'php4', 'php5', 'phtml', 'asp', 'aspx', 'ascx', 'jsp', 'cfm', 'cfc', 'pl', 'bat', 'exe', 'dll', 'reg', 'cgi', 'sh', 'py', 'asa', 'asax', 'config', 'com', 'inc')));
}

/**
 * Матчит текущую страницу по номеру
 *
 * @param $pageParam
 * @return null
 */
function matchCurrentPage($pageParam) {
    return preg_match('/^page([0-9]+)$/', $pageParam, $match) ? $match[1] : null;
}

/**
 * Функция обратного вызова для array_walk().
 * Заменяет $replace[0] на $replace[1] в каждом элементе массива
 *
 * @param $value
 * @param $key
 * @param array $replace Параметры для sqr_replace()
 */
function arrayWalkReplace(&$value, $key, $replace = array('', '')) {
    $value = str_replace($replace[0], $replace[1], $value);
    unset($value);
}

function curlRequest($url, $params = array()) {

    //decode bad urls
    $url = html_entity_decode($url);

    //set parameters
    $method = (isset($params['method'])) ? $params['method'] : 'GET';
    $postdata = (isset($params['postdata']) && is_array($params['postdata'])) ? $params['postdata'] : array();

    //set user agent
    $agent = 'Mozilla/5.0 (compatible; MSIE 10.0; Windows NT 6.2; Win64; x64; Trident/6.0)';

    //set custom headers
    $headers = array(
        'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
        'Accept-Language: ru-ru,ru;q=0.8,en-us;q=0.5,en;q=0.3',
        'Connection: keep-alive'
    );

    //set curl options
    $handle = curl_init();

    curl_setopt($handle, CURLOPT_URL, $url);
    curl_setopt($handle, CURLOPT_USERAGENT, $agent);
    curl_setopt($handle, CURLOPT_FOLLOWLOCATION, true);

    curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($handle, CURLOPT_HEADER, false);

    curl_setopt($handle, CURLOPT_HTTPGET, true);
    curl_setopt($handle, CURLOPT_AUTOREFERER, 1);

    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_TIMEOUT, 30);
    curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 30);
    curl_setopt($handle, CURLOPT_MAXREDIRS, 6);
    curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);

    if ($method === 'POST') {
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, http_build_query($postdata));
    } else {
        curl_setopt($handle, CURLOPT_HTTPGET, true);
    }

    $content = curl_exec($handle);
    curl_close($handle);

    return $content;
    //return json_decode($content, true);
}

function preLinkAbsolute($link) {
    $link = trim($link);
    $result = array('href' => $link, 'title' => $link);
    $pattern = '/^(?:https?|s?ftp|mailto):(?:\/\/)?/i';
    if (!preg_match($pattern, $link)) {
        $result['href'] = ('http://' . $link);
    }
    $result['title'] = trim(preg_replace($pattern, '', $link), '/');
    return $result;
}

function getMailer($cat = 1) {

    $cat = is_array($cat) ? 'cat IN(' . implode(',', $cat) . ')' : 'cat = ' . (int)$cat;
    $Mailer = new MSTable('{mailer}');
    $Mailer
        ->setFields(array('mail'))
        ->setFilter(array('cat' => $cat));

    $result = $Mailer->getItems() or array();

    if (count($result)) {

        if (count($result) > 1) {
            foreach ($result as $mailer) {
                $tmp[] = $mailer['mail'];
            }
            unset($result);
            return implode(',', $tmp);
        } else {
            return $result[0]['mail'];
        }
    }

    return false;
}

function cutSpaces($str) {
    return str_replace(' ', '', $str);
}

function formatCost($value) {
    return number_format((int)cutSpaces($value), 0, '', ' ');
}

function pluralForm($n, $form1, $form2, $form5) {
    $n = abs($n) % 100;
    $n1 = $n % 10;
    if ($n > 10 && $n < 20) return $form5;
    if ($n1 > 1 && $n1 < 5) return $form2;
    if ($n1 == 1) return $form1;
    return $form5;
}

function readMore($text, $breakPattern = '[#READMORE#]', $view = 'common/text-divider') {

    $searchPattern = preg_replace('/([^\w\s])/usiU', '\\\\$1', $breakPattern);
    $result = preg_replace('/(\<[a-z]+\>[^\<\>]*)' . $searchPattern . '\s*(\<\/[a-z]+\>)/suiU', '$1$2' . $breakPattern, $text);
    $result = preg_replace('/(\<[a-z]+\>)\s*(\<\/[a-z]+\>)\s*' . $searchPattern . '/suiU', $breakPattern, $result, 1);
    $texts = explode($breakPattern, $result);
    if (count($texts) > 1) {
        return template($view, array('visible' => $texts[0], 'hidden' => $texts[1]));
    }
    return template($view, array('visible' => $texts[0], 'hidden' => null));
}

function formatPhone($phone) {
    return preg_replace('/^(\+?\d{1}\s)?\(?\d{3}\)?/', '<b>$0</b>', $phone);
}

function emptyFiles($items)
{
    if($items == 'a:0:{}' || empty($items) || $items == '' || $items == null) return true;
    return false;
}

function getDomain() {
    if(WWW_REDIRECT) {
        return 'www.'. DOMAIN ;
    } else {
        return DOMAIN;
    }
}

function getDomainMobile() {
    if(WWW_REDIRECT) {
        return 'www.'. DOMAIN_MOBILE ;
    } else {
        return DOMAIN_MOBILE;
    }
}

function getDomainDesktop() {
    if(WWW_REDIRECT) {
        return 'www.'. DOMAIN_DESKTOP ;
    } else {
        return DOMAIN_DESKTOP;
    }
}

function arrayColumnize($sourceArray, $columnCount) {
    if(empty($columnCount)) return array($sourceArray);

    $arraySize = count($sourceArray);
    $perColumn = floor($arraySize / $columnCount);
    $columnModulo = $arraySize % $columnCount;
    $columnOffsets = array_fill(0, $columnCount, $perColumn);

    if(!empty($columnModulo)) {
        for($i = 0; $i < $columnCount; $i++) {
            if(empty($columnModulo)) break;
            $columnOffsets[$i]++;
            $columnModulo--;
        }
    }

    $result = array();
    $offset = 0;
    for($i = 0; $i < $columnCount; $i++) {
        $result[] = array_slice($sourceArray, $offset, $columnOffsets[$i], true);
        $offset += $columnOffsets[$i];
    }

    unset($columnOffsets);
    return $result;
}

function addFooterScripts($code = null) {
$script = MSRegistry::get('footer-scripts') ?: '';
    $script .= "\n" . $code;
    MSRegistry::set('footer-scripts', $script);
}

function addFooterScriptsObject($key, $obj) {
    addFooterScripts('Site.registry.set(\''. $key .'\', \''. addslashes(json_encode($obj)) .'\');');
}
function getFooterScripts() {
    if ($scripts = MSRegistry::get('footer-scripts')) {
        return '<script type="text/javascript">' . $scripts . '</script>';
    }
    return null;
}

function siteURL()
{
	$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
	$domainName = $_SERVER['HTTP_HOST'];

	return $protocol . $domainName;
}

function setCanonicalUrl()
{
	$url = $_SERVER['REQUEST_URI'];
	$url = preg_replace('/page([0-9]+)\//', '', $url);
	$url = siteURL() . $url;

	addBeforetag('<link rel="canonical" href="'. $url .'"/>','</head>');
}