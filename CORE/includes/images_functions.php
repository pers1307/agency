<?php
function makeImageThumbs($data, $upload_files, $config_name, $create_sys_thumbs = false) {

    static $Images = null;

    if (isset($upload_files[$config_name]['error']) && $upload_files[$config_name]['error'] == 4) {
        $data['value'] = isset($result['path']) ? serialize($result['path']) : '';
        return false;
    }

    if (isset($upload_files[$config_name]['error']) || (isset($data['allowed']) && !in_array(ExtractExt($upload_files[$config_name]['path']), $data['allowed']))) {
        if (isset($upload_files[$config_name]['error']) && $upload_files[$config_name]['error'] != "") {
            debug($upload_files[$config_name]['error'], 1, 0, '', false);
        } else {
            debug('В конфигурационном поле "<b>' . $config_name . '</b>", загружаемый файл имеет не допустимое расширение. Доступны только: <b>' . implode(',', $data['allowed']) . '</b>', 1, 0, '', false);
        }

        $data['value'] = '';

        return false;
    }

    $temp = array();

    $upload_file_name = $upload_files[$config_name]['path'];

    $upload_file_name_fs = DOC_ROOT . $upload_file_name;

    $folder = explode(':', date('Y:m:d', NOW_TIME));
    $folder = FILES_DIR . DS . EndSlash(implode(DS, $folder), DS);

    //-------исходные размеры---------
    list($w0, $h0) = getimagesize($upload_file_name_fs);

    foreach ($data['thumbs'] as $th_name => $th) {
        //----создаем превьюшки аплоадного файла
        $w1 = (int)$th[0];
        $h1 = (int)$th[1];

        $crop = !empty($th[2]) ? true : false;

        $new_name = explode('.', basename($upload_files[$config_name]['path']));
        $new_name[0] = $new_name[0] . '_' . $w1 . '_' . $h1;
        $new_name = uniqFile($folder . implode('.', $new_name));

        //--------условия, при которых изменять размер изображения не надо
        if (
            ($w1 == 0 && $h1 == 0) || //----указаны нули - сохранение оригинала
            ($w1 != 0 && $h1 != 0 && $h0 <= $h1 && $w0 <= $w1) || //----указан прямоугольник для масшатбирования, но исходное изображение меньше прямоугольника
            ($h1 == 0 && $w0 <= $w1) || //----указано масштабирование по ширине, но она меньше указанной
            ($w1 == 0 && $h0 <= $h1) //----то же, только по высоте
        ) {

            //-----сохраняем оригинал----------
            $z = copy($upload_file_name_fs, $new_name);

        } else {
            //------изменим размер---------------------------

            if ($w1 != 0 && $h1 != 0 && !$crop) {
                //-----надо вписывать в прямоугольник-------


                //-----пропорции изображения-----------------
                $p0 = $w0 / $h0;
                $p1 = $w1 / $h1;

                if ($p0 > $p1) {
                    $h1 = 0;
                } else $w1 = 0;

            }

            if (class_exists('Imagick')) {

                $thumb = new Imagick();
                $thumb->readImage($upload_file_name_fs);
                if ($crop) {
                    $r = $thumb->cropThumbnailImage($w1, $h1);
                } else $r = $thumb->thumbnailImage($w1, $h1);
                $thumb->writeImage($new_name);
                $thumb->destroy();

            } else {

                if(is_null($Images))
                {
                    $Images = new ImageToolbox();
                }
                $Images->newImage($upload_file_name_fs);
                $Images->newOutputSize($w1, $h1, $crop, false);

                $r = $Images->save($new_name, 'jpg');

            }
        }

        chmod($new_name, 0755);
        $temp[$th_name] = str_replace(array(DOC_ROOT, '\\'), array('', '/'), $new_name);
    }

    //----создаем системное изображение если надо
    if ($create_sys_thumbs) {
        $temp['sys_thumb'] = str_replace(array(DOC_ROOT, '\\'), array('', '/'), makeSysImageThumb($upload_file_name));
    }

    //----удаляем ориджинал
    unlink($upload_file_name_fs);

    $data['value'] = serialize($temp);

    if (isset($temp) && is_array($temp)) {
        $data['value'] = serialize($temp);
    }

    return $data['value'];
}


function deleteAllimagesBySerialize($str) {
    $images = unserialize($str);
    foreach ($images as $img) {
        if ($img != '' && file_exists(DOC_ROOT . $img)) {
            unlink(DOC_ROOT . $img);
        }
    }
}


function getImageURL($images, $num) {
    if (!empty($images)) {
        $image = unserialize(html_entity_decode($images, ENT_QUOTES));

        if (!empty($image[$num]) && is_file(DOC_ROOT . $image[$num])) {
            return $image[$num];
        }
    }

    return false;
}

function getImageHTML($images, $num = 1, $title = '', $params = '') {
    $image = getImageURL($images, $num);

    if (!$image) {
        return false;
    }

    list($w, $h, $p) = getimagesize(DOC_ROOT . $image);
    return '<img src="' . $image . '" width="' . $w . '" height="' . $h . '" alt="' . $title . '" title="' . $title . '" border="0" ' . $params . ' />';
}

function getGroupImagesURL($images, $num = 0, $text_enable = false) {
    $res = array();

    if (!empty($images)) {
        $images = unserialize(html_entity_decode($images, ENT_QUOTES));

        foreach ($images as $n => $image) {
            if (!empty($image['path'][$num]) && is_file(DOC_ROOT . $image['path'][$num])) {
                if ($text_enable) {
                    $res[$n]['path'] = $image['path'][$num];
                    $res[$n]['text'] = isset($image['text']) ? $image['text'] : '';
                } else {
                    $res[$n] = $image['path'][$num];
                }
            }
        }
    }

    return $res;
}

function getGroupImagesHTML($images, $num = 0, $text_enable = false, $title = '', $params = '') {
    $images = getGroupImagesURL($images, $num, true);

    $res = array();
    foreach ($images as $n => $image) {
        if ($text_enable) {
            $title = $image['text'];
        }

        list($w, $h, $p) = getimagesize(DOC_ROOT . $image['path']);
        $res[$n] = '<img src="' . $image['path'] . '" width="' . $w . '" height="' . $h . '" alt="' . $title . '" title="' . $title . '" border="0" ' . $params . ' />';
    }

    return $res;
}

function getGroupImageURL($images, $num = 0, $col = 0) {
    $images = getGroupImagesURL($images, $num);
    return isset($images[$col]) ? $images[$col] : false;
}

function getGroupImageHTML($images, $num = 0, $col = 0, $text_enable = false, $title = '', $params = '') {
    $images = getGroupImagesHTML($images, $num, $text_enable, $title, $params);
    return isset($images[$col]) ? $images[$col] : false;
}

function getImageURLFromCollection($images, $item_id, $num) {
    if ($images != "") {
        $s = html_entity_Decode($images, ENT_QUOTES);
        $s = unserialize($s);
        return isset($s[$item_id]['path'][$num]) ? $s[$item_id]['path'][$num] : '';
    } else return '';
}

function makeSysImageThumb($orig_filename) {
    static $Images;

    $folder = explode(':', date('Y:m:d', NOW_TIME));
    $folder = FILES_DIR . DS . EndSlash(implode(DS, $folder), DS);

    /** Делаем првеьюшку для вывда в конфигураторе в админке */
    $new_name = explode('.', basename($orig_filename));
    $new_name[0] = $new_name[0] . '_75_0';
    $new_name = uniqFile($folder . implode('.', $new_name));

    if (class_exists('Imagick')) {

        $thumb = new Imagick();
        $thumb->readImage(DOC_ROOT . $orig_filename);
        $thumb->thumbnailImage(75, 0);
        $thumb->writeImage($new_name);
        $thumb->destroy();

    } else {

        if(is_null($Images))
        {
            $Images = new ImageToolbox();
        }

        $Images->newImage(DOC_ROOT . $orig_filename);
        $Images->newOutputSize(75, 0, false, false);
        $Images->save($new_name, 'jpg');

    }

    chmod($new_name, 0755);

    return $new_name;
}