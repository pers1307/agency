<?php

class ApiCallback extends MSBaseApi
{
    protected $_errorMessages = array(
        1001 => 'System error',
        1004 => 'Not found',
    );

    public function before()
    {
        parent::before();
        $csrf = MSCore::csrf();
        if(empty($_POST['csrf']) || !$csrf->isTokenValid($_POST['csrf'])) {
            $this->errorAction(400);
        }
        unset($csrf);
    }

    public function contactAction()
    {
        if (isset($_POST)) {
            $data = [];

            try {

                if (!isset($_POST['name'])) {
                    throw new Exception(
                        'name'
                    );
                }

                $data['name'] = htmlspecialchars($_POST['name']);

                if (!isset($_POST['phone'])) {
                    throw new Exception(
                        'phone'
                    );
                }

                $data['phone'] = htmlspecialchars($_POST['phone']);

                if (!isset($_POST['email'])) {
                    throw new Exception(
                        'email'
                    );
                }

                $data['email'] = htmlspecialchars($_POST['email']);

                if (!isset($_POST['comment'])) {
                    throw new Exception(
                        'comment'
                    );
                }

                $data['comment'] = htmlspecialchars($_POST['comment']);

                if (!isset($_POST['address'])) {
                    throw new Exception(
                        'honeyPot'
                    );
                }

                $honeyPot = htmlspecialchars($_POST['address']);
                $data['honeyPot'] = $honeyPot;

                // Проверка на бота
                if ($honeyPot != '') {
                    $this->errorAction(1001, 'Custom system error', ['honeyPot' => 'honeyPot']);
                }

                // Валидация
                $v = new Validator([
                    'name'    => $data['name'],
                    'phone'   => $data['phone'],
                    'email'   => $data['email'],
                    'comment' => $data['comment'],
                ]);

                $v->rule('required', 'name')->message('name!');
                $v->rule('regex', 'name', '/^([a-zа-я\s\-]+)$/iu')->message('name!!');

                $v->rule('required', 'phone')->message('phone!');
                $v->rule('phone', 'phone')->message('phone!!');

                $v->rule('required', 'email')->message('email!');
                $v->rule('email', 'email')->message('email!!');

                if($v->validate()) {

                    $msg   = template('email/contact', $data);
                    $title = "Заказ обратного звонка с сайта " . DOMAIN;

                    $mail = new SendMail();
                    $mail->init();
                    $mail->setEncoding("utf8");
                    $mail->setEncType("base64");
                    $mail->setSubject($title);
                    $mail->setMessage($msg);
                    $mail->setFrom("noreply@" . DOMAIN, "");
                    $emails = MSCore::db()->getCol('SELECT `mail` FROM `'.PRFX.'mailer`');

                    foreach ($emails as $email) {
                        $mail->setTo($email);
                        $mail->send();
                    }

                    $sql = "
                        INSERT INTO mp_list(`title`,`text`)
                        VALUES('" . $title . "','" . $msg . "');
                    ";
                    MSCore::db()->execute($sql);

                    $this->addData(['succes' => 'Ok']);
                    $this->successAction();
                } else {
                    $errors = $v->errors();
                    foreach($errors as $_name => $_error) {
                        if(is_array($_error)) {
                            $errors[$_name] = reset($_error);
                        }
                    }

                    $this->errorAction(1001, 'Custom system error', ['data' => $data, 'error' => $errors]);
                }

            } catch (Exception $exception) {

                $error = $exception->getMessage();
                $this->errorAction(1001, 'Custom system error', ['error' => $error, 'postArgument' => 'noPostArgument']);
            }
        }
    }

    public function callbackAction()
    {
        if (isset($_POST)) {
            $data = [];

            try {
                if (!isset($_POST['name'])) {
                    throw new Exception(
                        'name'
                    );
                }

                $data['name'] = htmlspecialchars($_POST['name']);

                if (!isset($_POST['phone'])) {
                    throw new Exception(
                        'phone'
                    );
                }

                $data['phone'] = htmlspecialchars($_POST['phone']);

                if (!isset($_POST['comment'])) {
                    throw new Exception(
                        'comment'
                    );
                }

                $data['comment'] = htmlspecialchars($_POST['comment']);

                if (!isset($_POST['address'])) {
                    throw new Exception(
                        'honeyPot'
                    );
                }

                $honeyPot = htmlspecialchars($_POST['address']);
                $data['honeyPot'] = $honeyPot;

                // Проверка на бота
                if ($honeyPot != '') {
                    $this->errorAction(1001, 'Custom system error', ['honeyPot' => 'honeyPot']);
                }

                // Валидация
                $v = new Validator([
                    'name'    => $data['name'],
                    'phone'   => $data['phone'],
                    'comment' => $data['comment'],
                ]);

                $v->rule('required', 'name')->message('name!');
                $v->rule('regex', 'name', '/^([a-zа-я\s\-]+)$/iu')->message('name!!');

                $v->rule('required', 'phone')->message('phone!');
                $v->rule('phone', 'phone')->message('phone!!');

                if($v->validate()) {

                    $msg   = template('email/contact', $data);
                    $title = "Заказ обратного звонка с сайта " . DOMAIN;

                    $mail = new SendMail();
                    $mail->init();
                    $mail->setEncoding("utf8");
                    $mail->setEncType("base64");
                    $mail->setSubject($title);
                    $mail->setMessage($msg);
                    $mail->setFrom("noreply@" . DOMAIN, "");
                    $emails = MSCore::db()->getCol('SELECT `mail` FROM `'.PRFX.'mailer`');

                    foreach ($emails as $email) {
                        $mail->setTo($email);
                        $mail->send();
                    }

                    $sql = "
                        INSERT INTO mp_list(`title`,`text`)
                        VALUES('" . $title . "','" . $msg . "');
                    ";
                    MSCore::db()->execute($sql);

                    $this->addData(['succes' => 'Ok']);
                    $this->successAction();
                } else {
                    $errors = $v->errors();
                    foreach($errors as $_name => $_error) {
                        if(is_array($_error)) {
                            $errors[$_name] = reset($_error);
                        }
                    }

                    $this->errorAction(1001, 'Custom system error', ['data' => $data, 'error' => $errors]);
                }

            } catch (Exception $exception) {

                $error = $exception->getMessage();
                $this->errorAction(1001, 'Custom system error', ['error' => $error, 'postArgument' => 'noPostArgument']);
            }
        }
    }

    public function agentAction()
    {
        if (isset($_POST)) {
            $data = [];

            try {

                if (!isset($_POST['name'])) {
                    throw new Exception(
                        'name'
                    );
                }
                $data['name'] = htmlspecialchars($_POST['name']);

                if (!isset($_POST['email'])) {
                    throw new Exception(
                        'email'
                    );
                }
                $data['email'] = htmlspecialchars($_POST['email']);

                if (!isset($_POST['comment'])) {
                    throw new Exception(
                        'comment'
                    );
                }
                $data['comment'] = htmlspecialchars($_POST['comment']);

                if (!isset($_POST['agentId'])) {
                    throw new Exception(
                        'agentId'
                    );
                }
                $data['agentId'] = htmlspecialchars($_POST['agentId']);

                if (!isset($_POST['address'])) {
                    throw new Exception(
                        'honeyPot'
                    );
                }

                $honeyPot = htmlspecialchars($_POST['address']);
                $data['honeyPot'] = $honeyPot;

                // Проверка на бота
                if ($honeyPot != '') {
                    $this->errorAction(1001, 'Custom system error', ['honeyPot' => 'honeyPot']);
                }

                // Валидация
                $v = new Validator([
                    'name'    => $data['name'],
                    'email'   => $data['email'],
                    'comment' => $data['comment'],
                ]);

                $v->rule('required', 'name')->message('name!');
                $v->rule('regex', 'name', '/^([a-zа-я\s\-]+)$/iu')->message('name!!');

                $v->rule('required', 'email')->message('email!');
                $v->rule('email', 'email')->message('email!!');

                if($v->validate()) {
                    $msg   = template('email/contact', $data);
                    $title = "Заявка агенту с сайта " . DOMAIN;

                    $mail = new SendMail();
                    $mail->init();
                    $mail->setEncoding("utf8");
                    $mail->setEncType("base64");
                    $mail->setSubject($title);
                    $mail->setMessage($msg);
                    $mail->setFrom("noreply@" . DOMAIN, "");
                    $emails = MSCore::db()->getCol('SELECT `mail` FROM `'.PRFX.'mailer`');

                    if (!empty($data['agentId'])) {
                        /** @var ModelAgents $modelAgents */
                        $modelAgents = MSCore::site()->agents;

                        $agentEmail = $modelAgents->getEmailById($data['agentId']);

                        if (!is_null($agentEmail)) {
                            array_push($emails, $agentEmail);
                        }
                    }

                    foreach ($emails as $email) {
                        $mail->setTo($email);
                        $mail->send();
                    }

                    $sql = "
                        INSERT INTO mp_list(`title`,`text`)
                        VALUES('" . $title . "','" . $msg . "');
                    ";
                    MSCore::db()->execute($sql);

                    $this->addData(['succes' => 'Ok']);
                    $this->successAction();
                } else {
                    $errors = $v->errors();
                    foreach($errors as $_name => $_error) {
                        if(is_array($_error)) {
                            $errors[$_name] = reset($_error);
                        }
                    }

                    $this->errorAction(1001, 'Custom system error', ['data' => $data, 'error' => $errors]);
                }

            } catch (Exception $exception) {

                $error = $exception->getMessage();
                $this->errorAction(1001, 'Custom system error', ['error' => $error, 'postArgument' => 'noPostArgument']);
            }
        }
    }

    public function propertyAction()
    {
        if (isset($_POST)) {
            $data = [];

            try {
                if (!isset($_POST['name'])) {
                    throw new Exception(
                        'name'
                    );
                }

                $data['name'] = htmlspecialchars($_POST['name']);

                if (!isset($_POST['phone'])) {
                    throw new Exception(
                        'phone'
                    );
                }

                $data['phone'] = htmlspecialchars($_POST['phone']);

                if (!isset($_POST['comment'])) {
                    throw new Exception(
                        'comment'
                    );
                }

                $data['comment'] = htmlspecialchars($_POST['comment']);

                if (!isset($_POST['address'])) {
                    throw new Exception(
                        'honeyPot'
                    );
                }

                $honeyPot = htmlspecialchars($_POST['address']);
                $data['honeyPot'] = $honeyPot;

                // Проверка на бота
                if ($honeyPot != '') {
                    $this->errorAction(1001, 'Custom system error', ['honeyPot' => 'honeyPot']);
                }

                // Валидация
                $v = new Validator([
                    'name'    => $data['name'],
                    'phone'   => $data['phone'],
                    'comment' => $data['comment'],
                ]);

                $v->rule('required', 'name')->message('name!');
                $v->rule('regex', 'name', '/^([a-zа-я\s\-]+)$/iu')->message('name!!');

                $v->rule('required', 'phone')->message('phone!');
                $v->rule('phone', 'phone')->message('phone!!');

                if($v->validate()) {

                    $msg   = template('email/contact', $data);
                    $title = "Заявка на продажу недвижимости с сайта " . DOMAIN;

                    $mail = new SendMail();
                    $mail->init();
                    $mail->setEncoding("utf8");
                    $mail->setEncType("base64");
                    $mail->setSubject($title);
                    $mail->setMessage($msg);
                    $mail->setFrom("noreply@" . DOMAIN, "");
                    $emails = MSCore::db()->getCol('SELECT `mail` FROM `'.PRFX.'mailer`');

                    foreach ($emails as $email) {
                        $mail->setTo($email);
                        $mail->send();
                    }

                    $sql = "
                        INSERT INTO mp_list(`title`,`text`)
                        VALUES('" . $title . "','" . $msg . "');
                    ";
                    MSCore::db()->execute($sql);

                    $this->addData(['succes' => 'Ok']);
                    $this->successAction();
                } else {
                    $errors = $v->errors();
                    foreach($errors as $_name => $_error) {
                        if(is_array($_error)) {
                            $errors[$_name] = reset($_error);
                        }
                    }

                    $this->errorAction(1001, 'Custom system error', ['data' => $data, 'error' => $errors]);
                }

            } catch (Exception $exception) {

                $error = $exception->getMessage();
                $this->errorAction(1001, 'Custom system error', ['error' => $error, 'postArgument' => 'noPostArgument']);
            }
        }
    }
}