<?php

    class ApiTest extends MSBaseApi {

        protected $_errorMessages = array(
            1001 => 'System error',
            1004 => 'Not found',
        );

        public function methodAction() {

            $a = 20;
            $b = 30;

            if($a > $b) {
                $this->errorAction(1001, 'Custom system error', array('dataError' => 'error message'));
            }

            $this->addData(array(
                'content' => 'Hello, world!'
            ));

            //$this->successAction();
        }
    }