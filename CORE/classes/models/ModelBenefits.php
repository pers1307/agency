<?php

    class ModelBenefits extends MSBaseTape
    {
        public $itemsTableName = '{benefits}';

        /**
         * @return array
         */
        public function getAll()
        {
            $query = new MSTable($this->itemsTableName);
            $query->setFields(['*']);
            $query->setOrder('order');
            $query->setFilter('`active` = 1');

            return $query->getItems();
        }
    }