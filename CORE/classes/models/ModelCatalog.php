<?php
    /**
     * @author Andruha <andruha@mediasite.ru>
     * @copyright Mediasite LLC (http://www.mediasite.ru/)
     */

    class ModelCatalog extends MSBaseCatalog
    {
        public
            $itemsTableName = '{catalog_items}',
            $articlesTableName = '{catalog_articles}';
    }