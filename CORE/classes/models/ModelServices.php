<?php
/**
 * @author Andruha <andruha@mediasite.ru>
 * @copyright Mediasite LLC (http://www.mediasite.ru/)
 */

class ModelServices extends MSBaseCatalog
{
    public
        $itemsTableName = '{services_items}',
        $articlesTableName = '{services_articles}';

    /**
     * @param int $articleId
     * @return array
     */
    public function getArticlesByParent($articleId)
    {
        $query = new MSTable($this->articlesTableName);
        $query->setFields(['*']);
        $query->setFilter("`parent` = " . $articleId);
        $query->setFilter("`active` = 1");

        return $query->getItems();
    }

    /**
     * @param bool $recursive
     *
     * @return array
     */
    public function getItemsSeparetedByArticles($recursive = false)
    {
        $this->itemsOrder = '`parent`, `order`';
        $items = $this->getItems(['*'], $recursive);

        $result = [];

        foreach ($items['items'] as $item) {
            $result[$item['parent']][] = $item;
        }

        foreach ($this->_articles as $article) {
            if (isset($result[$article['id']])) {
                $result[$article['name']] = $result[$article['id']];
                unset($result[$article['id']]);
            }
        }

        $items['items'] = $result;

        return $items;
    }
}