<?php

class ModelSiteSearch {

    /**
     * Имя таблицы с поисковым индексом
     *
     * @var mixed|string
     */
    public $indexTableName = '{search_index}';
    /**
     * Экземпляр таблицы с поисковым индексом
     *
     * @var MSTable|null
     */
    public $indexTable = null;
    /**
     * Экземпляр класса постраничной навигации
     *
     * @var null
     */
    public $pagination = null;
    /**
     * Колонки, по которым будет осуществлен поиск
     * Не забудь внести их в полнотекстовый индекс MySQL
     *
     * ВНИМАНИЕ: поля необходимо указывать, от большего к меньшему
     * в обратной зависимости от релевантности поля.
     * Например, array('title', 'text'), здесь `title` будет иметь
     * больший вес чем `text`
     *
     * @var array
     */
    public $columns = array();

    /**
     * Поля которые будут выбраны в результате поиска
     *
     * @var array
     */
    public $fields = array();

    /**
     * Указываем на необходимость подсветки в результатах поиска
     * По умолчание подсветка отключена
     *
     * @var bool
     */
    public $backlight = false;

    /**
     * Поле, по которому будет осуществлена сортировка
     * По умолчанию сортировка по релевантности
     *
     * @var string
     */
    public $order = '`score`';

    /**
     * Указыает на необходимость подсчета найденных результатов
     * По умолчанию подсчет не ведется
     *
     * @var bool
     */
    public $needCount = false;
    /**
     * Текущая страница
     *
     * @var null
     */
    public $currentPage = null;

    /**
     * Метод конструктор
     * 1. Создает экземпляр таблицы поискового индекса
     * 2. Принимает и назначает поля для поиска и выборки
     *
     * @param $columns
     * @param $fields
     */
    public function __construct($columns = array(), $fields = array()) {

        $this->indexTableName = MSTable::tablePrefix($this->indexTableName);
        $this->indexTable = new MSTable($this->indexTableName);

        $this->columns = $columns ?: array('title', 'text'); // где ищем
        $this->fields = $fields ?: array('title', 'uri as `itemLink`', 'text'); // что выбираем

        $this->columns = array_map(
            function($element) {
                return '`'. trim($element, '.` ') . '`';
            },
            $this->columns
        );

        $vars = MSCore::urls()->vars;
        $lastParam = null;
        if($vars) $lastParam = end($vars);
        $this->currentPage = matchCurrentPage($lastParam);

        if(!$this->currentPage && getParam(0)) {
            Page404();
        }
    }

    /**
     * Возвращает массив позиций по результатам поиска
     *
     * @param $query
     * @return array
     */
    public function findItems($query = null) {

        $limit = null;
        $out = array(
            'modelSearch' => $this,
            'items' => array(),
            'pagination' => null,
            'query' => HelperSearch::decodeSearchQuery($query),
            'count' => 0
        );

        $preparedQuery = HelperSearch::prepareSearchQuery($query);
        if(empty($preparedQuery) or empty($this->fields) or empty($this->columns)) return array();
        $fields = $this->fields;

        if(trim($this->order, '` ') === 'score') {
            if($scoreField = HelperSearch::getScoreField($this->columns, $query, $preparedQuery)) {
                $fields[] = $scoreField;
            }
        }

        $this->indexTable->setFields($fields);
        $this->indexTable->setFilter(array('MATCH ('.implode(' , ' ,$this->columns).') AGAINST ("'.$preparedQuery.'" IN BOOLEAN MODE)'));
        $this->indexTable->setOrder($this->order .' DESC');
        $this->indexTable->setHaving(array('score > 0.2'));

        $count = null;
        if($this->needCount) {
            $count = $this->indexTable->getCount();
        }

        if ($this->pagination) {

            is_array($this->pagination) or $this->pagination = array();

            !empty($this->pagination['linkMask']) or $this->pagination['linkMask']  = '/' . MSCore::urls()->path . '/%link%/';
            !empty($this->pagination['autoTitle']) or $this->pagination['autoTitle'] = true;

            $this->pagination['needFilters'] = true;

            $Pagination = new MSBasePagination(
                $this->pagination,
                $count ?: $this->indexTable->getCount(),
                $this->currentPage
            );

            $limit = $Pagination->getLimit();
            $out['pagination'] = $Pagination;
        }

        $this->indexTable->setLimit($limit);

        //debug($this->indexTable->makeSql()); die;

        $items = $this->indexTable->getItems();

        if($this->backlight) {
            foreach ($items as &$item) {
                foreach($this->columns as $_column) {
                    $_column = trim($_column, '.`i ');
                    if(isset($item[$_column])) {
                        $item[$_column] = HelperSearch::processingSearchResult($item[$_column], $preparedQuery);
                    }
                }
            } unset($item);
        }

        $out['count'] = $count;
        $out['items'] = $items;
        return $out;
    }
}