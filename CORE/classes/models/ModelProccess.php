<?php
/**
 * Proccess pid checker
 * @author bmxnt <bmxnt@mail.ru>
 */

/*
    $pid = 'converter.pid';
    if(ModelProccess::checkPid($pid) == 1) {
        echo 'Pid file already exists';
        exit;
    } else {
        ModelProccess::removePid($pid);
        ModelProccess::createPid($pid);
    }

    // [Your code here]

    ModelProccess::removePid($pid);
*/

class ModelProccess {
    /**
     * @param $name
     * @param int $ttl
     * @return int 0 - not exists, 1 - success, 2 - expired
     */
    public static function checkPid($name, $ttl = 3600) {
        $pid = TMP_DIR . '/' . $name;
        if(file_exists($pid)) {
            $created = (int) file_get_contents($pid);
            if($created + $ttl < time()) {
                return 2;
            }
            return 1;
        }
        return 0;
    }

    /**
     * @param $name
     */
    public static function createPid($name) {
        $pid = TMP_DIR . '/' . $name;
        $handle = fopen($pid, 'w');
        fwrite($handle, time());
        fclose($handle);
    }

    /**
     * @param $name
     */
    public static function removePid($name) {
        $pid = TMP_DIR . '/' . $name;
        if(file_exists($pid)) {
            @unlink($pid);
        }
    }
}