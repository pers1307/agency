<?php
/**
 * @author Andruha <andruha@mediasite.ru>
 * @copyright Mediasite LLC (http://www.mediasite.ru/)
 */

class ModelProperty extends MSBaseTape
{
    public $itemsTableName = '{properties}';

    /**
     * @param string $ids
     *
     * @return array
     */
    public function getItemsByIds($ids)
    {
        if (empty($ids)) {
            return [];
        }

        $query = new MSTable($this->itemsTableName);
        $query->setFields(['*']);
        $query->setFilter("`id` IN (" . $ids . ")");
        $query->setFilter("`active` = 1");
        $items = $query->getItems();

        foreach ($items as &$item) {
            if (!empty($item[$this->keyUniqueName])) {
                $item['itemLink'] = $this->path . '/' . ($item[$this->keyUniqueName] ? $item[$this->keyUniqueName] : $item[$this->keyPrimaryName]) . '/';
            }

            $item['image']    = unserialize($item['image']);
            $item['geocoder'] = json_decode($item['geocoder'], true);
        }

        return $items;
    }

    public function getLastItems()
    {
        $query = new MSTable($this->itemsTableName);
        $query->setFields(['*']);
        $query->setFilter("`active` = 1");
        $query->setOrder('`date_create` DESC');
        $query->setLimit(3);
        $items = $query->getItems();

        foreach ($items as &$item) {
            if (!empty($item[$this->keyUniqueName])) {
                $item['itemLink'] = $this->path . '/' . ($item[$this->keyUniqueName] ? $item[$this->keyUniqueName] : $item[$this->keyPrimaryName]) . '/';
            }
        }

        return $items;
    }
}