<?php

    class ModelTestimonials extends MSBaseTape
    {
        public $itemsTableName = '{testimonials}';

        /**
         * @return array
         */
        public function getAll()
        {
            $query = new MSTable($this->itemsTableName);
            $query->setFields(['*']);
            $query->setFilter('`active` = 1');

            return $query->getItems();
        }
    }