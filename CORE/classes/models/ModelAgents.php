<?php
    /**
     * @author Andruha <andruha@mediasite.ru>
     * @copyright Mediasite LLC (http://www.mediasite.ru/)
     */

class ModelAgents extends MSBaseTape
{
    public $itemsTableName = '{agents}';

    public static $type = [
        0 => 'Юрист',
        1 => 'Риэлтор'
    ];

    /**
     * @return array
     */
    public function getFilter()
    {
        $filter = [];
        $url = '/' . preg_replace('/\/page\d+$/suiU','', MSCore::urls()->current) . '/';

        $filter[$url] = 'Все';
        foreach (self::$type as $key => $type) {
            $get = '?filter=' . $key;
            $filter[$url . $get] = $type;
        }

        return $filter;
    }

    /**
     * @param $propertyId
     * @return array|null
     */
    public function getByPropertyId($propertyId)
    {
        $query = new MSTable($this->itemsTableName);
        $query->setFields(['*']);
        $query->setFilter("FIND_IN_SET(" . $propertyId . ", properties)>0 ");
        $query->setFilter("`active` = 1");
        $result = $query->getItem();

        if (!empty($result)) {
            $result['itemLink'] = '/' . path($result['path_id']) . $result['code'] . '/';

            $result['images'] = $this->getImage($result['image']);

            if (!empty($result['properties']) && !is_null($result['properties'])) {
                $result['propertyCount'] = count(
                    explode(',', $result['properties'])
                );
            } else {
                $result['propertyCount'] = 0;
            }
        }

        return $result;
    }

    /**
     * @param string $serializeImage
     * @return array
     */
    public function getImage($serializeImage)
    {
        $images = unserialize($serializeImage);

        $result = [];
        $result['list']  = !empty($images[0]['path']['list'])  ? $images[0]['path']['list']  : '/DESIGN/SITE/images/no-image/no-image_260_280.jpg';
        $result['list2'] = !empty($images[0]['path']['list2']) ? $images[0]['path']['list2'] : '/DESIGN/SITE/images/no-image/no-image_280_300.jpg';
        $result['list3'] = !empty($images[0]['path']['list3']) ? $images[0]['path']['list3'] : '/DESIGN/SITE/images/no-image/no-image_770_830.jpg';
        $result['list4'] = !empty($images[0]['path']['list4']) ? $images[0]['path']['list4'] : '/DESIGN/SITE/images/no-image/no-image_950_1024.jpg';
        $result['about'] = !empty($images[0]['path']['about']) ? $images[0]['path']['about'] : '/DESIGN/SITE/images/no-image/no-image_350_380.jpg';

        return $result;
    }

    /**
     * @param string $ids
     * @return array
     */
    public function getByIds($ids)
    {
        $query = new MSTable($this->itemsTableName);
        $query->setFields(['*']);
        $query->setFilter("`id` IN (" . $ids . ")");
        $query->setFilter("`active` = 1");
        $items = $query->getItems();

        if (!empty($items)) {
            foreach ($items as &$result) {
                $result['itemLink'] = '/' . path($result['path_id']) . $result['code'] . '/';

                $result['images'] = $this->getImage($result['image']);

                if (!empty($result['properties']) && !is_null($result['properties'])) {
                    $result['propertyCount'] = count(
                        explode(',', $result['properties'])
                    );
                } else {
                    $result['propertyCount'] = 0;
                }
            }
        }

        return $items;
    }

    /**
     * @param $id
     * @return string|null
     */
    public function getEmailById($id)
    {
        $query = new MSTable($this->itemsTableName);
        $query->setFields(['email']);
        $query->setFilter("`id` = " . $id);
        $email = $query->getItemOne();

        return $email;
    }
}