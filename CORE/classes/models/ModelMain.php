<?php
/**
     * @author Andruha <andruha@mediasite.ru>
     * @copyright Mediasite LLC (http://www.mediasite.ru/)
     */

class ModelMain extends MSBaseTape
{
    public $itemsTableName = '{main}';

    /**
     * @param string $key
     *
     * @return string
     */
    public function getValueByKey($key)
    {
        $query = new MSTable($this->itemsTableName);
        $query->setFields(['value']);
        $query->setFilter("`key` = '{$key}'");
        $query->setFilter("`path_id` = {$this->pathId}");
        $result = $query->getItemOne();

        return $result;
    }
}