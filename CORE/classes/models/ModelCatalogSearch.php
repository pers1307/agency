<?php

class ModelCatalogSearch {

    /**
     * Экземпляр каталога
     *
     * @var
     */
    public $catalog;
    /**
     * Экземпляр таблицы позиций каталога
     *
     * @var
     */
    public $itemsTable;
    /**
     * Колонки, по которым будет осуществлен поиск
     * Не забудь внести их в полнотекстовый индекс MySQL
     *
     * ВНИМАНИЕ: поля необходимо указывать, от большего к меньшему
     * в обратной зависимости от релевантности поля.
     * Например, array('title', 'text'), здесь `title` будет иметь
     * больший вес чем `text`
     *
     * @var array
     */
    public $columns = array();

    /**
     * Поля которые будут выбраны в результате поиска
     *
     * @var array
     */
    public $fields = array();

    /**
     * Указываем на необходимость подсветки в результатах поиска
     * По умолчание подсветка отключена
     *
     * @var bool
     */
    public $backlight = false;

    /**
     * Поле, по которому будет осуществлена сортировка
     * По умолчанию сортировка по релевантности
     *
     * @var string
     */
    public $order = '`score`';

    /**
     * Указыает на необходимость подсчета найденных результатов
     * По умолчанию подсчет не ведется
     *
     * @var bool
     */
    public $needCount = false;
    /**
     * Текущая страница
     *
     * @var null
     */
    public $currentPage = null;

    /**
     * Метод конструктор
     * Устанавливает переданный экземпляр каталога, колонки для выборки и для поиска
     * Создает экземпляр таблицы позиций каталога
     *
     * @param $catalog
     * @param $columns
     * @param $fields
     */
    public function __construct($catalog, $columns, $fields) {

        $this->catalog = $catalog;
        $this->itemsTable = new MSTable($this->catalog->itemsTableName .' as `i`');

        $this->columns = array_map(
            function($element) {
                return '`i`.`'. trim($element, '.`i ') . '`';
            },
            $columns
        );

        $this->fields = $fields;

        if($this->catalog->currentPage) {
            $this->currentPage = $this->catalog->currentPage;
        } else {
            $vars = MSCore::urls()->vars;
            $lastParam = null;
            if($vars) $lastParam = end($vars);
            $this->currentPage = matchCurrentPage($lastParam);
        }

        if(!$this->currentPage && getParam(0)) {
            Page404();
        }
    }

    /**
     * Возвращает массив позиций по результатам поиска
     *
     * @param $query
     * @return array
     */
    public function findItems($query = null) {

        $limit = null;

        $model = clone $this;
        unset($model->catalog);

        $out = array(
            'modelSearch' => $model,
            'items' => array(),
            'pagination' => null,
            'query' => HelperSearch::decodeSearchQuery($query),
            'count' => 0
        );

        $preparedQuery = HelperSearch::prepareSearchQuery($query);
        if(empty($preparedQuery) or empty($this->fields) or empty($this->columns)) return $out;
        $fields = $this->fields;

        if(trim($this->order, '` ') === 'score') {
            if($scoreField = HelperSearch::getScoreField($this->columns, $query, $preparedQuery)) {
                $fields[] = $scoreField;
            }
        }

        $this->itemsTable->setFields($fields);
        $this->itemsTable->setFilter(array('MATCH ('.implode(' , ' ,$this->columns).') AGAINST ("'.$preparedQuery.'" IN BOOLEAN MODE)'));
        $this->itemsTable->setFilter(array('`i`.`active` = 1', '`i`.`path_id` =  '. $this->catalog->pathId));
        $this->itemsTable->setOrder($this->order . ' DESC');

        $count = null;
        if($this->needCount) {
            $count = $this->itemsTable->getCount();
        }

        if ($this->catalog->pagination) {

            is_array($this->catalog->pagination) or $this->catalog->pagination = array();

            !empty($this->catalog->pagination['linkMask']) or $this->catalog->pagination['linkMask']  = '/' . MSCore::urls()->path . '/%link%/';
            !empty($this->catalog->pagination['autoTitle']) or $this->catalog->pagination['autoTitle'] = $this->catalog->autoTitle;

            $this->catalog->pagination['needFilters'] = true;

            $Pagination = new MSBasePagination(
                $this->catalog->pagination,
                $count ?: $this->itemsTable->getCount(),
                $this->currentPage
            );

            $limit = $Pagination->getLimit();
            $out['pagination'] = $Pagination;
        }

        $this->itemsTable->setLimit($limit);

        //debug($this->itemsTable->makeSql()); die;

        $items = $this->itemsTable->getItems();

        if($this->backlight) {
            foreach ($items as &$item) {
                $item['itemLink'] = $this->catalog->path . '/' . $this->catalog->articleId2path[$item['parent']] . '/' . ($item[$this->catalog->itemKeyUniqueName] ? $item[$this->catalog->itemKeyUniqueName] : $item[$this->catalog->itemKeyPrimaryName]) . '/';
                foreach($this->columns as $_column) {
                    $_column = trim($_column, '.`i ');
                    if(isset($item[$_column])) {
                        $item[$_column] = HelperSearch::processingSearchResult($item[$_column], $preparedQuery);
                    }
                }
            }
        } else {
            foreach ($items as &$item) {
                $item['itemLink'] = $this->catalog->path . '/' . $this->catalog->articleId2path[$item['parent']] . '/' . ($item[$this->catalog->itemKeyUniqueName] ? $item[$this->catalog->itemKeyUniqueName] : $item[$this->catalog->itemKeyPrimaryName]) . '/';
            }
        }

        //debug($items);

        $out['count'] = $count;
        $out['items'] = $items;
        return $out;
    }
}