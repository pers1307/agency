<?php
/**
 * @author Andruha <andruha@mediasite.ru>
 * @copyright Mediasite LLC (http://www.mediasite.ru/)
 */

/**
 * Базовый класс модуля «Каталог»
 *
 * @property array $articles массив со всеми разделами
 * @property array $articleId2path
 * @property array $articleId2name
 * @property Tree $articlesTreeObject
 * @property bool $isItem
 * @property bool $isCatalogIndexPage
 * @property array $fullTree
 *
 * @property MSTable $itemsTable Экземпляр класса для работы с таблицей позиций
 * @property MSTable $articlesTable Экземпляр класса для работы с таблицей разделов
 *
 * @package Core.base
 */

class MSBaseCatalog extends MSBaseComponent
{
    /**
     * @var string Имя таблицы с позициями
     */
    public $itemsTableName;
    /**
     * @var string Имя поля с первичным ключом в таблице позиций
     */
    public $itemKeyPrimaryName = 'id';
    /**
     * @var string Имя поля с уникальным ключом в таблице позиций
     */
    public $itemKeyUniqueName = 'code';
    /**
     * @var bool Делать выборку только по активным позициям
     */
    public $onlyActiveItems = true;
    /**
     * @var string Имя таблицы с разделами
     */
    public $articlesTableName;
    /**
     * @var array Массив с именами столбцов для построения дерева разделов
     */
    public $articlesTreeFields = array();
    /**
     * @var string Имя поля для сортировки разделов каталога
     */
    public $articlesOrder = 'order';
    /**
     * @var string Имя поля для сортировки позиций каталога
     */
    public $itemsOrder = null;
    /**
     * @var boolean Делать выборку только по активным разделам каталога
     */
    public $onlyActiveArticles = true;
    /**
     * @var boolean Выбирать только те разделы, в которых есть позиции
     */
    public $onlyHaveItems = true;
    /**
     * @var boolean Выбирать только те разделы, в которых есть активные позиции
     */
    public $onlyHaveActiveItems = true;
    /**
     * @var boolean Автоматически строить хлебные крошки
     */
    public $autoBreadCrumbs = true;
    /**
     * @var boolean Автоматическое построение заголовков
     */
    public $autoTitle = true;
    /**
     * @var int|null Идентификатор страницы. Если указать, то будут выбраны разделы и позиции, привязанные к этой странице.
     */
    public $pathId = null;
    /**
     * @var string Путь относительно корня сайта. Если не указать, то будет сгенерирован автоматически для текущей страницы.
     */
    public $path = null;
    /**
     * @var boolean Посчитать количество позиций в каждом разделе
     */
    public $countItems = true;
    /**
     * @var array Фильтр позиций
     */
    public $filter = array();
    /**
     * @var string Имя поля с количеством позиций в разделе
     */
    public $countName = 'itemsCount';
    /**
     * @var null|mixed постраничная навигация. Если true — включается постраничная навигация со стандартными настройками.
     * Если массив — включается постраничная навигация с настройками, заданными в этом массиве.
     */
    public $pagination = null;
    public $currentPage = null;

    protected $_articles = null;
    protected $_articleId2path = null;
    protected $_articleId2name = null;
    protected $_articlesTreeObject = null;
    protected $_articlesFullTree = null;
    public  $_currentArticle = null;
    protected $_currentItem = null;
    protected $_itemKey = null;
    protected $_itemCurrentKey = null;
    protected $_articleRequiredFields = array('id', 'parent', 'code', 'name', 'level');
    protected $_vars = array();
    protected $_id2article;
    protected $_articlesTable;
    protected $_itemsTable;

    /**
     * @param array $config
     *
     * @throws Exception
     */
    public function __construct($config = array())
    {
        parent::__construct($config);

        if ($this->countItems && is_null($this->countName))
        {
            throw new Exception('Необходимо указать имя поля счётчика позиций в категории');
        }

        if (!$this->countItems && !is_null($this->countName))
        {
            $this->countName = null;
        }

        $this->_articlesTable = new MSTable($this->articlesTableName . ' `a`');
        $this->_itemsTable = new MSTable($this->itemsTableName . ' `i`');

        if ($this->countItems)
        {
            $this->itemsTable->setFields(array('COUNT(*)'));
            $this->itemsTable->setFilter('`i`.`parent` = `a`.id', 'countFilter');

            if($this->filter)
            {
                if (is_array($this->filter))
                {
                    foreach($this->filter as $key => $filter)
                    {
                        $this->_itemsTable->setFilter($filter,$key);
                    }
                }
                else
                {
                    $this->_itemsTable->setFilter($this->filter);
                }
            }

            if($this->onlyHaveActiveItems)
            {
                $this->itemsTable->setFilter('`i`.`active` = 1', 'countActive');
            }
            $this->itemsTable->setFields(array('*'))->unsetFilter('countFilter')->unsetFilter('countActive');
        }

        if($this->onlyActiveItems)
        {
            $this->itemsTable->setFilter('`i`.`active` = 1', 'active');
        }

        if ($this->pathId !== null)
        {
            $this->articlesTable->setFilter('`a`.`path_id` = ' . $this->pathId, 'path_id');
            $this->itemsTable->setFilter('`i`.`path_id` = ' . $this->pathId, 'path_id');
        }

        if ($this->path === null)
        {
            $this->path = $this->pathId ? '/' . trim(path($this->pathId), '/') : '/' . MSCore::page()->path;
        }
        else
        {
            if(!empty($this->path)) {
                $this->path = '/' . trim($this->path, '/');
            } else {
                $this->path = '';
            }
        }

        list($this->_articlesFullTree, $this->_id2article) = $this->articlesTreeObject->makeTree();
    }

    /**
     * @param $articleId
     * @return bool
     */
    public function isCurrentArticle($articleId) {
        return $this->_currentArticle['id'] == $articleId && empty($this->_currentItem);
    }

    /**
     * @param $id
     * @return null
     */
    public function getArticleById($id)
    {
        return isset($this->_id2article[$id]) ? $this->_id2article[$id] : null;
    }

    /**
     * @return MSTable
     */
    public function getArticlesTable()
    {
        return $this->_articlesTable;
    }

    /**
     * @return MSTable
     */
    public function getItemsTable()
    {
        return $this->_itemsTable;
    }

    /**
     * @param $tableName
     * @param $pathId
     * @param string $idName
     * @param string $parentName
     * @param string $orderField
     */
    public static function rebuildNestedSets($tableName, $pathId, $idName = 'id', $parentName = 'parent', $orderField = 'order')
    {
        MSCore::db()->execute("CALL rebuild_nested_set_tree('" . $tableName . "', " . $pathId . ", '" . MSTable::prepareField($idName) . "', '" . MSTable::prepareField($parentName) . "', '" . MSTable::prepareOrder($orderField) . "');");
    }


    /**
     * @param $pageParam
     * @return mixed
     */
    public function setCurrentPage($pageParam)
    {
        return $this->currentPage = $pageParam;
    }

    /**
     * @param $article
     * @return mixed
     */
    public function setArticle($article) {
        return $this->_currentArticle = $article;
    }

    /**
     * @param $articleId
     *
     * @return string
     */
    public function getArticleLink($articleId)
    {
        return $this->path . '/' . $this->articleId2path[$articleId] . '/';
    }

    /**
     * @param $articleId
     *
     * @return string
     */
    public function getArticleName($articleId)
    {
        return $this->articleId2name[$articleId];
    }

    /**
     * @return mixed
     */
    public function getFullTree()
    {
        return $this->getChildrenTree();
    }

    /**
     * @param int $parent
     *
     * @return mixed
     */
    public function getChildrenTree($parent = 0)
    {
        return isset($this->_articlesFullTree[$parent]) ? $this->_articlesFullTree[$parent] : array();
    }

    /**
     * @param null $fields
     * @param bool|array $withItems
     *
     * @return array|bool|null
     */
    public function getArticle($fields = null, $withItems = true)
    {
        if ($this->_currentArticle === null)
        {
            if (count($this->_vars))
            {
                $lastParam = null;

                if (
                    ($articleId = array_search(implode('/', $this->checkArticleRedirect($this->_vars)), $this->articleId2path)) !== false ||
                    (($lastParam = array_pop($this->_vars)) && $articleId = array_search(implode('/', $this->checkArticleRedirect($this->_vars, $lastParam)), $this->articleId2path)) !== false
                )
                {
                    if ($lastParam !== null && (($this->pagination !== null && !$this->getCurrentPage($lastParam)) || $this->pagination === null))
                    {
                        $this->_itemKey = $lastParam;
                    }

                    $out = array(
                        'model' => $this,
                        'article' => null
                    );

                    $this->articlesTable->setFields($fields !== null ? $fields : array('*'));
                    $this->articlesTable->setOrder();
                    $this->articlesTable->setFilter('`a`.`id` = ' . $articleId, 'id');

                    $this->_currentArticle = $this->articlesTable->getItem();

                    if (empty($this->_currentArticle))
                    {
                        return false;
                    }
                    else
                    {
                        $out['article'] = $this->_currentArticle;

                        $lastPath = '';
                        foreach ($this->_vars as $var)
                        {
                            $lastPath .= ($lastPath ? '/' : '') . $var;
                            $articleId = array_search($lastPath, $this->articleId2path);

                            $this->_id2article[$articleId]['selected'] = true;

                            if ($this->autoBreadCrumbs)
                            {
                                MSCore::page()->breadCrumbs->addLevel($this->path . '/' . $this->articleId2path[$articleId] . '/', $this->articleId2name[$articleId]);
                            }

                            if ($this->autoTitle)
                            {
                                MSCore::page()->title_page = $this->articleId2name[$articleId] . ' &mdash; ' . MSCore::page()->title_page;
                            }
                        }

                        if ($withItems !== false)
                        {
                            $itemsOptions = array(
                                'fields' => null,
                                'recursive' => true
                            );
                            !is_array($withItems) or $itemsOptions = array_merge($itemsOptions, $withItems);

                            $out += $this->getItems($itemsOptions['fields'], $itemsOptions['recursive']);
                        }

                        return $out;
                    }
                }
                else
                {
                    return false;
                }
            }

            if($this->onlyHaveActiveItems)
            {
                $this->itemsTable->setFilter('`i`.`active` = 1', 'countActive');
            }
        }

        return $this->_currentArticle;
    }

    /**
     * @param $vars
     * @param null $lastParam
     *
     * @return array
     */
    protected function checkArticleRedirect($vars, $lastParam = null)
    {
        $replaced = false;

        foreach ($vars as &$var)
        {
            if (is_numeric($var) && array_key_exists($var, $this->articleId2path))
            {
                $var = array_pop(explode('/', $this->articleId2path[$var]));
                $replaced = true;
            }
        }

        if ($replaced && array_search(implode('/', $vars), $this->articleId2path) !== false)
        {
            is_null($lastParam) or $vars[] = $lastParam;
            Redirects::go($this->path . '/' . implode('/', $vars));
        }

        return $vars;
    }

    /**
     * @param $pageParam
     *
     * @return null
     */
    public function getCurrentPage($pageParam)
    {
        return $this->currentPage = preg_match('/^page([0-9]+)$/', $pageParam, $match) ? $match[1] : null;
    }

    /**
     * @param null $fields
     * @param bool $recursive
     *
     * @return array
     */
    public function getItems($fields = null, $recursive = true)
    {
        $limit = null;
        $out = array(
            'model' => $this,
            'items' => null,
            'pagination' => null
        );

        $this->itemsTable->setFields($fields !== null ? $fields : array('*'));
        $this->itemsTable->setOrder($this->itemsOrder);

        if ($recursive)
        {
            $this->articlesTable->unsetFilter('id');
            $this->articlesTable->setFields(array('`a`.`id`'));
            $this->articlesTable->setFilter('`a`.`parents_visible` = 1', 'parents_visible');
            $this->articlesTable->setFilter('`a`.`lft` >= ' . $this->_currentArticle['lft'] . ' AND `a`.`rgt` <= ' . $this->_currentArticle['rgt'], 'children');

            $this->itemsTable->setFilter('`i`.`parent` in (' . $this->articlesTable->makeSql() . ')', 'parent');
        }
        else
        {
            if(!empty($this->_currentArticle['id']))
            {
                $this->itemsTable->setFilter('`i`.`parent` = ' . $this->_currentArticle['id'], 'parent');
            }
        }

        if ($this->pagination)
        {
            is_array($this->pagination) or $this->pagination = array();

            if ($recursive == true) {

                !empty($this->pagination['linkMask']) or $this->pagination['linkMask'] = $this->path . '/' . $this->articleId2path[$this->_currentArticle['id']] . '/%link%/';
            } else {

                !empty($this->pagination['linkMask']) or $this->pagination['linkMask'] = $this->path . '/%link%/';
            }

            !empty($this->pagination['autoTitle']) or $this->pagination['autoTitle'] = $this->autoTitle;

            $Pagination = new MSBasePagination(
                $this->pagination,
                $this->itemsTable->count,
                $this->currentPage
            );

            $limit = $Pagination->getLimit();
            $out['pagination'] = $Pagination;
        }

        $items = $this->itemsTable->setLimit($limit)->getItems();

        foreach ($items as &$item)
        {
            $item['itemLink'] = $this->path . '/' . $this->articleId2path[$item['parent']] . '/' . ($item[$this->itemKeyUniqueName] ? $item[$this->itemKeyUniqueName] : $item[$this->itemKeyPrimaryName]) . '/';
        }
        unset($item);

        $out['items'] = $items;

        return $out;
    }

    /**
     * @param null $fields
     *
     * @return array|bool|string
     */
    public function getItem($fields = null)
    {
        if ($this->_itemKey)
        {
            $keyCondition = $this->getKeyCondition($this->_itemKey);
            if ($keyCondition === false)
            {
                return $keyCondition;
            }

            $out = array(
                'model' => $this,
                'item' => null
            );

            $this->itemsTable->setFields($fields !== null ? $fields : array('*'));
            $this->itemsTable->setFilter($keyCondition, 'keyFilter');
            $this->itemsTable->setFilter('`i`.`parent` = ' . $this->_currentArticle['id'], 'parent');
            $this->itemsTable->setOrder();

            $this->_currentItem = $this->itemsTable->getItem();

            if (empty($this->_currentItem))
            {
                return false;
            }
            else
            {
                $out['item'] = $this->checkItemRedirect($this->_currentItem);

                if ($this->autoBreadCrumbs)
                {
                    MSCore::page()->breadCrumbs->addLevel('', $this->_currentItem['title']);
                }

                if ($this->autoTitle)
                {
                    MSCore::page()->title_page = $this->_currentItem['title'] . ' &mdash; ' . MSCore::page()->title_page;
                }

                return $out;
            }
        }

        return false;
    }

    /**
     * @param $itemKeyParam
     *
     * @return bool|string
     */
    public function getKeyCondition($itemKeyParam)
    {
        //это условие уже не актуально, но может пригодиться

        /*if (preg_match('/^[0-9]+$/', $itemKeyParam))
        {
            return ('`i`.' . MSTable::prepareField($this->_itemCurrentKey = $this->itemKeyPrimaryName)) . " = " . getInt($itemKeyParam);
        }*/

        if (preg_match('/^[a-z0-9-]+$/', $itemKeyParam))
        {
            return ('`i`.' . MSTable::prepareField($this->_itemCurrentKey = $this->itemKeyUniqueName)) . " = '" . $this->itemsTable->pre($itemKeyParam) . "'";
        }

        return false;
    }

    /**
     * @param $item
     *
     * @return mixed
     */
    protected function checkItemRedirect($item)
    {
        if ($this->_itemCurrentKey == $this->itemKeyPrimaryName && $item[$this->itemKeyUniqueName])
        {
            Redirects::go($this->path . '/' . $this->articleId2path[$item['parent']] . '/' . $item[$this->itemKeyUniqueName]);
        }

        return $item;
    }

    /**
     * @return bool
     */
    public function getIsItem()
    {
        return (bool)$this->_itemKey;
    }

    /**
     * @return null
     */
    public function getArticles()
    {
        if ($this->_articles === null)
        {
            $fields = array_search('*', $this->articlesTreeFields) !== false ? $this->articlesTreeFields : array_unique(array_merge($this->articlesTreeFields, $this->_articleRequiredFields));

            $this->articlesTable->setFields($fields);
            if ($this->onlyActiveArticles)
            {
                $this->articlesTable->setFilter('`a`.`visible` = 1', 'visible');
            }

            $this->articlesTable->setOrder('`level` DESC,' . $this->articlesOrder);
            $this->_articles = $this->articlesTable->getItems();

            // Считаем количество товаров в категориях
            if($this->countItems && !empty($this->_articles)) {
                $countItemsQuery = clone $this->itemsTable;
                $countItemsQuery->setFields(array('parent', 'COUNT(*) as cnt'));
                $countItemsQuery->setGroup('`parent`');
                $counts = assoc('parent', $countItemsQuery->getItems());
                foreach ($this->_articles as $key => $article) {
                    $this->_articles[$key][$this->countName] = (!empty($counts[$article['id']])) ? $counts[$article['id']]['cnt'] : 0;
                }
                unset($counts);
            }
        }

        return $this->_articles;
    }

    /**
     * @return null
     */
    public function getArticleId2Name()
    {
        if ($this->_articleId2name === null)
        {
            foreach ($this->articles as $article)
            {
                $this->_articleId2name[$article['id']] = $article['name'];
            }
        }

        return $this->_articleId2name;
    }

    /**
     * @return null
     */
    public function getArticleId2Path()
    {
        if ($this->_articleId2path === null)
        {
            $this->_articleId2path = $this->articlesTreeObject->makeId2Path()->get();
        }

        return $this->_articleId2path;
    }

    /**
     * @return null|Tree
     */
    public function getArticlesTreeObject()
    {
        if (is_null($this->_articlesTreeObject))
        {
            $this->_articlesTreeObject = new Tree($this->articles, array(
                'countField' => $this->countName,
                'onlyHaveItems' => $this->onlyHaveItems,
                'onlyHaveActiveItems' => $this->onlyHaveActiveItems
            ));
        }

        return $this->_articlesTreeObject;
    }

    /**
     * @return bool
     */
    public function getIsCatalogIndexPage()
    {
        if (count($this->_vars) == 1) {

            if (mb_stripos($this->_vars[0], 'page') === 0) {

                return true;
            }
        }

        return !count($this->_vars);
    }

    /**
     * @return array
     */
    public function getVars()
    {
        return $this->_vars;
    }

    /**
     * @param $value
     *
     * @return $this
     */
    public function setVars($value)
    {
        $this->_vars = $value;

        return $this;
    }
}

/**
 * Class Tree
 *
 * @package Core.base
 */

class Tree extends MSBaseComponent
{
    public
        $countField = null,
        $onlyHaveItems = false,
        $onlyHaveActiveItems = false;
    protected
        $_array = array(),
        $_three = array(),
        $_rebuild = array(),
        $_itemsCount = array(),
        $_links = array();

    /**
     * @param array $array
     * @param array $config
     */
    public function __construct(array $array, $config = array())
    {
        parent::__construct($config);
        $this->_array = $array;
        unset($array, $config);
    }

    /**
     * @return array
     */
    public function get()
    {
        return $this->_three;
    }

    /**
     * @return \Tree
     */
    public function makeTree()
    {
        $this->_clear();

        foreach ($this->_array as &$row)
        {
            $this->_links[$row['id']] =& $row;
            $this->_rebuild[$row['parent']][] =& $row;
            $row['selected'] = false;

            if ($this->countField !== null)
            {
                isset($this->_itemsCount[$row['parent']]) or $this->_itemsCount[$row['parent']] = 0;
                $this->_itemsCount[$row['id']] = & $row[$this->countField];
            }
        }

        foreach ($this->_array as &$row)
        {
            is_null($this->countField) or $this->_itemsCount[$row['parent']] += $row[$this->countField];

            if (isset($this->_rebuild[$row['id']]))
            {
                $row['children'] = $this->_rebuild[$row['id']];
            }
        }

        if ($this->onlyHaveItems && $this->countField !== null)
        {
            foreach ($this->_array as $key => &$row)
            {
                if (!$row[$this->countField])
                {
                    $this->_array[$key] = 'emptyArticle';
                    unset($this->_array[$key]);
                }
            }

            $this->_rebuild = self::removeEmptyArticles($this->_rebuild);
        }

        unset($row);

        return array($this->_rebuild, $this->_links);
    }

    protected function _clear()
    {
        $this->_three = $this->_itemsCount = $this->_rebuild = array();
    }

    private static function removeEmptyArticles($haystack)
    {
        foreach ($haystack as $key => $value)
        {
            if (is_array($value))
            {
                $haystack[$key] = static::removeEmptyArticles($haystack[$key]);
            }

            if ($haystack[$key] === 'emptyArticle')
            {
                unset($haystack[$key]);
            }
        }

        return $haystack;
    }

    /**
     * @return $this
     */
    public function makeId2Path()
    {
        $this->_clear();

        $this->_array = array_reverse($this->_array);

        foreach ($this->_array as &$row)
        {
            $row['path'] = $row['code'] ? $row['code'] : $row['id'];
            $this->_rebuild[$row['id']] =& $row['path'];
        }

        foreach ($this->_array as &$row)
        {
            if (isset($this->_rebuild[$row['parent']]))
            {
                $row['path'] = $this->_rebuild[$row['parent']] . '/' . $row['path'];
            }
        }

        unset($row);

        $this->_three = $this->_rebuild;

        return $this;
    }
}