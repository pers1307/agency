<?php
    /**
     * @author Andruha <andruha@mediasite.ru>
     * @copyright Mediasite LLC (http://www.mediasite.ru/)
     */

    /**
     * Базовый класс модуля «Лента»
     *
     * @property string|int $currentPage
     * @property MSTable $itemsTable экземпляр класса для работы с таблицей позиций
     *
     * @package Core.base
     */

    class MSBaseTape extends MSBaseComponent
    {
        /**
         * @var string имя таблицы с позициями
         */
        public $itemsTableName;
        /**
         * @var string имя первичного ключа
         */
        public $keyPrimaryName = 'id';
        /**
         * @var string имя уникального ключа
         */
        public $keyUniqueName = 'code';
        /**
         * @var bool Делать выборку только по активным позициям
         */
        public $onlyActiveItems = true;
        /**
         * @var null идентификатор страницы. Если указан, то будет выборка по записям, привязанным только к этой странице
         */
        public $pathId = null;
        /**
         * @var null|string путь текущей страницы
         */
        public $path = null;
        /**
         * @var null|string имя поля по которому производить сортировку
         */
        public $order;
        /**
         * @var bool автоматически строить крошки
         */
        public $autoBreadCrumbs = true;
        /**
         * @var bool автоматически строить заголовок <title></title>
         */
        public $autoTitle = true;
        /**
         * @var null|mixed постраничная навигация. Если true — включается постраничная навигация со стандартными настройками.
         * Если массив — включается постраничная навигация с настройками, заданными в этом массиве.
         */
        public $pagination = null;

        protected $_currentKey = null;
        protected $_currentItem = null;
        protected $_currentPage = null;
        protected $_itemsTable;

        /**
         * Конструктор класса
         *
         * @param array $config
         */
        public function __construct($config = array())
        {
            parent::__construct($config);

            $this->_itemsTable = new MSTable($this->itemsTableName);

            if($this->onlyActiveItems)
            {
                $this->itemsTable->setFilter('`active` = 1', 'active');
            }

            if ($this->pathId !== null)
            {
                $this->itemsTable->setFilter('`path_id` = ' . $this->pathId, 'path_id');
            }

            !is_null($this->path) or $this->path = '/' . MSCore::page()->path;
            if ($this->path === null)
            {
                $this->path = $this->pathId ? '/' . trim(path($this->pathId), '/') : '/' . MSCore::page()->path;
            }
            else
            {
                $this->path = '/' . trim($this->path, '/');
            }
        }

        public function getItemsTable()
        {
            return $this->_itemsTable;
        }

        /**
         * @param $itemKeyParam
         * @param null $fields
         *
         * @return array|bool|null
         */
        public function getItem($itemKeyParam, $fields = null)
        {
            $keyCondition = $this->getKeyCondition($itemKeyParam);
            if ($keyCondition === false || $this->_currentPage)
            {
                return false;
            }

            $out = array(
                'model' => $this,
                'item' => null
            );

            $this->itemsTable->setFilter($keyCondition, 'itemKey');

            is_null($fields) or $this->itemsTable->setFields($fields);
            $this->_currentItem = $this->itemsTable->getItem();
            if (empty($this->_currentItem))
            {
                return null;
            }
            else
            {
                $out['item'] = $this->checkRedirects($this->_currentItem);

                if ($this->autoBreadCrumbs)
                {
                    MSCore::page()->breadCrumbs->addLevel('', $this->_currentItem['title']);
                }

                if ($this->autoTitle)
                {
                    MSCore::page()->title_page = $this->_currentItem['title'] . ' &mdash; ' . MSCore::page()->title_page;
                }
            }

            return $out;
        }

        /**
         * @param $itemKeyParam
         *
         * @return bool|string
         */
        public function getKeyCondition($itemKeyParam)
        {
            //это условие уже не актуально, но может пригодиться
            /*if (preg_match('/^[0-9]+$/', $itemKeyParam))
            {
                return MSTable::prepareField($this->_currentKey = $this->keyPrimaryName) . " = " . getInt($itemKeyParam);
            }*/

            if (preg_match('/^[a-z0-9-]+$/', $itemKeyParam))
            {
                return MSTable::prepareField($this->_currentKey = $this->keyUniqueName) . " = '" . $this->itemsTable->pre($itemKeyParam) . "'";
            }

            return false;
        }

        /**
         * @param $item
         *
         * @return mixed
         */
        protected function checkRedirects($item)
        {
            if ($this->_currentKey == $this->keyPrimaryName && $item[$this->keyUniqueName])
            {
                Redirects::go($this->path . '/' . $item[$this->keyUniqueName]);
            }

            return $item;
        }

        /**
         * @param array|string $fields Поля, которые необходимо поместить в выборку
         *
         * @return array
         */
        public function getItems($fields = null, $filter = -1)
        {
            $limit = null;
            $out = array(
                'model' => $this,
                'items' => array(),
                'pagination' => null
            );

            is_null($fields) or $this->itemsTable->setFields($fields);
            $this->itemsTable->setOrder($this->order);

            if ($filter != -1) {
                $this->itemsTable->setFilter('`type` = ' . $filter);
            }

            if ($this->pagination)
            {
                is_array($this->pagination) or $this->pagination = array();

                !empty($this->pagination['linkMask']) or $this->pagination['linkMask'] = $this->path . '/%link%/';
                !empty($this->pagination['autoTitle']) or $this->pagination['autoTitle'] = $this->autoTitle;

                $Pagination = new MSBasePagination(
                    $this->pagination,
                    $this->itemsTable->count,
                    $this->_currentPage
                );

                $limit = $Pagination->getLimit();
                $out['pagination'] = $Pagination;
            }

            $items = $this->itemsTable->setLimit($limit)->getItems();

            foreach ($items as &$item)
            {
                if (!empty($item[$this->keyUniqueName])) {

                    $item['itemLink'] = $this->path . '/' . ($item[$this->keyUniqueName] ? $item[$this->keyUniqueName] : $item[$this->keyPrimaryName]) . '/';
                }
            }
            unset($item);

            $out['items'] = $items;

            return $out;
        }

        /**
         * @param $pageParam
         *
         * @return null
         */
        public function setCurrentPage($pageParam)
        {
            return $this->_currentPage = preg_match('/^page([0-9]+)$/', $pageParam, $match) ? $match[1] : null;
        }

        public function getCurrentPage()
        {
            return $this->_currentPage ;
        }
    }