<?php
    /**
     * @author Andruha <andruha@mediasite.ru>
     * @copyright Mediasite LLC (http://www.mediasite.ru/)
     */

    /**
     * Базовый компонент
     *
     * @package Core.base
     */

    class MSBaseComponent
    {
        /**
         * Конструктор
         *
         * @param array $config массив с параметрами компонента, либо путь до файла, который возвращает этот массив.
         *
         * @throws Exception
         */
        public function __construct($config = array())
        {
            static::configure($this, $config);
        }

        /**
         * @param $name
         *
         * @return mixed
         * @throws Exception
         */
        public function __get($name)
        {
            $getter = 'get' . $name;
            if (method_exists($this, $getter))
            {
                return $this->$getter();
            }

            if (method_exists($this, 'set' . $name))
            {
                throw new Exception('Свойство ' . get_class($this) . '::' . $name . ' доступно только для записи');
            }
            else
            {
                throw new Exception('Свойство ' . get_class($this) . '::' . $name . ' не определено');
            }
        }

        /**
         * @param $name
         * @param $value
         *
         * @return mixed
         * @throws Exception
         */
        public function __set($name, $value)
        {
            $setter = 'set' . $name;
            if (method_exists($this, $setter))
            {
                return $this->$setter($value);
            }

            if (method_exists($this, 'get' . $name))
            {
                throw new Exception('Свойство ' . get_class($this) . '::' . $name . ' доступно только для чтения');
            }
            else
            {
                throw new Exception('Свойство ' . get_class($this) . '::' . $name . ' не определено');
            }
        }

        /**
         * @param $name
         *
         * @return bool
         */
        public function __isset($name)
        {
            $getter = 'get' . $name;
            if (method_exists($this, $getter))
            {
                return $this->$getter() !== null;
            }

            return false;
        }

        /**
         * @param $name
         *
         * @throws Exception
         */
        public function __unset($name)
        {
            $setter = 'set' . $name;
            if (method_exists($this, $setter))
            {
                $this->$setter(null);
            }
            elseif (method_exists($this, 'get' . $name))
            {
                throw new Exception('Свойство ' . get_class($this) . '::' . $name . ' доступно только для чтения');
            }
        }

        public static function configure($object, $config)
        {
            if (is_string($config))
            {
                $config = require_once($config);
            }

            if (is_array($config))
            {
                foreach ($config as $name => $value)
                {
                    if (is_array($object->{$name}) && is_array($value))
                    {
                        $object->{$name} = array_replace_recursive($object->{$name}, $value);
                    }
                    elseif (is_array($object->{$name}) && !is_array($value) && !is_null($value))
                    {
                        throw new Exception('Параметр ' . get_class($object) . '->' . $name . ' должен быть массивом, либо null');
                    }
                    else
                    {
                        $object->{$name} = $value;
                    }
                }
            }

            return $object;
        }

    }