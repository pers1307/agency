<?php

class HelperSearch {

    /**
     * Патерн для подсветки искомой фразы
     */
    const SEARCH_HIGHLIGHT_PATTERN = '<span>$0</span>';

    /**
     * Минимальное количество букв поисковой фразы
     * для осуществление поиска
     */
    const SEARCH_MIN_WORD_LENGTH = 3;

    /**
     * Декодирует в исходную строку поиска
     *
     * @param $query
     * @return mixed|string
     */
    public static function decodeSearchQuery($query) {
        $query = trim($query, '/');
        $query = strip_tags(urldecode($query));
        $query = MSCore::db()->pre($query);
        $query = str_replace(
            array('¨','¸'),
            array('ё','е'),
            $query
        );
        return $query;
    }

    /**
     * Подготоваливает строку для полнотекстового поиска
     *
     * @param $query
     * @return string
     */
    public static function prepareSearchQuery($query) {

        $query = self::decodeSearchQuery($query);

        $words = preg_split('/\s+/sui', $query);
        $words = array_unique($words);
        $words = array_filter($words);

        // Stemm words
        if (!empty($words)) {
            $min_word_length = self::SEARCH_MIN_WORD_LENGTH;
            $words = array_map(
                function($element) use($min_word_length){
                    if (strlen($element) >= $min_word_length) {
                        if(preg_match('/[^a-zA-Zа-яА-Я\-]{1,}/suiU', $element)){
                            $stemm = $element;
                        } else if (preg_match('/[А-Яа-я]+/si', $element)) {
                            $stemm = RuStemmer::stem($element) . '*';
                        } else {
                            $stemm = EnStemmer::stem($element) . '*';
                        }
                        return $stemm;
                    }
                    return false;
                },
                $words
            );
        }
        return implode(' ', $words);
    }

    /**
     * Производит подсветку результатов поиска,
     * если активен флаг backlight
     *
     * @param $content
     * @param $query
     * @return mixed
     */
    public static function processingSearchResult($content, $query) {
        $content = strip_tags($content);

        // Prepare patterns
        $grepPattern = '/' . str_replace('*', '\w*|', $query) . '/ui';
        $grepPattern = str_replace(' ', '', $grepPattern);
        $grepPattern = str_replace('|/ui', '/ui', $grepPattern);

        $highlightPattern = self::SEARCH_HIGHLIGHT_PATTERN;
        $searchContent = preg_replace($grepPattern, $highlightPattern, $content);
        return $searchContent;
    }

    /**
     * Возвращает динамически созданное поле score для
     * сортировки по релевантности
     *
     * @param $columns
     * @param $query
     * @param $preparedQuery
     * @return null|string
     */
    public static function getScoreField($columns, $query, $preparedQuery) {
        $score = '';
        $index = 1;
        $reverseColumns = array_reverse($columns);

        foreach($reverseColumns as $column) {
            $index = $index * 2;
            $score .= $index . ' * (MATCH ('. $column .') AGAINST (\'' . $preparedQuery . '\' IN BOOLEAN MODE)) +'. PHP_EOL;
        }

        foreach($reverseColumns as $column) {
            $index = $index * 2;
            $score .= $index . ' * (MATCH ('. $column .')  AGAINST (\'' . MSCore::db()->pre($query) . '\')) +'. PHP_EOL;
        }

        if(!empty($score)) {

            return '('. trim($score, PHP_EOL.' +') .') as `score`';
        }

        return null;
    }
}