<?php

/**
 * Class SortWidget
 *
 * Виджет вывода блока сортировки по полям
 */
class WidgetSort extends MSBaseWidget {

    /**
     * Указывает на необходимость кэширования
     * экземпляра при создании
     *
     * @var bool
     */
    public $_cache = true;

    /**
     * Настройки из файла конфигурации
     *
     * @var array
     */
    private $_settings = array();

    /**
     * Поля для сортировки из файла конфигурации
     *
     * @var array
     */
    private $_fields = array();

    /**
     * Поле-ключ из $_GET['sort'] для сортировки
     *
     * @var null
     */
    public $customSort = null;

    /**
     * Направление сортировки из $_GET['order']
     *
     * @var null
     */
    public $customOrder = null;

    /**
     * Констрктор
     *
     * @param $name
     * @param $config
     * @param $view
     * @throws Exception
     */
    public function __construct($name, $config, $view) {
        $result = parent::__construct($name, $config, $view);

        if(isset($this->config['settings'])) {
            $this->_settings = $this->config['settings'];
        }

        if(empty($this->config['fields'])) {
            throw new Exception('Не переданы поля для сортировки в файле конфигурации');
        }

        $this->_fields = $this->config['fields'];

        return $result;
    }

    /**
     * @return null|string
     */
    public function getSqlStringQuery() {

        $sqlOrder =  null;
        $GET = $_GET;
        $tmp = array();
        $filtersString = null;

        if(isset($this->_settings['another_gets'])
            && $this->_settings['another_gets'] === true) {
            unset($GET['sort']);
            unset($GET['order']);
            $filtersString = http_build_query($GET);
            if($filtersString) {
                $filtersString = '?'. $filtersString;
            }
        }

        foreach($this->_fields as $key => &$item) {
            $item['current'] = false;
            $item['link'] = 'none';
            $item['arrow'] = 'none';
            if(empty($item['skip']) || !$item['skip']) {
                $getSortQuery = 'sort='. $item['code'] .'&order='. $item['order'];
                $item['arrow'] = $item['order'] === 'asc' ? 'top' : 'bottom';
                $item['link'] = $filtersString . ($filtersString ? ('&' . $getSortQuery) : ('?' . $getSortQuery));
            };
            $tmp[(@$item['code'] ?: $key)] = $item;
        }
        $this->_fields = $tmp; unset($tmp);

        if(!empty($_GET['sort'])) {
            $this->customSort = trim($_GET['sort']);
            $this->customSort = isset($this->_fields[$this->customSort])
                ? $this->customSort
                : key($this->_fields);
        } else {
            $this->customSort = key($this->_fields);
        }

        $currentSort = &$this->_fields[$this->customSort];

        if(!empty($_GET['order'])) {
            $this->customOrder = strtoupper(trim($_GET['order']));
            if($this->customOrder != 'ASC' && $this->customOrder != 'DESC') {
                $this->customOrder = strtoupper($currentSort['order']);
            }
        } else {
            $this->customOrder = strtoupper($currentSort['order']);
        }

        $currentSort['current'] = true;
        $currentSort['arrow'] = $this->customOrder === 'ASC' ? 'top' : 'bottom';
        $getSortQuery = 'sort='. $currentSort['code'] .'&order='. ($this->customOrder === 'ASC' ? 'desc' : 'asc');
        $currentSort['link'] = $filtersString . ($filtersString ? ('&' . $getSortQuery) : ('?' . $getSortQuery));

        if(isset($currentSort['hard_orders'])) {
            $lastField = array_pop($currentSort['fields']);
            $currentSort['fields'][] = $lastField;
            $sqlOrder = '`'. implode('` '. strtoupper($currentSort['hard_orders'][$currentSort['fields'][key($currentSort['fields'])]] ?: $this->customOrder) .', `', $currentSort['fields']) . '` '. strtoupper(!empty($currentSort['hard_orders'][$lastField]) ? $currentSort['hard_orders'][$lastField] : $this->customOrder);
        } else {
            $sqlOrder = '`'. implode('` '. $this->customOrder .', `', $currentSort['fields']) . '` '. $this->customOrder;
        }
        //debug($sqlOrder);

        unset($filtersString, $getSortQuery, $GET);
        return $sqlOrder;
    }

    /**
     * Возвразащает массив с паараметрами текущей сортировки
     *
     * @return array
     */
    public function getHttpArrayQuery() {
        $GET = array();
        $GET['sort'] = !empty($_GET['sort']) ? $_GET['sort'] : null;
        $GET['order'] = !empty($_GET['order']) ? $_GET['order'] : null;
        if(is_null($GET['sort'])) {
            unset($GET['sort']);
        }
        if(is_null($GET['order'])) {
            unset($GET['order']);
        }
        return $GET;
    }

    /**
     * Возвращает http строку с текущей сортировкой
     *
     * @return null|string
     */
    public function getHttpStringQuery() {
        $string = null;
        $GET = array();
        $GET['sort'] = !empty($_GET['sort']) ? $_GET['sort'] : null;
        $GET['order'] = !empty($_GET['order']) ? $_GET['order'] : null;
        if(is_null($GET['sort'])) {
            unset($GET['sort']);
        }
        if(is_null($GET['order'])) {
            unset($GET['order']);
        }
        if(count($GET)) {
            $string = http_build_query($GET);
        }
        return $string;
    }

    /**
     * Отрисовыает блок с ссылками для сортировки
     *
     * @param null $view
     * @return string
     */
    public function render($view = null) {

        if(!count($this->_params)) {
            //$this->getSqlStringQuery();
            $this->_params['fields'] = $this->_fields;
        }
        return $this->_render($view);
    }

}