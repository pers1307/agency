<?php
/**
 * @author Xploit <Xploit@mediasite.ru>
 * @author Andruha <andruha@mediasite.ru>
 * @copyright Mediasite LLC (http://www.mediasite.ru/)
 */

/**
 * Класс работы с путями и ссылками
 *
 * @property string $requestUri
 * @property array $vars
 * @property array $tree
 * @property mixed segments
 *
 * @package Core.system
 *
 */
class UrlsAndPaths extends MSBaseComponent {
    private $_db = null;
    private $_vars = array();
    private $_segments = array();
    private $_prefix = '';
    private $_requestUri = null;
    private $_tree = null;

    public $redirects = null;

    /**
     * @access public
     * @var $ids
     *  Содержит массив значений path_id и путей
     *  <code>
     *  array( [1] => / ) // key=Path_ID; value=path;
     *  </code>
     */
    public $ids;

    /**
     *  Текущий путь в формате /asdasd/asdasda/asdasd/asdasdas/aasd/
     *
     * @access public
     */
    public $current;
    public $path;
    public $storeAddBefTag;

    /**
     *  Конструктор класса. Объявление переменных
     *
     * @access public
     * @param array $config
     */
    public function __construct($config = array()) {
        parent::__construct($config);

        if(PHP_SAPI !== 'cli') {
            $this->redirects = new Redirects(HTTP_HOST, $this->requestUri);
        }

        $this->_db = MSCore::db();

        $this->_prefix = 'http://';
        $this->storeAddBefTag = array();
    }

    public function processCurrentUrl($url) {
        $this->current = trim($url, '/');
        $current = $this->current;
        $this->getTree();
        $info = $this->getInfoByPath($this->current);
        if($info['path'] == '404' && $url != '404') {
            $prefixes = MSConfig::get('system/common:url_prefixes');
            if(!empty($prefixes)) {
                foreach ($prefixes as $_pfx) {
                    $this->current = $_pfx .'/'. $current;
                    $info = $this->getInfoByPath($this->current);
                    if($info['path'] != '404') {break;}
                }
            }
        }
        return $info;
    }

    /**
     *  Преобразует имя папки к разрешенному имени в файловой системе
     *
     * @access public
     * @return string DirectoryName
     *
     * @param string $folder DirectoryName
     */
    public function modify_to_valid($folder) {
        return preg_replace('#[^A-Za-z0-9-_/+]#', '_', $folder);
    }

    /**
     *  Возвращает структуру раздела
     *
     * @access private
     * @return array PageStructure
     *
     * @param string $path path
     */
    private function getInfoByPath($path) {
        $path = (string)$path;

        /** Показываем 404 страницу, если в пути 2 или более слеша подряд */
        if (preg_match('/\/\//', $this->requestUri)) {
            $path = '404';
        }

        $this->_vars = array();
        $this->_segments = explode('/', trim($this->current, '/'));

        //----отсекаем GET------------------------
        if (!empty($_GET)) {
            $tmp = explode('?', $path);
            $get_path = isset($tmp[1]) ? $tmp[1] : '';
            $path = trim($tmp[0], '/');
            $this->current = $path;
        }

        if (isset($this->tree[$path])) {
            $this->path = $path;

            return self::checkGetVars($this->tree[$path], $path, isset($get_path) ? $get_path : '');
        } else {
            $exp = explode('/', trim($this->current, '/'));

            $from = 0;
            $to = sizeof($exp);
            foreach ($exp as $chunk) {
                $path = implode('/', array_slice($exp, $from, $to));

                $to--;
                if (isset($this->tree[$path]) && $path != '' && sizeof($exp) > 1) {
                    $this->_vars = array_reverse($this->_vars);
                    $this->path = $path;

                    if (static::HereModuleAttached($this->tree[$path])) {
                        return self::checkGetVars($this->tree[$path], $path, isset($get_path) ? $get_path : '');
                    } else {
                        return $this->getInfoByPath('404');
                    }
                } else {
                    $temp = array_reverse(explode('/', trim($path, '/')));
                    $this->_vars[] = $temp[key($temp)];
                }
            }
        }

        return $this->getInfoByPath('404');
    }

    /**
     * Прверка допустимых GET переменных
     */

    private static function checkGetVars($return, $path, $get_path) {
        if (!empty($_GET) && ($return['path_id'] > 10 || $return['path_id'] == 1)) {

            /* Если нам нужны какие-то GET переменные то вписываем их имена в этот массив */
            $par = array(
                '_openstat',
                'ym_playback',
                'ym_cid',
                'ym_lang',
                'yclid',
                'gclid',
                'utm_source',
                'utm_medium',
                'utm_campaign',
                'utm_term',
                'utm_content'
            );

            $allow_get_keys = array_merge($par, explode(',', $return['getvars']));
            /* ========================= */

            if ($return['getvars'] != '*' && count(array_diff(array_keys($_GET), $allow_get_keys)) && !preg_match('%' . ROOT_PLACE . '/(.*)%m', $path)) {
                Page404();
            }

            if ($get_path) {
                $return['get_path'] = $get_path;
            }
        }

        $return['lastFileLine'] = lastFileLine(1);

        return $return;
    }

    /**
     *  Строит дерево всего сайта и вносит массив в $this->tree при переданом параметре $is_return=false
     *  иначе возвращает массив в переменную не заменя $urls->tree
     *
     * @access public
     */
    public function getTree() {
        if ($this->_tree === null) {
            MSCore::modules()->UseModule('admin', __FILE__, __LINE__);

            $sql = "SELECT * FROM `" . PRFX . "www` ORDER BY `path`";

            $list = $this->_db->getAll($sql);

            $tmp_tree = array();
            foreach ($list as $path) {

                //$path['config'] = utf8_decode($path['config']); // иначе не работет с UTF8, возможно стопроится из-за неправильно переконвертированного символа в базе

                $config_page = trim($path['config']) ? unserialize(trim($path['config'])) : '';

                $tmp_tree[$path['path_id']]['parent'] = $path['parent'];
                $tmp_tree[$path['path_id']]['path'] = $path['path'];
                $tmp_tree[$path['path_id']]['path_id'] = $path['path_id'];
                $tmp_tree[$path['path_id']]['order'] = $path['order'];
                $tmp_tree[$path['path_id']]['root_only'] = $path['root_only'];
                $tmp_tree[$path['path_id']]['config'] = $config_page;
                $tmp_tree[$path['path_id']]['main_template'] = $path['main_template'];
                $tmp_tree[$path['path_id']]['visible'] = $path['visible'];
                $tmp_tree[$path['path_id']]['title_menu'] = $path['title_menu'];
                $tmp_tree[$path['path_id']]['header'] = $path['header'];
                $tmp_tree[$path['path_id']]['meta_description'] = $path['meta_description'];
                $tmp_tree[$path['path_id']]['meta_keywords'] = $path['meta_keywords'];
                $tmp_tree[$path['path_id']]['title_page'] = $path['title_page'];

                foreach ($GLOBALS['PAGE_CONFIG']['pages']['config'] as $field => $data) {
                    $tmp_tree[$path['path_id']][$field] = $path[$field];
                }

                $tmp_tree[$path['path_id']]['rdr_path_id'] = $path['rdr_path_id'];
                $tmp_tree[$path['path_id']]['rdr_url'] = $path['rdr_url'];
                $tmp_tree[$path['path_id']]['www_type'] = $path['www_type'];
                $tmp_tree[$path['path_id']]['notfound'] = $path['notfound'];
                $tmp_tree[$path['path_id']]['getvars'] = $path['getvars'];

                $this->ids[$path['path_id']] = $path['path'];
            }

            $this->_tree = assoc('path', $tmp_tree);
            unset($tmp_tree);

        }

        return $this->_tree;
    }

    public function setTree($value) {
        return $this->_tree = $value;
    }

    /**
     *  Хранилище добавляемой информации к тегам.
     */
    public
    function AddBeforeTag($text = null, $tag = null) {
        $this->storeAddBefTag[$tag][] = $text;
    }

    public
    function getRequestUri() {
        if ($this->_requestUri === null) {
            $requestUri = null;

            if (isset($_SERVER['HTTP_X_REWRITE_URL'])) // IIS
            {
                $requestUri = $_SERVER['HTTP_X_REWRITE_URL'];
            } elseif (isset($_SERVER['REQUEST_URI'])) {
                $requestUri = $_SERVER['REQUEST_URI'];
                if (!empty($_SERVER['HTTP_HOST'])) {
                    if (strpos($requestUri, $_SERVER['HTTP_HOST']) !== false) {
                        $requestUri = preg_replace('/^\w+:\/\/[^\/]+/', '', $requestUri);
                    }
                } else {
                    $requestUri = preg_replace('/^(http|https):\/\/[^\/]+/i', '', $requestUri);
                }
            } elseif (isset($_SERVER['ORIG_PATH_INFO'])) // IIS 5.0 CGI
            {
                $requestUri = $_SERVER['ORIG_PATH_INFO'];
                if (!empty($_SERVER['QUERY_STRING'])) {
                    $requestUri .= '?' . $_SERVER['QUERY_STRING'];
                }
            }

            $this->_requestUri = $requestUri;
        }

        return $this->_requestUri;
    }

    public function getVars() {
        return $this->_vars;
    }

    public function getSegments() {
        return $this->_segments;
    }

    public static function HereModuleAttached($data) {

        if ($data['path_id'] < 10) {
            return true;
        }

        $data = $data['config'];

        foreach ($data as $zone => $info) {

            for ($i = 0; $i < sizeof($info); $i++) {
                if (isset($info[$i])) {
                    if ((isset($info[$i]['module']) && $info[$i]['module'] != "") || ($zone == '_CONTENT_' && $info[$i]['script'] != "" && $info[$i]['script'] !== 'SITE/blocks/text.php')) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

}