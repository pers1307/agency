<?php

/**
 * Class MSConfig
 */
class MSConfig {
    const PATH_SEPARATOR = ':';
    private static $_data = array();
    private function __construct() { }
    private function __clone() { }
    private function __wakeup() { }

    /**
     * Get parameter from config
     * @param $string
     * @param string $path
     * @return null
     */
    public static function get($string, $path = CONFIG_DIR) {
        $params = explode(self::PATH_SEPARATOR, $string);
        $file = array_shift($params);

        if (isset(self::$_data[$file])) {
            if (sizeof($params)) {
                return self::_extract(self::$_data[$file], $params);
            }
            return self::$_data[$file];
        }

        $self = array();
        $filename = $path .'/'. $file . '.php';
        if (is_file($filename)) {
            /** @noinspection PhpIncludeInspection */
            $self = include($filename);
        }

        self::$_data[$file] = $self;
        if (sizeof($params)) {
            return self::_extract(self::$_data[$file], $params);
        }
        return self::$_data[$file];
    }

    /**
     * @param $haystack
     * @param array $path
     * @return null
     */
    protected static function _extract($haystack, $path = array()) {
        $slug = array_shift($path);
        $element = (isset($haystack[$slug])) ? $haystack[$slug] : null;

        if (!is_null($element) && sizeof($haystack)) {

            while ($path) {
                $slug = array_shift($path);
                if (isset($element[$slug])) {
                    $element = $element[$slug];
                } else {
                    $element = null;
                    break;
                }
            }
        }

        return $element;
    }
}