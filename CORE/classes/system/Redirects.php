<?php
/**
 * Модуль редиректов
 *
 * @author Andruha <andruha@mediasite.ru>
 * @copyright Mediasite LLC (http://www.mediasite.ru/)
 *
 * @package Core.system
 */

class Redirects
{

    private
        $requestUri,
        $httpHost,
        $doRedirect = false,
        $redirectType = 301;

    public function __construct($httpHost, $requestUri)
    {
        $this->requestUri = $requestUri;
        $this->httpHost = $httpHost;

        $this->checkUrl();
    }

    /**
     * Проверка текущего url
     * Убрано добавление хоста к урлу при относительном редиректе.
     */
    private function checkUrl()
    {
        $this->checkModuleRedirect();

        $currentHost = $this->httpHost;

        $this->checkWWW();
        $this->checkDomainEndComma();
        $this->checkEndSlash();

        if ($this->doRedirect) {
            $currentHost = $currentHost == $this->httpHost ? '' : $this->httpHost;
            self::go($currentHost . $this->requestUri, $this->redirectType);
        }
    }

    /**
     * Редиректы установленные в админке
     */
    private function checkModuleRedirect()
    {
        $redirect = MSCore::db()->getRow(
            'SELECT * FROM `' . PRFX . 'redirects` WHERE (`from` = "' . MSCore::db()->pre($this->requestUri) . '" OR `from` = "' . MSCore::db()->pre($this->httpHost) . '") AND `active` = 1'
        );

        if (count($redirect) && $redirect['to'] != '') {
            /**
             * TODO: Допилить функцию go, сейчас она знает только 301 редирект
             */
            $this->redirectType = $redirect['type'];

            $redirect['to'] = str_replace('%REQUEST_URI%', $this->requestUri, $redirect['to']);

            $url = parse_url($redirect['to']);

            if (!empty($url['host']))
            {
                $this->httpHost = $url['host'];
                $this->requestUri = $url['path'];
                $this->doRedirect = true;
            }
            else
            {
                self::go($redirect['to'], $this->redirectType);
            }
        }
    }

    /**
     * Проверка на www в начале имени домена для доменов 2го уровня
     */
    private function checkWWW()
    {
        $noWwwHttpHost = str_replace('www.', '', $this->httpHost);
        $commaCount = mb_substr_count($noWwwHttpHost, '.');
        $hasWww = preg_match("/^www\./", $this->httpHost);

        // Если домен второго уровня
        if ($commaCount <= 1) {
            if (WWW_REDIRECT && !$hasWww) {
                $this->httpHost = 'www.' . $noWwwHttpHost;
                $this->doRedirect = true;
            }

            if (!WWW_REDIRECT && $hasWww) {
                $this->httpHost = $noWwwHttpHost;
                $this->doRedirect = true;
            }
        }
    }

    /**
     * Проверка на точку в конце доменного имени
     */
    private function checkDomainEndComma()
    {
        if (preg_match("/\.$/", $this->httpHost)) {
            $this->httpHost = trim($this->httpHost, '.');
            $this->doRedirect = true;
        }
    }

    /**
     * Проверка на слеш в конце запроса
     * Убран слеш в конце строки, если передан пустой GET массив.
     */
    private function checkEndSlash()
    {
        $path = explode('/', $this->requestUri);

        if (!sizeof($_GET) &&
            $path[count($path) - 1] != '' &&
            !preg_match('#/' . ROOT_PLACE . '/#', $this->requestUri) &&
            !preg_match('#ajax#', $this->requestUri) &&
            !preg_match('#\.#', $this->requestUri)
        ) {

            $this->requestUri = rtrim($this->requestUri, '?');
            $this->requestUri .= '/';
            $this->doRedirect = true;
        }
    }

    /**
     * Редирект по заданному адресу, нужного типа
     *
     * @param $href
     * @param int $rdr_type
     */
    public static function go($href, $rdr_type = 301)
    {
        $url = 'Location: /' . EndSlash(trim($href, '/'));
        $url = str_replace('//', '/', $url);

        $domainPattern = '/^(?:(?:https?|ftp|telnet):\/\/(?:[a-z0-9_-]{1,32}(?::[a-z0-9_-]{1,32})?@)?)?(?:(?:[a-z0-9-]{1,128}\.)+(?:ru|su|com|net|org|mil|edu|arpa|gov|biz|info|aero|inc|name|local|loc|x|xn--p1ai|[a-z]{2})|(?!0)(?:(?!0[^.]|255)[0-9]{1,3}\.){3}(?!0|255)[0-9]{1,3})(?:\/[a-z0-9.,_@\$%&?+=~\/-]*)?(?:#[^ \'"&]*)?$/im';

        /** Если указан домен */
        if (preg_match($domainPattern, $href)) {
            /** Если не указан протокол */
            if (!preg_match('/^(?:(?:https?|ftp|telnet):\/\/(?:[a-z0-9_-]{1,32}(?::[a-z0-9_-]{1,32})?@)?)/im', $href)) {
                $href = 'http://' . $href;
            }

            $url = 'Location: ' . $href;
        }

        if ($rdr_type == 301) {
            header("HTTP/1.1 301 Moved Permanently");
        }
        header($url);
        exit();
    }
}