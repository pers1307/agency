<?php

class MSSeeder {
    protected $_config = array();
    protected $_faker;

    public function __construct($config, $fakerLang = 'ru_RU') {
        $this->_config = $config;
        $this->_faker = Faker\Factory::create($fakerLang);
    }

    // Методы генерации значений
    protected function _imageSeed($config, $data) {
        $uploadFolder = MSFiles::getUploadFolder();
        foreach ($config as $field => $properties) {
            $thumbData = array();
            $imageLimit = (!empty($properties['limit'])) ? $properties['limit'] : 1;
            for ($j = 0; $j < $imageLimit; $j++) {
                $imageOriginal = $this->_faker->image($uploadFolder, 800, 800);
                $imageData = array(
                    'original' => MSFiles::getFileRootPath($imageOriginal),
                    'sys_thumb' => MSFiles::makeImageThumb($imageOriginal, array(80, 80))
                );

                if(!empty($properties['settings'])) {
                    foreach ($properties['settings'] as $key => $setting) {
                        $imageData[$key] = MSFiles::makeImageThumb($imageOriginal, $setting);
                    }
                }

                $thumbData[] = array('id' => md5($imageOriginal . 'min'), 'path' => $imageData, 'text' => '');
            }

            $data[$field] = serialize($thumbData);
        }
        return $data;
    }

    // Типы заданий для генератора
    protected function _listTask($config) {
        $name = (!empty($config['name'])) ? $config['name'] : 'listTask';
        $table = (!empty($config['table'])) ? $config['table'] : null;
        $seed = (!empty($config['seed']) && is_callable($config['seed'])) ? $config['seed'] : null;
        $limit = (!empty($config['limit'])) ? $config['limit'] : 10;

        if (empty($seed) || empty($table)) {
            echo $name . ': [error]' . PHP_EOL;
            exit;
        }

        echo $name . ': [start]' . PHP_EOL;
        if (!empty($config['before']) && is_callable($config['before'])) {
            $config['before']($config);
        }

        // Основной цикл генерации данных
        for ($i = 0; $i < $limit; $i++) {
            /** @var Closure $seed */
            $data = $seed($config, $this->_faker);

            // Генерация изображений
            if(!empty($config['images'])) {
                $data = $this->_imageSeed($config['images'], $data);
            }

            if(!empty($data)) {
                MSCore::db()->insert($table, $data);
                $data['id'] = MSCore::db()->id;
                if (!empty($config['callback']) && is_callable($config['callback'])) {
                    $config['callback']($config, $data);
                }
            }
        }

        if (!empty($config['after']) && is_callable($config['after'])) {
            $config['after']($config);
        }
        echo $name . ': [end]' . PHP_EOL;
    }

    protected function _catalogTask($config) {
        $name = (!empty($config['name'])) ? $config['name'] : 'catalogTask';
        echo $name.': [start]' . PHP_EOL;
        static::_catalogArticlesTask($config);
        static::_catalogItemsTask($config);
        echo $name.': [end]' . PHP_EOL;

    }

    protected function _catalogArticlesTask($config) {
        $name = (!empty($config['name'])) ? $config['name'] : 'catalogTask';
        $articlesTable = (!empty($config['table']['articles'])) ? $config['table']['articles'] : null;
        $articlesSeed = (!empty($config['section']['articles']['seed']) && is_callable($config['section']['articles']['seed'])) ? $config['section']['articles']['seed'] : null;
        $articlesDeep = (!empty($config['section']['articles']['deep'])) ? $config['section']['articles']['deep'] : 1;
        $articlesLimit = (!empty($config['section']['articles']['limit'])) ? $config['section']['articles']['limit'] : 5;

        if (empty($articlesTable) || empty($articlesSeed)) {
            echo $name . ': [error]' . PHP_EOL;
            exit;
        }

        if (!empty($config['section']['articles']['before']) && is_callable($config['section']['articles']['before'])) {
            $config['section']['articles']['before']($config);
        }

        for ($articlesLevel = 0; $articlesLevel < $articlesDeep; $articlesLevel++) {
            $parents = MSCore::db()->getCol('SELECT id FROM `' . $articlesTable . '` WHERE `level`=' . ($articlesLevel - 1));
            if (empty($parents)) $parents = array(0);
            foreach ($parents as $parent) {
                for ($i = 0; $i < $articlesLimit; $i++) {
                    /** @var Closure $articlesSeed */
                    $data = $articlesSeed($config, $this->_faker, $parent);

                    // Генерация изображений
                    if(!empty($config['section']['articles']['images'])) {
                        $data = $this->_imageSeed($config['section']['articles']['images'], $data);
                    }

                    if(!empty($data)) {
                        MSCore::db()->insert($articlesTable, $data);
                        $data['id'] = MSCore::db()->id;
                        MSNestedSets::updateNodeParameters($data['id'], $articlesTable);
                        MSNestedSets::updateNodeMaterializedPath($data['id'], $data['id'], $articlesTable, $articlesTable, false);
                        if (!empty($config['section']['articles']['callback']) && is_callable($config['section']['articles']['callback'])) {
                            $config['section']['articles']['callback']($config, $data);
                        }
                    }
                }
            }
        }

        if (!empty($config['section']['articles']['after']) && is_callable($config['section']['articles']['after'])) {
            $config['section']['articles']['after']($config);
        }
    }

    protected function _catalogItemsTask($config) {
        $name = (!empty($config['name'])) ? $config['name'] : 'catalogTask';

        $articlesTable = (!empty($config['table']['articles'])) ? $config['table']['articles'] : null;
        $articlesDeep = (!empty($config['section']['articles']['deep'])) ? $config['section']['articles']['deep'] : 1;

        $itemsTable = (!empty($config['table']['items'])) ? $config['table']['items'] : null;
        $itemsLimit = (!empty($config['section']['items']['limit'])) ? $config['section']['items']['limit'] : 5;
        $itemsSeed = (!empty($config['section']['items']['seed']) && is_callable($config['section']['items']['seed'])) ? $config['section']['items']['seed'] : null;

        if (empty($articlesTable) || empty($itemsTable) || empty($itemsSeed)) {
            echo $name . ': [error]' . PHP_EOL;
            exit;
        }

        $parents = MSCore::db()->getCol('SELECT id FROM `' . $articlesTable . '` WHERE `level`=' . ($articlesDeep - 1));
        if (!empty($config['section']['items']['before']) && is_callable($config['section']['items']['before'])) {
            $config['section']['items']['before']($config);
        }

        foreach ($parents as $parent) {
            for ($i = 0; $i < $itemsLimit; $i++) {
                /** @var Closure $itemsSeed */
                $data = $itemsSeed($config, $this->_faker, $parent);

                // Генерация изображений
                if(!empty($config['section']['items']['images'])) {
                    $data = $this->_imageSeed($config['section']['items']['images'], $data);
                }

                if(!empty($data)) {
                    MSCore::db()->insert($itemsTable, $data);
                    $data['id'] = MSCore::db()->id;
                    MSNestedSets::updateNodeMaterializedPath($parent, $data['id'], $articlesTable, $itemsTable, true);
                    if (!empty($config['section']['items']['callback']) && is_callable($config['section']['items']['callback'])) {
                        $config['section']['items']['callback']($config, $data);
                    }
                }
            }
        }

        if (!empty($config['section']['items']['after']) && is_callable($config['section']['items']['after'])) {
            $config['section']['items']['after']($config);
        }
    }

    public function execute() {
        if(empty($this->_config) || !is_array($this->_config)) {
            echo 'Seeder: [error]' . PHP_EOL;
            return false;
        }

        echo 'Seeder: [start]' . PHP_EOL;
        foreach ($this->_config as $config) {
            $enable = (isset($config['enable'])) ? $config['enable'] : true;
            $type = (!empty($config['type'])) ? $config['type'] : null;
            $method = '_'.$type.'Task';

            if(empty($enable)) continue;
            if(is_null($type) || !method_exists($this, $method)) {
                echo 'Seeder: [error]' . PHP_EOL;
                exit;
            }

            $this->{$method}($config);
        }

        echo 'Seeder: [end]' . PHP_EOL;
    }
}