<?php

class MSGCF {

    public $config = array();
    public $generalList = array();
    public $pathsList = array();

    public $filesDir = 'UPLOAD'; // относительно корня
    public $ignoreFiles = array('.htaccess', '.gitignore'); // список файлов, которые ну будут затронуты при удалении (имя.расширение)
    public $ignoreExtensions = array(); // список разрешений файлов, которые не будут затронуты при удалении
    public $ignoreFolders = array('temp', 'user'); // относительно директории $filesDir

    public function __construct($config = null) {

        if(!is_array($config)) {
            $config = $this->_getConfig();
        }

        if(empty($config)) {
            debug('Не указан конфиг'); die;
        }

        $this->config = $config;



        $this->filesDir = MSFiles::getFileFSPath(DOC_ROOT . DS . trim($this->filesDir, '\/'));
        //$this->filesDir = trim($this->filesDir, '\/');

        foreach($this->ignoreFolders as &$folder) {
            $folder =  MSFiles::getFileFSPath(trim($this->filesDir, '\/') . DS . trim($folder. '\/'));
            //$folder = trim($folder, '\/');
        }


        foreach($config['tables'] as $tableName => $tableParams) {
            $gotData[$tableName] = MSCore::db()->getAll('SELECT `'. $tableParams['primary_key'] .'`, `'. implode('`, `', $tableParams['files_fields']) .'` FROM  '. $tableName);
        }

        if(!empty($gotData)) {
            foreach($gotData as $tableName => $data) {
                foreach($data as $key => $fields) {
                    foreach($config['tables'][$tableName]['files_fields'] as $fieldName) {
                        if(!empty($fields[$fieldName])) {
                            $this->generalList[$tableName][$fields[$config['tables'][$tableName]['primary_key']]][$fieldName] = $fields[$fieldName] = unserialize($fields[$fieldName]);
                        }
                    }
                }
            }
        }

        unset($gotData);

        if(!empty($this->generalList)) {
            foreach($this->generalList as $tableName => $itemIds) {
                foreach($itemIds as $itemId => $filesFields) {
                    foreach($filesFields as $key => $files) {
                        foreach($files as $fileParams) {
                            if(is_array($fileParams['path'])) {
                                foreach($fileParams['path'] as $thumbName => $path) {
                                    $this->pathsList[$path] = true;
                                }
                            } else {
                                $this->pathsList[$fileParams['path']] = true;
                            }
                        }
                    }
                }
            }
        }

    }

    private function _getConfig() {
        $resultConfig = array();
        $tmpConfig = array();
        $modules = MSCore::db()->getCol("SELECT module_name FROM `" . PRFX . "modules` WHERE `type` != 4 ORDER BY `order`");

        if(!empty($modules)) {
            foreach($modules as $module) {
                $config = MSCore::modules()->getModuleConfig($module);
                if(!empty($config['tables'])) {
                    foreach($config['tables'] as $tableType) {
                        if(!empty($tableType['db_name']) && !empty($tableType['config'])) {
                            $tmpConfig = array(
                                'primary_key' => !empty($tableType['key_field']) ? $tableType['key_field'] : 'id',
                                'files_fields' => array()
                            );

                            foreach($tableType['config'] as $fieldName => $fieldValue) {
                                if($fieldValue['type'] == 'loader') {
                                    $tmpConfig['files_fields'][] = $fieldName;
                                }
                            }

                            if(!empty($tmpConfig['files_fields'])) {
                                $resultConfig['mp_' . $tableType['db_name']] = $tmpConfig;
                            }
                        }
                    }
                }
            }
        }

        if(!empty($resultConfig)) {
            $resultConfig = array('tables' => $resultConfig);
        }

        return $resultConfig;
    }

    public function scan($path, $pattern = '*', $flags = GLOB_NOSORT, $depth = 0) {
        $matches = array();
        $folders = array(rtrim($path, DIRECTORY_SEPARATOR));
        while($folder = array_shift($folders)) {
            $matches = array_merge($matches, glob($folder.DIRECTORY_SEPARATOR.$pattern, $flags));
            $matches = array_diff($matches, $this->ignoreFolders);
            if($depth != 0) {
                $moreFolders = glob($folder.DIRECTORY_SEPARATOR.'*', GLOB_ONLYDIR);
                $moreFolders = array_diff($moreFolders, $this->ignoreFolders);
                $depth   = ($depth < -1) ? -1: $depth + count($moreFolders) - 2;
                $folders = array_merge($folders, $moreFolders);
            }
        }
        return $matches;
    }

    /**
     * Удаляет файлы из указанной папки, которые не были найдены ни в одной записи БД
     */
    public function deleteFiles() {

        $dirsAndFiles = $this->scan($this->filesDir, "*", GLOB_NOSORT, 10);

        function getExtension($filename) {
            return substr($filename, strrpos($filename, '.') + 1);
        }

        if(!empty($dirsAndFiles)) {
            foreach($dirsAndFiles as $path) {
                if(is_file($path) && !isset($this->pathsList[MSFiles::getFileRootPath($path)])) { // Если это файл, и его нет в базе
                    if(!in_array(getExtension($path), $this->ignoreExtensions) && !in_array(basename($path), $this->ignoreFiles)) {
                        @unlink($path);
                        debug('file: '. $path .' has been deleted by deleteFiles method');
                    }
                }
            }
        }
    }

    /**
     * Удаляет записи в БД, для файлов, которые не найдены, по указанному там пути
     */
    public function deleteEntries() {

        $edited = array();

        foreach($this->generalList as $tableName => &$itemIds) {
            foreach($itemIds as $itemId => &$fields) {
                foreach($fields as &$entries) {
                    foreach($entries as $key => &$entry) {

                        foreach((array)$entry['path'] as $path) {

                            $filePath = MSFiles::getFileFSPath(DOC_ROOT . DS . trim($path, '\/'));
                            //$filePath = trim($filePath, '\/');

                            if(!file_exists($filePath)) {

                                if(!empty($entries[$key]['path']) && is_array($entries[$key]['path'])) {
                                    foreach($entries[$key]['path'] as $file2delete) {
                                        $file2delete = MSFiles::getFileFSPath(DOC_ROOT . DS . trim($file2delete, '\/'));
                                        //$file2delete = trim($file2delete, '\/');
                                        @unlink($file2delete);
                                        debug('file: '. $file2delete .' has been deleted by deleteEntries method (array)');
                                    }
                                } else {
                                    @unlink($filePath);
                                    debug('file: '. $filePath .' has been deleted by deleteEntries method (string)');
                                }

                                unset($entries[$key]);
                                $edited[$tableName][] = $itemId;
                            }
                        }
                    }
                }
            }
        }

        if(!empty($edited)) {
            $fields1 = array();

            foreach($edited as $tableName => $ids) {
                $ids = array_unique($ids);
                foreach($ids as $id) {

                    $fields1 = array();
                    if(isset($this->generalList[$tableName][$id])) {
                        $fields1 = $this->generalList[$tableName][$id];

                        foreach($fields1 as $fieldName => &$fieldValue) {
                            $fieldValue = serialize($fieldValue);
                        }

                        MSCore::db()->update($tableName, $fields1, '`'.$this->config['tables'][$tableName]['primary_key'] .'` = '. $id);
                        debug('db entry (table: '. $tableName .', '.$this->config['tables'][$tableName]['primary_key']. ': '. $id .') has been updated');
                    }
                }
            }

        }
    }


    public function sync() {

        $this->deleteFiles();
        $this->deleteEntries();
        $this->_replaceEmptySerialize();
    }

    private function _replaceEmptySerialize() {
        foreach($this->config['tables'] as $tableName => $tableParams) {

            foreach($tableParams['files_fields']  as $fieldName) {
                MSCore::db()->execute($sql = 'UPDATE `'. $tableName .'` SET `'. $fieldName .'` = "" WHERE `'. $fieldName .'` = "a:0:{}"');
                MSCore::db()->execute('UPDATE `'. $tableName .'` SET `'. $fieldName .'` = "" WHERE `'. $fieldName .'` = "'. mysql_real_escape_string('s:6:"a:0:{}";') .'"');
            }
        }
    }
}