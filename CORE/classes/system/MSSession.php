<?php
/**
 * @author bmxnt <bmxnt@mediasite.ru>
 * @copyright Mediasite LLC (http://www.mediasite.ru/)
 */

class MSSession {
    /**
     * @var array
     */
    protected static $_data;

    protected function __construct() {}
    protected function __clone() {}
    protected function __wakeup() {}

    public static function __initStatic() {
        self::$_data = &$_SESSION;
    }

    /**
     * @return array
     */
    public static function all() {
        return self::$_data;
    }

    /**
     * @param $key
     * @return null
     */
    public static function get($key) {
        if (isset(self::$_data[$key])) return self::$_data[$key];
        return null;
    }

    /**
     * @param $key
     * @param $value
     */
    public static function set($key, $value) {
        self::$_data[$key] = $value;
    }

    /**
     * @param $key
     */
    public static function remove($key) {
        if (isset(self::$_data[$key])) unset(self::$_data[$key]);
    }

    /**
     * Destroy session
     */
    public static function destroy() {
        unset($_SESSION);
    }
}