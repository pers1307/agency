<?php
/**
 * Basic registry class
 * @author bmxnt <bmxnt@mediasite.ru>
 * @copyright Mediasite LLC (http://www.mediasite.ru/)
 */

class MSRegistry
{
    /**
     * @var array
     */
    private static $vars = array();

    protected function __construct() {}
    protected function __clone() {}
    protected function __wakeup() {}

    /**
     * @param string $key
     * @param mixed $item
     * @return void
     */
    public static function set($key, $item) {
        self::$vars[$key] = $item;
    }

    /**
     * @param string $key
     * @return null|mixed
     */
    public static function get($key) {
        if (self::has($key)) return self::$vars[$key];
        return null;
    }

    /**
     * @param string $key
     * @return void
     */
    public static function remove($key) {
        if (self::has($key)) unset(self::$vars[$key]);
    }

    /**
     * @param string $key
     * @return void
     */
    public static function has($key) {
        return array_key_exists($key, self::$vars);
    }
}
