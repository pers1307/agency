<?php

/**
 * Class Widget
 */
class Widget {

    private static $_cache = array();

    // TODO: Сделать класс для загрузки конфигов
    private static function _getConfig($name) {
        $config = array();
        $file = CORE_DIR . DS . 'configs' . DS . 'widgets' . DS . $name . '.php';
        if($file) {
            $config = include($file);
        }
        return $config;
    }

    /**
     * @param $name
     * @param array $config
     * @param string $view
     * @return MSBaseWidget
     * @throws Exception
     */
    static function load($name, $config = array(), $view = 'default') {
        if(empty($name)) {
            throw new Exception('Необходимо указать имя виджета, который вызывается');
        }
        $name = strtolower($name);
        if(isset(self::$_cache[$name])) {
            return self::$_cache[$name];
        }

        $config = $config ?: self::_getConfig($name);

        $class = 'Widget' . ucfirst($name);
        $object = new $class($name, $config, $view);
        if($object->_cache) {
            self::$_cache[$name] = $object;
        }
        return $object;
    }

    static function available() {}
}