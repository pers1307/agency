<?php

class StaticFiles
{
    /**
     * Получить файл по имени в конфиге
     *
     * @param string $groupName Ммя в конфиге $GLOBALS['staticFiles'], расположенном в корневом config.php
     * @param boolean $minify Объединять и сжимать в один файл, либо выводит списком
     * @param string $type Тип файлов js или css
     * @return bool|string
     */
    private static function getStatic($groupName, $minify, $type)
    {
        empty($GLOBALS['staticFiles'][$groupName]) or $filesConfig = $GLOBALS['staticFiles'][$groupName];
        if (!empty($filesConfig)) {
            if ($minify) {
                $filePath = '/STATIC/' . $type . '/' . $groupName . '.v' . ASSETS_VERSION . '.' . $type;
                return self::returnByType($filePath, $type);
            } else {
                $filesList = array();
                foreach ($filesConfig as $filePath) {
                    $filesList[] = self::returnByType('/' . $filePath, $type);
                }
                return implode('', $filesList) . "\r\n";

            }
        }
        return false;

    }

    /**
     * Возвращает HTML подключения файла, в зависимости от его типа
     *
     * @param string $filePath Путь до файла
     * @param string $type Тип файла
     * @return bool|string
     */
    private static function returnByType($filePath, $type)
    {
        switch ($type) {
            case 'css':
                return self::returnCSS($filePath);
                break;

            case 'js':
                return self::returnJS($filePath);
                break;
        }
        return false;
    }

    /**
     * Возвращает HTML для CSS
     *
     * @param $filePath
     * @return string
     */
    private static function returnCSS($filePath)
    {
        return '<link rel="stylesheet" href="' . $filePath . '" type="text/css">' . "\r\n";
    }

    /**
     * Возвращает HTML для JS
     *
     * @param $filePath
     * @return string
     */
    private static function returnJS($filePath)
    {
        return '<script src="' . $filePath . '"></script>' . "\r\n";

    }

    /**
     * Возвращает сгенерированный HTML подключения CSS
     *
     * @param string|array $groupName Имя группы файлов из корневого config.php
     * @param bool $minify Сжимать или выводить списком
     *
     * @return bool|string
     */
    public static function getCSS($groupName, $minify = null)
    {
        $rendered = '';
        !is_null($minify) or $minify = COMPRESS_CSS;
        if (is_array($groupName)) {
            foreach ($groupName as $_group) {
                $rendered .= self::getStatic($_group, $minify, 'css');
            }
        } else {
            $rendered = self::getStatic($groupName, $minify, 'css');
        }
        return $rendered;
    }


    /**
     * Возвращает сгенерированный HTML подключения JS
     *
     * @param string $groupName Имя группы файлов из конфига
     * @param bool $minify Сжимать или выводить списком
     *
     * @return bool|string
     */
    public static function getJS($groupName, $minify = null)
    {
        $rendered = '';
        !is_null($minify) or $minify = COMPRESS_JS;
        if (is_array($groupName)) {
            foreach ($groupName as $_group) {
                $rendered .= self::getStatic($_group, $minify, 'js');
            }
        } else {
            $rendered = self::getStatic($groupName, $minify, 'js');
        }
        return $rendered;
    }
}