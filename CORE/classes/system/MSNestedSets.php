<?php
/**
 * Класс для работы с Nested Sets
 * @author bmxnt <bmxnt@mediasite.ru>
 * @copyright Mediasite LLC (http://www.mediasite.ru/)
 */

class MSNestedSets {

    /**
     * @var array
     */
    public static $options = array(
        'columns' => array(
            'id' => 'id',
            'parent' => 'parent',
            'level' => 'level',
            'lft' => 'lft',
            'rgt' => 'rgt',
            'visible' => 'visible',
            'parents_visible' => 'parents_visible',
            'parents' => 'parents'
        ),
        'update_visible' => TRUE
    );

    private function __construct(){}
    private function __clone(){}
    private function __wakeup(){}

    /**
     * @param $id
     * @param $table
     * @return bool
     */
    public static function updateNodeParameters($id, $table) {
        $table = trim($table, '`');
        if(strpos($table, PRFX) === 0) $table = preg_replace('/^'.PRFX.'/', '', $table, 1);

        $item = MSCore::db()->getRow(
            'SELECT `'.static::$options['columns']['id'].'`, `'.static::$options['columns']['parent'].'` '.
            'FROM `'.PRFX.$table.'` WHERE `'.static::$options['columns']['id'].'` = '.intval($id)
        );

        if(!$item) return FALSE;

        $parents_visible = $rgt = $level = 0;
        $columns = array(
            static::$options['columns']['rgt'],
            static::$options['columns']['lft'],
            static::$options['columns']['level'],
        );

        if(static::$options['update_visible']) {
            $columns = array_merge($columns, array(
                static::$options['columns']['parents_visible'],
                static::$options['columns']['visible'],
            ));
        }

        $parent = MSCore::db()->getRow(
            'SELECT '.implode(',',$columns).' FROM `'.PRFX . $table.'` '.
            'WHERE `'.static::$options['columns']['id'].'` = '.$item['parent']
        );

        if($parent) {
            $rgt = $parent[static::$options['columns']['rgt']];
            $level = $parent[static::$options['columns']['level']] + 1;
            if(static::$options['update_visible']) {
                $parents_visible = $parent[static::$options['columns']['parents_visible']] == 0 ? 0 : ($parent[static::$options['columns']['visible']] == 0 ? 0 : 1);
            }
        } else {
            $rgt = MSCore::db()->getOne('SELECT MAX('.static::$options['columns']['rgt'].') FROM `'.PRFX . $table.'`') + 1;
            if(static::$options['update_visible']) {
                $parents_visible = 1;
            }
        }

        // Update parents
        MSCore::db()->execute(
            'UPDATE `'.PRFX . $table.'` SET '.static::$options['columns']['rgt'].' = '.static::$options['columns']['rgt'].' + 2, '.
            ''.static::$options['columns']['lft'].' = IF('.static::$options['columns']['lft'].' > '.intval($rgt).', '.static::$options['columns']['lft'].' + 2, lft) '.
            'WHERE '.static::$options['columns']['rgt'].' >= '.intval($rgt)
        );

        // Update item
        $sql = 'UPDATE `'.PRFX . $table.'` SET '.static::$options['columns']['lft'].' = '.$rgt.', '.static::$options['columns']['rgt'].' = '. ($rgt + 1) .', '.static::$options['columns']['level'].' = '.$level;
        if(static::$options['update_visible']) {
            $sql .= ', '.static::$options['columns']['parents_visible'].' = '.$parents_visible.' ';
        }
        $sql .= ' WHERE `'.static::$options['columns']['id'].'` = '.$item['id'];
        MSCore::db()->execute($sql);
        return TRUE;
    }

    /**
     * @param $id
     * @param $status
     * @param $table
     * @return bool
     */
    public static function updateNodeVisibility($id, $status, $table) {
        $table = trim($table, '`');
        if(strpos($table, PRFX) === 0) $table = preg_replace('/^'.PRFX.'/', '', $table, 1);

        $columns = array(
            static::$options['columns']['rgt'],
            static::$options['columns']['lft'],
            static::$options['columns']['level'],
            static::$options['columns']['parents_visible'],
            static::$options['columns']['visible'],
        );
        $group = MSCore::db()->getRow('SELECT '.implode(',',$columns).'  FROM ' . PRFX . $table . ' WHERE `'.static::$options['columns']['id'].'`=' . (int) $id);
        $group[static::$options['columns']['visible']] = $status;
        $parents_visible = $group[static::$options['columns']['parents_visible']] == 0 ? 0 : ($group[static::$options['columns']['visible']] == 0 ? 0 : 1);

        MSCore::db()->execute(
            'UPDATE ' . PRFX . $table . ' SET `'.static::$options['columns']['visible'].'`=' . $group[static::$options['columns']['visible']] . ' ' .
            'WHERE `'.static::$options['columns']['id'].'`=' . (int) $id
        );

        // Update children parents_visible
        MSCore::db()->execute(
            'UPDATE ' . PRFX . $table . ' SET `'.static::$options['columns']['parents_visible'].'` = '.$parents_visible.' '.
            'WHERE `'.static::$options['columns']['lft'].'` > '.$group['lft'] . ' AND `'.static::$options['columns']['rgt'].'` < ' . $group['rgt']
        );
        return true;
    }

    /**
     * @param $nodeId - идентификатор ноды
     * @param $destId - текущий идентификатор (например для позиций)
     * @param $nodeTable - таблица с нодами
     * @param $destTable - текущая таблица (например для позиций)
     * @param $withSelf - текущий узел попадает в путь
     * @return bool
     */
    public static function updateNodeMaterializedPath($nodeId, $destId, $nodeTable, $destTable, $withSelf) {
        $nodeTable = trim($nodeTable, '`');
        $destTable = trim($destTable, '`');
        if(strpos($nodeTable, PRFX) === 0) $nodeTable = preg_replace('/^'.PRFX.'/', '', $nodeTable, 1);
        if(strpos($destTable, PRFX) === 0) $destTable = preg_replace('/^'.PRFX.'/', '', $destTable, 1);

        $currentNode = MSCore::db()->getRow(
            'SELECT `'.static::$options['columns']['rgt'].'`, `'.static::$options['columns']['lft'].'` '.
            'FROM `'.PRFX.$nodeTable.'` WHERE `'.static::$options['columns']['id'].'`='.$nodeId
        );
        if($currentNode) {
            $parents = MSCore::db()->getCol(
                'SELECT `'.static::$options['columns']['id'].'` FROM `'.PRFX.$nodeTable.'` '.
                'WHERE `'.static::$options['columns']['lft'].'` <= '.$currentNode['lft'].' '.
                'AND `'.static::$options['columns']['rgt'].'` >= '.$currentNode['rgt'].' '.
                (!$withSelf ? ' AND `'.static::$options['columns']['id'].'` <> '.$nodeId.' ' : ' ').' '.
                'ORDER BY `'.static::$options['columns']['lft'].'`'
            );
            if($parents) {
                $parents = implode(',', $parents);
                MSCore::db()->execute(
                    'UPDATE `'.PRFX.$destTable.'` SET `'.static::$options['columns']['parents'].'`=\''.$parents.'\' '.
                    'WHERE `'.static::$options['columns']['id'].'`='.$destId
                );
            }
        }
        return true;
    }
}