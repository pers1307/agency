<?php
    /**
     * @author Andruha <andruha@mediasite.ru>
     * @copyright Mediasite LLC (http://www.mediasite.ru/)
     *
     * @package Core.system
     */

    class PagesTree
    {
        private
            $pagesTable = 'www',
            $pagesTypeTable = 'www_types',
            $items = array(),
            $pagesTypes = array(),
            $pagesTreeOpen = array(),
            $controls;

        public function __construct()
        {
            $this->controls = new ModuleControls('admin');

            empty($_COOKIE['pagesTreeOpen']) or $this->pagesTreeOpen = json_decode($_COOKIE['pagesTreeOpen']);
        }

        public function makeTree($pageId = null, $typeId = null)
        {
            $this->pagesTypes = $this->getTypes($typeId);

            if (is_null($pageId))
            {
                foreach ($this->pagesTypes as $type)
                {
                    if ($type['id'])
                    {
                        $this->items[] = array_merge(
                            $this->addItemControls($type, 'type'),
                            array(
                                'children' => $this->getPages(1, $type['id'])
                            )

                        );
                    }
                    else
                    {
                        foreach ($this->getPages(0, $type['id']) as $item)
                        {
                            $this->items[] = $item;
                        }
                    }
                }
            }
            else
            {
                $this->items = $this->getPageById($pageId);
            }

            return $this;
        }

        private function addItemControls($item, $type = 'page')
        {
            $newItem = array(
                'id' => null,
                'type' => $type,
                'title' => '---',
                'visible' => true,
                'controls' => array()
            );

            switch ($type)
            {
                case 'page':

                    $newItem['id'] = $item['path_id'];

                    empty($item['title_menu']) or $newItem['title'] = $item['title_menu'];
                    empty($item['last']) or $newItem['last'] = true;

                    $newItem['openChild'] = in_array($newItem['id'], $this->pagesTreeOpen);

                    $newItem['visible'] = $item['visible'];
                    if ($item['www_type'] == 0)
                    {
                        $newItem['icon'] = 'icon-file';
                    }

                    $newItem['pageLink'] = $item['path'] ? '/' . $item['path'] . '/' : '/';
                    $newItem['infoLink'] = $newItem['pageLink'];
                    $newItem['moveAccess'] = $this->controls->actionAccess('move');

                    if(AUTH_IS_ROOT)
                    {
                        $newItem['infoLink'] .= '&#10;ID: ' . $item['path_id'];
                    }

                    $this->controls->addData('page-id', $item['path_id']);
                    $this->controls->addData('type-id', $item['www_type']);
                    $this->controls->addData('parent-id', $item['parent']);

                    /** Редактирование */

                    $this->controls->addControl('edit');

                    /** Добавление подраздела */
                    if ($this->pagesTypes[$item['www_type']]['flag_addsub'] && $item['www_type'] != 1 && !$item['noadd'])
                    {
                        $this->controls->addControl('add');
                    }

                    /** Смена видимости */
                    if (!$item['noview'])
                    {
                        $this->controls->addControl($item['visible'] ? 'vis-hide' : 'vis-show');
                    }

                    /** Перемещение */
                    /*if ($item['www_type'] != 1)
                    {
                        $this->controls->addControl(
                            'move-up',
                            array(
                                'data' => ' data-page-id="' . $item['path_id'] . '" data-type-id="' . $item['www_type'] . '"',
                                'class' => 'move-up'
                            )
                        );
                        $this->controls->addControl(
                            'move-down',
                            array(
                                'data' => ' data-page-id="' . $item['path_id'] . '" data-type-id="' . $item['www_type'] . '"',
                                'class' => 'move-down'
                            )
                        );
                    }*/

                    /** Удаление */
                    if ($item['www_type'] != 1 && !$item['nodel'])
                    {
                        $this->controls->addControl('del');
                    }

                    break;

                case 'type':

                    $newItem['id'] = $item['id'];
                    empty($item['title']) or $newItem['title'] = $item['title'];
                    $newItem['icon'] = 'icon-sitemap';

                    $this->controls->addData('page-id', 1);
                    $this->controls->addData('type-id', $item['id']);

                    /** Добавление подраздела */
                    if ($item['flag_addroot'])
                    {
                        $this->controls->addControl('add');
                    }

                    break;
            }

            $newItem['controls'] = $this->controls->render();

            empty($item['children']) or $newItem['children'] = $item['children'];

            unset($item);

            return $newItem;
        }

        private function getTypes($typeId = null)
        {
            $items = new MSTable('`' . PRFX . $this->pagesTypeTable . '`');
            $items->setOrder('`order`');

            if (!(SHOW_SYSTEM_PAGES && AUTH_IS_ROOT))
            {
                $items->setFilter('`id` != 1', 'id');
            }

            if (empty($typeId))
            {
                $types = array_merge(
                    array(
                        array(
                            'id' => 0,
                            'flag_addsub' => false
                        )
                    ),
                    $items->getItems()
                );
            }
            else
            {
                $items->setFilter(
                    array(
                        '`id` = ' . $typeId
                    )
                );

                $types = $items->getItems();
            }

            $types = assoc('id', $types);

            return $types;
        }

        private function getPageById($pageId)
        {
            return $this->getPages(null, null, $pageId);
        }

        private function getPages($parent = null, $type = null, $pageId = null)
        {
            $items = new MSTable('`' . PRFX . $this->pagesTable . '`');
            $items->setOrder('`order`');
            $items->setFields(
                array(
                    'path_id',
                    'parent',
                    'title_menu',
                    'nodel',
                    'noview',
                    'noadd',
                    'notfound',
                    'www_type',
                    'visible',
                    'path'
                )
            );

            is_null($parent) or $items->setFilter('`parent` = ' . $parent, 'parent');
            is_null($type) or $items->setFilter('`www_type` = ' . $type, 'www_type');
            is_null($pageId) or $items->setFilter('`path_id` = ' . $pageId, 'path_id');

            $items = $items->getItems();

            $i = 0;
            $count = count($items);

            foreach ($items as &$item)
            {
                if (++$i == 1)
                {
                    $item['first'] = true;
                }

                if ($i == $count)
                {
                    $item['last'] = true;
                }

                if ($item['path_id'] > 1)
                {
                    $item['children'] = $this->getPages($item['path_id'], $type);
                }
                $item = $this->addItemControls($item);
            }

            return $items;
        }

        public function render($onlyPage = false, $countItems = 1)
        {
            if ($onlyPage)
            {
                return template(
                    'admin/pagesTreeItem',
                    array(
                        'item' => empty($this->items) ? array() : $this->items[0],
                        'countItems' => $countItems
                    )
                );
            }
            else
            {
                return template(
                    'admin/pagesTree',
                    array(
                        'items' => $this->items
                    )
                );
            }
        }

        public function __toString()
        {
            return $this->render();
        }
    }