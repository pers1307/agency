<?php
/**
 * @author Andruha <andruha@mediasite.ru>
 * @copyright Mediasite LLC (http://www.mediasite.ru/)
 */
/**
 * Класс для выборки из указанной таблицы базы данных
 *
 * @property string|array $fields
 * @property string|array $filter
 * @property string|array $group
 * @property string|array $order
 * @property string|array $limit
 *
 * @property array $item
 * @property array $items
 * @property int $count
 *
 * @package Core.system
 */
class MSTable extends MSBaseComponent
{
    protected $tableName = array();
    protected $db;
    private $_fields = array('*');
    private $_order;
    private $_group;
    private $_limit;
    private $_sql;
    private $_filter = array();
    private $_having = array();
    private $_join = array();
    private $_distinct = null;

    /**
     * @param string $tableName имя таблицы
     * @param array $config
     */
    public function __construct($tableName, $config = array())
    {
        parent::__construct($config);

        $this->setTableName($tableName);
        $this->db = MSCore::db();
    }

    /**
     * @param mixed $tableName имя таблицы или массив имен таблиц
     * @return $this
     */
    private function setTableName($tableName)
    {
        if (is_array($tableName))
        {
            $this->tableName = $tableName;
        }
        else
        {
            $this->tableName[] = $tableName;
        }
        return $this;
    }

    /**
     * Добавляет фильтр для запроса к таблице.
     * В дальнейшем преобразуется в условие запроса, разделяя каждый фильтр оператором AND
     *
     * @param mixed $condition условие для выборки
     * Если строка, то добавляется в массив фильтров с ключом $key. Например: '`active` = 1'
     * Если массив, то заменяет массив фильтров $this->_filter. Например:
     * array(
     *     '`active` = 1',
     *     '`type` = 2'
     * )
     *
     * @param string $key ключ фильтра. Необязателен, но нужен, если необходимо убрать фильтр.
     *
     * @return $this
     */
    public function setFilter($condition, $key = '')
    {
        if (is_array($condition))
        {
            $this->_filter = array_merge($this->_filter, $condition);
        }
        else
        {
            $key ? $this->_filter[$key] = $condition : $this->_filter[] = $condition;
        }

        return $this;
    }

    /**
     * @param array $keys
     *
     * @return $this
     */
    public function unsetFilters(array $keys)
    {
        foreach ($keys as $key)
        {
            $this->unsetFilter($key);
        }

        return $this;
    }

    /**
     * Убирает фильтр из массива по ключи фильтра.
     * @param string $key ключ фильтра
     *
     * @return $this
     */
    public function unsetFilter($key)
    {
        if (isset($this->_filter[$key]))
        {
            unset($this->_filter[$key]);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function unsetAllFilters()
    {
        $this->_filter = array();

        return $this;
    }

    /**
     * @param $condition
     * @param string $key
     * @return $this
     */
    public function setHaving($condition, $key = '')
    {
        if (is_array($condition))
        {
            $this->_having = array_merge($this->_having, $condition);
        }
        else
        {
            $key ? $this->_having[$key] = $condition : $this->_having[] = $condition;
        }

        return $this;
    }

    /**
     * @param array $keys
     *
     * @return $this
     */
    public function unsetHavings(array $keys)
    {
        foreach ($keys as $key)
        {
            $this->unsetHaving($key);
        }

        return $this;
    }

    /**
     * Убирает хэвинг из массива по ключу.
     * @param string $key ключ фильтра
     *
     * @return $this
     */
    public function unsetHaving($key)
    {
        if (isset($this->_having[$key]))
        {
            unset($this->_having[$key]);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function unsetAllHavings()
    {
        $this->_having = array();

        return $this;
    }

    /**
     * @param string $group поле или несколько полей для группировки
     *
     * @return $this
     */
    public function setGroup($group)
    {
        $this->_group = $group;

        return $this;
    }

    /**
     * @param mixed $limit лимит позиций для запроса
     *
     * @return $this
     */
    public function setLimit($limit)
    {
        $this->_limit = is_numeric($limit) ? "\nLIMIT " . $limit : $limit;

        return $this;
    }

    /**
     * @param string|array $order поле или несколько полей, по которым сортировать
     *
     * @return $this
     */
    public function setOrder($order = null, $prepare = true)
    {
        if (empty($order))
        {
            $this->_order = null;
        }
        else
        {
            if($prepare) {
                $this->_order = static::prepareOrder($order);
            } else {
                $this->_order = $order;
            }
        }

        return $this;
    }

    /**
     * @param mixed $fields поля для выборки из таблицы
     * Если строка, то добавляется в массив полей для выборки, по-умолчанию * — все поля
     * Если массив, то заменяет массив полей для выборки
     *
     * @return $this
     */
    public function setFields($fields)
    {
        if (is_array($fields))
        {
            $this->_fields = array_map(array($this, 'prepareField'), $fields);
        }
        else
        {
            $this->_fields[] = static::prepareField($fields);
        }

        return $this;
    }

    /**
     * @param $field
     *
     * @return mixed
     */
    public static function prepareField($field)
    {
        if (mb_strpos($field, '(') !== false)
        {
            return $field;
        }

        return str_replace('`*`', '*', '`' . str_ireplace(array('`', '.', ' as '), array('', '`.`', '` as `'), $field) . '`');
    }

    /**
     * @param $tableName
     * @param string $joinType
     * @param string $joinCondition
     * @return $this
     */
    public function setJoin($tableName, $joinType = 'INNER', $joinCondition = '', $joinId = null)
    {
        $join = array(
            'table' => self::tablePrefix($tableName),
            'type' => $joinType,
            'condition' => is_array($joinCondition) ? implode(' AND ', $joinCondition) : $joinCondition
        );

        if($joinId) {
            if(!$this->isJoined($joinId)) {
                $this->_join[$joinId] = $join;
            }
        } else {
            $this->_join[] = $join;
        }
        unset($join);

        return $this;
    }

    /**
     * @param $key
     * @return $this
     */
    public function unsetJoin($key)
    {
        if(isset($this->_join[$key]))
        {
            unset($this->_join[$key]);
        }
        return $this;
    }

    /**
     * @param $joinId
     * @return bool
     */
    public function isJoined($joinId)
    {
        return isset($this->_join[$joinId]);
    }

    /**
     * Set DISTINCT
     */
    public function setDistinct()
    {
        $this->_distinct = 'DISTINCT';
    }

    /**
     * Unset DISTINCT
     */
    public function unsetDistinct()
    {
        $this->_distinct = null;
    }

    /**
     * @param $tableName
     *
     * @return mixed
     */
    public static function tablePrefix($tableName)
    {
        return preg_replace('/{([\w\_\d]+)}/i', '`' . PRFX . '$1`', $tableName);
    }

    /**
     * Выполняет запрос по заданным параметрам и возвращает массив с полями одной позиции
     *
     * @return mixed
     */
    public function getItem()
    {
        return $this->db->getRow($this->makeSql());
    }

    /**
     * Выполняет запрос по заданным параметрам и возвращает массив с полями одной позиции
     *
     * @return mixed
     */
    public function getItemOne()
    {
        return $this->db->getOne($this->makeSql());
    }

    /**
     * @return string
     */
    public function makeSql()
    {
        return $this->_sql = "SELECT\n\t" . $this->getDistinct() .' '. $this->getFields() . "\nFROM\n\t" . $this->getTableName() . $this->getJoin() . $this->getFilter() . $this->getGroup() . $this->getOrder() . $this->getLimit();
    }

    /**
     * @return string
     */
    private function getFields()
    {
        return implode(",\n\t", $this->_fields);
    }

    /**
     * @return string отформатированное имя таблицы, либо нескольких таблиц для SQL-запроса
     */
    private function getTableName()
    {
        return self::tablePrefix(implode(', ', $this->tableName));
    }

    /**
     * @return string
     */
    private function getJoin()
    {
        $out = array();

        foreach ($this->_join as $join)
        {
            $out[] = ($join['type'] ? $join['type'] . ' ' : '') . "JOIN\n\t" . $join['table'] . ($join['condition'] ? ' ON (' . $join['condition'] . ')' : '');
        }

        return empty($out) ? '' : "\n" . implode("\n", $out);
    }

    /**
     * @return null
     */
    private function getDistinct()
    {
        return $this->_distinct;
    }

    /**
     * @return string
     */
    private function getFilter()
    {
        return empty($this->_filter) ? '' : "\nWHERE\n\t" . implode(" AND\n\t", array_filter($this->_filter));
    }

    /**
     * @return string
     */
    private function getHaving()
    {
        return empty($this->_having) ? '' : "\nHAVING\n\t" . implode(" AND\n\t", array_filter($this->_having));
    }

    /**
     * @return string
     */
    private function getGroup()
    {
        return empty($this->_group) ? '' : "\nGROUP BY\n\t" . $this->_group;
    }

    /**
     * @return string
     */
    private function getOrder()
    {
        return empty($this->_order) ? '' : "\nORDER BY\n\t" . $this->_order;
    }

    /**
     * @return string
     */
    private function getLimit()
    {
        return "\n" . $this->_limit;
    }

    /**
     * Выполняет запрос по заданным параметрам и возвращает массив с позициями
     *
     * @return mixed
     */
    public function getItems()
    {
        return $this->db->getAll($this->makeSql());
    }

    /**
     * Выполняет запрос по заданным параметрам и возвращает массив с позициями
     *
     * @return mixed
     */
    public function getItemsCol()
    {
        return $this->db->getCol($this->makeSql());
    }

    /**
     * Выполняет запрос по заданным параметрам и возвращает массив с позициями
     *
     * @return mixed
     */
    public function getItemsRow()
    {
        return $this->db->getRow($this->makeSql());
    }

    /**
     * Возвращает количество позиций по заданным параметрам
     *
     * @return int
     */
    public function getCount()
    {
        $distinct = $this->getDistinct();
        $having = $this->getHaving();

        if(!empty($having))
        {
            $sql = "SELECT COUNT(". ($distinct ? (' '. $distinct .' i.id') : '*') .") FROM " . "(". $this->makeSql() .") as `table` ";
        }
        else
        {
            $sql = "SELECT COUNT(". ($distinct ? (' '. $distinct .' i.id') : '*') .") FROM " . $this->getTableName() . $this->getJoin() . $this->getFilter() . $this->getGroup();
        }

        return (int)$this->db->getOne($sql);
    }

    /**
     * @param bool $return
     *
     * @return bool
     */
    public function debug($return = false)
    {
        if ($return !== false)
        {
            return $this->_sql;
        }
        else
        {
            debug($this->_sql);

            return true;
        }
    }

    /**
     * @param $var
     *
     * @return string
     */
    public function pre($var)
    {
        return $this->db->pre($var);
    }

    /**
     * @param $orders
     *
     * @return string
     */
    public static function prepareOrder($orders)
    {
        $out = array();
        is_array($orders) or $orders = explode(',', $orders);

        foreach($orders as $order)
        {
            $order = trim($order);
            $direction = 'ASC';

            if (mb_strpos($order, ' '))
            {
                list($order, $direction) = explode(' ', trim($order));
            }

            $out[] = static::prepareField($order) . ' ' . $direction;
        }

        return implode(', ', $out);
    }
}