<?php
    /**
     * @author Andruha <andruha@mediasite.ru>
     * @copyright Mediasite LLC
     */
    /**
     * Class MSUser
     *
     * @property array $user
     *
     * @package Core.system
     */

    class MSUser extends MSBaseComponent
    {
        private $_user;

        public function __construct($config = array())
        {
            parent::__construct($config);

            $this->_user = auth_Authorization();
        }

        public function __destruct()
        {
            auth_StoreSession();
        }

        public function getUser()
        {
            return $this->_user;
        }
    }