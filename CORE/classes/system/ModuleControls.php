<?php
    /**
     * @author Andruha <andruha@mediasite.ru>
     * @copyright Mediasite LLC (http://www.mediasite.ru/)
     *
     * @package Core.system
     */

    class ModuleControls
    {

        private
            $actions = array(),
            $access = array(),
            $moduleName = '',
            $controls = array(),
            $controlsData = array();

        public function __construct($moduleName = 'rootOnly')
        {

            $this->moduleName = $moduleName;

            $this->actions = array(
                'add' => array(
                    'title' => 'Добавить подраздел',
                    'icon' => 'icon-plus'
                ),
                'edit' => array(
                    'title' => 'Редактировать',
                    'icon' => 'icon-pencil'
                ),
                'vis-hide' => array(
                    'title' => 'Не показывать',
                    'icon' => 'icon-eye-close'
                ),
                'vis-show' => array(
                    'title' => 'Показывать',
                    'icon' => 'icon-eye-open'
                ),
                'move-up' => array(
                    'title' => 'Переместить вверх',
                    'icon' => 'icon-chevron-up'
                ),
                'move-down' => array(
                    'title' => 'Переместить вниз',
                    'icon' => 'icon-chevron-down'
                ),
                'del' => array(
                    'title' => 'Удалить',
                    'icon' => 'icon-trash'
                ),
                'settings' => array(
                    'title' => 'Настройки',
                    'icon' => 'icon-cog'
                )
            );

            $this->access = array(
                'add' => AUTH_IS_ROOT || $this->moduleName == '*' || auth_Access($this->moduleName, 'add'),
                'edit' => AUTH_IS_ROOT || $this->moduleName == '*' || auth_Access($this->moduleName, 'edit'),
                'del' => AUTH_IS_ROOT || $this->moduleName == '*' || auth_Access($this->moduleName, 'del'),
                'move' => AUTH_IS_ROOT || $this->moduleName == '*' || auth_Access($this->moduleName, 'move'),
                'vis' => AUTH_IS_ROOT || $this->moduleName == '*' || auth_Access($this->moduleName, 'vis'),
                'settings' => AUTH_IS_ROOT
            );
            $this->access['move-up'] = $this->access['move-down'] = $this->access['move'];
            $this->access['vis-hide'] = $this->access['vis-show'] = $this->access['vis'];

            return $this;
        }

        public function addControl($action, $settings = array())
        {

            if ($this->actionAccess($action)) {

                empty($settings['events']) or $settings['events'] = implode(' ', $settings['events']);

                $this->controls[$action] = array_merge($this->actions[$action], $settings);
            }
        }

        public function actionAccess($action)
        {

            return !empty($this->access[$action]);
        }

        private function getControls()
        {

            $controls = $this->controls;
            $this->controls = array();

            return $controls;
        }

        public function addData($name, $value)
        {

            $this->controlsData[] = 'data-' . $name . '="' . $value . '"';
        }

        private function getControlsData()
        {

            $data = implode(' ', $this->controlsData);
            $this->controlsData = array();

            return $data;
        }

        public function render()
        {

            return template('moduleControls', array(
                'data' => $this->getControlsData(),
                'items' => $this->getControls()
            ));
        }
    }