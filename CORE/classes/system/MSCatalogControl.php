<?php
/**
 * @author Andruha <andruha@mediasite.ru>
 * @copyright Mediasite LLC
 */
/**
 * @package Core.system
 */

class MSCatalogControl extends MSModuleController
{
    public function articles_generateVars()
    {
        global $CONFIG, $path_id, $articles_isorder, $Controls;

        $vars = array();
        $filter = '';
        if (isset($_SESSION['link_path']) && sizeof($_SESSION['link_path']))
        {
            $link_path = $_SESSION['link_path'][sizeof($_SESSION['link_path']) - 1];
            if (isset($link_path['inmodule']) && $link_path['inmodule'] == $CONFIG['module_name'])
            {
                $filter = '`' . $link_path['to_key'] . '`="' . $link_path['from_id'] . '"';
            }
        }

        if (!isset($CONFIG['tables']['articles']['order_field']))
        {
            $CONFIG['tables']['articles']['order_field'] = '`order`';
        }

        $moduleName = $CONFIG['module_name'];

        $Controls = new ModuleControls($moduleName);

        $addAction = "displayMessage('/" . ROOT_PLACE . "/" . $CONFIG['module_name'] . "/add_article/" . (int)$path_id . "/0/')";

        if (empty($CONFIG['tables']['articles']['noAddArticles']))
        {
            $Controls->addControl('add', array(
                'events' => array(
                    'onClick="' . $addAction . '"'
                )
            ));
        }

        $vars['controls'] = $Controls->render();
        $order = isset($CONFIG['tables']['articles']['order_field']) ? $CONFIG['tables']['articles']['order_field'] : '`order`';
        $Catalog = new MSBaseCatalog(array(
            'articlesTableName' => '{' . $CONFIG['tables']['articles']['db_name'] . '}',
            'itemsTableName' => '{' . $CONFIG['tables']['items']['db_name'] . '}',
            'pathId' => $path_id,
            'articlesOrder' => $order,
            'articlesTreeFields' => array('visible', 'level'),
            'onlyActiveArticles' => false,
            'onlyHaveItems' => false
        ));

        ob_start();
        $this->writeCatalogTree($Catalog->fullTree, $CONFIG['tables']['articles'], $CONFIG['tables']['articles']['title_field'], 0, -1, $path_id, $filter, $CONFIG['tables']['articles']['order_field'], 'article', 'items');
        $articles_html = ob_get_clean();


        $vars['CONFIG'] = $CONFIG;
        $vars['path_id'] = $path_id;
        $vars['articles_html'] = $articles_html;

        return $vars;
    }

//-------------------------------------------------------------------------------------------------------------------------------------
    public function writeCatalogTree($tree, $table_config, $title_field, $pid = 0, $level = -1, $path_id = 0, $where = 1, $order, $article_act = 'article', $items_act = 'items', $visibleChild = false)
    {
        /**
         * @var ModuleControls $Controls
         */
        global $CONFIG, $Controls;

        if (isset($_COOKIE['catalog' . (int)$path_id . '_sel_childs']))
        {
            $cookie = json_decode($_COOKIE['catalog' . (int)$path_id . '_sel_childs']);
        }
        else
        {
            $cookie = array();
        }
        $isOrder = !empty($CONFIG['tables']['articles']['is_order']) ? true : false;
        if (sizeof($tree))
        {
            echo '<ul' . ($visibleChild ? ' class="visible"' : '') . '>';
            foreach ($tree as $num => $info)
            {
                $level = $info['level'];

                $title_page = htmlspecialchars($info[$title_field], ENT_QUOTES);
                if (mb_strlen($title_page) > 50 - ($level * 3))
                {
                    $title_page_small = mb_substr($title_page, 0, 50 - ($level * 3));

                    if (mb_strrpos($title_page_small, ' ') !== false)
                    {
                        $title_page_small = mb_substr($title_page_small, 0, mb_strrpos($title_page_small, ' '));
                    }

                    $title_page_small .= '...';
                }
                else
                {
                    $title_page_small = $title_page;
                }

                if (!$title_page_small)
                {
                    $title_page_small = "<small>-----------------</small>";
                }

                if($isOrder) {
                    $moveUpAction = "doLoad('', '/" . ROOT_PLACE . "/" . $CONFIG['module_name'] . "/swap_" . $article_act . "/" . (int)$path_id . "/up/" . $info['id'] . "/', '" . $article_act . "s');";
                    $moveDownAction = 'doLoad(\'\', \'/' . ROOT_PLACE . '/' . $CONFIG['module_name'] . '/swap_' . $article_act . '/' . (int)$path_id . '/down/' . $info['id'] . '/\', \'' . $article_act . 's\');';
                }

                $visible_article = 'doLoad(\'\', \'/' . ROOT_PLACE . '/' . $CONFIG['module_name'] . '/toggle_vis_' . $article_act . '/' . (int)$path_id . '/' . (int)$info['id'] . '/\', \'' . $article_act . 's\');';
                $show_items = 'doLoad(\'\', \'/' . ROOT_PLACE . '/' . $CONFIG['module_name'] . '/show_' . $items_act . '/' . (int)$path_id . '/1/' . (int)$info['id'] . '/\', \'' . $items_act . '\');return false';
                $addAction = 'displayMessage(\'/' . ROOT_PLACE . '/' . $CONFIG['module_name'] . '/add_' . $article_act . '/' . (int)$path_id . '/0/' . (int)$info['id'] . '/'.($level+1).'/\', ' . $table_config['dialog']['width'] . ', ' . $table_config['dialog']['height'] . ');';
                $editAction = 'displayMessage(\'/' . ROOT_PLACE . '/' . $CONFIG['module_name'] . '/add_' . $article_act . '/' . (int)$path_id . '/' . (int)$info['id'] . '/'.(int)$info['parent'].'/'.$level.'/\', ' . $table_config['dialog']['width'] . ', ' . $table_config['dialog']['height'] . ');';
                $del_article = 'if (confirm(\'Вы действительно хотите удалить раздел &laquo;' . safe(addslashes($info[$title_field])) . '&raquo;?\')) {doLoad(\'\', \'/' . ROOT_PLACE . '/' . $CONFIG['module_name'] . '/del_' . $article_act . '/' . (int)$path_id . '/' . (int)$info['id'] . '/\', \'' . $article_act . 's\');}';

                $Controls->addControl('edit', array(
                    'events' => array(
                        'onClick="' . $editAction . '"'
                    )
                ));
                if ($level < $CONFIG['tables']['articles']['max_levels'] - 1)
                {
                    $Controls->addControl('add', array(
                        'events' => array(
                            'onClick="' . $addAction . '"'
                        )
                    ));
                }

                $Controls->addControl($info['visible'] ? 'vis-hide' : 'vis-show', array(
                    'events' => array(
                        'onClick="' . $visible_article . '"'
                    )
                ));

                if ($order == '`order`')
                {
                    $Controls->addControl('move-up', array(
                        'events' => array(
                            'onClick="' . $moveUpAction . '"'
                        ),
                        'class' => 'move-up'
                    ));
                    $Controls->addControl('move-down', array(
                        'events' => array(
                            'onClick="' . $moveDownAction . '"'
                        ),
                        'class' => 'move-down'
                    ));
                }
                $Controls->addControl('del', array(
                    'events' => array(
                        'onClick="' . $del_article . '"'
                    )
                ));

                $visibleChild = in_array($info['id'], $cookie);
                $wasActive = isset($_SESSION['moduleCatalogActiveArticle']) && $_SESSION['moduleCatalogActiveArticle'] == $info['id'];

                echo '
				<li>
                    <span class="' . ($info['visible'] ? '' : ' invisible') . ($wasActive ? ' wasActive' : '') . '">
                        ' . (!empty($info['children']) > 0 ? '<i class="icon-sort-' . ($visibleChild ? 'up' : 'down') . '" data-item-id="' . $info['id'] . '"><span></span></i>' : '') . '<a href="#" onclick="' . $show_items . '" title="' . $title_page . '" id="catalogArt' . $info['id'] . '">' . $title_page_small . '</a>
                        ' . $Controls->render() . '
                    </span>';

                if (!empty($info['children']))
                {
                    $this->writeCatalogTree($info['children'], $table_config, $title_field, $info['id'], $level, $path_id, $where, $order, $article_act, $items_act, $visibleChild);
                }


                echo '</li>';
            }

            echo '</ul>';

        }
    }

//-------------------------------------------------------------------------------------------------------------------------------------

// Выбор итема для смены позиции-----------------------------------

    public function getItemsToSwap($table_name, $order = 0, $up = true, $parent = 0, $with_path_id = true)
    {
        global $path_id, $CONFIG;

        //--------учитывать или нет path_id, для итемов path_id нет
        if ($with_path_id)
        {
            $where = " AND `path_id`=" . (int)$path_id . " AND `parent`=" . $parent;
        }
        else
        {
            $where = " AND `parent`=" . (int)$parent;
        }

        //--------если есть повторяющиеся значения поля ордер надо их по порядку выстроить----------------------------
        $count_unique = MSCore::db()->GetOne("SELECT COUNT(*) FROM `" . PRFX . $table_name . "` WHERE `order`=" . $order . $where);
        if ($count_unique > 1)
        {
            $this->resetSwapItemsOrder($table_name, $path_id, $parent, '`order`', $with_path_id);

            return $this->getItemsToSwap($table_name, $order, $up, $parent);
        }
        //---------------------------------------------------------------------------------------------------------

        if ($up)
        {
            $sql = "SELECT `id` FROM `" . PRFX . $table_name . "` WHERE `order`<" . $order . $where . " ORDER BY `order` DESC LIMIT 1";
        }
        else
        {
            $sql = "SELECT `id` FROM `" . PRFX . $table_name . "` WHERE `order`>" . $order . $where . " ORDER BY `order` ASC LIMIT 1";
        }

        $id = MSCore::db()->getOne($sql);

        return $id;
    }

//-------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Смена позиции сортировки
     */
    public function setSwapItemsOrder($table_name, $item_id, $action, $with_path_id = true, $type = false)
    {
        global $CONFIG, $path_id;

        $item = MSCore::db()->getRow('SELECT * FROM `' . PRFX . $table_name . '` WHERE `id`=' . (int)$item_id);

        $to_swap = $this->getItemsToSwap($table_name, $item['order'], $action, $item['parent'], $with_path_id);

        if ((int)$to_swap > 0)
        {
            $target = MSCore::db()->getOne('SELECT `order` FROM `' . PRFX . $table_name . '` WHERE `id`=' . (int)$to_swap);

            $sql1 = "UPDATE `" . PRFX . $table_name . "` SET `order` = " . (int)$target . " WHERE `id`=" . $item_id;
            $sql2 = "UPDATE `" . PRFX . $table_name . "` SET `order` = " . (int)$item['order'] . " WHERE `id`=" . (int)$to_swap;

            MSCore::db()->execute($sql1);
            MSCore::db()->execute($sql2);
        }

    }

//-------------------------------------------------------------------------------------------------------------------------------------

    public function resetSwapItemsOrder($table_name, $path_id, $parent, $order_by = '`order`', $with_path_id = true)
    {
        //--------учитывать или нет path_id, для итемов path_id нет
        if ($with_path_id)
        {
            $where = "`path_id`=" . (int)$path_id . " AND `parent`=" . (int)$parent;
        }
        else
        {
            $where = "`parent`=" . (int)$parent;
        }

        $all = MSCore::db()->getAll('SELECT `id` FROM `' . PRFX . $table_name . '` WHERE ' . $where . ' ORDER by ' . $order_by);
        if (sizeof($all))
        {
            $order = 0;
            foreach ($all as $num => $item)
            {
                $order++;
                $sql = 'UPDATE `' . PRFX . $table_name . '` SET `order`="' . (int)$order . '" WHERE `id`=' . $item['id'];
                MSCore::db()->execute($sql, true, ($order == 1) ? true : false);
            }
        }
    }

//-------------------------------------------------------------------------------------------------------------------------------------

    public function articles_generateConfigValues($article_id, $parent, $path_id)
    {

        global $CONFIG;

        $CONFIG['tables']['articles']['config']['path_id']['caption'] = '';
        $CONFIG['tables']['articles']['config']['path_id']['value'] = $path_id;
        $CONFIG['tables']['articles']['config']['path_id']['type'] = 'hidden';

        $CONFIG['tables']['articles']['config']['parent']['caption'] = '';
        $CONFIG['tables']['articles']['config']['parent']['value'] = $parent;
        $CONFIG['tables']['articles']['config']['parent']['type'] = 'hidden';

        if ($article_id > 0)
        {
            $list = MSCore::db()->getRow("SELECT * FROM `" . PRFX . $CONFIG['tables']['articles']['db_name'] . "` WHERE `id`=" . $article_id);
            foreach ($CONFIG['tables']['articles']['config'] as $k => $v)
            {
                if (isset($list[$k]))
                {
                    $CONFIG['tables']['articles']['config'][$k]['value'] = $list[$k];
                }
            }
        }

        if (isset($_SESSION['link_path']) && sizeof($_SESSION['link_path']))
        {
            $link_path = $_SESSION['link_path'][sizeof($_SESSION['link_path']) - 1];
            if (isset($link_path['inmodule']) && $link_path['inmodule'] == $CONFIG['module_name'])
            {
                $CONFIG['tables']['articles']['config'][$link_path['to_key']]['value'] = $link_path['from_id'];
            }
        }

        return $CONFIG;
    }

//-------------------------------------------------------------------------------------------------------------------------------------

    public function articles_saveItem($showdberr = true)
    {
        global $CONFIG, $table_articles;

        $CONFIG['tables']['articles']['config'] = MSCore::forms()->save($CONFIG['tables']['articles']['config']);

        $sql_ = array();

        $newRecord = false;

        foreach ($CONFIG['tables']['articles']['config'] as $field => $info)
        {

            if (isset($info['value']))
            {

                if ($info['type'] == 'code' && empty($info['value']))
                {

                    $info['value'] = "NULL";

                }
                else
                {

                    $info['value'] = "'" . MSCore::db()->pre($info['value']) . "'";

                }

                $sql_[] = "`" . $field . "` = " . $info['value'];

            }

        }

        $orderField = isset($CONFIG['tables']['articles']['order_field']) ? str_replace('`', '', $CONFIG['tables']['articles']['order_field']) : 'order';

        if ($_REQUEST['id'] > 0)
        {
            $sql = "UPDATE `" . PRFX . $table_articles . "` SET " . implode(', ', $sql_) . " WHERE `id` = " . (int)$_REQUEST['id'];
        }
        else
        {

            if ($orderField == 'order')
            {

                //-----------уккажем сортировку в зависимости от того что указано в конфиге
                if ($CONFIG['tables']['articles']['add_new_on_top'])
                {
                    $order = MSCore::db()->GetOne("SELECT MIN(`order`) FROM `" . PRFX . $table_articles . "` WHERE `path_id`=" . (int)$CONFIG['tables']['articles']['config']['path_id']['value'] . " AND `parent`=" . (int)$CONFIG['tables']['articles']['config']['parent']['value']);
                    $sql_[] = "`order`=" . ((int)$order - 1);
                }
                else
                {
                    $order = MSCore::db()->GetOne("SELECT MAX(`order`) FROM `" . PRFX . $table_articles . "` WHERE `path_id`=" . (int)$CONFIG['tables']['articles']['config']['path_id']['value'] . " AND `parent`=" . (int)$CONFIG['tables']['articles']['config']['parent']['value']);
                    $sql_[] = "`order`=" . ((int)$order + 1);
                }
            }

            $sql = "INSERT INTO `" . PRFX . $table_articles . "` SET " . implode(', ', $sql_);
            $newRecord = true;
        }

        if (MSCore::db()->execute($sql, $showdberr))
        {
            $newId = MSCore::db()->id;
            if ($newRecord)
            {
                MSNestedSets::updateNodeParameters($newId, $table_articles);
            }

            $articleId = ($_REQUEST['id']) ? intval($_REQUEST['id']) : $newId;
            MSNestedSets::updateNodeMaterializedPath($articleId, $articleId, $table_articles, $table_articles, false);
            return $articleId;
        }
        else
        {
            return false;
        }

    }

//-------------------------------------------------------------------------------------------------------------------------------------

    public function deleteArticle($id)
    {
        global $CONFIG, $path_id;

        $all = MSCore::db()->getAll('SELECT `id` FROM `' . PRFX . $CONFIG['tables']['articles']['db_name'] . '` WHERE `parent`=' . (int)$id);
        if (sizeof($all))
        {
            foreach ($all as $article)
            {
                if ((int)$article['id'] != 0)
                {
                    $this->deleteArticle($article['id']);
                }
            }
        }

        //-----удалим сам раздел------------------
        $this->deleteAbstractItem('articles', (int)$id);

        //---------удалим все позиции в этом разделе----------------
        foreach (MSCore::db()->GetAll("SELECT `id` FROM `" . PRFX . $CONFIG['tables']['items']['db_name'] . "` WHERE `parent`=" . $id) as $i)
        {
            $this->deleteAbstractItem('items', $i['id']);
        }
    }

//-------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------
//---------------------------------------итемы-----------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------------------------------

    public function showItems($CONFIG, $parent, $sel_page)
    {

        global $path_id, $output_id;

        $moduleName = $CONFIG['module_name'];
        $Controls = new ModuleControls($moduleName);

        $filter = $this->generateFilterWhereSQL($CONFIG['tables']['items']['db_name']);

        $order = isset($CONFIG['tables']['items']['order_field']) ? $CONFIG['tables']['items']['order_field'] : '`order`';

        do
        {

            $count_items = MSCore::db()->GetOne("SELECT COUNT(*) FROM `" . PRFX . $CONFIG['tables']['items']['db_name'] . "` WHERE `parent`=" . (int)$parent . (empty($filter) ? '' : ' AND ' . $filter));

            $num_pages = ceil($count_items / $CONFIG['tables']['items']['onpage']);

            $items = MSCore::db()->GetAll("SELECT * FROM `" . PRFX . $CONFIG['tables']['items']['db_name'] . "` WHERE `parent`=" . (int)$parent . (empty($filter) ? '' : ' AND ' . $filter) . " ORDER by " . $order . " LIMIT " . ($CONFIG['tables']['items']['onpage'] * ($sel_page - 1)) . "," . $CONFIG['tables']['items']['onpage']);

            if ($sel_page > $num_pages)
            {
                $sel_page = $num_pages;
            }
            else
            {
                break;
            }

            !empty($sel_page) or $sel_page = 1;

        } while ($sel_page <= $num_pages);

        foreach ($items as &$item)
        {

            $editAction = "displayMessage('/" . ROOT_PLACE . "/" . $moduleName . "/add_item/" . $path_id . "/" . (int)$parent . "/" . (int)$sel_page . "/" . (int)$item['id'] . "/')";
            $moveUpAction = 'doLoad(\'\', \'/' . ROOT_PLACE . '/' . $moduleName . '/swap_item/up/' . $item['id'] . '/' . $sel_page . '/' . $parent . '/' . $path_id . '/\', \'items\');';
            $moveDownAction = 'doLoad(\'\', \'/' . ROOT_PLACE . '/' . $moduleName . '/swap_item/down/' . $item['id'] . '/' . $sel_page . '/' . $parent . '/' . $path_id . '/\', \'items\');';
            $deleteAction = "if (confirm('Удалить запись?')) doLoad('','/" . ROOT_PLACE . "/" . $moduleName . "/delete_item/" . $path_id . "/" . (int)$parent . "/" . (int)$sel_page . "/" . (int)$item['id'] . "/','items')";

            $Controls->addData('item-id', $item['id']);

            $Controls->addControl('edit', array(
                'events' => array(
                    'onClick="' . $editAction . '"'
                )
            ));
            if ($order == '`order`')
            {
                $Controls->addControl('move-up', array(
                    'events' => array(
                        'onClick="' . $moveUpAction . '"'
                    )
                ));
                $Controls->addControl('move-down', array(
                    'events' => array(
                        'onClick="' . $moveDownAction . '"'
                    )
                ));
            }
            $Controls->addControl('del', array(
                'events' => array(
                    'onClick="' . $deleteAction . '"'
                )
            ));

            $item['editAction'] = $Controls->actionAccess('edit') ? $editAction : '';
            $item['controls'] = $Controls->render();
            $item['sub_config']['output_id'] = $output_id;
        }
        unset($item);

        $items_vars = array();
        $items_vars['CONFIG'] = $CONFIG;
        $items_vars['items'] = $items;
        $items_vars['parent'] = $parent;
        $items_vars['page'] = $sel_page;
        $items_vars['num_pages'] = $num_pages;
        $items_vars['output_id'] = 'items';
        $items_vars['path_id'] = empty($path_id) ? 0 : (int)$path_id;
        $items_vars['actionAdd'] = $Controls->actionAccess('add');
        $items_vars['actionEdit'] = $Controls->actionAccess('edit');

        $items_vars['pager'] = $this->getPagerTemplate('show_items', 'items', $sel_page, $num_pages, '%%/' . $parent . '');

        return $items_vars;
    }

//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------------
//----------удаление позиции или раздела  с учетом удаления картинок-------------------------------------------------------------------
    public function deleteAbstractItem($table, $id)
    {
        global $CONFIG;

        foreach ($CONFIG['tables'][$table]['config'] as $fname => $field)
        {
            if ($field['type'] == 'image')
            {
                $item = MSCore::db()->GetRow("SELECT * FROM `" . PRFX . $CONFIG['tables'][$table]['db_name'] . "` WHERE `id`=" . (int)$id);

                if (isset($item[$fname]))
                {
                    $images = unserialize($item[$fname]);

                    foreach ($images as $img)
                    {
                        if ($img != '' && file_exists($_SERVER['DOCUMENT_ROOT'] . $img))
                        {
                            unlink($_SERVER['DOCUMENT_ROOT'] . $img);
                        }
                    }
                }


            }
        }
        MSCore::db()->execute('DELETE FROM `' . PRFX . $CONFIG['tables'][$table]['db_name'] . '` WHERE `id`=' . (int)$id);

    }

//---------------------------------------------------------------------------------------------------
    /**
     * создаем приятный вывод дерева разделов в SELECT при добавлении
     */
    public function getTreeArticles($table, $title_field = 'name', $path_id = 0)
    {
        global $vac_out;

        $tree_array = array();

        function writeTreeArts($table, $title_field, $parent, $level = 0)
        {
            global $tree_array, $path_id;

            $sql = "SELECT * FROM `" . PRFX . $table . "` WHERE `parent`=" . $parent . " AND `path_id`=" . $path_id . " ORDER BY `id`";

            $all = MSCore::db()->getAll($sql);
            if (sizeof($all))
            {
                foreach ($all as $num => $item)
                {
                    $level++;
                    $tree_array[$item["id"]] = str_Repeat("&nbsp;&nbsp;&nbsp;&nbsp;", $level - 1) . $item[$title_field];
                    writeTreeArts($table, $title_field, $item["id"], $level);
                    $level--;
                }
            }

            return $tree_array;
        }

        $tree_array = writeTreeArts($table, $title_field, 0);

        $a = array(0 => "В корне разделов");
        $tree_array = (array)$a + (array)$tree_array;

        return $tree_array;
    }
}