<?php
/**
 * @author Andruha <andruha@mediasite.ru>
 * @copyright Mediasite LLC (http://www.mediasite.ru/)
 */

/**
 *
 * @package Core.system
 */

class MSSite extends MSBaseComponent
{
    private $_components = array();
    private $_componentsConfig = array();

    public function __construct($config = array())
    {
        parent::__construct($config);

        $this->_componentsConfig = require_once(DOC_ROOT . DS . 'components.php');
    }

    /**
     * @param $name
     *
     * @return mixed
     */
    public function __get($name)
    {
        if ($this->hasComponent($name))
        {
            return $this->getComponent($name);
        }
        else
        {
            return parent::__get($name);
        }
    }

    /**
     * @param $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        if ($this->hasComponent($name))
        {
            return $this->getComponent($name) !== null;
        }
        else
        {
            return parent::__isset($name);
        }
    }

    public function __call($name, $params)
    {
        if ($this->hasComponent($name))
        {
            $config = isset($params[0]) ? $params[0] : array();

            return $this->getComponent($name, $config);
        }

        throw new Exception('Вызов несуществующего метода: ' . get_class($this) . '::' . $name . '()');
    }

    /**
     * @param $id
     * @return mixed|null
     */
    public function getComponentConfig($id) {
        if (isset($this->_componentsConfig[$id]))
        {
            return $this->_componentsConfig[$id];
        }
        return null;
    }

    /**
     * @param $id
     *
     * @param array $config
     *
     * @throws Exception
     * @return mixed
     */
    public function getComponent($id, $config = array())
    {
        if (isset($this->_components[$id]))
        {
            return static::configure($this->_components[$id], $config);
        }
        elseif (isset($this->_componentsConfig[$id]))
        {
            $config = array_replace_recursive($this->_componentsConfig[$id], $config);

            return $this->setComponent($id, $config);
        }
        else
        {
            throw new Exception('Нет компонента с id ' . $id);
        }
    }

    /**
     * @param $id
     * @param array $config
     *
     * @throws Exception
     * @return mixed
     */
    public function setComponent($id, $config = array())
    {
        if (isset($config['className']))
        {
            $className = $config['className'];
            unset($config['className']);
        }
        else
        {
            throw new Exception('Для создания компонента, в конфигурации необходимо указать свойство className');
        }

        return $this->_components[$id] = new $className($config);
    }

    public function hasComponent($id)
    {
        return isset($this->_components[$id]) || isset($this->_componentsConfig[$id]);
    }
}