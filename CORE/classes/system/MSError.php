<?php

/**
 * @author bmxnt <bmxnt@mediasite.ru>
 * @copyright Mediasite LLC (http://www.mediasite.ru/)
 */
class MSError {
    private function __construct() {
    }

    private function __clone() {
    }

    private function __wakeup() {
    }

    protected static $_errorId = 1;
    public static $phpErrors = array(
        E_ERROR => 'Error',
        E_WARNING => 'Warning',
        E_PARSE => 'Parsing Error',
        E_NOTICE => 'Notice',
        E_CORE_ERROR => 'Core Error',
        E_CORE_WARNING => 'Core Warning',
        E_COMPILE_ERROR => 'Compile Error',
        E_COMPILE_WARNING => 'Compile Warning',
        E_USER_ERROR => 'User Error',
        E_USER_WARNING => 'User Warning',
        E_USER_NOTICE => 'User Notice',
        E_STRICT => 'Runtime Notice',
    );

    public static function errorHandler($code, $error, $file = NULL, $line = NULL) {
        if (error_reporting() & $code) {
            throw new ErrorException($error, $code, 0, $file, $line);
        }
        return true;
    }

    /**
     * @param Exception $exception
     */
    public static function exceptionHandler($exception) {
        $class = get_class($exception);
        $code = $exception->getCode();
        if (isset(self::$phpErrors[$code])) {
            $code = self::$phpErrors[$code];
        }
        $message = $exception->getMessage();
        $file = $exception->getFile();
        $line = $exception->getLine();

        // Get trace
        $traceline = "#%s %s(%s): %s";
        $trace = $exception->getTrace();

        // Build your tracelines
        $result = array();
        foreach ($trace as $key => $stackPoint) {
            $result[] = sprintf(
                $traceline,
                $key,
                isset($stackPoint['file']) ? $stackPoint['file'] : '',
                isset($stackPoint['line']) ? $stackPoint['line'] : '',
                isset($stackPoint['function']) ? $stackPoint['function'] : ''
            );

        }
        $result[] = '#' . ++$key . ' {run}';

        // Prepare message
        $error = "\nТип ошибки: " . $code . "\n";
        $error .= "Ошибка: " . $message . "\n";
        $error .= "Класс: " . $class . "\n";
        $error .= "Файл: " . $file . "\n";
        $error .= "Строка: " . $line . "\n";
        $error .= "\n";
        $error .= "Стек вызовов: " . "\n\n";
        $error .= implode("\n", $result);

        if (PHP_SAPI === 'cli') {
            echo $error;
        } else {
            $url = explode('/', trim($_SERVER['REQUEST_URI'], '/'));
            if ($url[0] !== ROOT_PLACE) {
                // Site
                if (self::$_errorId == 1) {
                    echo '<script type="text/javascript">
                        function showhide_errorblock(num,act){
                            var obj = document.getElementById(\'errorblock\'+num);

                            if (act==1){
                                obj.style.display=\'\';
                            } else {
                                obj.style.display=\'none\';
                            }
                        }
                        function showerror(num){
                            var obj = document.getElementById(\'errorblock\'+num);
                            var ttt = obj.innerHTML;
                            ttt=ttt.replace("<pre>","");
                            ttt=ttt.replace("</pre>","");
                            ttt=ttt.replace("<PRE>","");
                            ttt=ttt.replace("</PRE>","");
                            alert(ttt);
                        }
                    </script>
                    <style type="text/css">
                        .err_link {text-decoration: none; color: red; font-size: 12px; font-weight: bold; font-family: Verdana;}
                        .err_block { position: absolute; background: #FDFAE3; border: 1px solid black; padding: 10px; }
                    </style>';
                }
                echo '[<a href="#" onclick="showerror(' . self::$_errorId . '); return false;" class="err_link" onmouseover="showhide_errorblock(' . self::$_errorId . ',1);" onmouseout="showhide_errorblock(' . self::$_errorId . ',0);">X</a>]<div id="errorblock' . self::$_errorId . '" class="err_block" style="display: none;"><pre>' . $error . '</pre></div>';
                self::$_errorId++;
            } else {
                // Admin
                echo '<div style="background: #FDFAE3; border: 1px solid black"><pre>' . $error . '</pre></div>';
            }
        }

        exit;
    }

    public static function shutdownHandler() {
        exit;
    }
}