<?php

/**
 * Interface InterfaceWidget
 */
interface InterfaceWidget {
    public function __construct($name, $config, $view);
    public function render($view);
}