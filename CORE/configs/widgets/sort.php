<?php

/*
 * Конфиг виджета блока сортировки
 */

    return array(
        'settings' => array(
            'another_gets' => true // Учитвать в ссылках другие $_GET параметры
        ),
        'fields' => array(
            array(
                'label' => 'Дата', // Наименование поля
                'code' => 'date',  // Символьный код (НЕ ПОЛЕ В БАЗЕ!)
                'order' => 'asc', // Направление сортировки по умолчанию
                'hard_orders' => array('title' => 'asc'), // Жестко установленное направление для каждого поля (при наличае перекрывает установленное даже из $_GET)
                'fields' => array('title', 'date_start'), // Поля, по которым будет произведена сортировка
            ),
            array(
                'label' => 'Фото',
                'code' => 'img',
                'skip' => true // Если поле не участвует в сортировке, следует указать skip => true
            ),
            array(
                'label' => 'Название',
                'code' => 'title',
                'order' => 'asc',
                'fields' => array('title', 'sub_title'),
            ),
        )
    );