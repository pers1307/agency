<? 
/**
* Шаблон поля select.
*/

$help = (isset($help)) ? $help : '';
$s = '<select class="wide" '.(isset($multiple) ? 'multiple' : '').' name="'.$sysname.(isset($multiple) ? '[]' : '').'">'."\r\n";

$values = FormConfigs::$months1;

foreach ($values as $key=>$val)
	$s .= '<option value="'.$key.'"'.($key==$value ? 'selected="selected"':'').'>'.$val.'</option>'."\r\n";

$s .= '</select><span class="help">'.$help.'</span>';
return $s;
?>