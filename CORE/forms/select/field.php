<?php
if (isset($config['from'])) {
	$from = $config['from'];

	$all = getSelectValuesFromTable(
		$from['table_name'],
		$from['key_field'],
		$from['name_field'],
		(isset($from['where']) ? $from['where'] : ''),
		(isset($from['order']) ? $from['order'] : '')
	);

	if (is_array($all)) {
		$id = $item[$config['field_table']];
		if (!empty($config['multi'])) {
			$ids = array_filter(explode(',', $item[$config['field_table']]));
			$values = array();
			if(!empty($ids)) {
				$values = array_intersect_key($all, array_flip($ids));
			}
			echo '<span>' . (!empty($values) ? implode('<br />', $values) : 'Нет значения') . '</span>';
		} else {
			$id = $item[$config['field_table']];
			echo '<span ' . (isset($config['colors']) && isset($config['colors'][$id]) ? 'style="color: ' . $config['colors'][$id] . '; font-weight: bold;"' : '') . '>' . (isset($all[$item[$config['field_table']]]) ? $all[$item[$config['field_table']]] : 'Нет значения') . '</span>';
		}

	} else {
		echo 'Ошибка ' . __FILE__;
	}
} else {
	$val = $item[$config['field_table']];
	if (isset($config['values'][$val]))
		echo '<span ' . (isset($colors) && sizeof($colors) ? 'style="color: ' . $colors[$tmp[$i]] . '; font-weight: bold;"' : '') . '>' . $config['values'][$val] . '</span>';
	elseif (!isset($config['values'])) {
		echo '<span ' . (isset($colors) && sizeof($colors) ? 'style="color: ' . $colors[$tmp[$i]] . '; font-weight: bold;"' : '') . '>' . $val . '</span>';
	} else {
		$tmp = explode(",", $val);
		if ($tmp != false) {
			for ($i = 0; $i <= count($tmp) - 1; $i++) {
				if (isset($config['values'][$tmp[$i]])) {
					if ($tmp[$i] == $tmp[count($tmp) - 1])
						echo '<span ' . (isset($colors) && sizeof($colors) ? 'style="color: ' . $colors[$tmp[$i]] . '; font-weight: bold;"' : '') . '>' . $config['values'][$tmp[$i]] . '</span>';
					else
						echo $config['values'][$tmp[$i]] . ", ";
				}
			}
		} else
			echo '<b style="color: red">Неизвестное значение</b>';
	}
}
?>