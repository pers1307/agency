<?
/**
 * Шаблон поля select.
 */

$size = isset($size) ? intval($size) : 1;
$help = (isset($help)) ? $help : '';

$s = '<select class="wide" ' . (isset($multi) ? 'multiple' : '') . ' name="' . $sysname . (isset($multi) ? '[]' : '') . '" size="' . $size . '">' . "\r\n";

if(!empty($empty))
    $s.='<option value="0">'. $empty .'</option>';

if($value) {
    if(isset($multi)) {
        $value = explode(',', $value);
        $value = array_flip(array_filter($value));
    } else {
        $value = array($value => true);
    }
}
$value = (array) $value;

if (isset($values) && sizeof($values)) {
    foreach ($values as $key => $val) {

        if (is_array($val)) {

            $s .= '<optgroup label="' . $key . '">';

            foreach ($val as $group_key => $group_val) {
                $s .= '<option value="' . $group_key . '"' . (isset($value[$group_key]) ? 'selected="selected"' : '') . '>' . $group_val . '</option>' . "\r\n";
            }

            $s .= '</optgroup>';

        } else {

            $s .= '<option value="' . $key . '"' . (isset($value[$key]) ? 'selected="selected"' : '') . '>' . $val . '</option>' . "\r\n";

        }

    }
}
/*
if(sizeof($values)==0 || (!isset($values) && !isset($from) && $value))
	{	
	$s .= '<option selected="selected">'.$value.'</option>';
	}
*/

if (isset($from)) {
    $values = getSelectValuesFromTable(
        $from['table_name'],
        $from['key_field'],
        $from['name_field'],
        (isset($from['where']) ? $from['where'] : ''),
        (isset($from['order']) ? $from['order'] : '')
    );

    foreach ($values as $key => $val) {
        $s .= '<option value="' . $key . '"' . (isset($value[$key]) ? 'selected="selected"' : '') . '>' . $val . '</option>' . "\r\n";
    }
}


$s .= '</select><span class="help">'.$help.'</span>';
return $s;
?>