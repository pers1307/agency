<?
    $output = (isset($output)) ? $output : '';
    $caption = (isset($caption)) ? $caption : '';

    if($output == 'open') {
        return '<fieldset><legend>'.$caption.'</legend>';
    } elseif($output == 'close') {
        return '</fieldset>';
    }
?>