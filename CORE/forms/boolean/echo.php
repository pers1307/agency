<? 
/*
BOOLEAN

Да / Нет = 1 / 0

*/
$help = (isset($help)) ? $help : '';
return '
<label><input name="'.$sysname.'" type="radio" value="1"'.($value?' checked':'').'>Да</label><br><label><input name="'.$sysname.'" type="radio" value="0"'.(!$value?' checked':'').'>Нет</label><span class="help">'.$help.'</span>';