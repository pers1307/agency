<?php

$users = HelperMetrika::getUsers();
$template = '<textarea id="metrikaCodeField" style="width:' . ((isset($width)) ? $width : '100%') . '; height:' . ((isset($height)) ? (int)$height : 115) . 'px; margin-top: 0" name="' . $sysname . '">' . $value . '</textarea>';
$template .= '<div><button class="grey" style="margin: 10px 0;" id="metrikaFetchLink">Получить код метрики</button></div>';
$template .= '
<fieldset>
    <legend>Делегирование доступа</legend>
    <p class="metrika-delegate-hint">Для возможности делегирования доступа необходимо получить код метрики.</p>
    <div class="table">
        <div>Яндекс логин</div>
        <div>
            <input type="text" id="metrikaDelegateField" class="wide metrika-delegate-field" value="">
            <button class="grey" id="metrikaDelegateLink">Делегировать</button>
            <div id="metrikaDelegateError" class="metrika-delegate-error"></div>
        </div>
    </div>
    <table class="table table-list" id="metrikaDelegateTable">
         <thead>
            <th>Пользователи</th>
        </thead>
        <tbody>';

if(!empty($users)) {
    foreach ($users as $user) {
        $template .= '<tr><td>'.$user['user_login'].'</td></tr>';
    }
} else {
    $template .= '<tr class="list-empty"><td>Список пуст</td></tr>';
}
$template .= '</tbody></table></fieldset>';
return $template;