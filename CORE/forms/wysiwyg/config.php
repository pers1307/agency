<script>

    $(function () {

        var initWysiwyg = function () {

            $('textarea.wysiwyg').tinymce({

                /**
                 * Core options
                 */
                oninit: function () {

                },

                /** Location of TinyMCE script */
                script_url: '/DESIGN/CONTROL/js/tinymce/tinymce.min.js',

                /** General options */
                auto_focus: false,
                directionality: 'ltr',
                browser_spellcheck: true,
                language: 'ru',
                nowrap: false,
                object_resizing: true,
                plugins: ['anchor,charmap,code,contextmenu,directionality,emoticons,fullscreen,hr,image',
                    'layer,legacyoutput,link,lists,media,nonbreaking,noneditable,paste,searchreplace,tabfocus',
                    'table,visualblocks,visualchars,filemanager,codemirror,importcss,pagebreak'],
                selector: null,
                skin: 'mediapublisher',
                theme: 'modern',
                inline: false,

                external_filemanager_path: '/DESIGN/CONTROL/js/tinymce/plugins/filemanager/filemanager/',
                filemanager_title: 'Диспетчер файлов',

                /** User interface */
                toolbar: 'fullscreen | code | undo redo | styleselect removeformat | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
                pagebreak_separator: '[#READMORE#]',

                /** Cleanup/Output */
                convert_fonts_to_spans: true,
                custom_elements: '',
                doctype: '<!DOCTYPE html>',
                element_format: 'html',
                //entities: '',
                //entity_encoding: '',
                extended_valid_elements: '',
                fix_list_elements: true,
                force_p_newlines: true,
                force_hex_style_colors: true,
                forced_root_block: 'p',
                formats: {},
                indentation: '30px',
                invalid_elements: '',
                protect: [],
                schema: 'html5',
                style_formats: [
                    {title: 'Заголовок', items: [
                        //{title: 'Заголовок 1', format: 'h1'},
                        {title: 'Заголовок 2', format: 'h2'},
                        {title: 'Заголовок 3', format: 'h3'}
                    ]},

                    {title: 'Текст', items: [
                        {title: 'Полужирный', icon: 'bold', format: 'bold'},
                        {title: 'Курсив', icon: 'italic', format: 'italic'},
                        {title: 'Подчеркнутый', icon: 'underline', format: 'underline'},
                        {title: 'Зачеркнутый', icon: 'strikethrough', format: 'strikethrough'},
                        {title: 'Цитата', icon: 'blockquote', format: 'blockquote'}
                    ]},
                    {title: 'Изображение', items: [
                        {title: 'Слева от текста', selector: 'img', styles: {
                            'float': 'left',
                            'margin': '0 10px 10px 0'
                        }},
                        {title: 'Справа от текста', selector: 'img', styles: {
                            'float': 'right',
                            'margin': '0 0 10px 10px'
                        }}
                    ]},
                    {title: 'Таблица', items: [
                        {title: 'Текст по верху', selector: 'td,th', styles: {
                            'vertical-align': 'top'
                        }},
                        {title: 'Текст по центру', selector: 'td,th', styles: {
                            'vertical-align': 'middle'
                        }},
                        {title: 'Текст по низу', selector: 'td,th', styles: {
                            'vertical-align': 'bottom'
                        }},
                        {title: 'Стилизованая таблица', selector: 'table', classes: 'table'}
                    ]}
                ],

                visualblocks_default_state: false,
                end_container_on_empty_block: true,
                valid_children: '',
                valid_elements: '',

                /** Content style */
                body_class: 'content',
                body_id: null,
                content_css: "/DESIGN/SITE/css/content.css",

                /** Visual aids */
                visual: true,

                /** Undo/Redo */
                custom_undo_redo_levels: 20,

                /** URL */
                convert_urls: false,
                relative_urls: false,
                remove_script_host: true,
                document_base_url: '',

                /**
                 * Plugin options
                 */

                /** Link plugin */

                link_list: '',
                target_list: '',
                rel_list: '',

                /** Image */
                image_list: false,
                image_advtab: true,

                /** Theme */
                statusbar: false,
                menubar: 'edit,insert,view,table',

                codemirror: {
                    indentOnInit: true, // Whether or not to indent code on init.
                    path: 'CodeMirror', // Path to CodeMirror distribution
                    config: {
                        mode: 'application/x-httpd-php',
                        lineNumbers: true,
                        lineWrapping: true,
                        indentUnit: 4,
                        tabSize: 4,
                        matchBrackets: true,
                        styleActiveLine: true
                    },
                    jsFiles: [          // Additional JS files to load

                    ]
                }

            });
        };

        setTimeout(initWysiwyg, 1);
    });

</script>