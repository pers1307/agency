<?php
/**
 * field.php
 * Предствление для отображения Color picker
 * в списке модуля
 *
 * @author      Pereskokov Yurii
 * @copyright   2016 Pereskokov Yurii
 * @license     Mediasite LLC
 * @link        http://www.mediasite.ru/
 */

echo '
<div align="center">
    <div style="
                    width: 50px;
                    height: 50px;
                    border:1px solid black;
                    margin: auto;
                    margin-top: 10%;
                    background: #'.str_Replace("#","",$item[$config['field_table']]).'
                "
    >
        &nbsp;
    </div>
</div>
';
?>