<?
/**
 * save.php
 * Сохранение значения цвета
 *
 * @author      Pereskokov Yurii
 * @copyright   2016 Pereskokov Yurii
 * @license     Mediasite LLC
 * @link        http://www.mediasite.ru/
 */

$data['value'] = (string) $result;
?>