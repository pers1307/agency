<? 
/* 
GROUP_CHECKBOX

Параметры конфигуратора:

caption - название
in_list - показывать в таблице 0/1
filter - показывать в фильтрах 0/1
value - значение (дефолтное или при редактировании)

separator - разделитель записей при выводе (по дефолту ",")
from - от куда данные array('table_name'=>'', 'key_field'=>'', 'name_field'=>'', 'where'=>'', 'order'=>'')
values - данные массива array(key1=>value1, key2=>value2, ...)
*/

$s= '';
$help = (isset($help)) ? $help : '';
$value = array_flip(explode(',',$value));  

if (is_Array($values) && (!isset($from) || !is_array($from) || sizeof($from)<2))
	{
	foreach ($values as $key => $val)
		{
		$s.= '<div><label><input type="checkbox" name="'.$sysname.'['.$key.']" value="'.((int)$key).'" '.(isset($value[$key]) ? 'checked' : '').'>'.$val.'</label></div><span class="help">'.$help.'</span>';
		}
	}
else
	{	
	$values = getSelectValuesFromTable(
		$from['table_name'],
		$from['key_field'],
		$from['name_field'],
		(isset($from['where']) ? $from['where'] : ''),
		(isset($from['order']) ? $from['order'] : '')
	);

	$height = '';
	if (sizeof($values)>6) $height = 'height: 160px;';

	$s.= '<div style="width: auto; overflow-y: scroll; '.$height.' border: 1px solid silver; padding:5px">';
	foreach ($values as $key => $val)
		{
            $s.= '<div><label><input type="checkbox" name="'.$sysname.'['.$key.']" value="'.((int)$key).'" '.(isset($value[$key]) ? 'checked' : '').'>'.$val.'</label></div>';
		}
	$s.= '</div><span class="help">'.$help.'</span>';
	}

return $s;
?>
