<?php

    isset($settings) or $settings = array();

    $moduleName = $settings['module-name'];

    MSFiles::orderAndCheckFiles($moduleName, $config_name, $result);
    MSFiles::deleteNotSavedFiles($moduleName, $config_name, 'delete');

    $data['value'] = MSFiles::getItemsForSave($moduleName, $config_name);