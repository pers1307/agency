<?php
    $data = '';
    isset($settings) or $settings = array();
    isset($thumbs) or $thumbs = null;
    isset($value) or $value = '';

    $help = (isset($help)) ? $help : '';

    $settings['config-name'] = $config_name;
    $settings['type'] = is_null($thumbs) ? 'file' : 'image';
    $settings['sys-name'] = $sysname;

    $thumbsSum = md5(serialize($thumbs));
    $moduleName = $settings['module-name'];

    MSFiles::deleteNotSavedFiles($moduleName, $config_name);

    $loaderData = array(
        'old' => !empty($old),
        'sysName' => $sysname,
        'limit' => isset($settings['limit']) ? $settings['limit'] : 0,
        'sizeLimit' => isset($settings['size-limit']) ? $settings['size-limit'] : 0,
        'allowedExtensions' => isset($settings['allowed-extensions']) ? explode(',', preg_replace('/[\s]+/', '', $settings['allowed-extensions'])) : array(),
        'thumbs' => $thumbs,
        'items' => $items = MSFiles::getItemsByValue($value)
    );

    $settings['count-uploaded'] = count($items);

    foreach ($settings as $settingsKey => $settingsValue)
    {
        $data .= ' data-' . $settingsKey . '="' . $settingsValue . '"';
    }

    ob_start();
?>

    <div class="clearfix">
        <div class="fileLoader"<?php echo $data ?>></div>
        <div class="fileLoader-message error"></div>
    </div>
    <div class="fileLoader-offset">
        <div class="fileLoader-wrap baron-scroll">
            <div class="fileLoader-list" id="fileLoader-list<?php echo $config_name ?>"><?php MSFiles::renderItems($loaderData) ?></div>
        </div>
    </div>
    <span class="help"><?= $help ?></span>

<?php

    $_SESSION[MSFiles::$dataKeyName][$moduleName][$config_name] = $loaderData;

    return ob_get_clean();