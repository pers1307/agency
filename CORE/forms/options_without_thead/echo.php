<?
    ob_start();
    isset($value) or $value = serialize(array());
    $items = unserialize($value);
    $help = (isset($help)) ? $help : '';
?>


<script type="text/javascript">
    $(document).ready(function(){
        $(document)
            .off("click", ".options-<?=$config_name?>-add")
            .on("click", ".options-<?=$config_name?>-add", function() {
                var $table = $(this).closest('table.options-<?=$config_name?>-table');
                if($table.find('tr.options-<?=$config_name?>-empty').size() == 1){
                    $table.find('tr.options-<?=$config_name?>-empty').remove();
                }

                // Get indexes
                var $parameters_row_index = $table.find('tr.options-<?=$config_name?>-row:last').data('index');
                if(typeof $parameters_row_index == "undefined") {
                    $parameters_row_index = 0;
                } else {
                    $parameters_row_index += 1;
                }

                // Get templates
                var $parameters_row_template   = '<tr class="options-<?=$config_name?>-row" data-index="'+$parameters_row_index+'">\
                        <td>\
                            <input type="text" value="" name="<?=$sysname?>['+$parameters_row_index+'][key]">\
                        </td>\
                        <td>\
                            <input type="text" value="" name="<?=$sysname?>['+$parameters_row_index+'][value]">\
                        </td>\
                        <td class="action">\
                            <a href="#" class="options-<?=$config_name?>-remove"><i class="icon-minus-sign"></i></a>\
                        </td>\
                </tr>';

                $table.children('tbody').append($parameters_row_template);
                return false;
            });

        $(document)
            .off("click", ".options-<?=$config_name?>-remove")
            .on("click", ".options-<?=$config_name?>-remove", function() {
                var $table = $(this).closest('table.options-<?=$config_name?>-table');
                var $parameters_empty_template = '<tr class="options-empty options-<?=$config_name?>-empty"><td colspan="3">Список пуст</td></tr>';

                $(this).closest('tr.options-<?=$config_name?>-row').remove();
                if($table.find('tr.options-<?=$config_name?>-row').size() == 0){
                    $table.children('tbody').append($parameters_empty_template);
                }

                return false;
            });
    });
</script>

<table class="table table-options options-<?=$config_name?>-table">
    <thead data-no-absolute="true">
        <th><?= (isset($labels['key']) && !empty($labels['key']) ? $labels['key'] : 'Ключ') ?></th>
        <th><?= (isset($labels['value']) && !empty($labels['value']) ? $labels['value'] : 'Значение') ?></th>
        <th width="20" class="action"><a href="#" class="options-<?=$config_name?>-add"><i class="icon-plus-sign"></i></a></th>
    </thead>

    <tbody>

    <? if($items) { ?>
        <? $counter = 0; foreach($items as $_item) { ?>
            <tr class="options-<?=$config_name?>-row" data-index="<?=$counter?>">
                <td><input type="text" value="<?=$_item['key']?>" name="<?=$sysname?>[<?=$counter?>][key]"></td>
                <td><input type="text" value="<?=$_item['value']?>" name="<?=$sysname?>[<?=$counter?>][value]"></td>
                <td class="action"><a href="#" class="options-<?=$config_name?>-remove"><i class="icon-minus-sign"></i></a></td>
            </tr>
        <? $counter++; } ?>
    <? } else { ?>
        <tr class="options-empty options-<?=$config_name?>-empty">
            <td colspan="3">Список пуст</td>
        </tr>
    <? } ?>
    </tbody>
</table>
<span class="help"><?= $help ?></span>
<?
    return ob_get_clean();
?>