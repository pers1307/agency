<?
$over = (isset($over)) ? $over : '';
$out = (isset($out)) ? $out : '';
$help = (isset($help)) ? $help : '';

$onover = 'onmouseover="'.$over.'"';
$onout = 'onmouseout="'.$out.'"';
$validate = (!empty($validate)) ? $validate : array();
$to_code = isset($to_code) ? ' onkeyup="toCode(this)" onchange="toCode(this)" ': '';

if(isset($precision)) {
    $precision = pow(10, intval($precision));
    $value = str_replace(',', '.',floor($value * $precision) / $precision);
}

return '<input class="wide numberFloat'.(!empty($validate['required'])?' required':'').'" name="'.$sysname.'" type="text" value="'.htmlspecialchars($value).'" '.$onover.' '.$onout.' '.$to_code.' '.(isset($readonly)?'readonly':'').'><span class="help">'.$help.'</span>';
?>