<?php
    /** Базовый класс */
    require_once CORE_DIR . DS . 'classes' . DS . 'MSCore.php';

    /** Модуль дебага */
    require_once CORE_DIR . '/includes/error_handling.php';

    /** Функции авторизации */
    require_once CORE_DIR . '/includes/authorizate_functions.php';

    /** Глобальные функции */
    require_once CORE_DIR . '/includes/global_functions.php';

    /** Функции работы с изображениями */
    require_once CORE_DIR . '/includes/images_functions.php';

    /** Пользовательские функции */
    require_once CORE_DIR . '/includes/user_functions.php';

    /** Обрабочики событий */
    require_once CORE_DIR . '/includes/events_listeners.php';

    /** Авторизуем */
    $ModelUser = new MSUser();
    $_USER = $ModelUser->user;

    /** Запустили output buffer */
    ob_start(array('MSPage', 'obHandler'));
    $allZones = MSCore::page()->allZones;

    MSEvent::trigger('app_start');

    /** Проходим по всем существующим зонам */
    foreach ($allZones as $values)
    {
        $zone = $values['value'];
        $$zone = '';
        $zoneOutput =& $$zone;

        MSCore::page()->setZoneOutput($zone, $zoneOutput);

        if(empty(MSCore::page()->config[$zone]))
        {
            continue;
        }

        $search = isset($values['for_search']) && $values['for_search'] == 1;
        $searchLinks = isset($values['search_links']) && $values['search_links'] == 1;

        /** Пробегаемся по всем блокам и выполняем их код в соответствии с заданной структурой раздела. */
        foreach (MSCore::page()->config[$zone] as $part)
        {
            /** Если указан скрипт, то выполняем его */
            if ($part['script'] != '' && is_file($partFile = DES_DIR . DS . $part['script']))
            {
                ob_start();

                /** @noinspection PhpIncludeInspection */
                include $partFile;

                $zoneOutput .= ob_get_clean();
            }
        }

        if ($search)
        {
            $zoneOutput = '<!--[INDEX' . ($searchLinks ? '*' : '') . ']-->' . $zoneOutput . '<!--[/INDEX]-->';
        }
    }

    MSEvent::trigger('app_finish');

    /**
     * Вывод HTML страницы
     */
    echo MSCore::page();