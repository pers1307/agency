<?php

require_once 'console.php';

return array(
    'db' => array(
        'development' => array(
            'type'      => 'mysql',
            'host'      => CONFIG_DBHOST,
            'port'      => 3306,
            'database'  => CONFIG_DBNAME,
            'user'      => CONFIG_DBUSER,
            'password'  => CONFIG_DBPASS,
            //'charset' => 'utf8',
            'directory' => '',
            //'socket' => '/var/run/mysqld/mysqld.sock'
        ),
    ),
    'migrations_dir' => array('default' => RUCKUSING_WORKING_BASE . '/MIGRATIONS'),
    'db_dir' => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'MIGRATIONS',
    'log_dir' => RUCKUSING_WORKING_BASE . DIRECTORY_SEPARATOR . 'MIGRATIONS',
    'ruckusing_base' => dirname(__FILE__) . DIRECTORY_SEPARATOR . 'VENDOR/ruckusing/ruckusing-migrations'
);
